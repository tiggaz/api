<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle;

/**
 * A wrapper for holding data to be used for a application/problem+json response.
 */
class ApiError
{
    const DEFAULT_STATUS_CODE = 500;
    const DEFAULT_MESSAGE     = 'Technical Error! Please contact our support team if the problem persists.';
    const INTERNAL            = 'internal_exception';
    const UNAUTHORIZED        = 'unauthorized_error';
    const FORBIDDEN           = 'forbidden_error';
    const DB                  = 'db_error';
    const ORM                 = 'orm_error';
    const FINDER              = 'finder_error';
    const TRANSFORMER         = 'transformer_error';
    const INVALID_PARAMETER   = 'invalid_parameter';
    const INVALID_API_KEY     = 'invalid_api_key';
    const INVALID_REQUEST     = 'invalid_request';
    const NOT_FOUND           = 'not_found_error';
    const TYPE_VALIDATION     = 'validation_error';

    public static $messagesAndCodes = [
        self::UNAUTHORIZED      => [
            'message' => 'You are not allowed to perform this action!',
            'code'    => 401,
        ],
        self::FORBIDDEN         => [
            'message' => 'Authentication is required to view this resource.',
            'code'    => 403,
        ],
        self::INVALID_PARAMETER => [
            'message' => 'Invalid parameter passed!',
            'code'    => 400,
        ],
        self::INVALID_REQUEST   => [
            'message' => 'Invalid request passed!',
            'code'    => 400,
        ],
        self::INVALID_API_KEY   => [
            'message' => 'Invalid API Key!',
            'code'    => 401,
        ],
        self::NOT_FOUND         => [
            'message' => 'Not Found!',
            'code'    => 404,
        ],
        self::TYPE_VALIDATION   => [
            'message' => 'There was a validation error!',
            'code'    => 400,
        ],
    ];

    /**
     * @param string $type
     *
     * @return string
     */
    public static function getMessage(string $type): string
    {
        return isset(self::$messagesAndCodes[$type]['message']) ?
            self::$messagesAndCodes[$type]['message'] :
            self::DEFAULT_MESSAGE;
    }

    /**
     * @param string $type
     *
     * @return int
     */
    public static function getCode(string $type): int
    {
        return isset(self::$messagesAndCodes[$type]['code']) ?
            self::$messagesAndCodes[$type]['code'] :
            self::DEFAULT_STATUS_CODE;
    }

    /**
     * @param int $code
     *
     * @return string
     */
    public static function getTypeByCode(int $code): string
    {
        foreach (self::$messagesAndCodes as $type => $messageAndCode) {
            if (isset($messageAndCode['code']) && $messageAndCode['code'] === $code) {
                return $type;
            }
        }

        return self::INTERNAL;
    }
}

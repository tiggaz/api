<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Model\Campaign;

use Doctrine\Common\Collections\ArrayCollection;
use Itsup\Bundle\ApiBundle\Entity\AbstractEntity;
use Itsup\Bundle\ApiBundle\Entity\Campaign;

/**
 * @author Martin Karadzinov <martin@myhost.mk>
 */
class CampaignCreativeCollection extends AbstractEntity
{
    /**
     * @var Campaign
     */
    protected $campaign;

    /**
     * @var ArrayCollection
     */
    protected $creatives;

    /**
     * AdZoneCampaign constructor.
     */
    public function __construct()
    {
        $this->creatives = new ArrayCollection();
    }

    /**
     * @return Campaign
     */
    public function getCampaign() // TODO PHP7.7 : ?Campaign
    {
        return $this->campaign;
    }

    /**
     * @param Campaign $campaign
     */
    public function setCampaign($campaign) // TODO PHP7.7 ?Campaign $campaign
    {
        $this->campaign = $campaign;
    }

    /**
     * @return ArrayCollection
     */
    public function getCreatives()
    {
        return $this->creatives;
    }

    /**
     * @param ArrayCollection $creatives
     */
    public function setCreatives($creatives)
    {
        $this->creatives = $creatives;
    }
}

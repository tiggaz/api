<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Model\AdZone;

use Doctrine\Common\Collections\ArrayCollection;
use Itsup\Bundle\ApiBundle\Entity\AbstractEntity;
use Itsup\Bundle\ApiBundle\Entity\AdZone;

/**
 * @author Martin Karadzinov <martin@myhost.mk>
 */
class AdZoneCampaignCollection extends AbstractEntity
{
    /**
     * @var AdZone
     */
    protected $adZone;

    /**
     * @var ArrayCollection
     */
    protected $campaigns;

    /**
     * AdZoneCampaign constructor.
     */
    public function __construct()
    {
        $this->campaigns = new ArrayCollection();
    }

    /**
     * @return AdZone
     */
    public function getAdZone() // TODO PHP7.1 : ?AdZone
    {
        return $this->adZone;
    }

    /**
     * @param AdZone $adZone
     */
    public function setAdZone($adZone) // TODO PHP7.1 ?AdZone $adZone
    {
        $this->adZone = $adZone;
    }

    /**
     * @return ArrayCollection
     */
    public function getCampaigns()
    {
        return $this->campaigns;
    }

    /**
     * @param ArrayCollection $campaigns
     */
    public function setCampaigns($campaigns)
    {
        $this->campaigns = $campaigns;
    }
}

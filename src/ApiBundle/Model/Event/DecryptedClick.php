<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Model\Event;

use Itsup\Bundle\ApiBundle\Entity\AbstractEntity;
use Itsup\Bundle\ApiBundle\Entity\AdZone;
use Itsup\Bundle\ApiBundle\Entity\Campaign;
use Itsup\Bundle\ApiBundle\Entity\Creative;
use Itsup\Bundle\ApiBundle\Entity\Offer;

/**
 * @author Martin Karadzinov <martin@myhost.mk>
 */
class DecryptedClick extends AbstractEntity
{
    /**
     * @var AdZone
     */
    protected $adZone;

    /**
     * @var Campaign
     */
    protected $campaign;

    /**
     * @var Creative
     */
    protected $creative;

    /**
     * @var Offer
     */
    protected $offer;

    /**
     * @var string
     */
    protected $keywords = '';

    /**
     * @var \DateTime
     */
    protected $time;

    /**
     * AdZoneCampaign constructor.
     */
    public function __construct()
    {
        $this->adZone   = new AdZone();
        $this->campaign = new Campaign();
        $this->creative = new Creative();
        $this->offer    = new Offer();
        $this->time     = new \DateTime();
    }

    /**
     * @return AdZone
     */
    public function getAdZone() // TODO PHP7.7 : ?Campaign
    {
        return $this->adZone;
    }

    /**
     * @param AdZone $adZone
     */
    public function setAdZone($adZone) // TODO PHP7.7 ?Campaign $campaign
    {
        $this->adZone = $adZone;
    }

    /**
     * @return Campaign
     */
    public function getCampaign() // TODO PHP7.7 : ?Campaign
    {
        return $this->campaign;
    }

    /**
     * @param Campaign $campaign
     */
    public function setCampaign($campaign) // TODO PHP7.7 ?Campaign $campaign
    {
        $this->campaign = $campaign;
    }

    /**
     * @return Creative
     */
    public function getCreative()
    {
        return $this->creative;
    }

    /**
     * @param Creative $creative
     */
    public function setCreative($creative)
    {
        $this->creative = $creative;
    }

    /**
     * @return Offer
     */
    public function getOffer()
    {
        return $this->offer;
    }

    /**
     * @param Offer $offer
     */
    public function setOffer($offer)
    {
        $this->offer = $offer;
    }

    /**
     * @return string
     */
    public function getKeywords()
    {
        return $this->keywords;
    }

    /**
     * @param string $keywords
     */
    public function setKeywords($keywords)
    {
        $this->keywords = $keywords;
    }

    /**
     * @return \DateTime
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * @param \DateTime $time
     */
    public function setTime($time)
    {
        $this->time = $time;
    }
}

<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Exception;

use Itsup\Bundle\ApiBundle\ApiError;

class ApiException extends \Exception
{
    /**
     * @var string
     */
    private $type;

    /**
     * @var array
     */
    private $errors;

    /**
     * ApiException constructor.
     *
     * @param string $type
     * @param array  $errors
     */
    public function __construct(string $type, array $errors = [])
    {
        $statusCode   = ApiError::getCode($type);
        $this->errors = $errors;
        $this->type   = $type;

        parent::__construct($type, $statusCode);
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return array
     */
    public function getErrors(): array
    {
        return $this->errors;
    }
}

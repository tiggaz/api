<?php
namespace Itsup\Bundle\ApiBundle\Command;

use Doctrine\Common\Collections\ArrayCollection;
use Itsup\Bundle\ApiBundle\Entity\Account;
use Itsup\Bundle\ApiBundle\Entity\User;
use Itsup\Bundle\ApiBundle\Entity\Contact;
use Itsup\Bundle\ApiBundle\Entity\Campaign;
use Itsup\Bundle\ApiBundle\Entity\Tag;
use Itsup\Bundle\ApiBundle\Entity\Offer;
use Itsup\Bundle\ApiBundle\Entity\Event;
use Itsup\Bundle\ApiBundle\Entity\AdZone;
use Itsup\Bundle\ApiBundle\Entity\AdZone\Accounting;
use Itsup\Bundle\ApiBundle\Generator\ApiKeyGenerator;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\Exception\InvalidArgumentException;
use Symfony\Component\Yaml\Yaml;

class DemoSetUpCommand extends ContainerAwareCommand
{

    /**
     * @var InputInterface
     */
    protected $input;

    /**
     * @var SymfonyStyle|OutputInterface
     */
    protected $output;

    protected $app;

    public function __construct(Application $app)
    {
        parent::__construct();
        $this->app = $app;
    }

    protected function configure()
    {
        $this
            ->setName('demo:setup')
            ->setDescription('Create demo account')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $accountId = 1;



        /* BEGIN USERS */
        $user = new User();
        $user->setAccount($accountId);
        $user->setEmail('demo@email.com');
        $user->setName('Demo User');
        $user->setUsername('demo');
        $user->setPassword('hqH0wVjyFpYF7ARCe3R3ZSARmJxgaoK8m92kzpzDpV4Q70/codVG9EM0frGQ53tn7SsUbZ6OCIy6sBz9WrvA4Q==');
        $user->setSalt('fe01ce2a7fbac8fafaed7c982a04e229');
        $user->setRoles(['ROLE_USER']);
        $user->setTimezone('America/Los_Angeles');
        $user->setEnabled('1');
        $user->setPhone('123 456 78');
        $user->setSkype('democontact');
        $user->setTimezone('America/New_York');
        $user->setApiKey('hqH0wVjyFpYF7ARCe3R3ZSARmJxgaoK8m92kzpz');
        $user->setEnabled(mt_rand(0, 1));
        $this->getContainer()->find('manager.user')->save($user);
        $userId = $user->getId();
        /* END USERS */


        /* BEGIN CONTACTS */
        $contacts = [];
        for ($i=1; $i<6; $i++) {
            $contact = new Contact();
            $contact->setAccount($accountId);
            $contact->setUser($userId);
            $contact->setName('Contact ' . $i);
            $contact->setEmail('democontact' . $i . '@email.com');
            $contact->setSkype('democontact' . $i);
            $contact->setPhone('123 456 78' . $i);
            $contact->setCompany('Demo Contact ' . $i . ' Company');
            $contact->setCountry('US');
            $contact->setWebsiteUrl('http://www.democontact' . $i . 'company.com');
            $contact->setPublisher(mt_rand(0, 1));
            $contact->setBroker(mt_rand(0, 1));
            $contact->setAdvertiser(mt_rand(0, 1));
            $this->getContainer()->find('manager.contact')->save($contact);
            $contacts[] = $contact->getId();
        }
        /* END CONTACTS */

        /* BEGIN TAGS */
        $tags = [];
        for ($i=1; $i<20; $i++) {
            $tag = new Tag();
            $tag->setAccount($accountId);
            $tag->setName('Tag ' . $i);
            $tag = [
                'accountId' => $accountId,
                'name' => 'Tag ' . $i,
            ];
            $this->getContainer()->find('manager.tag')->save($tag);
            $tags[] = $tag->getId();
        }
        /* END TAGS */

        /* BEGIN OFFERS */
        $offers = [];
        $offers_tags = [];
        $pt = ['pps','revshare','ppl','cpc','cpm imps','cpm clicks'];
        $ptv = [70,30,10,'0.01','0.15','1.95'];
        for ($i=1; $i<6; $i++) {
            $ptid = array_rand($pt);
            $cid = array_rand($contacts);
            $offer = new Offer;
            $offer->setAccount($accountId);
            $offer->setContact($contacts[$cid]);
            $offer->setName('Offer ' . $i);
            $offer->setStatisticsUrl('http://www.stats-demooffer' . $i . '.com/');
            $offer->setPayoutType($pt[$ptid]);
            $offer->setDefaultValue($ptv[$ptid]);
            $offer->setUrl('http://www.demooffer' . $i . '.com/?click_id={click_id}&country={country}');
            $offer->setStatus('active');

            $this->getContainer()->find('manager.offer')->save($offer);
            $oid = $offer->getId();

            $offers[] = $oid;
            $offers_tags[$oid] = [];
            $rd = mt_rand(2, 6);
            $tgIds = array_rand($tags, $rd);
            foreach ($tgIds as $tgId) {
                $offers_tags = new ArrayCollection();
                $offerTags = [
                    'offerId' => $oid,
                    'tagId' => $tags[$tgId],
                ];
                $offers_tags->add($offerTags);
                $offer->setTags($offers_tags);
                $offers_tags[$oid][] = $tags[$tgId];
            }
        }
        /* END OFFERS */

        /* BEGIN EVENTS */
        $events = ['count' => [], 'conversion' => []];
        $event = new Event();
        $event->setAccount($accountId);
        $event->setName('Join page hits');
        $event->setType('count');
        $event->setValue(0);
        $this->getContainer()->find('manager.event')->save($event);
        $events['count'][] = $event->getId();

        $event = new Event();
        $event->setAccount($accountId);
        $event->setName('$15 Membership');
        $event->setType('conversion');
        $event->setValue(15);
        $this->getContainer()->find('manager.event')->save($event);
        $events['conversion'][] = $event->getId();


        $event = new Event();
        $event->setAccount($accountId);
        $event->setName('$20 Membership');
        $event->setType('conversion');
        $event->setValue(20);
        $this->getContainer()->find('manager.event')->save($event);
        $events['conversion'][] = $event->getId();

        $event = new Event();
        $event->setAccount($accountId);
        $event->setName('Shopping Cart');
        $event->setType('conversion');
        $event->setValue(0);
        $this->getContainer()->find('manager.event')->save($event);
        $events['conversion'][] = $event->getId();
        /* END EVENTS */

        /* BEGIN ADZONES */
        $sizes = ['300x250', '950x250'];
        $cost = ['flat', 'cpm imps', 'cpm clicks', 'cpc'];
        $zones = ['iframe' => [], 'direct link' => []];
        /* iframe type adzones */
        for ($i=1; $i<6; $i++) {
            $rd = array_rand($sizes);
            $size = explode('x', $sizes[$rd]);
            $zone = new AdZone();
            $zone->setAccount($accountId);
            $zone->setContact($contacts[array_rand($contacts)]);
            $zone->setName('Zone iframe ' . $i);
            $zone->setHosting('local');
            $zone->setType('iframe');
            $zone->setWidth($size[0]);
            $zone->setHeight($size[1]);
            $zone->setStatus('on');
            $zone->setCreatedBy($userId);
            $this->getContainer()->find('manager.ad_zone')->save($zone);

            $zone['adZoneId'] = $zone->getId();
            $zones['iframe'][] = $zone;
            /* Cost */
            $costType = $cost[array_rand($cost)];
            switch ($costType) {
                case 'flat':
                    $costValue = mt_rand(500, 3000);
                    break;
                case 'cpm imps':
                    $costValue = mt_rand(100, 4000) / 1000;
                    break;
                case 'cpm clicks':
                    $costValue = mt_rand(1000, 5000) / 1000;
                    break;
                case 'cpc':
                    $costValue = mt_rand(10, 30)/1000;
                    break;
            }
            $accounting = new Accounting();
            $accounting->setAdZone($zone['adZoneId']);
            $accounting->setType($costType);
            $accounting->setCost($costValue);
            $accounting->setFrom('2016-09-01');
            $accounting->setTo('2016-09-30');
            $this->getContainer()->find('manager.ad_zone.accounting')->save($accounting);

            $accounting = new Accounting();
            $accounting->setAdZone($zone['adZoneId']);
            $accounting->setType($costType);
            $accounting->setCost($costValue);
            $accounting->setFrom('2016-10-01');
            $accounting->setTo('2016-10-30');
            $this->getContainer()->find('manager.ad_zone.accounting')->save($accounting);

            /* tags */
            $rd = mt_rand(2, 6);
            $tgIds = array_rand($tags, $rd);
            foreach ($tgIds as $tgId) {
                $zone_tags = new ArrayCollection();

                $zoneTags = [
                    'adZoneId' => $zone['adZoneId'],
                    'tagId' => $tags[$tgId],
                ];
                $zone_tags->add($zoneTags);
                $zone->setTags($zone_tags);
            }
        }
        /* direct link type adzones */
        for ($i=1; $i<3; $i++) {
            $zone = new AdZone();
            $zone->setAccount($accountId);
            $zone->setContact($contacts[array_rand($contacts)]);
            $zone->setName('Zone remote dl ' . $i);
            $zone->setHosting('remote');
            $zone->setType('direct link');
            $zone->setStatus('on');
            $zone->setCreatedBy($userId);
            $zone->setUpdatedBy($userId);
            $this->getContainer()->find('manager.ad_zone')->save($zone);
            $zone['adZoneId'] = $zone->getId();
            $zones['direct link'][] = $zone;
            /* Cost */
            $costType = $cost[array_rand($cost)];
            switch ($costType) {
                case 'flat':
                    $costValue = mt_rand(5000, 10000);
                    break;
                case 'cpm imps':
                    $costValue = mt_rand(100, 4000) / 1000;
                    break;
                case 'cpm clicks':
                    $costValue = mt_rand(1000, 5000) / 1000;
                    break;
                case 'cpc':
                    $costValue = mt_rand(10, 300)/1000;
                    break;
            }
            $accounting = new Accounting();
            $accounting->setAdZone($zone['adZoneId']);
            $accounting->setType($costType);
            $accounting->setCost($costValue);
            $accounting->setFrom('2016-09-01');
            $accounting->setTo('2016-09-30');
            $this->getContainer()->find('manager.ad_zone.accounting')->save($accounting);

            $accounting = new Accounting();
            $accounting->setAdZone($zone['adZoneId']);
            $accounting->setType($costType);
            $accounting->setCost($costValue);
            $accounting->setFrom('2016-09-01');
            $accounting->setTo('2016-09-30');
            $this->getContainer()->find('manager.ad_zone.accounting')->save($accounting);

            $accounting = new Accounting();
            $accounting->setAdZone($zone['adZoneId']);
            $accounting->setType($costType);
            $accounting->setCost($costValue);
            $accounting->setFrom('2016-10-01');
            $accounting->setTo('2016-10-30');
            $this->getContainer()->find('manager.ad_zone.accounting')->save($accounting);

            /* tags */
            $rd = mt_rand(2, 6);
            $tgIds = array_rand($tags, $rd);
            foreach ($tgIds as $tgId) {
                $zone_tags = new ArrayCollection();

                $zoneTags = [
                    'adZoneId' => $zone['adZoneId'],
                    'tagId' => $tags[$tgId],
                ];
                $zone_tags->add($zoneTags);
                $zone->setTags($zone_tags);
            }
        }
        /* END ADZONES */

        /* BEGIN CAMPAIGNS */
        $sizes = ['300x250', '950x250'];
        $campaigns = ['iframe' => [], 'direct link' => []];
        $rev = ['flat', 'cpm imps', 'cpm clicks', 'cpc'];
        /* iframe type campaign */
        for ($i=1; $i<16; $i++) {
            $rd = array_rand($sizes);
            $size = explode('x', $sizes[$rd]);
            $revenue = (mt_rand(0, 100)>10) ? 'conversion' : 'sale';
            switch ($i) { //'Demo Campaign iframe ' . $i
                case 11:
                    $name = 'Campaign Geo: USA, Canada';
                    break;
                case 12:
                    $name = 'Campaign Lang.: French, German, Italian ';
                    break;
                case 13:
                    $name = 'Campaign Browser: Chrome, Firefox';
                    break;
                case 14:
                    $name = 'Campaign OS: Mac OS, Mac OS X';
                    break;
                case 15:
                    $name = 'Campaign Device/OS: Mobile/Android';
                    $size = explode('x', '300x250'); 
                    break;
                default:
                    $name = 'Campaign iframe ' . $i;
            }
            $campaign = new Campaign();
            $campaign->setAccount($accountId);
            $campaign->setName($name);
            $campaign->setRevenue($revenue);
            $campaign->setType('iframe');
            $campaign->setWidth($size[0]);
            $campaign->setHeight($size[1]);
            $campaign->setStatus('on');
            $campaign->setCreatedBy($userId);
            $campaign->setUpdatedBy($userId);


            if ($revenue == 'conversion') {
                $campaign['offerId'] = $offers[array_rand($offers)];
                $campaign['eventId'] = $events['conversion'][array_rand($events['conversion'])];
            }
            $this->getContainer()->find('manager.campaign')->save($campaign);
            $campaign['campaignId'] = $campaign->getId();
            $campaigns['iframe'][] = $campaign;
            switch ($i) { //'Demo Campaign iframe ' . $i
                case 11:
                    $countries = new ArrayCollection();
                    $countries = $countries->add(['campaignId' => $campaign['campaignId'], 'countryId' => 'US']);
                    $campaign->setCountries($countries);
                    $this->getContainer()->find('manager.campaign')->save($campaign);
                    $countries = new ArrayCollection();
                    $countries = $countries->add(['campaignId' => $campaign['campaignId'], 'countryId' => 'CA']);
                    $campaign->setCountries($countries);
                    $this->getContainer()->find('manager.campaign')->save($campaign);
                    break;
                case 12:
                    $languages = new ArrayCollection();
                    $languages = $languages->add(['campaignId' => $campaign['campaignId'], 'languageId' => 'fr']);
                    $campaign->setLanguages($languages);
                    $this->getContainer()->find('manager.campaign')->save($campaign);
                    $languages = new ArrayCollection();
                    $languages = $languages->add(['campaignId' => $campaign['campaignId'], 'languageId' => 'de']);
                    $campaign->setLanguages($languages);
                    $this->getContainer()->find('manager.campaign')->save($campaign);
                    $languages = new ArrayCollection();
                    $languages = $languages->add(['campaignId' => $campaign['campaignId'], 'languageId' => 'it']);
                    $campaign->setLanguages($languages);
                    $this->getContainer()->find('manager.campaign')->save($campaign);
                    break;
                case 13:
                    $qb = $this->app['dbs']['try9_read']->createQueryBuilder()
                        ->select('b.browserId')
                        ->from('browser', 'b')
                        ->andWhere("b.name IN ('Chrome', 'Firefox')");
                    $tmp = $qb->execute()->fetchAll(PDO::FETCH_ASSOC);
                    foreach ($tmp as $tab) {
                        $this->app['dbs']['try9_write']->insert('campaign_browsers', ['campaignId' => $campaign['campaignId'], 'browserId' => $tab['browserId']]);
                    }
                    break;
                case 14:
                    $qb = $this->app['dbs']['try9_read']->createQueryBuilder()
                        ->select('o.osId')
                        ->from('os', 'o')
                        ->andWhere("o.name IN ('Mac OS', 'Mac OS X')");
                    $tmp = $qb->execute()->fetchAll(PDO::FETCH_ASSOC);
                    foreach ($tmp as $tab) {
                        $this->app['dbs']['try9_write']->insert('campaign_os', ['campaignId' => $campaign['campaignId'], 'osId' => $tab['osId']]);
                    }
                    break;
                case 15:
                    $qb = $this->app['dbs']['try9_read']->createQueryBuilder()
                        ->select('o.osId')
                        ->from('os', 'o')
                        ->andWhere("o.name IN ('Android')");
                    $tmp = $qb->execute()->fetch(PDO::FETCH_ASSOC);
                    $this->app['dbs']['try9_write']->insert('campaign_os', ['campaignId' => $campaign['campaignId'], 'osId' => $tmp['osId']]);
                    $qb = $this->app['dbs']['try9_read']->createQueryBuilder()
                        ->select('d.deviceId')
                        ->from('device', 'd')
                        ->andWhere("d.name IN ('Mobile')");
                    $tmp = $qb->execute()->fetch(PDO::FETCH_ASSOC);
                    $this->app['dbs']['try9_write']->insert('campaign_devices', ['campaignId' => $campaign['campaignId'], 'deviceId' => $tmp['deviceId']]);
                    break;
                default:
            }
            if ($revenue == 'sale') {
                $revType = $rev[array_rand($rev)];
                $revValue = 0;
                switch ($revType) {
                    case 'flat':
                        $revValue = mt_rand(500, 3000);
                        break;
                    case 'cpm imps':
                        $revValue = mt_rand(100, 4000) / 1000;
                        break;
                    case 'cpm clicks':
                        $revValue = mt_rand(1000, 5000) / 1000;
                        break;
                    case 'cpc':
                        $revValue = mt_rand(10, 30) / 1000;
                        break;
                }
                $expire = 1;
                if ($revType != 'flat') {
                    $expire = (mt_rand(0, 100)>30) ? 1 : 0;
                }
                if ($expire) {
                    $this->app['dbs']['try9_write']->insert('campaign_sales', [
                        'campaignId' => $campaign['campaignId'],
                        'fromDate' => '2015-09-01',
                        'toDate' => '2015-09-30',
                        'type' => $revType,
                        'expire' => '1',
                        'price' => $revValue,
                    ]);
                    $this->app['dbs']['try9_write']->insert('campaign_sales', [
                        'campaignId' => $campaign['campaignId'],
                        'fromDate' => '2015-10-01',
                        'toDate' => '2015-10-31',
                        'type' => $revType,
                        'expire' => '1',
                        'price' => $revValue,
                    ]);
                }
                /* tags */
                $rd = mt_rand(2, 6);
                $tgIds = array_rand($tags, $rd);
                foreach ($tgIds as $tgId) {
                    $this->app['dbs']['try9_write']->insert('campaign_tags', [
                        'campaignId' => $campaign['campaignId'],
                        'tagId' => $tags[$tgId],
                    ]);
                }
            } else {
                /* tags */
                $tgIds = $offers_tags[$campaign['offerId']];
                foreach ($tgIds as $tgId) {
                    $this->app['dbs']['try9_write']->insert('campaign_tags', [
                        'campaignId' => $campaign['campaignId'],
                        'tagId' => $tgId,
                    ]);
                }
            }
        }
        /* direct link type campaign */
        for ($i=1; $i<7; $i++) {
            $revenue = (mt_rand(0, 100)>10) ? 'conversion' : 'sale';
            $campaign = [
                'accountId' => $accountId,
                'name' => 'Campaign dl ' . $i,
                'revenue' => $revenue,
                'type' => 'direct link',
                'status' => 'on',
                'addedBy' => $userId,
                'updatedBy' => $userId,
            ];
            if ($revenue == 'conversion') {
                $campaign['offerId'] = $offers[array_rand($offers)];
                $campaign['eventId'] = $events['conversion'][array_rand($events['conversion'])];
            }
            $this->app['dbs']['try9_write']->insert('campaign', $campaign);
            $campaign['campaignId'] = $this->app['dbs']['try9_write']->lastInsertId();
            $campaigns['direct link'][] = $campaign;
            if ($revenue == 'sale') {
                $revType = $rev[array_rand($rev)];
                switch ($revType) {
                    case 'flat':
                        $revValue = mt_rand(500, 3000);
                        break;
                    case 'cpm imps':
                        $revValue = mt_rand(100, 4000) / 1000;
                        break;
                    case 'cpm clicks':
                        $revValue = mt_rand(1000, 5000) / 1000;
                        break;
                    case 'cpc':
                        $revValue = mt_rand(10, 30) / 1000;
                        break;
                }
                $expire = 1;
                if ($revType != 'flat') {
                    $expire = (mt_rand(0, 100)>30) ? 1 : 0;
                }
                if ($expire) {
                    $this->app['dbs']['try9_write']->insert('campaign_sales', [
                        'campaignId' => $campaign['campaignId'],
                        'fromDate' => '2015-09-01',
                        'toDate' => '2015-09-30',
                        'type' => $revType,
                        'expire' => '1',
                        'price' => $revValue,
                    ]);
                    $this->app['dbs']['try9_write']->insert('campaign_sales', [
                        'campaignId' => $campaign['campaignId'],
                        'fromDate' => '2015-10-01',
                        'toDate' => '2015-10-31',
                        'type' => $revType,
                        'expire' => '1',
                        'price' => $revValue,
                    ]);
                }
                /* tags */
                $rd = mt_rand(2, 6);
                $tgIds = array_rand($tags, $rd);
                foreach ($tgIds as $tgId) {
                    $this->app['dbs']['try9_write']->insert('campaign_tags', [
                        'campaignId' => $campaign['campaignId'],
                        'tagId' => $tags[$tgId],
                    ]);
                }
            } else {
                /* tags */
                $tgIds = $offers_tags[$campaign['offerId']];
                foreach ($tgIds as $tgId) {
                    $this->app['dbs']['try9_write']->insert('campaign_tags', [
                        'campaignId' => $campaign['campaignId'],
                        'tagId' => $tgId,
                    ]);
                }
            }
        }
        /* END CAMPAIGNS */

        /* BEGIN CREATIVES */
        $sizes = ['300x250', '950x250'];
        $creatives = ['300x250' => [], '950x250' => []];
        /* iframe/image type creatives */
        for ($i=1; $i<46; $i++) {
            $rd = array_rand($sizes);
            $size = explode('x', $sizes[$rd]);
            $type = mt_rand(0, 100)>30 ? 'iframe' : 'image';
            $creative = [
                'accountId' => $accountId,
                'name' => 'Creative ' . $i,
                'type' => $revenue,
                'type' => $type,
                'width' => $size[0],
                'height' => $size[1],
                'status' => 'on',
                'code' => '',
            ];
            $this->app['dbs']['try9_write']->insert('creative', $creative);
            $creative['creativeId'] = $this->app['dbs']['try9_write']->lastInsertId();
            $creatives[$sizes[$rd]][] = $creative;
            /* tags */
            $rd = mt_rand(2, 6);
            $tgIds = array_rand($tags, $rd);
            foreach ($tgIds as $tgId) {
                $this->app['dbs']['try9_write']->insert('creative_tags', [
                    'creativeId' => $creative['creativeId'],
                    'tagId' => $tags[$tgId],
                ]);
            }
        }
        /* END CREATIVES */

        foreach ($campaigns['iframe'] as $cp) {
            $crs = $creatives[$cp['width'] . 'x' . $cp['height']];
            $rds = array_rand($crs, mt_rand(2, 5));
            foreach ($rds as $cid) {
                $this->app['dbs']['try9_write']->insert('campaign_creatives', [
                    'creativeId' => $crs[$cid]['creativeId'],
                    'campaignId' => $cp['campaignId'],
                    'weight' => mt_rand(1, 10) * 10,
                    'status' => 'on',
                ]);
            }
        }

        foreach ($zones as $type => $azs) {
            $cps = $campaigns[$type];
            foreach ($azs as $az) {
                $rds = array_rand($cps, mt_rand(2, 5));
                foreach ($rds as $cid) {
                    $this->app['dbs']['try9_write']->insert('campaign_zones', [
                        'adZoneId' => $az['adZoneId'],
                        'campaignId' => $cps[$cid]['campaignId'],
                        'weight' => mt_rand(1, 10) * 10,
                        'status' => 'active',
                    ]);
                }
            }
        }

    }
}
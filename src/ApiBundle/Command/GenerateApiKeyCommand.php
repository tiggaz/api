<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Command;

use Itsup\Bundle\ApiBundle\Entity\User;
use Itsup\Bundle\ApiBundle\Generator\ApiKeyGenerator;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\Exception\InvalidArgumentException;
use Symfony\Component\Yaml\Yaml;

/**
 * @author Martin Karadzinov <martin@myhost.mk>
 */
class GenerateApiKeyCommand extends ContainerAwareCommand
{
    /**
     * @var InputInterface
     */
    protected $input;

    /**
     * @var SymfonyStyle|OutputInterface
     */
    protected $output;

    public function configure()
    {
        $this->setName('itsup:api:key:generate')
            ->addOption(
                'type',
                't',
                InputOption::VALUE_REQUIRED,
                'Type of key to generate (User or Internal)'
            )
            ->addArgument(
                'id',
                InputArgument::OPTIONAL,
                'If user, the ID of the user to generate a key for.'
            )
            ->addOption(
                'regenerate',
                'r',
                InputOption::VALUE_NONE,
                'Should we regenerate the key, if there already is one?'
            )
            ->setDescription('Generate an api key for a user or for the internal application.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output = new SymfonyStyle($input, $output);
        $output->title('API Key Generator');

        $type       = $input->getOption('type');
        $id         = $input->getArgument('id');
        $regenerate = $input->getOption('regenerate');

        if (empty($type)) {
            throw new \InvalidArgumentException('Type is required: (user, internal)');
        }

        $apiKey = ApiKeyGenerator::generate();
        switch (strtolower($type)) {
            case 'user':
                $this->saveUser($id, $apiKey, $regenerate);
                break;
            case 'internal':
                $this->saveInternal($apiKey, $regenerate);
                break;
        }

        $output->success(['API Key generated:', $apiKey]);
    }

    /**
     * @param int    $id
     * @param string $apiKey
     * @param bool   $regenerate
     *
     * @throws \Exception
     */
    protected function saveUser(int $id, string $apiKey, bool $regenerate)
    {
        $finder = $this->getContainer()->get('finder.user');
        $user   = $finder->find(['id' => (int) $id]);

        if (empty($user)) {
            $user = new User();
            $user->setId($id);
        }

        $this->checkApiKey('User', $user->getApiKey(), $regenerate);
        $user->setApiKey($apiKey);

        $manager = $this->getContainer()->get('manager.user');
        $manager->save($user);
    }

    /**
     * @param string $apiKey
     * @param bool   $regenerate
     *
     * @throws \Exception
     */
    protected function saveInternal(string $apiKey, bool $regenerate)
    {
        try {
            $currentKey = $this->getContainer()->getParameter('api_key');
        } catch (InvalidArgumentException $e) {
            $currentKey = null;
        }
        $this->checkApiKey('Internal', $currentKey, $regenerate);
        $parameters = $this->getContainer()->getParameter('kernel.config_dir').'/parameters.yml';

        $yaml                          = Yaml::parse(file_get_contents($parameters));
        $yaml['parameters']['api_key'] = $apiKey;

        file_put_contents($parameters, Yaml::dump($yaml, 4));
    }

    /**
     * @param string $type
     * @param string $apiKey
     * @param bool   $regenerate
     *
     * @throws \Exception
     */
    protected function checkApiKey(string $type, $apiKey, bool $regenerate) // TODO PHP7.1 ?string $apiKey
    {
        if (!empty($apiKey) && !$regenerate) {
            throw new \Exception(
                $type.' already has an API key. Run again with --regenerate to create a new one.'
            );
        }
    }
}

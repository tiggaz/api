<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Security;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Itsup\Bundle\ApiBundle\Entity\User;
use Itsup\Bundle\ApiBundle\Model\InternalUser;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

/**
 * @author Martin Karadzinov <martin@myhost.mk>
 */
class ApiKeyUserProvider implements UserProviderInterface
{
    /**
     * @var Registry
     */
    private $doctrine;

    /**
     * @var null
     */
    private $internalKey;

    /**
     * @param Registry $doctrine
     * @param null     $internalKey
     */
    public function __construct(Registry $doctrine, $internalKey = null)
    {
        $this->doctrine    = $doctrine;
        $this->internalKey = $internalKey;
    }

    /**
     * @return Registry
     */
    public function getDoctrine(): Registry
    {
        return $this->doctrine;
    }

    /**
     * @return EntityManager
     */
    public function getManager(): EntityManager
    {
        return $this->getDoctrine()->getManager();
    }

    /**
     * @param string $name
     *
     * @return EntityRepository
     */
    public function getRepository(string $name): EntityRepository
    {
        return $this->getManager()->getRepository($name);
    }

    /**
     * @param string $type 'user', 'internal'
     * @param string $key  An api key
     *
     * @return User|InternalUser
     */
    public function loadUserByApiKey($type, $key)
    {
        if ($type === 'internal') {
            if ($key === $this->internalKey) {
                return new InternalUser($key);
            }

            return;
        }

        $repository = $this->getRepository('Itsup\Bundle\ApiBundle\Entity\User');

        return $repository->findOneBy(['apiKey' => $key]);
    }

    /**
     * @param string $apiKey
     *
     * @return object
     */
    public function loadUserByUsername($apiKey)
    {
        throw new \BadMethodCallException('Use loadUserByApiKey($type, $key)');
    }

    /**
     * @param UserInterface $user
     *
     * @return UserInterface|void
     *
     * @codeCoverageIgnore
     */
    public function refreshUser(UserInterface $user)
    {
        throw new UnsupportedUserException();
    }

    /**
     * @param string $class
     *
     * @return bool
     *
     * @codeCoverageIgnore
     */
    public function supportsClass($class): bool
    {
        return 'Symfony\Component\Security\Core\User\User' === $class;
    }
}

<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Security;

use Itsup\Bundle\ApiBundle\ApiError;
use Itsup\Bundle\ApiBundle\Entity\User;
use Itsup\Bundle\ApiBundle\Exception\ApiException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;

/**
 * @author Martin Karadzinov <martin@myhost.mk>
 */
class ApiKeyAuthenticator extends AbstractGuardAuthenticator
{
    /**
     * {@inheritdoc}
     */
    public function start(Request $request, AuthenticationException $authException = null)
    {
        if ($authException !== null) {
            throw new ApiException(ApiError::FORBIDDEN);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getCredentials(Request $request)
    {
        if ($request->query->has('u-api-key')) {
            return ['type' => 'user', 'key' => $request->query->get('u-api-key')];
        }

        if ($request->query->has('i-api-key')) {
            return ['type' => 'internal', 'key' => $request->query->get('i-api-key')];
        }

        $auth = $request->headers->get('authorization');
        if (empty($key) && !empty($auth)) {
            $tmp = explode(' ', $auth);

            return ['type' => strtolower($tmp[0]), 'key' => $tmp[1]];
        }
    }

    /**
     * @param ApiKeyUserProvider $userProvider
     *
     * {@inheritdoc}
     */
    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        if (empty($credentials['key'])) {
            return;
        }

        return $userProvider->loadUserByApiKey($credentials['type'], $credentials['key']);
    }

    /**
     * {@inheritdoc}
     */
    public function checkCredentials($credentials, UserInterface $user)
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        throw new ApiException(ApiError::FORBIDDEN, [strtr($exception->getMessageKey(), $exception->getMessageData())]);
    }

    /**
     * {@inheritdoc}
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function supportsRememberMe()
    {
        return false;
    }
}

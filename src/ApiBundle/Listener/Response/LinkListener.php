<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Listener\Response;

use Itsup\Bundle\ApiBundle\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;
use Symfony\Component\Routing\Generator\UrlGenerator;
use Symfony\Component\Routing\Router;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class LinkListener extends AbstractResponseListener
{
    /**
     * @var Router
     */
    private $router;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * LinkListener constructor.
     *
     * @param Router                $router
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(Router $router, TokenStorageInterface $tokenStorage)
    {
        $this->router       = $router;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * Format the output.
     *
     * @param GetResponseForControllerResultEvent $event
     */
    public function onKernelView(GetResponseForControllerResultEvent $event)
    {
        $result = $event->getControllerResult();

        $content = $result->getData();
        if (!array_key_exists('_links', $content)) {
            $content['_links'] = [];
        }

        $content['_links']['self'] = $event->getRequest()->getSchemeAndHttpHost().$event->getRequest()->getRequestUri();
        $content['_links']['docs'] = $this->generateUrl($event->getRequest(), 'nelmio_api_doc_index', []);

        $result->setData($content);
        $event->setControllerResult($result);
    }

    /**
     * @param Request $request
     * @param string  $route
     * @param array   $parameters
     * @param int     $referenceType
     *
     * @return string
     */
    public function generateUrl(Request $request, $route, $parameters = [], $referenceType = UrlGenerator::ABSOLUTE_URL)
    {
        $url = $this->router->generate($route, $parameters, $referenceType);

        $token = $this->tokenStorage->getToken();
        if ($token !== null && $token->getUser() !== 'anon.') {
            $user = $token->getUser();
            $type = $user instanceof User ? 'user' : 'internal';

            $url .= '?'.$type[0].'-api-key='.$user->getApiKey();
        }

        return $url;
    }
}

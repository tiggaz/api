<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Listener\Response;

use Itsup\Bundle\ApiBundle\Entity\User;
use JMS\Serializer\SerializerInterface;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

/**
 * @author Aaron Scherer <aequasi@gmail.com>
 */
class AuthenticationListener extends AbstractResponseListener
{
    /**
     * @var TokenStorage
     */
    private $tokenStorage;

    /**
     * @var AuthorizationChecker
     */
    private $authorizationChecker;

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * AuthenticationListener constructor.
     *
     * @param TokenStorage         $tokenStorage
     * @param AuthorizationChecker $authorizationChecker
     * @param SerializerInterface  $serializer
     */
    public function __construct(
        TokenStorage $tokenStorage,
        AuthorizationChecker $authorizationChecker,
        SerializerInterface $serializer
    ) {
        $this->tokenStorage         = $tokenStorage;
        $this->authorizationChecker = $authorizationChecker;
        $this->serializer           = $serializer;
    }

    /**
     * @param GetResponseForControllerResultEvent $event
     */
    public function onKernelView(GetResponseForControllerResultEvent $event)
    {
        $result = $event->getControllerResult();

        $content = $result->getData();
        if ($this->isAuthenticated()) {
            //$content['authentication']                  = $this->getUserArray();
            $content['authentication'] = ['authenticated' => true];
        } else {
            $content['authentication'] = ['authenticated' => false];
        }

        $result->setData($content);
        $event->setControllerResult($result);
    }

    public function isAuthenticated(): bool
    {
        return false !== $this->getUser() && $this->authorizationChecker->isGranted('ROLE_USER');
    }

    /**
     * Get a user from the Security Token Storage.
     *
     * @return User|false
     */
    public function getUser()
    {
        $token = $this->tokenStorage->getToken();

        if (!is_object($user = $token->getUser())) {
            return false;
        }

        return $user;
    }

    private function getUserArray(): array
    {
        return json_decode($this->serializer->serialize($this->getUser(), 'json'), true);
    }
}

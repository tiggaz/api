<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Listener\Response;

/**
 * @author Aaron Scherer <aequasi@gmail.com>
 */
abstract class AbstractResponseListener
{
}

<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Listener;

use Itsup\Bundle\ApiBundle\ApiError;
use Itsup\Bundle\ApiBundle\Exception\ApiException;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;

class ExceptionListener
{
    protected $logger;

    /**
     * @var string
     */
    protected $environment;

    public function __construct(LoggerInterface $logger = null, string $environment)
    {
        $this->logger      = $logger;
        $this->environment = $environment;
    }

    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        $exception = $event->getException();

        $this->logException(
            $exception,
            $event->getRequest(),
            sprintf(
                'Uncaught PHP Exception %s: "%s" at %s line %s',
                get_class($exception),
                $exception->getMessage(),
                $exception->getFile(),
                $exception->getLine()
            )
        );

        if (!$exception instanceof ApiException) {
            $errors    = in_array($this->environment, ['dev', 'test']) ? [$exception->getMessage()] : [];
            $type      = ApiError::getTypeByCode($exception->getCode());
            $exception = new ApiException($type, $errors);
        }

        $data = [
            'code'    => $exception->getCode(),
            'type'    => $exception->getType(),
            'message' => ApiError::getMessage($exception->getType()),
            'errors'  => $exception->getErrors(),
        ];

        $event->setResponse(new JsonResponse(['ApiException' => $data], $exception->getCode()));
    }

    /**
     * Logs an exception.
     *
     * @param \Exception $exception The \Exception instance
     * @param Request    $request
     * @param string     $message   The error message to log
     */
    protected function logException(\Exception $exception, Request $request, $message)
    {
        $extraMessage = ['request' => (string) $request, 'exception' => $exception];
        if ($exception instanceof ApiException) {
            if (is_array($exception->getErrors()) && count($exception->getErrors()) > 0) {
                $extraMessage['messages'] = $exception->getErrors();
            }
        }

        if (null !== $this->logger) {
            if ($exception instanceof ApiException && (int) $exception->getCode() === 404) {
                $this->logger->info($message, $extraMessage);
            } elseif ($exception instanceof ApiException && (int) $exception->getCode() >= 500) {
                $this->logger->critical($message, $extraMessage);
            } else {
                $this->logger->error($message, $extraMessage);
            }
        }
    }
}

<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Manager;

use Itsup\Bundle\ApiBundle\Entity\AbstractEntity;

class CampaignManager extends AbstractManager
{
    /**
     * @param AbstractEntity $object
     *
     * @return AbstractEntity
     */
    public function clone(AbstractEntity $object)
    {
        $campaign = clone $object;
        $this->getManager()->detach($campaign);

        return $campaign;
    }
}

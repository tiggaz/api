<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Manager;

class EventManager extends AbstractManager
{
    public function decryptClick($encryptedId)
    {
        $decryptedId = $this->decryptText($this->decryptSpecial($this->decryptNumber($encryptedId)));
        if (preg_match('/^([0-9]+):([0-9]+):(.+)$/', $decryptedId)) {
            return $decryptedId;
        } else {
            return $this->deprecatedDecryptId($encryptedId);
        }
    }

    private function decryptText($str)
    {
        $return        = '';
        $originLetters = str_split('abcdefghijklmnopqrstuvwxyz');
        $outLetters    = array_reverse($originLetters);
        $outLetters    = array_combine($outLetters, $originLetters);
        for ($i = 0; $i < strlen($str); $i++) {
            if (preg_match('/^([a-z]+)$/', $str[$i])) {
                $return .= $outLetters[$str[$i]];
            } else {
                $return .= $str[$i];
            }
        }

        return $return;
    }

    private function decryptSpecial($str)
    {
        return str_replace(['K', 'L', 'M', 'N', 'Z'], ['_', '-', '.', ',', ':'], $str);
    }

    private function decryptNumber($str)
    {
        return str_replace(['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J'], [0, 1, 2, 3, 4, 5, 6, 7, 8, 9], $str);
    }

    private function deprecatedDecryptId($encryptedId)
    {
        $key         = 'ShKO65fdatel45trQxm23';
        $encryptedId = str_replace(['3eP', '3eC'], ['?', '&'], $encryptedId);
        for ($i = 0; $i < strlen($encryptedId); $i++) {
            for ($j = 0; $j < strlen($key); $j++) {
                $encryptedId[$i] = $key[$j] ^ $encryptedId[$i];
            }
        }

        return strtolower($encryptedId);
    }

    public function encryptId($id)
    {
        return $this->encryptNumber($this->encryptSpecial($this->encryptText(strtolower($id))));
    }

    private function encryptNumber($nb)
    {
        return str_replace([0, 1, 2, 3, 4, 5, 6, 7, 8, 9], ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J'], $nb);
    }

    private function encryptText($str)
    {
        $ret           = '';
        $originLetters = str_split('abcdefghijklmnopqrstuvwxyz');
        $outLetters    = array_reverse($originLetters);
        $originLetters = array_combine($originLetters, $outLetters);
        for ($i = 0; $i < strlen($str); $i++) {
            if (preg_match('/^([a-z]+)$/', $str[$i])) {
                $ret .= $originLetters[$str[$i]];
            } else {
                $ret .= $str[$i];
            }
        }

        return $ret;
    }

    private function encryptSpecial($str)
    {
        return str_replace(['_', '-', '.', ',', ':'], ['K', 'L', 'M', 'N', 'Z'], $str);
    }
}

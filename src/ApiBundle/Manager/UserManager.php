<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Manager;

use Itsup\Bundle\ApiBundle\Entity\User;

class UserManager extends AbstractManager
{
    /**
     * @param User $user
     */
    public function delete(User $user)
    {
        $user->setAccount(null);

        parent::delete($user);
    }
}

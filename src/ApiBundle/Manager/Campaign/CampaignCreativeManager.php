<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Manager\Campaign;

use Itsup\Bundle\ApiBundle\Entity\AbstractEntity;
use Itsup\Bundle\ApiBundle\Manager\AbstractManager;

class CampaignCreativeManager extends AbstractManager
{
    /**
     * @param AbstractEntity $object
     *
     * @return AbstractEntity
     */
    public function clone(AbstractEntity $object)
    {
        $cw = clone $object;
        $this->getManager()->detach($cw);

        return $cw;
    }
}

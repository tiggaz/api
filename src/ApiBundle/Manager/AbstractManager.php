<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Manager;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Itsup\Bundle\ApiBundle\ApiError;
use Itsup\Bundle\ApiBundle\Entity\AbstractEntity;
use Itsup\Bundle\ApiBundle\Exception\ApiException;

abstract class AbstractManager
{
    /**
     * @var Registry
     */
    private $doctrine;

    /**
     * AbstractFinder constructor.
     *
     * @param Registry $doctrine
     */
    public function __construct(Registry $doctrine)
    {
        $this->doctrine = $doctrine;
    }

    /**
     * @return Registry
     */
    public function getDoctrine(): Registry
    {
        return $this->doctrine;
    }

    /**
     * @return EntityManager
     */
    public function getManager(): EntityManager
    {
        return $this->getDoctrine()->getManager();
    }

    /**
     * @param string $name
     *
     * @return EntityRepository
     */
    public function getRepository(string $name): EntityRepository
    {
        return $this->getManager()->getRepository($name);
    }

    /**
     * @param AbstractEntity $object
     *
     * @throws ApiException
     */
    public function save(AbstractEntity &$object)
    {
        try {
            $this->getManager()->persist($object);
            $this->getManager()->flush();
        } catch (\Exception $e) {
            self::throwException($e);
        }
    }

    /**
     * @param AbstractEntity $object
     *
     * @throws ApiException
     */
    public function update(AbstractEntity &$object)
    {
        $this->save($object);
    }

    /**
     * @param AbstractEntity $object
     *
     * @throws ApiException
     */
    public function delete(AbstractEntity $object)
    {
        try {
            $this->getManager()->remove($object);
            $this->getManager()->flush();
        } catch (\Exception $e) {
            self::throwException($e);
        }
    }

    /**
     * @param AbstractEntity $object
     *
     * @throws ApiException
     */
    public function clone(AbstractEntity $object)
    {
        throw new ApiException(ApiError::INVALID_REQUEST, ['Object not clonable']);
    }

    /**
     * @param \Exception $e
     * @param string     $errorType
     *
     * @throws ApiException
     */
    public static function throwException(\Exception $e, string $errorType = ApiError::ORM)
    {
        if ($e instanceof ApiException) {
            throw $e;
        }

        throw new ApiException(
            $errorType,
            [
                'message' => $e->getMessage(),
                'file'    => $e->getFile(),
                'line'    => $e->getLine(),
                'stack'   => $e->getTrace(),
            ]
        );
    }
}

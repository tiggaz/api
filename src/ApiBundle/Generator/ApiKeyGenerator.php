<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Generator;

/**
 * @author Martin Karadzinov <martin@myhost.mk>
 */
class ApiKeyGenerator
{
    /**
     * Generate an API key.
     *
     * @return string
     */
    public static function generate()
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $apiKey     = '';
        for ($i = 0; $i < 64; $i++) {
            $apiKey .= $characters[rand(0, strlen($characters) - 1)];
        }
        $apiKey = base64_encode(sha1(uniqid('ue'.rand(rand(), rand())).$apiKey));

        return $apiKey;
    }
}

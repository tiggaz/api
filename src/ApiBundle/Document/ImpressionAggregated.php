<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Document;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @author Martin Karadzinov <martin@myhost.mk>
 * @MongoDB\Document(collection="impressions")
 */
class ImpressionAggregated
{
    /**
     * @var string
     * @MongoDB\Id
     */
    protected $id;

    /**
     * @var int
     * @MongoDB\Field(type="integer", name="z")
     */
    protected $adZoneId;

    /**
     * @var int
     * @MongoDB\Field(type="integer", name="ca")
     */
    protected $campaignId;

    /**
     * @var int
     * @MongoDB\Field(type="integer", name="imps")
     */
    protected $count;

    /**
     * @var ArrayCollection
     * @MongoDB\EmbedMany(targetDocument="Itsup\Bundle\ApiBundle\Document\Metric", name="cr")
     */
    protected $creatives;

    /**
     * @var ArrayCollection
     * @MongoDB\EmbedMany(targetDocument="Itsup\Bundle\ApiBundle\Document\Metric", name="b")
     */
    protected $browsers;

    /**
     * @var ArrayCollection
     * @MongoDB\EmbedMany(targetDocument="Itsup\Bundle\ApiBundle\Document\Metric", name="c")
     */
    protected $countries;

    /**
     * @var ArrayCollection
     * @MongoDB\EmbedMany(targetDocument="Itsup\Bundle\ApiBundle\Document\Metric", name="d")
     */
    protected $devices;

    /**
     * @var ArrayCollection
     * @MongoDB\EmbedMany(targetDocument="Itsup\Bundle\ApiBundle\Document\Metric", name="i")
     */
    protected $internetServiceProviders;

    /**
     * @var ArrayCollection
     * @MongoDB\EmbedMany(targetDocument="Itsup\Bundle\ApiBundle\Document\Metric", name="k")
     */
    protected $keywords;

    /**
     * @var ArrayCollection
     * @MongoDB\EmbedMany(targetDocument="Itsup\Bundle\ApiBundle\Document\Metric", name="l")
     */
    protected $languages;

    /**
     * @var ArrayCollection
     * @MongoDB\EmbedMany(targetDocument="Itsup\Bundle\ApiBundle\Document\Metric", name="o")
     */
    protected $operatingSystems;

    /**
     * @var \DateTime
     * @MongoDB\Field(type="date", name="time")
     */
    protected $date;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->creatives                = new ArrayCollection();
        $this->browsers                 = new ArrayCollection();
        $this->devices                  = new ArrayCollection();
        $this->countries                = new ArrayCollection();
        $this->internetServiceProviders = new ArrayCollection();
        $this->keywords                 = new ArrayCollection();
        $this->operatingSystems         = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getAdZoneId(): int
    {
        return $this->adZoneId;
    }

    /**
     * @param int $adZoneId
     */
    public function setAdZoneId(int $adZoneId)
    {
        $this->adZoneId = $adZoneId;
    }

    /**
     * @return int
     */
    public function getCampaignId(): int
    {
        return $this->campaignId;
    }

    /**
     * @param int $campaignId
     */
    public function setCampaignId(int $campaignId)
    {
        $this->campaignId = $campaignId;
    }

    /**
     * @return int
     */
    public function getCount(): int
    {
        return $this->count;
    }

    /**
     * @param int $count
     */
    public function setCount(int $count)
    {
        $this->count = $count;
    }

    /**
     * @return ArrayCollection
     */
    public function getCreatives(): ArrayCollection
    {
        return $this->creatives;
    }

    /**
     * @param ArrayCollection $creatives
     */
    public function setCreatives(ArrayCollection $creatives)
    {
        $this->creatives = $creatives;
    }

    /**
     * @return ArrayCollection
     */
    public function getBrowsers(): ArrayCollection
    {
        return $this->browsers;
    }

    /**
     * @param ArrayCollection $browsers
     */
    public function setBrowsers(ArrayCollection $browsers)
    {
        $this->browsers = $browsers;
    }

    /**
     * @return ArrayCollection
     */
    public function getCountries(): ArrayCollection
    {
        return $this->countries;
    }

    /**
     * @param ArrayCollection $countries
     */
    public function setCountries(ArrayCollection $countries)
    {
        $this->countries = $countries;
    }

    /**
     * @return ArrayCollection
     */
    public function getDevices(): ArrayCollection
    {
        return $this->devices;
    }

    /**
     * @param ArrayCollection $devices
     */
    public function setDevices(ArrayCollection $devices)
    {
        $this->devices = $devices;
    }

    /**
     * @return ArrayCollection
     */
    public function getInternetServiceProviders(): ArrayCollection
    {
        return $this->internetServiceProviders;
    }

    /**
     * @param ArrayCollection $internetServiceProviders
     */
    public function setInternetServiceProviders(ArrayCollection $internetServiceProviders)
    {
        $this->internetServiceProviders = $internetServiceProviders;
    }

    /**
     * @return ArrayCollection
     */
    public function getKeywords(): ArrayCollection
    {
        return $this->keywords;
    }

    /**
     * @param ArrayCollection $keywords
     */
    public function setKeywords(ArrayCollection $keywords)
    {
        $this->keywords = $keywords;
    }

    /**
     * @return ArrayCollection
     */
    public function getLanguages(): ArrayCollection
    {
        return $this->languages;
    }

    /**
     * @param ArrayCollection $languages
     */
    public function setLanguages(ArrayCollection $languages)
    {
        $this->languages = $languages;
    }

    /**
     * @return ArrayCollection
     */
    public function getOperatingSystems(): ArrayCollection
    {
        return $this->operatingSystems;
    }

    /**
     * @param ArrayCollection $operatingSystems
     */
    public function setOperatingSystems(ArrayCollection $operatingSystems)
    {
        $this->operatingSystems = $operatingSystems;
    }

    /**
     * @return \DateTime
     */
    public function getDate(): \DateTime
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     */
    public function setDate(\DateTime $date)
    {
        $this->date = $date;
    }
}

<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @author Martin Karadzinov <martin@myhost.mk>
 * @MongoDB\Document(collection="clicks")
 */
class Click
{
    /**
     * @var string
     * @MongoDB\Id(name="_id")
     */
    protected $_id;

    /**
     * @var string
     * @MongoDB\Field(type="string", name="id")
     */
    protected $clickId;

    /**
     * @var int
     * @MongoDB\Field(type="integer", name="z")
     */
    protected $adZoneId;

    /**
     * @var int
     * @MongoDB\Field(type="integer", name="ca")
     */
    protected $campaignId;

    /**
     * @var int
     * @MongoDB\Field(type="integer", name="cr")
     */
    protected $creativeId;

    /**
     * @var string
     * @MongoDB\Field(type="string", name="ip")
     */
    protected $ip;

    /**
     * @var string
     * @MongoDB\Field(type="string", name="b")
     */
    protected $browsers;

    /**
     * @var string
     * @MongoDB\Field(type="string", name="c")
     */
    protected $countries;

    /**
     * @var string
     * @MongoDB\Field(type="string", name="d")
     */
    protected $devices;

    /**
     * @var string
     * @MongoDB\Field(type="string", name="i")
     */
    protected $internetServiceProviders;

    /**
     * @var string
     * @MongoDB\Field(type="string", name="k")
     */
    protected $keywords;

    /**
     * @var string
     * @MongoDB\Field(type="string", name="l")
     */
    protected $languages;

    /**
     * @var string
     * @MongoDB\Field(type="string", name="o")
     */
    protected $operatingSystems;

    /**
     * @var \DateTime
     * @MongoDB\Field(type="date", name="time")
     */
    protected $date;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->_id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id)
    {
        $this->_id = $id;
    }

    /**
     * @return string
     */
    public function getClickId(): string
    {
        return $this->clickId;
    }

    /**
     * @param string $clickId
     */
    public function setClickId(string $clickId)
    {
        $this->clickId = $clickId;
    }

    /**
     * @return int
     */
    public function getAdZoneId(): int
    {
        return $this->adZoneId;
    }

    /**
     * @param int $adZoneId
     */
    public function setAdZoneId(int $adZoneId)
    {
        $this->adZoneId = $adZoneId;
    }

    /**
     * @return int
     */
    public function getCampaignId(): int
    {
        return $this->campaignId;
    }

    /**
     * @param int $campaignId
     */
    public function setCampaignId(int $campaignId)
    {
        $this->campaignId = $campaignId;
    }

    /**
     * @return int
     */
    public function getCreativeId(): int
    {
        return $this->creativeId;
    }

    /**
     * @param int $creativeId
     */
    public function setCreativeId(int $creativeId)
    {
        $this->creativeId = $creativeId;
    }

    /**
     * @return string
     */
    public function getIp(): string
    {
        return $this->ip;
    }

    /**
     * @param string $ip
     */
    public function setIp(string $ip)
    {
        $this->ip = $ip;
    }

    /**
     * @return string
     */
    public function getBrowsers(): string
    {
        return $this->browsers;
    }

    /**
     * @param string $browsers
     */
    public function setBrowsers(string $browsers)
    {
        $this->browsers = $browsers;
    }

    /**
     * @return string
     */
    public function getCountries(): string
    {
        return $this->countries;
    }

    /**
     * @param string $countries
     */
    public function setCountries(string $countries)
    {
        $this->countries = $countries;
    }

    /**
     * @return string
     */
    public function getDevices(): string
    {
        return $this->devices;
    }

    /**
     * @param string $devices
     */
    public function setDevices(string $devices)
    {
        $this->devices = $devices;
    }

    /**
     * @return string
     */
    public function getInternetServiceProviders(): string
    {
        return $this->internetServiceProviders;
    }

    /**
     * @param string $internetServiceProviders
     */
    public function setInternetServiceProviders(string $internetServiceProviders)
    {
        $this->internetServiceProviders = $internetServiceProviders;
    }

    /**
     * @return string
     */
    public function getKeywords(): string
    {
        return $this->keywords;
    }

    /**
     * @param string $keywords
     */
    public function setKeywords(string $keywords)
    {
        $this->keywords = $keywords;
    }

    /**
     * @return string
     */
    public function getLanguages(): string
    {
        return $this->languages;
    }

    /**
     * @param string $languages
     */
    public function setLanguages(string $languages)
    {
        $this->languages = $languages;
    }

    /**
     * @return string
     */
    public function getOperatingSystems(): string
    {
        return $this->operatingSystems;
    }

    /**
     * @param string $operatingSystems
     */
    public function setOperatingSystems(string $operatingSystems)
    {
        $this->operatingSystems = $operatingSystems;
    }

    /**
     * @return \DateTime
     */
    public function getDate(): \DateTime
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     */
    public function setDate(\DateTime $date)
    {
        $this->date = $date;
    }
}

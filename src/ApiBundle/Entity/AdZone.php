<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Itsup\Bundle\ApiBundle\Entity\Account\ExternalStatisticsProvider;
use Itsup\Bundle\ApiBundle\Entity\AdZone\AdZoneCampaign;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(
 *     name="ad_zone",
 *     indexes={@ORM\Index(name="type_hosting_status_idx", columns={"type", "hosting", "status"})}
 * )
 */
class AdZone extends AbstractFollowerEntity
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     * @JMS\Groups({"list", "details"})
     */
    protected $id = 0;

    /**
     * @Assert\NotNull(message="Account not found")
     *
     * @var Account
     * @ORM\ManyToOne(targetEntity="Itsup\Bundle\ApiBundle\Entity\Account")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $account = null;

    /**
     * @var Contact
     * @ORM\ManyToOne(targetEntity="Itsup\Bundle\ApiBundle\Entity\Contact")
     * @JMS\Groups({"list", "details"})
     */
    protected $contact = null;

    /**
     * @Assert\NotBlank(message="Name can not be empty")
     *
     * @var string
     * @ORM\Column(type="string", length=150)
     * @JMS\Groups({"list", "details"})
     */
    protected $name = '';

    /**
     * @Assert\NotBlank(message="Hosting can not be empty")
     *
     * @var string
     * @ORM\Column(type="string", columnDefinition="ENUM('local', 'remote')")
     * @JMS\Groups({"list", "details"})
     */
    protected $hosting = '';

    /**
     * @Assert\NotBlank(message="Type can not be empty")
     *
     * @var string
     * @ORM\Column(type="string", columnDefinition="ENUM('iframe','direct link','text','json','popunder')")
     * @JMS\Groups({"list", "details"})
     */
    protected $type = '';

    /**
     * @var int
     * @ORM\Column(type="integer", length=4, options={"unsigned"=true})
     * @JMS\Groups({"list", "details"})
     */
    protected $width = 0;

    /**
     * @var int
     * @ORM\Column(type="integer", length=4, options={"unsigned"=true})
     * @JMS\Groups({"list", "details"})
     */
    protected $height = 0;

    /**
     * @var string
     * @ORM\Column(type="string", columnDefinition="ENUM('on','off','paused')")
     * @JMS\Groups({"list", "details"})
     */
    protected $status = 'on';

    /**
     * @var string
     * @ORM\Column(type="string", columnDefinition="ENUM('order','weight')")
     * @JMS\Groups({"list", "details"})
     */
    protected $display = 'order';

    /**
     * @var Campaign
     * @ORM\ManyToOne(targetEntity="Itsup\Bundle\ApiBundle\Entity\Campaign")
     * @ORM\OrderBy({"id"="ASC"})
     * @JMS\Groups({"list", "details"})
     */
    protected $defaultCampaign = null;

    /**
     * @var ExternalStatisticsProvider
     * @ORM\ManyToOne(targetEntity="Itsup\Bundle\ApiBundle\Entity\Account\ExternalStatisticsProvider")
     * @JMS\Groups({"list", "details"})
     */
    protected $externalStatisticsProvider = null;

    /**
     * @var string
     * @ORM\Column(type="string")
     * @JMS\Groups({"list", "details"})
     */
    protected $externalStatisticsId = '';

    /**
     * @var string
     * @ORM\Column(type="string")
     * @JMS\Groups({"list", "details"})
     */
    protected $externalStatisticsIdComplement = '';

    /**
     * @var array
     * @ORM\Column(type="simple_array", nullable=true)
     * @JMS\Groups({"list", "details"})
     */
    protected $externalStatisticsOptions = [];

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     * @JMS\Groups({"list", "details"})
     */
    protected $dateCreated;

    /**
     * @Assert\NotNull(message="createdBy must be set")
     *
     * @var User
     * @ORM\ManyToOne(targetEntity="Itsup\Bundle\ApiBundle\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     * @JMS\Groups({"list", "details"})
     */
    protected $createdBy = null;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     * @JMS\Groups({"list", "details"})
     */
    protected $dateUpdated;

    /**
     * @Assert\NotNull(message="updatedBy must be set")
     *
     * @var User
     * @ORM\ManyToOne(targetEntity="Itsup\Bundle\ApiBundle\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     * @JMS\Groups({"list", "details"})
     */
    protected $updatedBy = null;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(
     *     targetEntity="Itsup\Bundle\ApiBundle\Entity\AdZone\Accounting",
     *     mappedBy="adZone",
     *     cascade={"persist", "remove"}
     * )
     * @ORM\OrderBy({"from"="ASC"})
     * @JMS\Groups({"details"})
     */
    protected $accounting;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(
     *     targetEntity="Itsup\Bundle\ApiBundle\Entity\AdZone\AdZoneCampaign",
     *     cascade={"persist", "remove"},
     *     mappedBy="adZone"
     * )
     * @ORM\OrderBy({"status"="ASC"})
     * @JMS\Groups({"details"})
     */
    protected $campaigns;

    /**
     * AdZone constructor.
     */
    public function __construct()
    {
        $this->accounting  = new ArrayCollection();
        $this->tags        = new ArrayCollection();
        $this->followers   = new ArrayCollection();
        $this->campaigns   = new ArrayCollection();
        $this->notes       = new ArrayCollection();
        $this->dateCreated = new \DateTime();
    }

    public function __clone()
    {
        $this->name .= ' - DUPLICATE';
        $this->id                             = null;
        $this->dateCreated                    = new \DateTime();
        $this->dateUpdated                    = new \DateTime();
        $this->externalStatisticsProvider     = null;
        $this->externalStatisticsId           = '';
        $this->externalStatisticsIdComplement = '';
        $this->externalStatisticsOptions      = null;
        $this->followers                      = new ArrayCollection();
        $this->campaigns                      = new ArrayCollection();
        $this->notes                          = new ArrayCollection();
        $this->accounting                     = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return Account
     */
    public function getAccount(): ?Account
    {
        return $this->account;
    }

    /**
     * @param Account $account
     */
    public function setAccount(?Account $account)
    {
        $this->account = $account;
    }

    /**
     * @return Contact
     */
    public function getContact() // TODO PHP7.1 : ?Contact
    {
        return $this->contact;
    }

    /**
     * @param Contact $contact
     */
    public function setContact(Contact $contact)
    {
        $this->contact = $contact;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getHosting(): string
    {
        return $this->hosting;
    }

    /**
     * @param string $hosting
     */
    public function setHosting(string $hosting)
    {
        $this->hosting = $hosting;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type)
    {
        $this->type = $type;
    }

    /**
     * @return int
     */
    public function getWidth(): int
    {
        return $this->width;
    }

    /**
     * @param int $width
     */
    public function setWidth(int $width)
    {
        $this->width = $width;
    }

    /**
     * @return int
     */
    public function getHeight(): int
    {
        return $this->height;
    }

    /**
     * @param int $height
     */
    public function setHeight(int $height)
    {
        $this->height = $height;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus(string $status)
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getDisplay(): string
    {
        return $this->display;
    }

    /**
     * @param string $display
     */
    public function setDisplay(string $display)
    {
        $this->display = $display;
    }

    /**
     * @return Campaign|null
     */
    public function getDefaultCampaign() // TODO PHP7.1 : ?Campaign
    {
        return $this->defaultCampaign;
    }

    /**
     * @param Campaign $defaultCampaign
     */
    public function setDefaultCampaign(Campaign $defaultCampaign)
    {
        $this->defaultCampaign = $defaultCampaign;
    }

    /**
     * @return ExternalStatisticsProvider
     */
    public function getExternalStatisticsProvider()
    {
        return $this->externalStatisticsProvider;
    }

    /**
     * @param ExternalStatisticsProvider $externalStatisticsProvider
     */
    public function setExternalStatisticsProvider(ExternalStatisticsProvider $externalStatisticsProvider)
    {
        $this->externalStatisticsProvider = $externalStatisticsProvider;
    }

    /**
     * @return string
     */
    public function getExternalStatisticsId(): string
    {
        return $this->externalStatisticsId;
    }

    /**
     * @param string $externalStatisticsId
     */
    public function setExternalStatisticsId(string $externalStatisticsId)
    {
        $this->externalStatisticsId = $externalStatisticsId;
    }

    /**
     * @return string
     */
    public function getExternalStatisticsIdComplement(): string
    {
        return $this->externalStatisticsIdComplement;
    }

    /**
     * @param string $externalStatisticsIdComplement
     */
    public function setExternalStatisticsIdComplement(string $externalStatisticsIdComplement)
    {
        $this->externalStatisticsIdComplement = $externalStatisticsIdComplement;
    }

    /**
     * @return array
     */
    public function getExternalStatisticsOptions(): array
    {
        return $this->externalStatisticsOptions;
    }

    /**
     * @param array $externalStatisticsOptions
     */
    public function setExternalStatisticsOptions(array $externalStatisticsOptions)
    {
        $this->externalStatisticsOptions = $externalStatisticsOptions;
    }

    /**
     * @return \DateTime
     */
    public function getDateCreated(): \DateTime
    {
        return $this->dateCreated;
    }

    /**
     * @param \DateTime $dateCreated
     */
    public function setDateCreated(\DateTime $dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param User $createdBy
     */
    public function setCreatedBy(User $createdBy)
    {
        $this->createdBy = $createdBy;
    }

    /**
     * @return \DateTime
     */
    public function getDateUpdated(): \DateTime
    {
        return $this->dateUpdated;
    }

    /**
     * @param \DateTime $dateUpdated
     */
    public function setDateUpdated(\DateTime $dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
    }

    /**
     * @return User
     */
    public function getUpdatedBy() // TODO PHP7.1 : ?User
    {
        return $this->updatedBy;
    }

    /**
     * @param User $updatedBy
     */
    public function setUpdatedBy(User $updatedBy)
    {
        $this->updatedBy = $updatedBy;
    }

    /**
     * @return ArrayCollection
     */
    public function getAccounting(): ArrayCollection
    {
        return $this->accounting;
    }

    /**
     * @param ArrayCollection $accounting
     */
    public function setAccounting(ArrayCollection $accounting)
    {
        $this->accounting = $accounting;
    }

    /**
     * @return ArrayCollection
     */
    public function getCampaigns()
    {
        return $this->campaigns;
    }

    /**
     * @param ArrayCollection $campaigns
     */
    public function setCampaigns(ArrayCollection $campaigns)
    {
        $this->campaigns = $campaigns;
    }

    /**
     * @param AdZoneCampaign $campaign
     */
    public function addCampaign(AdZoneCampaign $campaign)
    {
        $index = $this->campaigns->indexOf($campaign);
        if ($index !== false) {
            $this->campaigns->set($index, $campaign);
        } else {
            $campaign->setAdZone($this);
            $this->campaigns->add($campaign);
        }
    }

    /**
     * @param AdZoneCampaign $campaign
     */
    public function removeCampaign(AdZoneCampaign $campaign)
    {
        $this->campaigns->removeElement($campaign);
    }

    /**
     * @return ArrayCollection
     */
    public function getNotes(): ArrayCollection
    {
        return $this->notes;
    }

    /**
     * @param ArrayCollection $notes
     */
    public function setNotes(ArrayCollection $notes)
    {
        $this->notes = $notes;
    }
}

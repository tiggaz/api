<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Entity\Metrics;

use Doctrine\ORM\Mapping as ORM;
use Itsup\Bundle\ApiBundle\Entity\AbstractEntity;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(
 *     name="metrics_country",
 *     indexes={
 *          @ORM\Index(name="name_idx", columns={"name"}),
 *          @ORM\Index(name="position_idx", columns={"position"}),
 *     }
 * )
 */
class Country extends AbstractEntity
{
    /**
     * @var string
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(type="string")
     * @JMS\Groups({"list", "details"})
     */
    protected $id='';

    /**
     * @Assert\NotBlank(message="Name can not be empty")
     *
     * @var string
     * @ORM\Column(type="string", length=70)
     * @JMS\Groups({"list", "details"})
     */
    protected $name='';

    /**
     * @var string
     * @ORM\Column(type="string", length=120)
     * @JMS\Groups({"list", "details"})
     */
    protected $officialName='';

    /**
     * @var string
     * @ORM\Column(type="string", length=3)
     * @JMS\Groups({"list", "details"})
     */
    protected $countryCode3='';

    /**
     * @var string
     * @ORM\Column(type="string", length=50)
     * @JMS\Groups({"list", "details"})
     */
    protected $region='';

    /**
     * @var string
     * @ORM\Column(type="string", length=50)
     * @JMS\Groups({"list", "details"})
     */
    protected $subRegion='';

    /**
     * @var int
     * @ORM\Column(type="integer", length=3)
     * @JMS\Groups({"list", "details"})
     */
    protected $position=0;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getOfficialName(): string
    {
        return $this->officialName;
    }

    /**
     * @param string $officialName
     */
    public function setOfficialName(string $officialName)
    {
        $this->officialName = $officialName;
    }

    /**
     * @return string
     */
    public function getCountryCode3(): string
    {
        return $this->countryCode3;
    }

    /**
     * @param string $countryCode3
     */
    public function setCountryCode3(string $countryCode3)
    {
        $this->countryCode3 = $countryCode3;
    }

    /**
     * @return string
     */
    public function getRegion(): string
    {
        return $this->region;
    }

    /**
     * @param string $region
     */
    public function setRegion(string $region)
    {
        $this->region = $region;
    }

    /**
     * @return string
     */
    public function getSubRegion(): string
    {
        return $this->subRegion;
    }

    /**
     * @param string $subRegion
     */
    public function setSubRegion(string $subRegion)
    {
        $this->subRegion = $subRegion;
    }

    /**
     * @return int
     */
    public function getPosition(): int
    {
        return $this->position;
    }

    /**
     * @param int $position
     */
    public function setPosition(int $position)
    {
        $this->position = $position;
    }
}

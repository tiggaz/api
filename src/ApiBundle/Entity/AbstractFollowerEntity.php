<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

abstract class AbstractFollowerEntity extends AbstractNoteTagEntity
{
    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="Itsup\Bundle\ApiBundle\Entity\User")
     * @ORM\OrderBy({"name"="ASC"})
     * @JMS\Groups({"details"})
     */
    protected $followers;

    /**
     * @return ArrayCollection
     */
    public function getFollowers(): ArrayCollection
    {
        return $this->followers;
    }

    /**
     * @param ArrayCollection $followers
     */
    public function setFollowers(ArrayCollection $followers)
    {
        $this->followers = $followers;
    }

    /**
     * @param User $follower
     */
    public function addFollower(User $follower)
    {
        $this->followers->add($follower);
    }

    /**
     * @param User $follower
     */
    public function removeFollower(User $follower)
    {
        $this->followers->removeElement($follower);
    }
}

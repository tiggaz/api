<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(
 *     name="offer",
 *     indexes={@ORM\Index(name="type_status_idx", columns={"payout_type", "status"})}
 * )
 */
class Offer extends AbstractNoteTagEntity
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     * @JMS\Groups({"list", "details"})
     */
    protected $id = 0;

    /**
     * @Assert\NotNull(message="Account not found")
     *
     * @var Account
     * @ORM\ManyToOne(targetEntity="Itsup\Bundle\ApiBundle\Entity\Account")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $account = null;

    /**
     * @var Contact
     * @ORM\ManyToOne(targetEntity="Itsup\Bundle\ApiBundle\Entity\Contact")
     * @JMS\Groups({"list", "details"})
     */
    protected $contact = null;

    /**
     * @Assert\NotBlank(message="Name can not be empty")
     *
     * @var string
     * @ORM\Column(type="string", length=100)
     * @JMS\Groups({"list", "details"})
     */
    protected $name = '';

    /**
     * @Assert\NotBlank(message="Url can not be empty")
     *
     * @var string
     * @ORM\Column(type="text")
     * @JMS\Groups({"list", "details"})
     */
    protected $url = '';

    /**
     * @var string
     * @ORM\Column(type="string", columnDefinition="ENUM('pps','revshare','ppl','cpc','cpm imps','cpm clicks')")
     * @JMS\Groups({"list", "details"})
     */
    protected $payoutType = '';

    /**
     * @var int
     * @ORM\Column(type="integer", length=3, options={"unsigned"=true})
     * @JMS\Groups({"list", "details"})
     */
    protected $defaultValue = 0;

    /**
     * @Assert\NotBlank(message="Statistics URL can not be empty")
     *
     * @var string
     * @ORM\Column(type="text")
     * @JMS\Groups({"list", "details"})
     */
    protected $statisticsUrl = '';

    /**
     * @Assert\NotBlank(message="Status can not be empty")
     *
     * @var string
     * @ORM\Column(type="string", columnDefinition="ENUM('active','paused','inactive')")
     * @JMS\Groups({"list", "details"})
     */
    protected $status = '';

    /**
     * Offer constructor.
     */
    public function __construct()
    {
        $this->tags    = new ArrayCollection();
        $this->notes   = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return Account
     */
    public function getAccount() // TODO PHP7.1 : ?Account
    {
        return $this->account;
    }

    /**
     * @param Account $account
     */
    public function setAccount($account) // TODO PHP7.1 ?Account $account
    {
        $this->account = $account;
    }

    /**
     * @return Contact
     */
    public function getContact() // TODO PHP7.1 : ?Contact
    {
        return $this->contact;
    }

    /**
     * @param Contact $contact
     */
    public function setContact($contact) // TODO PHP7.1 ?Contact $contact
    {
        $this->contact = $contact;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl(string $url)
    {
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getPayoutType(): string
    {
        return $this->payoutType;
    }

    /**
     * @param string $payoutType
     */
    public function setPayoutType(string $payoutType)
    {
        $this->payoutType = $payoutType;
    }

    /**
     * @return int
     */
    public function getDefaultValue(): int
    {
        return $this->defaultValue;
    }

    /**
     * @param int $defaultValue
     */
    public function setDefaultValue(int $defaultValue)
    {
        $this->defaultValue = $defaultValue;
    }

    /**
     * @return string
     */
    public function getStatisticsUrl(): string
    {
        return $this->statisticsUrl;
    }

    /**
     * @param string $statisticsUrl
     */
    public function setStatisticsUrl(string $statisticsUrl)
    {
        $this->statisticsUrl = $statisticsUrl;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus(string $status)
    {
        $this->status = $status;
    }
}

<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

abstract class AbstractNoteEntity extends AbstractEntity
{
    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="Itsup\Bundle\ApiBundle\Entity\Note", cascade={"all"})
     * @ORM\OrderBy({"date"="ASC"})
     * @JMS\Groups({"details"})
     */
    protected $notes;

    /**
     * @return ArrayCollection
     */
    public function getNotes(): ArrayCollection
    {
        return $this->notes;
    }

    /**
     * @param ArrayCollection $notes
     */
    public function setNotes(ArrayCollection $notes)
    {
        $this->notes = $notes;
    }

    /**
     * @param Note $note
     */
    public function addNote(Note $note)
    {
        $this->notes->add($note);
    }

    /**
     * @param Note $note
     */
    public function removeNote(Note $note)
    {
        $this->notes->removeElement($note);
    }
}

<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

abstract class AbstractNoteTagEntity extends AbstractEntity
{
    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="Itsup\Bundle\ApiBundle\Entity\Note", cascade={"persist", "remove"})
     * @ORM\OrderBy({"date"="ASC"})
     * @JMS\Groups({"details"})
     */
    protected $notes;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="Itsup\Bundle\ApiBundle\Entity\Tag", cascade={"persist", "detach"})
     * @ORM\OrderBy({"name"="ASC"})
     * @JMS\Groups({"details"})
     */
    protected $tags;

    /**
     * @return ArrayCollection
     */
    public function getNotes(): ArrayCollection
    {
        return $this->notes;
    }

    /**
     * @param ArrayCollection $notes
     */
    public function setNotes(ArrayCollection $notes)
    {
        $this->notes = $notes;
    }

    /**
     * @param Note $note
     */
    public function addNote(Note $note)
    {
        $this->notes->add($note);
    }

    /**
     * @param Note $note
     */
    public function removeNote(Note $note)
    {
        $this->notes->removeElement($note);
    }

    /**
     * @return ArrayCollection
     */
    public function getTags()//: ArrayCollection
    {
        return $this->tags;
    }

    /**
     * @param ArrayCollection $tags
     */
    public function setTags(ArrayCollection $tags)
    {
        $this->tags = $tags;
    }

    public function resetTags()
    {
        $this->tags = new ArrayCollection();
    }
}

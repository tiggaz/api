<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Entity\Account;

use Doctrine\ORM\Mapping as ORM;
use Itsup\Bundle\ApiBundle\Entity\AbstractEntity;
use Itsup\Bundle\ApiBundle\Entity\Account;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(
 *     name="account_billing",
 *     indexes={@ORM\Index(name="date_idx", columns={"date"})}
 * )
 */
class Billing extends AbstractEntity
{
    /**
     * @Assert\NotNull(message="Account not found")
     *
     * @var Account
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Itsup\Bundle\ApiBundle\Entity\Account", inversedBy="billing")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $account = null;

    /**
     * @var \DateTime
     * @ORM\Id
     * @ORM\Column(type="itsupdate")
     * @JMS\Groups({"list", "details"})
     */
    protected $date;

    /**
     * @var int
     * @ORM\Column(type="bigint")
     * @JMS\Groups({"list", "details"})
     */
    protected $impressions = 0;

    /**
     * @var int
     * @ORM\Column(type="bigint")
     * @JMS\Groups({"list", "details"})
     */
    protected $clicks = 0;

    /**
     * @var int
     * @ORM\Column(type="bigint")
     * @JMS\Groups({"list", "details"})
     */
    protected $events = 0;

    /**
     * @var float
     * @ORM\Column(type="float", precision=14, scale=5)
     * @JMS\Groups({"list", "details"})
     */
    protected $price = 0;

    /**
     * User constructor.
     */
    public function __construct()
    {
        $this->date = date('Y-m-d');
    }

    /**
     * @return Account
     */
    public function getAccount() // TODO PHP7.1 : ?Account
    {
        return $this->account;
    }

    /**
     * @param Account $account
     */
    public function setAccount($account) // TODO PHP7.1 ?Account $account
    {
        $this->account = $account;
    }

    /**
     * @return string
     */
    public function getDate(): string
    {
        return $this->date;
    }

    /**
     * @param string $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return int
     */
    public function getImpressions(): int
    {
        return $this->impressions;
    }

    /**
     * @param int $impressions
     */
    public function setImpressions(int $impressions)
    {
        $this->impressions = $impressions;
    }

    /**
     * @return int
     */
    public function getClicks(): int
    {
        return $this->clicks;
    }

    /**
     * @param int $clicks
     */
    public function setClicks(int $clicks)
    {
        $this->clicks = $clicks;
    }

    /**
     * @return int
     */
    public function getEvents(): int
    {
        return $this->events;
    }

    /**
     * @param int $events
     */
    public function setEvents(int $events)
    {
        $this->events = $events;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice(float $price)
    {
        $this->price = $price;
    }
}

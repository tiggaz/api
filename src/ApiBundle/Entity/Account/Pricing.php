<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Entity\Account;

use Doctrine\ORM\Mapping as ORM;
use Itsup\Bundle\ApiBundle\Entity\AbstractEntity;
use Itsup\Bundle\ApiBundle\Entity\Account;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(
 *     name="account_pricing",
 *     indexes={@ORM\Index(name="date_idx", columns={"from", "to"})}
 * )
 */
class Pricing extends AbstractEntity
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     * @JMS\Groups({"list", "details"})
     */
    protected $id = 0;

    /**
     * @Assert\NotNull(message="Account not found")
     *
     * @var Account
     * @ORM\ManyToOne(targetEntity="Itsup\Bundle\ApiBundle\Entity\Account", inversedBy="pricing")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $account = null;

    /**
     * @var \DateTime
     * @ORM\Column(type="itsupdate", name="`from`")
     * @JMS\Groups({"list", "details"})
     */
    protected $from;

    /**
     * @var \DateTime
     * @ORM\Column(type="itsupdate", name="`to`")
     * @JMS\Groups({"list", "details"})
     */
    protected $to;

    /**
     * @var float
     * @ORM\Column(type="float", precision=8, scale=4)
     * @JMS\Groups({"list", "details"})
     */
    protected $cpm = 0;

    /**
     * User constructor.
     */
    public function __construct()
    {
        $this->from = date('Y-m-d');
        $this->to   = date('Y-m-d');
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return Account
     */
    public function getAccount() // TODO PHP7.1 : ?Account
    {
        return $this->account;
    }

    /**
     * @param Account $account
     */
    public function setAccount($account) // TODO PHP7.1 ?Account $account
    {
        $this->account = $account;
    }

    /**
     * @return string
     */
    public function getFrom(): string
    {
        return $this->from;
    }

    /**
     * @param string $from
     */
    public function setFrom(string $from)
    {
        $this->from = $from;
    }

    /**
     * @return string
     */
    public function getTo(): string
    {
        return $this->to;
    }

    /**
     * @param string $to
     */
    public function setTo(string $to)
    {
        $this->to = $to;
    }

    /**
     * @return float
     */
    public function getCpm(): float
    {
        return $this->cpm;
    }

    /**
     * @param float $cpm
     */
    public function setCpm(float $cpm)
    {
        $this->cpm = $cpm;
    }
}

<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Entity\Account;

use Doctrine\ORM\Mapping as ORM;
use Itsup\Bundle\ApiBundle\Entity\AbstractEntity;
use Itsup\Bundle\ApiBundle\Entity\Account;
use JMS\Serializer\Annotation as JMS;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(
 *     name="account_external_statistics_provider",
 *     indexes={@ORM\Index(name="name_idx", columns={"name"})}
 * )
 * @UniqueEntity(
 *     fields={"account", "name"},
 *     errorPath="name",
 *     message="This External Statistics Provider is already set."
 * )
 */
class ExternalStatisticsProvider extends AbstractEntity
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     * @JMS\Groups({"list", "details"})
     */
    protected $id = 0;

    /**
     * @Assert\NotNull(message="Account not found")
     *
     * @var Account
     * @ORM\ManyToOne(targetEntity="Itsup\Bundle\ApiBundle\Entity\Account", inversedBy="externalStatisticsProviders")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $account = null;

    /**
     * @Assert\NotBlank(message="External Company can not be empty")
     *
     * @var string
     * @ORM\Column(type="string", columnDefinition="ENUM('trafficjunky','exoclick','trafficforce','reporo','adnium')")
     * @JMS\Groups({"list", "details"})
     */
    protected $name = '';

    /**
     * @Assert\NotBlank(message="API Key/Token can not be empty")
     *
     * @var string
     * @ORM\Column(type="string")
     * @JMS\Groups({"list", "details"})
     */
    protected $token = '';

    /**
     * @var string
     * @ORM\Column(type="string")
     * @JMS\Groups({"list", "details"})
     */
    protected $username = '';

    /**
     * @var string
     * @ORM\Column(type="string")
     * @JMS\Groups({"list", "details"})
     */
    protected $password = '';

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     * @JMS\Groups({"list", "details"})
     */
    protected $valid = true;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return Account
     */
    public function getAccount() // TODO PHP7.1 : ?Account
    {
        return $this->account;
    }

    /**
     * @param Account $account
     */
    public function setAccount($account) // TODO PHP7.1 ?Account $account
    {
        $this->account = $account;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }

    /**
     * @param string $token
     */
    public function setToken(string $token)
    {
        $this->token = $token;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @param string $username
     */
    public function setUsername(string $username)
    {
        $this->username = $username;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword(string $password)
    {
        $this->password = $password;
    }

    /**
     * @return bool
     */
    public function isValid(): bool
    {
        return $this->valid;
    }

    /**
     * @param bool $valid
     */
    public function setValid(bool $valid)
    {
        $this->valid = $valid;
    }
}

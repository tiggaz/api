<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(
 *     name="contact",
 *     indexes={
 *          @ORM\Index(name="name_email_company_idx", columns={"name", "email", "company"}),
 *          @ORM\Index(name="type_idx", columns={"publisher", "broker", "advertiser"})
 *     }
 * )
 */
class Contact extends AbstractEntity
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     * @JMS\Groups({"list", "details"})
     */
    protected $id = 0;

    /**
     * @Assert\NotNull(message="Account not found")
     *
     * @var Account
     * @ORM\ManyToOne(targetEntity="Itsup\Bundle\ApiBundle\Entity\Account")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $account = null;

    /**
     * @Assert\NotNull(message="User not found")
     *
     * @var User
     * @ORM\ManyToOne(targetEntity="Itsup\Bundle\ApiBundle\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     * @JMS\Groups({"list", "details"})
     */
    protected $user = null;

    /**
     * @Assert\NotBlank(message="Name can not be empty")
     *
     * @var string
     * @ORM\Column(type="string", length=100)
     * @JMS\Groups({"list", "details"})
     */
    protected $name = '';

    /**
     * @Assert\Email(message="Invalid email")
     * @Assert\NotBlank(message="Email can not be empty")
     *
     * @var string
     * @ORM\Column(type="string", length=100)
     * @JMS\Groups({"list", "details"})
     */
    protected $email = '';

    /**
     * @var string
     * @ORM\Column(type="string", length=50)
     * @JMS\Groups({"list", "details"})
     */
    protected $skype = '';

    /**
     * @var string
     * @ORM\Column(type="string", length=15)
     * @JMS\Groups({"list", "details"})
     */
    protected $phone = '';

    /**
     * @Assert\NotBlank(message="Company can not be empty")
     *
     * @var string
     * @ORM\Column(type="string", length=100)
     * @JMS\Groups({"list", "details"})
     */
    protected $company = '';

    /**
     * @var string
     * @ORM\Column(type="string", length=2)
     * @JMS\Groups({"list", "details"})
     */
    protected $country = '';

    /**
     * @Assert\NotBlank(message="Website can not be empty")
     *
     * @var string
     * @ORM\Column(type="string")
     * @JMS\Groups({"list", "details"})
     */
    protected $websiteUrl = '';

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     * @JMS\Groups({"list", "details"})
     */
    protected $publisher = false;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     * @JMS\Groups({"list", "details"})
     */
    protected $broker = false;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     * @JMS\Groups({"list", "details"})
     */
    protected $advertiser = false;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return Account
     */
    public function getAccount() // TODO PHP7.1 : ?Account
    {
        return $this->account;
    }

    /**
     * @param Account $account
     */
    public function setAccount(Account $account) // TODO PHP7.1 ?Account $account
    {
        $this->account = $account;
    }

    /**
     * @return User
     */
    public function getUser() // TODO PHP7.1 : ?User
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser($user) // TODO PHP7.1 ?User $user
    {
        $this->user = $user;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getSkype(): string
    {
        return $this->skype;
    }

    /**
     * @param string $skype
     */
    public function setSkype(string $skype)
    {
        $this->skype = $skype;
    }

    /**
     * @return string
     */
    public function getPhone(): string
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone(string $phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return string
     */
    public function getCompany(): string
    {
        return $this->company;
    }

    /**
     * @param string $company
     */
    public function setCompany(string $company)
    {
        $this->company = $company;
    }

    /**
     * @return string
     */
    public function getCountry(): string
    {
        return $this->country;
    }

    /**
     * @param string $country
     */
    public function setCountry(string $country)
    {
        $this->country = $country;
    }

    /**
     * @return string
     */
    public function getWebsiteUrl(): string
    {
        return $this->websiteUrl;
    }

    /**
     * @param string $websiteUrl
     */
    public function setWebsiteUrl(string $websiteUrl)
    {
        $this->websiteUrl = $websiteUrl;
    }

    /**
     * @return bool
     */
    public function isPublisher(): bool
    {
        return $this->publisher;
    }

    /**
     * @param bool $publisher
     */
    public function setPublisher(bool $publisher)
    {
        $this->publisher = $publisher;
    }

    /**
     * @return bool
     */
    public function isBroker(): bool
    {
        return $this->broker;
    }

    /**
     * @param bool $broker
     */
    public function setBroker(bool $broker)
    {
        $this->broker = $broker;
    }

    /**
     * @return bool
     */
    public function isAdvertiser(): bool
    {
        return $this->advertiser;
    }

    /**
     * @param bool $advertiser
     */
    public function setAdvertiser(bool $advertiser)
    {
        $this->advertiser = $advertiser;
    }
}

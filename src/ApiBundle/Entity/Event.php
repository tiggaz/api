<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(
 *     name="event",
 *     indexes={@ORM\Index(name="type_idx", columns={"type"})}
 * )
 */
class Event extends AbstractEntity
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     * @JMS\Groups({"list", "details"})
     */
    protected $id = 0;

    /**
     * @Assert\NotNull(message="Account not found")
     *
     * @var Account
     * @ORM\ManyToOne(targetEntity="Itsup\Bundle\ApiBundle\Entity\Account")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $account = null;

    /**
     * @Assert\NotBlank(message="Name can not be empty")
     *
     * @var string
     * @ORM\Column(type="string", length=120)
     * @JMS\Groups({"list", "details"})
     */
    protected $name = '';

    /**
     * @Assert\NotBlank(message="Type can not be empty")
     *
     * @var string
     * @ORM\Column(type="string", columnDefinition="ENUM('count','conversion','transaction')")
     * @JMS\Groups({"list", "details"})
     */
    protected $type = '';

    /**
     * @var int
     * @ORM\Column(type="float", precision=6, scale=2)
     * @JMS\Groups({"list", "details"})
     */
    protected $value = 0;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return Account
     */
    public function getAccount() // TODO PHP7.1 : ?Account
    {
        return $this->account;
    }

    /**
     * @param Account $account
     */
    public function setAccount($account) // TODO PHP7.1 ?Account $account
    {
        $this->account = $account;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type)
    {
        $this->type = $type;
    }

    /**
     * @return int
     */
    public function getValue(): int
    {
        return $this->value;
    }

    /**
     * @param int $value
     */
    public function setValue(int $value)
    {
        $this->value = $value;
    }
}

<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Itsup\Bundle\ApiBundle\Entity\Campaign\CampaignCreative;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(
 *     name="campaign",
 *     indexes={@ORM\Index(name="type_revenue_status_idx", columns={"type", "revenue", "status"})}
 * )
 */
class Campaign extends AbstractFollowerEntity
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     * @JMS\Groups({"list", "details"})
     */
    protected $id = 0;

    /**
     * @Assert\NotNull(message="Account not found")
     *
     * @var Account
     * @ORM\ManyToOne(targetEntity="Itsup\Bundle\ApiBundle\Entity\Account")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $account = null;

    /**
     * @var Offer
     * @ORM\ManyToOne(targetEntity="Itsup\Bundle\ApiBundle\Entity\Offer")
     * @ORM\JoinColumn(nullable=true)
     * @JMS\Groups({"list", "details"})
     */
    protected $offer = null;

    /**
     * @Assert\NotBlank(message="Name can not be empty")
     *
     * @var string
     * @ORM\Column(type="string", length=150)
     * @JMS\Groups({"list", "details"})
     */
    protected $name = '';

    /**
     * @var string
     * @ORM\Column(type="string", columnDefinition="ENUM('conversion', 'sale')")
     * @JMS\Groups({"list", "details"})
     */
    protected $revenue = '';

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     * @JMS\Groups({"list", "details"})
     */
    protected $keepAlive = false;

    /**
     * @var Campaign
     * @ORM\ManyToOne(targetEntity="Itsup\Bundle\ApiBundle\Entity\Campaign")
     * @JMS\Groups({"list", "details"})
     */
    protected $fallbackCampaign = null;

    /**
     * @var Event
     * @ORM\ManyToOne(targetEntity="Itsup\Bundle\ApiBundle\Entity\Event")
     * @ORM\JoinColumn(nullable=true)
     * @JMS\Groups({"list", "details"})
     */
    protected $event = null;

    /**
     * @var string
     * @ORM\Column(type="string", columnDefinition="ENUM('iframe','direct link','text','image','popunder')")
     * @JMS\Groups({"list", "details"})
     */
    protected $type = '';

    /**
     * @var int
     * @ORM\Column(type="integer", length=4, options={"unsigned"=true})
     * @JMS\Groups({"list", "details"})
     */
    protected $width = 0;

    /**
     * @var int
     * @ORM\Column(type="integer", length=4, options={"unsigned"=true})
     * @JMS\Groups({"list", "details"})
     */
    protected $height = 0;

    /**
     * @Assert\NotBlank(message="URL can not be empty")
     *
     * @var string
     * @ORM\Column(type="text")
     * @JMS\Groups({"list", "details"})
     */
    protected $url = '';

    /**
     * @var string
     * @ORM\Column(type="string", columnDefinition="ENUM('on','off')")
     * @JMS\Groups({"list", "details"})
     */
    protected $status = '';

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     * @JMS\Groups({"list", "details"})
     */
    protected $dateCreated;

    /**
     * @Assert\NotNull(message="createdBy must be set")
     *
     * @var User
     * @ORM\ManyToOne(targetEntity="Itsup\Bundle\ApiBundle\Entity\User")
     * @JMS\Groups({"details"})
     */
    protected $createdBy = null;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     * @JMS\Groups({"details"})
     */
    protected $dateUpdated;

    /**
     * @Assert\NotNull(message="updatedBy must be set")
     *
     * @var User
     * @ORM\ManyToOne(targetEntity="Itsup\Bundle\ApiBundle\Entity\User")
     * @JMS\Groups({"details"})
     */
    protected $updatedBy = null;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="Itsup\Bundle\ApiBundle\Entity\Metrics\Browser")
     * @ORM\OrderBy({"name"="ASC"})
     * @JMS\Groups({"details"})
     */
    protected $browsers;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="Itsup\Bundle\ApiBundle\Entity\Metrics\Country")
     * @ORM\OrderBy({"name"="ASC"})
     * @JMS\Groups({"details"})
     */
    protected $countries;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="Itsup\Bundle\ApiBundle\Entity\Metrics\Device")
     * @ORM\OrderBy({"name"="ASC"})
     * @JMS\Groups({"details"})
     */
    protected $devices;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="Itsup\Bundle\ApiBundle\Entity\Metrics\InternetServiceProvider")
     * @ORM\OrderBy({"name"="ASC"})
     * @JMS\Groups({"details"})
     */
    protected $internetServiceProviders;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="Itsup\Bundle\ApiBundle\Entity\Metrics\Language")
     * @ORM\OrderBy({"name"="ASC"})
     * @JMS\Groups({"details"})
     */
    protected $languages;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="Itsup\Bundle\ApiBundle\Entity\Metrics\OperatingSystem")
     * @ORM\OrderBy({"name"="ASC"})
     * @JMS\Groups({"details"})
     */
    protected $operatingSystems;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(
     *     targetEntity="Itsup\Bundle\ApiBundle\Entity\Campaign\CampaignCreative",
     *     cascade={"all"},
     *     mappedBy="campaign"
     * )
     * @ORM\OrderBy({"creative"="ASC"})
     * @JMS\Groups({"details"})
     */
    protected $creatives;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(
     *     targetEntity="Itsup\Bundle\ApiBundle\Entity\Campaign\Sale",
     *     mappedBy="campaign",
     *     cascade={"remove"}
     * )
     * @ORM\OrderBy({"from"="ASC"})
     * @JMS\Groups({"details"})
     */
    protected $sales;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(
     *     targetEntity="Itsup\Bundle\ApiBundle\Entity\Campaign\Limit",
     *     mappedBy="campaign",
     *     cascade={"remove"}
     * )
     * @ORM\OrderBy({"from"="ASC"})
     * @JMS\Groups({"details"})
     */
    protected $limits;

    /**
     * Campaign constructor.
     */
    public function __construct()
    {
        $this->tags                     = new ArrayCollection();
        $this->followers                = new ArrayCollection();
        $this->browsers                 = new ArrayCollection();
        $this->countries                = new ArrayCollection();
        $this->devices                  = new ArrayCollection();
        $this->internetServiceProviders = new ArrayCollection();
        $this->languages                = new ArrayCollection();
        $this->operatingSystems         = new ArrayCollection();
        $this->creatives                = new ArrayCollection();
        $this->limits                   = new ArrayCollection();
        $this->sales                    = new ArrayCollection();
        $this->notes                    = new ArrayCollection();
        $this->dateCreated              = new \DateTime();
        $this->dateUpdated              = new \DateTime();
    }

    public function __clone()
    {
        $this->name .= ' - DUPLICATE';
        $this->id          = null;
        $this->dateCreated = new \DateTime();
        $this->dateUpdated = new \DateTime();
        $this->followers   = new ArrayCollection();
        $this->creatives   = new ArrayCollection();
        $this->limits      = new ArrayCollection();
        $this->sales       = new ArrayCollection();
        $this->notes       = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return Account
     */
    public function getAccount() // TODO PHP7.1 : ?Account
    {
        return $this->account;
    }

    /**
     * @param Account $account
     */
    public function setAccount($account) // TODO PHP7.1 ?Account $account
    {
        $this->account = $account;
    }

    /**
     * @return Offer
     */
    public function getOffer() // TODO PHP7.1 : ?Offer
    {
        return $this->offer;
    }

    /**
     * @param Offer $offer
     */
    public function setOffer($offer)
    {
        $this->offer = $offer;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getRevenue(): string
    {
        return $this->revenue;
    }

    /**
     * @param string $revenue
     */
    public function setRevenue(string $revenue)
    {
        $this->revenue = $revenue;
    }

    /**
     * @return bool
     */
    public function isKeepAlive(): bool
    {
        return $this->keepAlive;
    }

    /**
     * @param bool $keepAlive
     */
    public function setKeepAlive(bool $keepAlive)
    {
        $this->keepAlive = $keepAlive;
    }

    /**
     * @return Campaign
     */
    public function getFallbackCampaign() // TODO PHP7.1 : ?Campaign
    {
        return $this->fallbackCampaign;
    }

    /**
     * @param Campaign $fallbackCampaign
     */
    public function setFallbackCampaign(Campaign $fallbackCampaign)
    {
        $this->fallbackCampaign = $fallbackCampaign;
    }

    /**
     * @return Event
     */
    public function getEvent()
    {
        return $this->event;
    }

    /**
     * @param Event $event
     */
    public function setEvent($event)
    {
        $this->event = $event;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type)
    {
        $this->type = $type;
    }

    /**
     * @return int
     */
    public function getWidth(): int
    {
        return $this->width;
    }

    /**
     * @param int $width
     */
    public function setWidth(int $width)
    {
        $this->width = $width;
    }

    /**
     * @return int
     */
    public function getHeight(): int
    {
        return $this->height;
    }

    /**
     * @param int $height
     */
    public function setHeight(int $height)
    {
        $this->height = $height;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl(string $url)
    {
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus(string $status)
    {
        $this->status = $status;
    }

    /**
     * @return \DateTime
     */
    public function getDateCreated(): \DateTime
    {
        return $this->dateCreated;
    }

    /**
     * @param \DateTime $dateCreated
     */
    public function setDateCreated(\DateTime $dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return User
     */
    public function getCreatedBy() // TODO PHP7.1 : ?User
    {
        return $this->createdBy;
    }

    /**
     * @param User $createdBy
     */
    public function setCreatedBy(User $createdBy)
    {
        $this->createdBy = $createdBy;
    }

    /**
     * @return \DateTime
     */
    public function getDateUpdated(): \DateTime
    {
        return $this->dateUpdated;
    }

    /**
     * @param \DateTime $dateUpdated
     */
    public function setDateUpdated(\DateTime $dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
    }

    /**
     * @return User
     */
    public function getUpdatedBy() // TODO PHP7.1 : ?User
    {
        return $this->updatedBy;
    }

    /**
     * @param User $updatedBy
     */
    public function setUpdatedBy(User $updatedBy)
    {
        $this->updatedBy = $updatedBy;
    }

    /**
     * @return ArrayCollection
     */
    public function getBrowsers(): ArrayCollection
    {
        return $this->browsers;
    }

    /**
     * @param ArrayCollection $browsers
     */
    public function setBrowsers(ArrayCollection $browsers)
    {
        $this->browsers = $browsers;
    }

    /**
     * @return ArrayCollection
     */
    public function getCountries(): ArrayCollection
    {
        return $this->countries;
    }

    /**
     * @param ArrayCollection $countries
     */
    public function setCountries(ArrayCollection $countries)
    {
        $this->countries = $countries;
    }

    /**
     * @return ArrayCollection
     */
    public function getDevices(): ArrayCollection
    {
        return $this->devices;
    }

    /**
     * @param ArrayCollection $devices
     */
    public function setDevices(ArrayCollection $devices)
    {
        $this->devices = $devices;
    }

    /**
     * @return ArrayCollection
     */
    public function getInternetServiceProviders(): ArrayCollection
    {
        return $this->internetServiceProviders;
    }

    /**
     * @param ArrayCollection $internetServiceProviders
     */
    public function setInternetServiceProviders(ArrayCollection $internetServiceProviders)
    {
        $this->internetServiceProviders = $internetServiceProviders;
    }

    /**
     * @return ArrayCollection
     */
    public function getLanguages(): ArrayCollection
    {
        return $this->languages;
    }

    /**
     * @param ArrayCollection $languages
     */
    public function setLanguages(ArrayCollection $languages)
    {
        $this->languages = $languages;
    }

    /**
     * @return ArrayCollection
     */
    public function getOperatingSystems(): ArrayCollection
    {
        return $this->operatingSystems;
    }

    /**
     * @param ArrayCollection $operatingSystems
     */
    public function setOperatingSystems(ArrayCollection $operatingSystems)
    {
        $this->operatingSystems = $operatingSystems;
    }

    /**
     * @return ArrayCollection
     */
    public function getCreatives()
    {
        return $this->creatives;
    }

    /**
     * @param ArrayCollection $creatives
     */
    public function setCreatives(ArrayCollection $creatives)
    {
        $this->creatives = $creatives;
    }

    /**
     * @param CampaignCreative $creative
     */
    public function addCreative(CampaignCreative $creative)
    {
        $index = $this->creatives->indexOf($creative);
        if ($index !== false) {
            $this->creatives->set($index, $creative);
        } else {
            $creative->setCampaign($this);
            $this->creatives->add($creative);
        }
    }

    /**
     * @param CampaignCreative $creative
     */
    public function removeCreative(CampaignCreative $creative)
    {
        $this->creatives->removeElement($creative);
    }

    /**
     * @return ArrayCollection
     */
    public function getLimits()
    {
        return $this->limits;
    }

    /**
     * @param ArrayCollection $limits
     */
    public function setLimits(ArrayCollection $limits)
    {
        $this->limits = $limits;
    }

    /**
     * @return ArrayCollection
     */
    public function getSales(): ArrayCollection
    {
        return $this->sales;
    }

    /**
     * @param ArrayCollection $sales
     */
    public function setSales(ArrayCollection $sales)
    {
        $this->sales = $sales;
    }

    /**
     * @return ArrayCollection
     */
    public function getNotes(): ArrayCollection
    {
        return $this->notes;
    }

    /**
     * @param ArrayCollection $notes
     */
    public function setNotes(ArrayCollection $notes)
    {
        $this->notes = $notes;
    }
}

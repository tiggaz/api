<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Entity\Campaign;

use Doctrine\ORM\Mapping as ORM;
use Itsup\Bundle\ApiBundle\Entity\AbstractEntity;
use Itsup\Bundle\ApiBundle\Entity\Campaign;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(
 *     name="campaign_limit",
 *     indexes={@ORM\Index(name="date_idx", columns={"from", "to"})},
 *     indexes={@ORM\Index(name="date_reached_idx", columns={"date_reached"})}
 * )
 */
class Limit extends AbstractEntity
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     * @JMS\Groups({"list", "details"})
     */
    protected $id = 0;

    /**
     * @Assert\NotNull(message="Campaign ca not be empty")
     *
     * @var Campaign
     * @ORM\ManyToOne(targetEntity="Itsup\Bundle\ApiBundle\Entity\Campaign", inversedBy="limits")
     * @ORM\JoinColumn(nullable=false)
     * @JMS\Groups({"list", "details"})
     */
    protected $campaign = null;

    /**
     * @var \DateTime
     * @ORM\Column(type="itsupdate", name="`from`", nullable=true)
     * @JMS\Groups({"list", "details"})
     */
    protected $from = null;

    /**
     * @var \DateTime
     * @ORM\Column(type="itsupdate", name="`to`", nullable=true)
     * @JMS\Groups({"list", "details"})
     */
    protected $to = null;

    /**
     * @var int
     * @ORM\Column(type="integer", options={"unsigned"=true})
     * @JMS\Groups({"list", "details"})
     */
    protected $impressions = 0;

    /**
     * @var int
     * @ORM\Column(type="integer", options={"unsigned"=true})
     * @JMS\Groups({"list", "details"})
     */
    protected $clicks = 0;

    /**
     * @var \DateTime
     * @ORM\Column(type="itsupdate", nullable=true)
     * @JMS\Groups({"list", "details"})
     */
    protected $dateReached = null;

    /**
     * @var string
     * @ORM\Column(type="string", columnDefinition="ENUM('impressions','clicks')", nullable=true)
     * @JMS\Groups({"list", "details"})
     */
    protected $typeReached = null;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return Campaign
     */
    public function getCampaign(): ?Campaign
    {
        return $this->campaign;
    }

    /**
     * @param Campaign $campaign
     */
    public function setCampaign(Campaign $campaign)
    {
        $this->campaign = $campaign;
    }

    /**
     * @return string
     */
    public function getFrom(): ?string
    {
        return $this->from;
    }

    /**
     * @param string $from
     */
    public function setFrom(string $from)
    {
        $this->from = $from;
    }

    /**
     * @return string
     */
    public function getTo(): ?string
    {
        return $this->to;
    }

    /**
     * @param string $to
     */
    public function setTo(string $to)
    {
        $this->to = $to;
    }

    /**
     * @return int
     */
    public function getImpressions(): int
    {
        return $this->impressions;
    }

    /**
     * @param int $impressions
     */
    public function setImpressions(int $impressions)
    {
        $this->impressions = $impressions;
    }

    /**
     * @return int
     */
    public function getClicks(): int
    {
        return $this->clicks;
    }

    /**
     * @param int $clicks
     */
    public function setClicks(int $clicks)
    {
        $this->clicks = $clicks;
    }

    /**
     * @return string
     */
    public function getDateReached(): ?string
    {
        return $this->dateReached;
    }

    /**
     * @param string $dateReached
     */
    public function setDateReached(string $dateReached)
    {
        $this->dateReached = $dateReached;
    }

    /**
     * @return string
     */
    public function getTypeReached(): ?string
    {
        return $this->typeReached;
    }

    /**
     * @param string $typeReached
     */
    public function setTypeReached(string $typeReached)
    {
        $this->typeReached = $typeReached;
    }
}

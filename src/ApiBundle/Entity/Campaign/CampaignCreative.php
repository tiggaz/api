<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Entity\Campaign;

use Doctrine\ORM\Mapping as ORM;
use Itsup\Bundle\ApiBundle\Entity\AbstractEntity;
use Itsup\Bundle\ApiBundle\Entity\Campaign;
use Itsup\Bundle\ApiBundle\Entity\Creative;
use JMS\Serializer\Annotation as JMS;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(
 *     name="campaign_creative",
 *     indexes={@ORM\Index(name="status_idx", columns={"status"})}
 * )
 * @UniqueEntity(
 *     fields={"campaign", "creative"},
 *     errorPath="creative",
 *     message="This Creative has already been added to this Campaign."
 * )
 */
class CampaignCreative extends AbstractEntity
{
    /**
     * @Assert\NotNull(message="Campaign can not be empty")
     *
     * @var Campaign
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Itsup\Bundle\ApiBundle\Entity\Campaign", inversedBy="creatives")
     * @ORM\JoinColumn(nullable=false)
     * @JMS\Groups({"list", "details"})
     */
    protected $campaign = null;

    /**
     * @Assert\NotNull(message="Creative can not be empty")
     *
     * @var Creative
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Itsup\Bundle\ApiBundle\Entity\Creative")
     * @ORM\JoinColumn(nullable=false)
     * @JMS\Groups({"list", "details"})
     */
    protected $creative = null;

    /**
     * @var int
     * @ORM\Column(type="integer", length=3, options={"unsigned"=true})
     * @JMS\Groups({"list", "details"})
     */
    protected $weight = 0;

    /**
     * @Assert\NotBlank(message="Status can not be empty")
     *
     * @var string
     * @ORM\Column(type="string", columnDefinition="ENUM('on','off')")
     * @JMS\Groups({"list", "details"})
     */
    protected $status = '';

    /**
     * @var string
     * @ORM\Column(type="string")
     * @JMS\Groups({"list", "details"})
     */
    protected $url = '';

    public function __clone()
    {
        $this->campaign = null;
    }

    /**
     * @return Campaign
     */
    public function getCampaign() // TODO PHP7.1 : ?Campaign
    {
        return $this->campaign;
    }

    /**
     * @param Campaign $campaign
     */
    public function setCampaign($campaign) // TODO PHP7.1 ?Campaign $campaign
    {
        $this->campaign = $campaign;
    }

    /**
     * @return Creative
     */
    public function getCreative() // TODO PHP7.1 : ?Creative
    {
        return $this->creative;
    }

    /**
     * @param Creative $creative
     */
    public function setCreative($creative) // TODO PHP7.1 ?Creative $creative
    {
        $this->creative = $creative;
    }

    /**
     * @return int
     */
    public function getWeight(): int
    {
        return $this->weight;
    }

    /**
     * @param int $weight
     */
    public function setWeight(int $weight)
    {
        $this->weight = $weight;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus(string $status)
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl(string $url)
    {
        $this->url = $url;
    }
}

<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Entity\Campaign;

use Doctrine\ORM\Mapping as ORM;
use Itsup\Bundle\ApiBundle\Entity\AbstractEntity;
use Itsup\Bundle\ApiBundle\Entity\Campaign;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(
 *     name="campaign_sale",
 *     indexes={@ORM\Index(name="date_expire_idx", columns={"from", "to", "expire"})}
 * )
 */
class Sale extends AbstractEntity
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     * @JMS\Groups({"list", "details"})
     */
    protected $id = 0;

    /**
     * @Assert\NotNull(message="Campaign can not be empty")
     *
     * @var Campaign
     * @ORM\ManyToOne(targetEntity="Itsup\Bundle\ApiBundle\Entity\Campaign", inversedBy="sales")
     * @ORM\JoinColumn(nullable=false)
     * @JMS\Groups({"list", "details"})
     */
    protected $campaign = null;

    /**
     * @var Limit
     * @ORM\ManyToOne(targetEntity="Itsup\Bundle\ApiBundle\Entity\Campaign\Limit", cascade={"all"})
     * @JMS\Groups({"list", "details"})
     */
    protected $limit = null;

    /**
     * @Assert\NotBlank(message="Type can not be empty")
     *
     * @var string
     * @ORM\Column(type="string", columnDefinition="ENUM('flat','cpm imps','cpc','cpm clicks')")
     * @JMS\Groups({"list", "details"})
     */
    protected $type = '';

    /**
     * @var \DateTime
     * @ORM\Column(type="itsupdate", name="`from`")
     * @JMS\Groups({"list", "details"})
     */
    protected $from;

    /**
     * @var \DateTime
     * @ORM\Column(type="itsupdate", name="`to`")
     * @JMS\Groups({"list", "details"})
     */
    protected $to;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     * @JMS\Groups({"list", "details"})
     */
    protected $expire = false;

    /**
     * @var float
     * @ORM\Column(type="float", precision=11, scale=4)
     * @JMS\Groups({"list", "details"})
     */
    protected $price = 0;

    /**
     * Sale constructor.
     */
    public function __construct()
    {
        $this->from = date('Y-m-d');
        $this->to   = date('Y-m-d');
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return Campaign
     */
    public function getCampaign() // TODO PHP7.1 : ?Campaign
    {
        return $this->campaign;
    }

    /**
     * @param Campaign $campaign
     */
    public function setCampaign($campaign) // TODO PHP7.1 ?Campaign $campaign
    {
        $this->campaign = $campaign;
    }

    /**
     * @return Limit
     */
    public function getLimit() // TODO PHP7.1 : ?Limit
    {
        return $this->limit;
    }

    /**
     * @param Limit $limit
     */
    public function setLimit($limit) // TODO PHP7.1 ?Limit $limit
    {
        $this->limit = $limit;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getFrom(): string
    {
        return $this->from;
    }

    /**
     * @param string $from
     */
    public function setFrom(string $from)
    {
        $this->from = $from;
    }

    /**
     * @return string
     */
    public function getTo(): string
    {
        return $this->to;
    }

    /**
     * @param string $to
     */
    public function setTo(string $to)
    {
        $this->to = $to;
    }

    /**
     * @return bool
     */
    public function isExpire(): bool
    {
        return $this->expire;
    }

    /**
     * @param bool $expire
     */
    public function setExpire(bool $expire)
    {
        $this->expire = $expire;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice(float $price)
    {
        $this->price = $price;
    }
}

<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Itsup\Bundle\ApiBundle\Entity\Account\Billing;
use Itsup\Bundle\ApiBundle\Entity\Account\Domain;
use Itsup\Bundle\ApiBundle\Entity\Account\ExternalStatisticsProvider;
use Itsup\Bundle\ApiBundle\Entity\Account\Pricing;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="account")
 */
class Account extends AbstractEntity
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     * @JMS\Groups({"list", "details"})
     */
    protected $id = 0;

    /**
     * @Assert\NotBlank(message="Type can not be empty")
     *
     * @var string
     * @ORM\Column(type="string", columnDefinition="ENUM('business', 'individual')")
     * @JMS\Groups({"list", "details"})
     */
    protected $type = '';

    /**
     * @Assert\NotBlank(message="Name can not be empty")
     *
     * @var string
     * @ORM\Column(type="string", length=100)
     * @JMS\Groups({"list", "details"})
     */
    protected $name = '';

    /**
     * @Assert\NotBlank(message="Address can not be empty")
     *
     * @var string
     * @ORM\Column(type="string", length=100)
     * @JMS\Groups({"list", "details"})
     */
    protected $address = '';

    /**
     * @var string
     * @ORM\Column(type="string", length=100)
     * @JMS\Groups({"list", "details"})
     */
    protected $addressComplement = '';

    /**
     * @Assert\NotBlank(message="City can not be empty")
     *
     * @var string
     * @ORM\Column(type="string", length=100)
     * @JMS\Groups({"list", "details"})
     */
    protected $city = '';

    /**
     * @Assert\NotBlank(message="ZipCode can not be empty")
     *
     * @var string
     * @ORM\Column(type="string", length=10)
     * @JMS\Groups({"list", "details"})
     */
    protected $zipCode = '';

    /**
     * @var string
     * @ORM\Column(type="string", length=2)
     * @JMS\Groups({"list", "details"})
     */
    protected $state = '';

    /**
     * @Assert\NotBlank(message="Country can not be empty")
     *
     * @var string
     * @ORM\Column(type="string", length=2)
     * @JMS\Groups({"list", "details"})
     */
    protected $country = '';

    /**
     * @var string
     * @ORM\Column(type="string", length=100)
     * @JMS\Groups({"list", "details"})
     */
    protected $taxId = '';

    /**
     * @Assert\Url(message="Website Url {{ value }} is invalid")
     *
     * @var string
     * @ORM\Column(type="string")
     * @JMS\Groups({"list", "details"})
     */
    protected $websiteUrl = '';

    /**
     * @var string
     * @ORM\Column(type="string", columnDefinition="ENUM('sctr')")
     * @JMS\Groups({"list", "details"})
     */
    protected $invoiceFrom = 'sctr';

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     * @JMS\Groups({"list", "details"})
     */
    protected $enabled = true;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(
     *     targetEntity="Itsup\Bundle\ApiBundle\Entity\Account\Domain",
     *     mappedBy="account",
     *     cascade={"remove"}
     * )
     * @ORM\OrderBy({"name"="ASC"})
     * @JMS\Groups({"details"})
     */
    protected $domains;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(
     *     targetEntity="Itsup\Bundle\ApiBundle\Entity\Account\Pricing",
     *     mappedBy="account",
     *     cascade={"remove"}
     * )
     * @ORM\OrderBy({"from"="ASC"})
     * @JMS\Groups({"details"})
     */
    protected $pricing;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(
     *     targetEntity="Itsup\Bundle\ApiBundle\Entity\Account\Billing",
     *     mappedBy="account",
     *     cascade={"remove"}
     * )
     * @ORM\OrderBy({"date"="ASC"})
     * @JMS\Groups({"details"})
     */
    protected $billing;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(
     *     targetEntity="Itsup\Bundle\ApiBundle\Entity\Account\ExternalStatisticsProvider",
     *     mappedBy="account",
     *     cascade={"remove"}
     * )
     * @ORM\OrderBy({"name"="ASC"})
     * @JMS\Groups({"details"})
     */
    protected $externalStatisticsProviders;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(
     *     targetEntity="Itsup\Bundle\ApiBundle\Entity\User",
     *     mappedBy="account",
     *     cascade={"remove"}
     * )
     * @ORM\OrderBy({"id"="ASC"})
     * @JMS\Groups({"details"})
     */
    protected $users;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(
     *     targetEntity="Itsup\Bundle\ApiBundle\Entity\AdZone",
     *     mappedBy="account",
     *     cascade={"remove"}
     * )
     * @ORM\OrderBy({"id"="ASC"})
     * @JMS\Groups({"details"})
     */
    protected $adZones;

    /**
     * Account constructor.
     */
    public function __construct()
    {
        $this->domains                     = new ArrayCollection();
        $this->pricing                     = new ArrayCollection();
        $this->billing                     = new ArrayCollection();
        $this->externalStatisticsProviders = new ArrayCollection();
        $this->users                       = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getAddress(): string
    {
        return $this->address;
    }

    /**
     * @param string $address
     */
    public function setAddress(string $address)
    {
        $this->address = $address;
    }

    /**
     * @return string
     */
    public function getAddressComplement(): string
    {
        return $this->addressComplement;
    }

    /**
     * @param string $addressComplement
     */
    public function setAddressComplement(string $addressComplement)
    {
        $this->addressComplement = $addressComplement;
    }

    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity(string $city)
    {
        $this->city = $city;
    }

    /**
     * @return string
     */
    public function getZipCode(): string
    {
        return $this->zipCode;
    }

    /**
     * @param string $zipCode
     */
    public function setZipCode(string $zipCode)
    {
        $this->zipCode = $zipCode;
    }

    /**
     * @return string
     */
    public function getState(): string
    {
        return $this->state;
    }

    /**
     * @param string $state
     */
    public function setState(string $state)
    {
        $this->state = $state;
    }

    /**
     * @return string
     */
    public function getCountry(): string
    {
        return $this->country;
    }

    /**
     * @param string $country
     */
    public function setCountry(string $country)
    {
        $this->country = $country;
    }

    /**
     * @return string
     */
    public function getTaxId(): string
    {
        return $this->taxId;
    }

    /**
     * @param string $taxId
     */
    public function setTaxId(string $taxId)
    {
        $this->taxId = $taxId;
    }

    /**
     * @return string
     */
    public function getWebsiteUrl(): string
    {
        return $this->websiteUrl;
    }

    /**
     * @param string $websiteUrl
     */
    public function setWebsiteUrl(string $websiteUrl)
    {
        $this->websiteUrl = $websiteUrl;
    }

    /**
     * @return string
     */
    public function getInvoiceFrom(): string
    {
        return $this->invoiceFrom;
    }

    /**
     * @param string $invoiceFrom
     */
    public function setInvoiceFrom(string $invoiceFrom)
    {
        $this->invoiceFrom = $invoiceFrom;
    }

    /**
     * @return bool
     */
    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    /**
     * @param bool $enabled
     */
    public function setEnabled(bool $enabled)
    {
        $this->enabled = $enabled;
    }

    /**
     * @return ArrayCollection
     */
    public function getDomains(): ArrayCollection
    {
        return $this->domains;
    }

    /**
     * @param ArrayCollection $domains
     */
    public function setDomains(ArrayCollection $domains)
    {
        $this->domains = $domains;
    }

    /**
     * @param Domain $domain
     */
    public function addDomain(Domain $domain)
    {
        $domain->setAccount($this);
        $index = $this->domains->indexOf($domain);
        if ($index !== false) {
            $this->domains->set($index, $domain);
        } else {
            $this->domains->add($domain);
        }
    }

    /**
     * @param Domain $domain
     */
    public function removeDomain(Domain $domain)
    {
        $this->domains->removeElement($domain);
    }

    /**
     * @return ArrayCollection
     */
    public function getPricing(): ArrayCollection
    {
        return $this->pricing;
    }

    /**
     * @param ArrayCollection $pricing
     */
    public function setPricing(ArrayCollection $pricing)
    {
        $this->pricing = $pricing;
    }

    /**
     * @param Pricing $pricing
     */
    public function addPricing(Pricing $pricing)
    {
        if (empty($this->pricing)) {
            $this->pricing = new ArrayCollection();
        }
        $pricing->setAccount($this);
        $this->pricing->add($pricing);
    }

    /**
     * @return ArrayCollection
     */
    public function getBilling(): ArrayCollection
    {
        return $this->billing;
    }

    /**
     * @param ArrayCollection $billing
     */
    public function setBilling(ArrayCollection $billing)
    {
        $this->billing = $billing;
    }

    /**
     * @param Billing $billing
     */
    public function addBilling(Billing $billing)
    {
        if (empty($this->billing)) {
            $this->billing = new ArrayCollection();
        }
        $billing->setAccount($this);
        $this->billing->add($billing);
    }

    /**
     * @return ArrayCollection
     */
    public function getExternalStatisticsProviders(): ArrayCollection
    {
        return $this->externalStatisticsProviders;
    }

    /**
     * @param ArrayCollection $externalStatisticsProviders
     */
    public function setExternalStatisticsProviders(ArrayCollection $externalStatisticsProviders)
    {
        $this->externalStatisticsProviders = $externalStatisticsProviders;
    }

    /**
     * @param ExternalStatisticsProvider $provider
     */
    public function addExternalStatisticsProviders(ExternalStatisticsProvider $provider)
    {
        if (empty($this->externalStatisticsProviders)) {
            $this->externalStatisticsProviders = new ArrayCollection();
        }
        $provider->setAccount($this);
        $this->externalStatisticsProviders->add($provider);
    }

    /**
     * @return ArrayCollection
     */
    public function getUsers(): ArrayCollection
    {
        return $this->users;
    }

    /**
     * @param ArrayCollection $users
     */
    public function setUsers(ArrayCollection $users)
    {
        $this->users = $users;
    }
}

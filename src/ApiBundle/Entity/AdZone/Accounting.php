<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Entity\AdZone;

use Doctrine\ORM\Mapping as ORM;
use Itsup\Bundle\ApiBundle\Entity\AbstractEntity;
use Itsup\Bundle\ApiBundle\Entity\AdZone;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(
 *     name="ad_zone_accounting",
 *     indexes={@ORM\Index(name="date_idx", columns={"from", "to"})}
 * )
 */
class Accounting extends AbstractEntity
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     * @JMS\Groups({"list", "details"})
     */
    protected $id = 0;

    /**
     * @Assert\NotNull(message="AdZone can not be empty")
     *
     * @var AdZone
     * @ORM\ManyToOne(targetEntity="Itsup\Bundle\ApiBundle\Entity\AdZone", inversedBy="accounting")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $adZone = null;

    /**
     * @var \DateTime
     * @ORM\Column(type="itsupdate", name="`from`")
     * @JMS\Groups({"list", "details"})
     */
    protected $from;

    /**
     * @var \DateTime
     * @ORM\Column(type="itsupdate", name="`to`")
     * @JMS\Groups({"list", "details"})
     */
    protected $to;

    /**
     * @Assert\NotBlank(message="Type can not be empty")
     *
     * @var string
     * @ORM\Column(type="string", columnDefinition="ENUM('flat','cpc','cpm imps','cpm clicks')")
     * @JMS\Groups({"list", "details"})
     */
    protected $type = '';

    /**
     * @var float
     * @ORM\Column(type="float", precision=11, scale=4)
     * @JMS\Groups({"list", "details"})
     */
    protected $cost = 0;

    /**
     * Accounting constructor.
     */
    public function __construct()
    {
        $this->from = date('Y-m-d');
        $this->to   = date('Y-m-d');
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return AdZone
     */
    public function getAdZone() // TODO PHP7.1 : ?AdZone
    {
        return $this->adZone;
    }

    /**
     * @param AdZone $adZone
     */
    public function setAdZone($adZone) // TODO PHP7.1 ?AdZone $adZone
    {
        $this->adZone = $adZone;
    }

    /**
     * @return string
     */
    public function getFrom(): string
    {
        return $this->from;
    }

    /**
     * @param string $from
     */
    public function setFrom(string $from)
    {
        $this->from = $from;
    }

    /**
     * @return string
     */
    public function getTo(): string
    {
        return $this->to;
    }

    /**
     * @param string $to
     */
    public function setTo(string $to)
    {
        $this->to = $to;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type)
    {
        $this->type = $type;
    }

    /**
     * @return float
     */
    public function getCost(): float
    {
        return $this->cost;
    }

    /**
     * @param float $cost
     */
    public function setCost(float $cost)
    {
        $this->cost = $cost;
    }
}

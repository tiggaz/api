<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Entity\AdZone;

use Doctrine\ORM\Mapping as ORM;
use Itsup\Bundle\ApiBundle\Entity\AbstractEntity;
use Itsup\Bundle\ApiBundle\Entity\AdZone;
use Itsup\Bundle\ApiBundle\Entity\Campaign;
use JMS\Serializer\Annotation as JMS;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(
 *     name="ad_zone_campaign",
 *     indexes={@ORM\Index(name="status_idx", columns={"status"})}
 * )
 * @UniqueEntity(
 *     fields={"adZone", "campaign"},
 *     errorPath="campaign",
 *     message="This Campaign has already been added to this AdZone."
 * )
 */
class AdZoneCampaign extends AbstractEntity
{
    /**
     * @Assert\NotNull(message="AdZone can not be empty")
     *
     * @var AdZone
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Itsup\Bundle\ApiBundle\Entity\AdZone", inversedBy="campaigns")
     * @ORM\JoinColumn(nullable=false)
     * @JMS\Groups({"list", "details"})
     */
    protected $adZone = null;

    /**
     * @Assert\NotNull(message="Campaign can not be empty")
     *
     * @var Campaign
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Itsup\Bundle\ApiBundle\Entity\Campaign")
     * @ORM\JoinColumn(nullable=false)
     * @JMS\Groups({"list", "details"})
     */
    protected $campaign = null;

    /**
     * @var int
     * @ORM\Column(type="integer", length=3, options={"unsigned"=true})
     * @JMS\Groups({"list", "details"})
     */
    protected $weight = 0;

    /**
     * @Assert\NotBlank(message="Status can not be empty")
     *
     * @var string
     * @ORM\Column(type="string", columnDefinition="ENUM('active','paused','archived')")
     * @JMS\Groups({"list", "details"})
     */
    protected $status = '';

    /**
     * @var int
     * @ORM\Column(type="integer")
     * @JMS\Groups({"list", "details"})
     */
    protected $impressionsLimit = 0;

    public function __clone()
    {
        $this->adZone = null;
    }

    /**
     * @return AdZone
     */
    public function getAdZone() // TODO PHP7.1 : ?AdZone
    {
        return $this->adZone;
    }

    /**
     * @param AdZone $adZone
     */
    public function setAdZone($adZone) // TODO PHP7.1 ?AdZone $adZone
    {
        $this->adZone = $adZone;
    }

    /**
     * @return Campaign
     */
    public function getCampaign() // TODO PHP7.1 : ?Campaign
    {
        return $this->campaign;
    }

    /**
     * @param Campaign $campaign
     */
    public function setCampaign($campaign) // TODO PHP7.1 ?Campaign $campaign
    {
        $this->campaign = $campaign;
    }

    /**
     * @return int
     */
    public function getWeight(): int
    {
        return $this->weight;
    }

    /**
     * @param int $weight
     */
    public function setWeight(int $weight)
    {
        $this->weight = $weight;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus(string $status)
    {
        $this->status = $status;
    }

    /**
     * @return int
     */
    public function getImpressionsLimit(): int
    {
        return $this->impressionsLimit;
    }

    /**
     * @param int $impressionsLimit
     */
    public function setImpressionsLimit(int $impressionsLimit)
    {
        $this->impressionsLimit = $impressionsLimit;
    }
}

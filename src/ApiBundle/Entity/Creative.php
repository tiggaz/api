<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(
 *     name="creative",
 *     indexes={@ORM\Index(name="type_status_idx", columns={"type", "status"})}
 * )
 */
class Creative extends AbstractTagEntity
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     * @JMS\Groups({"list", "details"})
     */
    protected $id = 0;

    /**
     * @Assert\NotNull(message="Account not found")
     *
     * @var Account
     * @ORM\ManyToOne(targetEntity="Itsup\Bundle\ApiBundle\Entity\Account")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $account = null;

    /**
     * @Assert\NotBlank(message="Name can not be empty")
     *
     * @var string
     * @ORM\Column(type="string", length=150)
     * @JMS\Groups({"list", "details"})
     */
    protected $name = '';

    /**
     * @var string
     * @ORM\Column(type="string", columnDefinition="ENUM('iframe','image','text','popunder')")
     * @JMS\Groups({"list", "details"})
     */
    protected $type = '';

    /**
     * @Assert\NotBlank(message="Code can not be empty")
     *
     * @var string
     * @ORM\Column(type="text")
     * @JMS\Groups({"list", "details"})
     */
    protected $code = '';

    /**
     * @var int
     * @ORM\Column(type="integer", length=4, options={"unsigned"=true})
     * @JMS\Groups({"list", "details"})
     */
    protected $width = 0;

    /**
     * @var int
     * @ORM\Column(type="integer", length=4, options={"unsigned"=true})
     * @JMS\Groups({"list", "details"})
     */
    protected $height = 0;

    /**
     * @Assert\NotBlank(message="Status can not be empty")
     *
     * @var string
     * @ORM\Column(type="string", columnDefinition="ENUM('on','off')")
     * @JMS\Groups({"list", "details"})
     */
    protected $status = 'on';

    /**
     * @var int
     * @ORM\Column(type="integer", options={"unsigned"=true})
     * @JMS\Groups({"list", "details"})
     */
    protected $size = 0;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     * @JMS\Groups({"list", "details"})
     */
    protected $dateCreated = '';

    /**
     * Creative constructor.
     */
    public function __construct()
    {
        $this->tags        = new ArrayCollection();
        $this->dateCreated = new \DateTime();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return Account
     */
    public function getAccount() // TODO PHP7.1 : ?Account
    {
        return $this->account;
    }

    /**
     * @param Account $account
     */
    public function setAccount($account) // TODO PHP7.1 ?Account $account
    {
        $this->account = $account;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode(string $code)
    {
        $this->code = $code;
    }

    /**
     * @return int
     */
    public function getWidth(): int
    {
        return $this->width;
    }

    /**
     * @param int $width
     */
    public function setWidth(int $width)
    {
        $this->width = $width;
    }

    /**
     * @return int
     */
    public function getHeight(): int
    {
        return $this->height;
    }

    /**
     * @param int $height
     */
    public function setHeight(int $height)
    {
        $this->height = $height;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus(string $status)
    {
        $this->status = $status;
    }

    /**
     * @return int
     */
    public function getSize(): int
    {
        return $this->size;
    }

    /**
     * @param int $size
     */
    public function setSize(int $size)
    {
        $this->size = $size;
    }

    /**
     * @return \DateTime
     */
    public function getDateCreated(): \DateTime
    {
        return $this->dateCreated;
    }

    /**
     * @param \DateTime $dateCreated
     */
    public function setDateCreated(\DateTime $dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }
}

<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(
 *     name="note",
 *     indexes={
 *          @ORM\Index(name="date_idx", columns={"date"}),
 *          @ORM\Index(name="user_id_idx", columns={"user_id"})
 *     }
 * )
 */
class Note extends AbstractEntity
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     * @JMS\Groups({"list", "details"})
     */
    protected $id = 0;

    /**
     * @Assert\NotNull(message="User not found")
     *
     * @var User
     * @ORM\ManyToOne(targetEntity="Itsup\Bundle\ApiBundle\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     * @JMS\Groups({"list", "details"})
     */
    protected $user = null;

    /**
     * @Assert\NotBlank(message="Text can not be empty")
     *
     * @var string
     * @ORM\Column(type="text")
     * @JMS\Groups({"list", "details"})
     */
    protected $text = '';

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     * @JMS\Groups({"list", "details"})
     */
    protected $date = '';

    /**
     * Note constructor.
     */
    public function __construct()
    {
        $this->date = new \DateTime();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return User
     */
    public function getUser() // TODO PHP7.1 : ?User
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user) // TODO PHP7.1 ?User $user
    {
        $this->user = $user;
    }

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText(string $text)
    {
        $this->text = $text;
    }

    /**
     * @return \DateTime
     */
    public function getDate(): \DateTime
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     */
    public function setDate(\DateTime $date)
    {
        $this->date = $date;
    }
}

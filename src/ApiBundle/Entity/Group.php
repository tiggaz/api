<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="`group`")
 */
class Group extends AbstractEntity
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     * @JMS\Groups({"list", "details"})
     */
    protected $id = 0;

    /**
     * @Assert\NotNull(message="Account not found")
     *
     * @var Account
     * @ORM\ManyToOne(targetEntity="Itsup\Bundle\ApiBundle\Entity\Account")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $account = null;

    /**
     * @Assert\NotBlank(message="Name can not be empty")
     *
     * @var string
     * @ORM\Column(type="string", length=120)
     * @JMS\Groups({"list", "details"})
     */
    protected $name = 0;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="Itsup\Bundle\ApiBundle\Entity\AdZone")
     * @JMS\Groups({"details"})
     */
    protected $adZones;

    /**
     * Group constructor.
     */
    public function __construct()
    {
        $this->adZones = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return Account
     */
    public function getAccount() // TODO PHP7.1 : ?Account
    {
        return $this->account;
    }

    /**
     * @param Account $account
     */
    public function setAccount($account) // TODO PHP7.1 ?Account $account
    {
        $this->account = $account;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return ArrayCollection
     */
    public function getAdZones(): ArrayCollection
    {
        return $this->adZones;
    }

    /**
     * @param ArrayCollection $adZones
     */
    public function setAdZones(ArrayCollection $adZones)
    {
        $this->adZones = $adZones;
    }

    /**
     * @param AdZone $adZone
     */
    public function addAdZone(AdZone $adZone)
    {
        $this->adZones->add($adZone);
    }

    /**
     * @param AdZone $adZone
     */
    public function removeAdZone(AdZone $adZone)
    {
        $this->adZones->removeElement($adZone);
    }
}

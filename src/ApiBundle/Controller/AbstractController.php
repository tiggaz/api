<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Controller;

use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use Itsup\Bundle\ApiBundle\ApiError;
use Itsup\Bundle\ApiBundle\Entity\User;
use Itsup\Bundle\ApiBundle\Exception\ApiException;
use Symfony\Component\HttpFoundation\Request;

abstract class AbstractController extends FOSRestController
{
    /**
     * @param null  $data
     * @param int   $statusCode
     * @param array $headers
     * @param array $links
     *
     * @return View
     */
    protected function view($data = null, $statusCode = null, array $headers = [], array $links = []): View
    {
        return parent::view(['content' => $data, '_links' => $links], $statusCode, $headers);
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return parent::getUser();
    }

    /**
     * @param Request $request
     * @param string  $property
     *
     * @return mixed
     */
    public function getRequestProperty(Request $request, string $property)
    {
        $return = $request->query->get($property, $request->request->get($property));
        if (!empty($return)) {
            return $return;
        }
        $merged = array_merge($request->query->all(), $request->request->all());

        return self::searchForProperty($property, $merged);
    }

    /**
     * @param Request $request
     * @param string  $defaultContext
     *
     * @return string
     */
    public function getContext(Request &$request, string $defaultContext = ''): string
    {
        if ($request->query->has('context')) {
            $context = $request->query->get('context', $defaultContext);
            $request->query->remove('context');
        } else {
            $context = $request->request->get('context', $defaultContext);
            $request->request->remove('context');
        }

        return $context;
    }

    /**
     * @param string $context
     * @param View   $view
     *
     * @return View
     */
    public function setContext(string $context, View $view): View
    {
        if (!empty($context)) {
            $context = (new Context())->setGroups([$context]);
            $view->setContext($context);
        }

        return $view;
    }

    /**
     * @param int $accountId
     *
     * @throws ApiException
     */
    public function checkAccount(int $accountId)
    {
        if (!$this->isGranted('ROLE_INTERNAL') && !empty($accountId)) {
            $adminAccountId = $this->getUser()->getAccount()->getId();
            if ((int) $accountId !== (int) $adminAccountId) {
                throw new ApiException(ApiError::UNAUTHORIZED);
            }
        }
    }

    /**
     * @param int $userId
     *
     * @throws ApiException
     */
    public function checkUser(int $userId)
    {
        if (!$this->isGranted('ROLE_INTERNAL') && !$this->isGranted('ROLE_ADMIN') && !empty($userId)) {
            $adminUserId = $this->getUser()->getId();
            if ((int) $userId !== (int) $adminUserId) {
                throw new ApiException(ApiError::UNAUTHORIZED);
            }
        }
    }

    /**
     * @param \Exception $e
     * @param string     $errorType
     *
     * @throws ApiException
     */
    public static function throwException(\Exception $e, string $errorType = ApiError::DB)
    {
        if ($e instanceof ApiException) {
            throw $e;
        }

        throw new ApiException(
            $errorType,
            [
                'message' => $e->getMessage(),
                'file'    => $e->getFile(),
                'line'    => $e->getLine(),
                'stack'   => $e->getTrace(),
            ]
        );
    }

    /**
     * @param string $property
     * @param array  $array
     *
     * @return mixed
     */
    private static function searchForProperty(string $property, array $array)
    {
        foreach ($array as $key => $val) {
            if ($key === $property) {
                return $val;
            }
            if (is_array($val)) {
                $tmp = self::searchForProperty($property, $val);
                if (!is_null($tmp)) {
                    return $tmp;
                }
            }
        }
    }
}

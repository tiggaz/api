<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Controller;

use FOS\RestBundle\View\View;
use Itsup\Bundle\ApiBundle\Entity\User;
use Itsup\Bundle\ApiBundle\Exception\ApiException;
use Itsup\Bundle\ApiBundle\Form\Type\UserType;
use Itsup\Bundle\ApiBundle\Generator\ApiKeyGenerator;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;

/**
 * @author Martin Karadzinov <martin@myhost.mk>
 * @Route("/user")
 */
class UserController extends AbstractEntityController
{
    /**
     * @var string
     */
    protected $type = 'user';

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     * @Route("", name="user_find")
     * @Method("GET")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     *
     * @ApiDoc(
     *     section="User",
     *     description="Fetch User information, at least one of the parameters must be passed",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     parameters={
     *         {"name"="id", "dataType"="string", "required"=false, "description"="id"},
     *         {"name"="name", "dataType"="string", "required"=false, "description"="name"},
     *         {"name"="username", "dataType"="string", "required"=false, "description"="username"},
     *         {"name"="email", "dataType"="string", "required"=false, "description"="email"},
     *         {"name"="apiKey", "dataType"="string", "required"=false, "description"="apiKey"}
     *     }
     * )
     */
    public function findAction(Request $request): View
    {
        $context = $this->getContext($request, 'details');

        return $this->setContext($context, $this->view($this->find($request)));
    }

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     * @Route("/all", name="user_find_all")
     * @Method("GET")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN')")
     *
     * @ApiDoc(
     *     section="User",
     *     description="Fetch many Users information",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     parameters={
     *         {"name"="id", "dataType"="string", "required"=false, "description"="id"},
     *         {"name"="name", "dataType"="string", "required"=false, "description"="name"},
     *         {"name"="username", "dataType"="string", "required"=false, "description"="username"},
     *         {"name"="email", "dataType"="string", "required"=false, "description"="email"},
     *         {"name"="apiKey", "dataType"="string", "required"=false, "description"="apiKey"},
     *         {"name"="enabled", "dataType"="boolean", "required"=false, "description"="enabled"},
     *         {"name"="roles", "dataType"="string", "required"=false, "description"="roles"}
     *     }
     * )
     */
    public function findAllAction(Request $request): View
    {
        $context = $this->getContext($request, 'list');

        return $this->setContext($context, $this->view($this->findAll($request)));
    }

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("", name="user_create")
     * @Method("POST")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN')")
     *
     * @ApiDoc(
     *     section="User",
     *     description="Create a new User",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     input="Itsup\Bundle\ApiBundle\Form\Type\UserType",
     *     output="Itsup\Bundle\ApiBundle\Entity\User",
     *     statusCodes={
     *         201="Returned when user is created",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function createAction(Request $request): View
    {
        $account = $this->getAccount($request);
        $user    = new User();
        $this->processForm($request, $user, UserType::class);
        $user->setAccount($account);
        $user->setApiKey(ApiKeyGenerator::generate());
        $user->setSalt(md5($user->getPassword()));
        $encoder = new MessageDigestPasswordEncoder();
        $user->setPassword($encoder->encodePassword($user->getPassword(), $user->getSalt()));
        $this->save($user);

        return $this->view($user, 201);
    }

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("/update", name="user_update")
     * @Method("POST")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     *
     * @ApiDoc(
     *     section="User",
     *     description="Update an existing Account",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     input="Itsup\Bundle\ApiBundle\Form\Type\UserType",
     *     output="Itsup\Bundle\ApiBundle\Entity\User",
     *     statusCodes={
     *         200="Returned when user is updated",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function updateAction(Request $request): View
    {
        $account = $this->getAccount($request);
        $userId  = $this->getRequestProperty($request, 'id');
        $userId  = empty($userId) ? $this->getUser()->getId() : $userId;
        $this->checkUser($userId);
        /* @var User $user */
        $user = $this->getEntityFinder()->find(['id' => (int) $userId, 'account' => $account]);
        $this->processForm($request, $user, UserType::class);
        $this->update($user);

        return $this->view($user, 200);
    }

    /**
     * @param int $userId
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("/{userId}", name="user_delete")
     * @Method("DELETE")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN')")
     *
     *
     * @ApiDoc(
     *     section="User",
     *     description="Delete a User",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     statusCodes={
     *         204="Returned when user is deleted",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function deleteAction(int $userId): View
    {
        /* @var User $user */
        $user = $this->getEntityFinder()->find(['id' => (int) $userId]);
        $this->checkAccount($user->getAccount()->getId());
        $this->delete($user);

        return $this->view(null, 204);
    }
}

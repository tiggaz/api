<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Controller;

use FOS\RestBundle\View\View;
use Itsup\Bundle\ApiBundle\Entity\Group;
use Itsup\Bundle\ApiBundle\Exception\ApiException;
use Itsup\Bundle\ApiBundle\Form\Type\GroupType;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author Martin Karadzinov <martin@myhost.mk>
 * @Route("/group")
 */
class GroupController extends AbstractEntityController
{
    /**
     * @var string
     */
    protected $type = 'group';

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     * @Route("", name="group_find")
     * @Method("GET")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     *
     * @ApiDoc(
     *     section="Group",
     *     description="Fetch Group information, at least one of the parameters must be passed",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     parameters={
     *         {"name"="id", "dataType"="string", "required"=false, "description"="id"},
     *         {"name"="name", "dataType"="string", "required"=false, "description"="name"}
     *     }
     * )
     */
    public function findAction(Request $request): View
    {
        $context = $this->getContext($request, 'details');

        return $this->setContext($context, $this->view($this->find($request)));
    }

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     * @Route("/all", name="group_find_all")
     * @Method("GET")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     *
     * @ApiDoc(
     *     section="Group",
     *     description="Fetch many Groups information",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     parameters={
     *         {"name"="id", "dataType"="string", "required"=false, "description"="id"},
     *         {"name"="name", "dataType"="string", "required"=false, "description"="name"},
     *         {"name"="account", "dataType"="string", "required"=false, "description"="account"}
     *     }
     * )
     */
    public function findAllAction(Request $request): View
    {
        $context = $this->getContext($request, 'list');

        return $this->setContext($context, $this->view($this->findAll($request)));
    }

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("", name="group_create")
     * @Method("POST")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     *
     * @ApiDoc(
     *     section="Group",
     *     description="Create a new Group",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     input="Itsup\Bundle\ApiBundle\Form\Type\GroupType",
     *     output="Itsup\Bundle\ApiBundle\Entity\Group",
     *     statusCodes={
     *         201="Returned when group is created",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function createAction(Request $request): View
    {
        $account = $this->getAccount($request);
        $group   = new Group();
        $this->processForm($request, $group, GroupType::class);
        $group->setAccount($account);
        $this->save($group);

        return $this->view($group, 201);
    }

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("/update", name="group_update")
     * @Method("POST")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     *
     * @ApiDoc(
     *     section="Group",
     *     description="Update an existing Group",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     input="Itsup\Bundle\ApiBundle\Form\Type\GroupType",
     *     output="Itsup\Bundle\ApiBundle\Entity\Group",
     *     statusCodes={
     *         200="Returned when group is updated",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function updateAction(Request $request): View
    {
        $account = $this->getAccount($request);
        $groupId = $this->getRequestProperty($request, 'id');

        /* @var Group $group */
        $group   = $this->getEntityFinder()->find(['id' => (int) $groupId, 'account' => $account]);
        $this->processForm($request, $group, GroupType::class);
        $this->update($group);

        return $this->view($group, 200);
    }

    /**
     * @param int $groupId
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("/{groupId}", name="group_delete")
     * @Method("DELETE")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     *
     *
     * @ApiDoc(
     *     section="Group",
     *     description="Delete a Group",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     statusCodes={
     *         204="Returned when group is deleted",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function deleteAction(int $groupId): View
    {
        /* @var Group $group */
        $group = $this->getEntityFinder()->find(['id' => (int) $groupId]);
        $this->checkAccount($group->getAccount()->getId());

        $this->delete($group);

        return $this->view(null, 204);
    }
}

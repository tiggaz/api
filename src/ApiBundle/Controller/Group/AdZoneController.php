<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Controller\Group;

use Doctrine\Common\Collections\ArrayCollection;
use FOS\RestBundle\View\View;
use Itsup\Bundle\ApiBundle\ApiError;
use Itsup\Bundle\ApiBundle\Controller\AbstractEntityController;
use Itsup\Bundle\ApiBundle\Entity\AdZone;
use Itsup\Bundle\ApiBundle\Entity\Group;
use Itsup\Bundle\ApiBundle\Exception\ApiException;
use Itsup\Bundle\ApiBundle\Finder\AdZoneFinder;
use Itsup\Bundle\ApiBundle\Form\Type\Group\AdZoneCollectionType;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author Martin Karadzinov <martin@myhost.mk>
 * @Route("/group/{groupId}/adzone")
 */
class AdZoneController extends AbstractEntityController
{
    /**
     * @var string
     */
    protected $type = 'group';

    /**
     * @param int $groupId
     * @param int $adZoneId
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("/{adZoneId}", name="group_add_ad_zone")
     * @Method("POST")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     *
     *
     * @ApiDoc(
     *     section="Group AdZone",
     *     description="Add an AdZone to a Group",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     statusCodes={
     *         200="Returned when AdZone is added to the Group",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function addAction(int $groupId, int $adZoneId): View
    {
        /* @var Group $group */
        $group = $this->getEntityFinder()->find(['id' => (int) $groupId]);
        $this->checkAccount($group->getAccount()->getId());

        /* @var AdZoneFinder $finder */
        $finder = $this->get('finder.ad_zone');
        /* @var AdZone $adZone */
        $adZone = $finder->find(['id' => (int) $adZoneId]);
        $this->checkAccount($adZone->getAccount()->getId());

        if ($group->getAccount()->getId() !== $adZone->getAccount()->getId()) {
            throw new ApiException(ApiError::UNAUTHORIZED);
        }

        $group->addAdZone($adZone);
        $this->update($group);

        return $this->view($group, 200);
    }

    /**
     * @param Request $request
     * @param int     $groupId
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("", name="ad_zone_campaignweight_create_bulk")
     * @Method("POST")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     *
     * @ApiDoc(
     *     section="Group AdZone",
     *     description="Bulk replace all AdZones in a Group",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     input="Itsup\Bundle\ApiBundle\Form\Type\Group\AdZoneCollectionType",
     *     output="Itsup\Bundle\ApiBundle\Entity\Group",
     *     statusCodes={
     *         200="Returned when AdZones are replaced in the Group",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function setAction(Request $request, int $groupId)
    {
        /* @var Group $group */
        $group = $this->getEntityFinder()->find(['id' => (int) $groupId]);
        $this->checkAccount($group->getAccount()->getId());

        $form = $this->createForm(AdZoneCollectionType::class);
        $form->submit($request->request->all(), false);
        $data    = $form->getData();
        $adZones = isset($data['adZones']) ? $data['adZones'] : [];

        $tmp = new ArrayCollection();
        foreach ($adZones as $adZone) {
            try {
                $this->checkAccount($adZone->getAccount()->getId());
                $tmp->add($adZone);
            } catch (ApiException $e) {
            }
        }
        $group->setAdZones($tmp);
        $this->update($group);

        return $this->view($adZones, 200);
    }

    /**
     * @param int $groupId
     * @param int $adZoneId
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("/{adZoneId}", name="group_remove_ad_zone")
     * @Method("DELETE")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     *
     *
     * @ApiDoc(
     *     section="Group AdZone",
     *     description="Remove an AdZone from a Group",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     statusCodes={
     *         204="Returned when AdZone is removed from the Group",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function removeAction(int $groupId, int $adZoneId): View
    {
        /* @var Group $group */
        $group = $this->getEntityFinder()->find(['id' => (int) $groupId]);
        $this->checkAccount($group->getAccount()->getId());

        /* @var AdZoneFinder $finder */
        $finder = $this->get('finder.ad_zone');
        /* @var AdZone $adZone */
        $adZone = $finder->find(['id' => (int) $adZoneId]);

        $group->removeAdZone($adZone);
        $this->update($group);

        return $this->view(null, 204);
    }
}

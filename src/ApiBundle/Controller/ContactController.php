<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Controller;

use FOS\RestBundle\View\View;
use Itsup\Bundle\ApiBundle\ApiError;
use Itsup\Bundle\ApiBundle\Entity\Contact;
use Itsup\Bundle\ApiBundle\Exception\ApiException;
use Itsup\Bundle\ApiBundle\Form\Type\ContactType;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author Martin Karadzinov <martin@myhost.mk>
 * @Route("/contact")
 */
class ContactController extends AbstractEntityController
{
    protected $type = 'contact';

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     * @Route("", name="contact_find")
     * @Method("GET")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     *
     * @ApiDoc(
     *     section="Contact",
     *     description="Fetch Contact information, at least one of the parameters must be passed",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     parameters={
     *         {"name"="id", "dataType"="string", "required"=false, "description"="id"},
     *         {"name"="name", "dataType"="string", "required"=false, "description"="name"},
     *     }
     * )
     */
    public function findAction(Request $request): View
    {
        $context = $this->getContext($request, 'details');

        return $this->setContext($context, $this->view($this->find($request)));
    }

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     * @Route("/all", name="contact_find_all")
     * @Method("GET")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     *
     * @ApiDoc(
     *     section="Contact",
     *     description="Fetch many Contact information",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     parameters={
     *         {"name"="id", "dataType"="string", "required"=false, "description"="id"},
     *         {"name"="name", "dataType"="string", "required"=false, "description"="name"},
     *         {"name"="email", "dataType"="string", "required"=false, "description"="email"},
     *         {"name"="skype", "dataType"="string", "required"=false, "description"="skype"},
     *         {"name"="phone", "dataType"="string", "required"=false, "description"="phone"},
     *         {"name"="company", "dataType"="string", "required"=false, "description"="company"},
     *         {"name"="country", "dataType"="string", "required"=false, "description"="country"},
     *         {"name"="website_url", "dataType"="string", "required"=false, "description"="website_url"},
     *         {"name"="publisher", "dataType"="integer", "required"=false, "description"="publisher"},
     *         {"name"="broker", "dataType"="integer", "required"=false, "description"="broker"},
     *         {"name"="advertiser", "dataType"="integer", "required"=false, "description"="advertiser"},
     *     }
     * )
     */
    public function findAllAction(Request $request): View
    {
        $context = $this->getContext($request, 'list');

        return $this->setContext($context, $this->view($this->findAll($request)));
    }

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("", name="contact_create")
     * @Method("POST")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     *
     * @ApiDoc(
     *     section="Contact",
     *     description="Create a new Contact",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     input="Itsup\Bundle\ApiBundle\Form\Type\ContactType",
     *     output="Itsup\Bundle\ApiBundle\Entity\Contact",
     *     statusCodes={
     *         201="Returned when contact is created",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function createAction(Request $request): View
    {
        $account = $this->getAccount($request);
        $user    = $this->getRequestedUser($request);
        $contact = new Contact();
        $this->processForm($request, $contact, ContactType::class);
        $contact->setAccount($account);
        $contact->setUser($user);
        $this->save($contact);

        return $this->view($contact, 201);
    }

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("/update", name="contact_update")
     * @Method("POST")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     *
     * @ApiDoc(
     *     section="Contact",
     *     description="Update an existing Contact",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     input="Itsup\Bundle\ApiBundle\Form\Type\ContactType",
     *     output="Itsup\Bundle\ApiBundle\Entity\Contact",
     *     statusCodes={
     *         200="Returned when contact is updated",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function updateAction(Request $request): View
    {
        $account   = $this->getAccount($request);
        $contactId = $this->getRequestProperty($request, 'id');

        if (empty($contactId)) {
            throw new ApiException(ApiError::INVALID_REQUEST, ['Missing ContactId']);
        }

        /* @var Contact $contact */
        $contact = $this->getEntityFinder()->find(['id' => (int) $contactId, 'account' => $account]);
        $this->processForm($request, $contact, ContactType::class);
        $this->update($contact);

        return $this->view($contact, 200);
    }

    /**
     * @param int $contactId
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("/{contactId}", name="contact_delete")
     * @Method("DELETE")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     *
     * @ApiDoc(
     *     section="Contact",
     *     description="Delete an Contact",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     statusCodes={
     *         204="Returned when contact is deleted",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function deleteAction(int $contactId): View
    {
        /* @var Contact $contact */
        $contact = $this->getEntityFinder()->find(['id' => (int) $contactId]);
        $this->checkAccount($contact->getAccount()->getId());
        $this->delete($contact);

        return $this->view(null, 204);
    }
}

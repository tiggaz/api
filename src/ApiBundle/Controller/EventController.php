<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Controller;

use FOS\RestBundle\View\View;
use Itsup\Bundle\ApiBundle\Entity\Event;
use Itsup\Bundle\ApiBundle\Exception\ApiException;
use Itsup\Bundle\ApiBundle\Form\Type\EventType;
use Itsup\Bundle\ApiBundle\Model\Event\DecryptedClick;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author Martin Karadzinov <martin@myhost.mk>
 * @Route("/event")
 */
class EventController extends AbstractEntityController
{
    /**
     * @var string
     */
    protected $type = 'event';

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     * @Route("", name="event_find")
     * @Method("GET")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     *
     * @ApiDoc(
     *     section="Event",
     *     description="Fetch Event information, at least one of the parameters must be passed",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     parameters={
     *         {"name"="id", "dataType"="string", "required"=false, "description"="id"},
     *         {"name"="name", "dataType"="string", "required"=false, "description"="name"}
     *     }
     * )
     */
    public function findAction(Request $request): View
    {
        $context = $this->getContext($request, 'details');

        return $this->setContext($context, $this->view($this->find($request)));
    }

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     * @Route("/all", name="event_find_all")
     * @Method("GET")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     *
     * @ApiDoc(
     *     section="Event",
     *     description="Fetch many Events information",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     parameters={
     *         {"name"="id", "dataType"="string", "required"=false, "description"="id"},
     *         {"name"="name", "dataType"="string", "required"=false, "description"="name"},
     *         {"name"="account", "dataType"="string", "required"=false, "description"="account"}
     *     }
     * )
     */
    public function findAllAction(Request $request): View
    {
        $context = $this->getContext($request, 'list');

        return $this->setContext($context, $this->view($this->findAll($request)));
    }

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("", name="event_create")
     * @Method("POST")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     *
     * @ApiDoc(
     *     section="Event",
     *     description="Create a new Event",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     input="Itsup\Bundle\ApiBundle\Form\Type\EventType",
     *     output="Itsup\Bundle\ApiBundle\Entity\Event",
     *     statusCodes={
     *         201="Returned when Event is created",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function createAction(Request $request): View
    {
        $account = $this->getAccount($request);
        $event   = new Event();
        $this->processForm($request, $event, EventType::class);
        $event->setAccount($account);
        $this->save($event);

        return $this->view($event, 201);
    }

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("/update", name="event_update")
     * @Method("POST")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     *
     * @ApiDoc(
     *     section="Event",
     *     description="Update an existing Event",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     input="Itsup\Bundle\ApiBundle\Form\Type\EventType",
     *     output="Itsup\Bundle\ApiBundle\Entity\Event",
     *     statusCodes={
     *         200="Returned when Event is updated",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function updateAction(Request $request): View
    {
        $account = $this->getAccount($request);
        $eventId = $this->getRequestProperty($request, 'id');

        /* @var Event $event */
        $event   = $this->getEntityFinder()->find(['id' => (int) $eventId, 'account' => $account]);
        $this->processForm($request, $event, EventType::class);
        $this->update($event);

        return $this->view($event, 200);
    }

    /**
     * @param int $eventId
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("/{eventId}", name="event_delete")
     * @Method("DELETE")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     *
     *
     * @ApiDoc(
     *     section="Event",
     *     description="Delete a Event",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     statusCodes={
     *         204="Returned when Event is deleted",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function deleteAction(int $eventId): View
    {
        /* @var Event $event */
        $event = $this->getEntityFinder()->find(['id' => (int) $eventId]);
        $this->checkAccount($event->getAccount()->getId());
        $this->delete($event);

        return $this->view(null, 204);
    }

    /**
     * @param string $clickId
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("/decrypt/{clickId}", name="event_decrypt_click")
     * @Method("GET")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     *
     *
     * @ApiDoc(
     *     section="Event",
     *     description="Decrypt ClickId of Event",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     statusCodes={
     *         204="Returned when ClickId is decrypted",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function decryptClickAction(string $clickId): View
    {
        $decryptedId = $this->getEntityManager()->decryptClick($clickId);

        @list($accountId, $adZoneId, $campaignId, $creativeId, $offerId, $keywords, $time, $ip) =
            explode(':', strtolower($decryptedId));

        $this->checkAccount($accountId);

        $decryptedClick = new DecryptedClick();

        if (is_numeric($adZoneId) && $adZoneId > 0) {
            $adZone = $this->get('finder.ad_zone')->find(['id' => (int) $adZoneId]);

            $decryptedClick->setAdZone($adZone);
        }

        if (is_numeric($campaignId) && $campaignId > 0) {
            $campaign = $this->get('finder.campaign')->find(['id' => (int) $campaignId]);

            $decryptedClick->setCampaign($campaign);
        }

        if (is_numeric($creativeId) && $creativeId > 0) {
            $creative = $this->get('finder.creative')->find(['id' => (int) $creativeId]);

            $decryptedClick->setCreative($creative);
        }

        if (is_numeric($offerId) && $offerId > 0) {
            $offer = $this->get('finder.offer')->find(['id' => (int) $offerId]);

            $decryptedClick->setOffer($offer);
        }

        $decryptedClick->setKeywords($keywords);

        $datetime = new \DateTime();
        $datetime->setTimeStamp($time);

        $decryptedClick->setTime($datetime);

        return $this->view($decryptedClick, 201);
    }
}

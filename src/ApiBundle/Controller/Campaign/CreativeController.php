<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Controller\Campaign;

use Doctrine\Common\Collections\ArrayCollection;
use FOS\RestBundle\View\View;
use Itsup\Bundle\ApiBundle\ApiError;
use Itsup\Bundle\ApiBundle\Controller\AbstractEntityController;
use Itsup\Bundle\ApiBundle\Entity\AbstractEntity;
use Itsup\Bundle\ApiBundle\Entity\Campaign;
use Itsup\Bundle\ApiBundle\Entity\Campaign\CampaignCreative;
use Itsup\Bundle\ApiBundle\Entity\Creative;
use Itsup\Bundle\ApiBundle\Exception\ApiException;
use Itsup\Bundle\ApiBundle\Form\Type\Campaign\CampaignCreativeCollectionType;
use Itsup\Bundle\ApiBundle\Form\Type\Campaign\CampaignCreativeType;
use Itsup\Bundle\ApiBundle\Model\Campaign\CampaignCreativeCollection;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author Martin Karadzinov <martin@myhost.mk>
 * @Route("/campaign/creative")
 */
class CreativeController extends AbstractEntityController
{
    protected $type = 'campaign.creative';

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("", name="campaign_creative_add")
     * @Method("POST")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     *
     * @ApiDoc(
     *     section="Campaign Creative",
     *     description="Add a Creative to a campaign",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     input="Itsup\Bundle\ApiBundle\Form\Type\Campaign\CampaignCreativeType",
     *     output="Itsup\Bundle\ApiBundle\Entity\Campaign",
     *     statusCodes={
     *         200="Returned when Creative is added to the Campaign",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function createAction(Request $request): View
    {
        $creative         = $this->getCreative($request);
        $campaign         = $this->getCampaign($request);
        $campaignCreative = new CampaignCreative();
        try {
            /* @var CampaignCreative $campaignCreative */
            $campaignCreative = $this->getEntityFinder()->find(['creative' => $creative, 'campaign' => $campaign]);
        } catch (ApiException $e) {
            if ($e->getCode() !== 404) {
                throw $e;
            }
        }
        $this->processForm($request, $campaignCreative, CampaignCreativeType::class);
        $this->save($campaignCreative);

        return $this->view($campaign, 201);
    }

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("/update", name="campaign_creative_update")
     * @Method("POST")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     *
     * @ApiDoc(
     *     section="Campaign Creative",
     *     description="Add a Creative to a campaign",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     input="Itsup\Bundle\ApiBundle\Form\Type\Campaign\CampaignCreativeType",
     *     output="Itsup\Bundle\ApiBundle\Entity\Campaign",
     *     statusCodes={
     *         200="Returned when Creative is added to the Campaign",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function updateAction(Request $request): View
    {
        $creative         = $this->getCreative($request);
        $campaign         = $this->getCampaign($request);
        $campaignCreative = new CampaignCreative();
        try {
            /* @var CampaignCreative $campaignCreative */
            $campaignCreative = $this->getEntityFinder()->find(['creative' => $creative, 'campaign' => $campaign]);
        } catch (ApiException $e) {
            if ($e->getCode() !== 404) {
                throw $e;
            }
        }
        $this->processForm($request, $campaignCreative, CampaignCreativeType::class);
        $this->update($campaignCreative);

        return $this->view($campaign, 200);
    }

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("/bulk", name="campaign_creative_add_bulk")
     * @Method("POST")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     *
     * @ApiDoc(
     *     section="Campaign Creative",
     *     description="Bulk add Creative to a Campaign",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     input="Itsup\Bundle\ApiBundle\Form\Type\Campaign\CampaignCreativeCollectionType",
     *     output="Itsup\Bundle\ApiBundle\Entity\Campaign",
     *     statusCodes={
     *         200="Returned when Creatives are added to the Campaign",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function setAction(Request $request)
    {
        $campaign         = $this->getCampaign($request);
        $campaignCreative = new CampaignCreativeCollection();
        $this->processForm($request, $campaignCreative, CampaignCreativeCollectionType::class);

        $creatives = $campaign->getCreatives();
        foreach ($creatives as $creativeWeight) {
            $this->delete($creativeWeight);
        }
        $campaign->setCreatives(new ArrayCollection());
        foreach ($campaignCreative->getCreatives() as $creativeWeight) {
            try {
                $this->checkAccount($creativeWeight->getCreative()->getAccount()->getId());
            } catch (ApiException $e) {
                continue;
            }
            $creativeWeight->setCampaign($campaign);
            $this->save($creativeWeight);
        }

        return $this->view($campaign, 200);
    }

    /**
     * @param int $campaignId
     * @param int $creativeId
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("/{campaignId}/{creativeId}", name="campaign_creative_delete")
     * @Method("DELETE")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     *
     * @ApiDoc(
     *     section="Campaign Creative",
     *     description="Remove a creative from a campaign",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     statusCodes={
     *         204="Returned when Creative is removed",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function removeAction(int $campaignId, int $creativeId): View
    {
        $campaign = $this->getCampaignById($campaignId);
        $creative = $this->getCreativeById($creativeId);

        /* @var CampaignCreative $campaignCreative */
        $campaignCreative = $this->getEntityFinder()->find(['campaign' => $campaign, 'creative' => $creative]);
        $this->delete($campaignCreative);

        return $this->view(null, 204);
    }

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return Campaign
     */
    private function getCampaign(Request $request): Campaign
    {
        $campaignId = $this->getRequestProperty($request, 'campaign');

        return $this->getCampaignById((int) $campaignId);
    }

    /**
     * @param int $campaignId
     *
     * @return Campaign
     */
    private function getCampaignById(int $campaignId): Campaign
    {
        /* @var Campaign $campaign */
        $campaign   = $this->get('finder.campaign')->find(['id' => (int) $campaignId]);
        $this->checkAccount($campaign->getAccount()->getId());

        return $campaign;
    }

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return Creative
     */
    private function getCreative(Request $request): Creative
    {
        $creativeId = $this->getRequestProperty($request, 'creative');

        return $this->getCreativeById((int) $creativeId);
    }

    /**
     * @param int $creativeId
     *
     * @return Creative
     */
    private function getCreativeById(int $creativeId): Creative
    {
        /* @var Creative $creative */
        $creative = $this->get('finder.creative')->find(['id' => (int) $creativeId]);
        $this->checkAccount($creative->getAccount()->getId());

        return $creative;
    }

    /**
     * @param AbstractEntity $entity
     *
     * @throws ApiException
     */
    public function validate(AbstractEntity &$entity)
    {
        parent::validate($entity);

        if ($entity->getCampaign()->getType() === 'iframe') {
            if (!in_array($entity->getCreative()->getType(), ['iframe', 'image'])) {
                throw new ApiException(ApiError::INVALID_REQUEST, ['Creative must be iframe or image']);
            } else {
                if ($entity->getCreative()->getType() === 'image') {
                    if ($entity->getCreative()->getWidth() !== $entity->getCampaign()->getWidth() ||
                        $entity->getCreative()->getHeight() !== $entity->getCampaign()->getHeight()
                    ) {
                        $message = 'Creative width and height must match Campaign';
                        throw new ApiException(ApiError::INVALID_REQUEST, [$message]);
                    }
                }
            }
        }
    }
}

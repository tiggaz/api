<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Controller\Campaign;

use FOS\RestBundle\View\View;
use Itsup\Bundle\ApiBundle\ApiError;
use Itsup\Bundle\ApiBundle\Controller\AbstractEntityController;
use Itsup\Bundle\ApiBundle\Entity\AbstractEntity;
use Itsup\Bundle\ApiBundle\Entity\Campaign\Sale;
use Itsup\Bundle\ApiBundle\Exception\ApiException;
use Itsup\Bundle\ApiBundle\Form\Type\Campaign\SaleType;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author Martin Karadzinov <martin@myhost.mk>
 * @Route("/campaign/sale")
 */
class SaleController extends AbstractEntityController
{
    protected $type = 'campaign.sale';

    protected $accountSearch = false;

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     * @Route("", name="campaign_sale_find")
     * @Method("GET")
     * @Security("has_role('ROLE_INTERNAL')")
     *
     * @ApiDoc(
     *     section="Campaign Sale",
     *     description="Fetch Sale information, at least one of the parameters must be passed",
     *     tags={"stable"},
     *     views={"internal"},
     *     parameters={
     *         {"name"="id", "dataType"="string", "required"=false, "description"="id"},
     *         {"name"="campaign", "dataType"="string", "required"=false, "description"="campaign"},
     *     }
     * )
     */
    public function findAction(Request $request): View
    {
        $context = $this->getContext($request, 'details');

        return $this->setContext($context, $this->view($this->find($request)));
    }

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     * @Route("/all", name="campaign_sale_find_all")
     * @Method("GET")
     * @Security("has_role('ROLE_INTERNAL')")
     *
     * @ApiDoc(
     *     section="Campaign Sale",
     *     description="Fetch many Sale information",
     *     tags={"stable"},
     *     views={"internal"},
     *     parameters={
     *         {"name"="id", "dataType"="integer", "required"=false, "description"="id"},
     *         {"name"="campaign", "dataType"="integer", "required"=false, "description"="campaign"},
     *         {"name"="limit", "dataType"="integer", "required"=false, "description"="limit"},
     *         {"name"="type", "dataType"="string", "required"=false, "description"="type"},
     *         {"name"="from", "dataType"="string", "required"=false, "description"="from"},
     *         {"name"="to", "dataType"="string", "required"=false, "description"="to"},
     *     }
     * )
     */
    public function findAllAction(Request $request): View
    {
        $context = $this->getContext($request, 'list');

        return $this->setContext($context, $this->view($this->findAll($request)));
    }

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("", name="campaign_sale_create")
     * @Method("POST")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     *
     * @ApiDoc(
     *     section="Campaign Sale",
     *     description="Create a new Sale",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     input="Itsup\Bundle\ApiBundle\Form\Type\Campaign\SaleType",
     *     output="Itsup\Bundle\ApiBundle\Entity\Campaign\Sale",
     *     statusCodes={
     *         201="Returned when Sale is created",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function createAction(Request $request): View
    {
        $sale = new Sale();
        $this->processForm($request, $sale, SaleType::class);

        $this->checkAccount($sale->getCampaign()->getAccount()->getId());

        $this->save($sale);

        return $this->view($sale, 201);
    }

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("/update", name="campaign_sale_update")
     * @Method("POST")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     *
     * @ApiDoc(
     *     section="Campaign Sale",
     *     description="Update an existing Sale",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     input="Itsup\Bundle\ApiBundle\Form\Type\Campaign\SaleType",
     *     output="Itsup\Bundle\ApiBundle\Entity\Campaign\Sale",
     *     statusCodes={
     *         200="Returned when Sale is updated",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function updateAction(Request $request): View
    {
        $saleId = $this->getRequestProperty($request, 'id');
        if (empty($saleId)) {
            throw new ApiException(ApiError::INVALID_REQUEST, ['Missing SaleId']);
        }

        /* @var Sale $sale */
        $sale       = $this->getEntityFinder()->find(['id' => (int) $saleId]);
        $this->processForm($request, $sale, SaleType::class);

        $this->checkAccount($sale->getCampaign()->getAccount()->getId());

        $this->update($sale);

        return $this->view($sale, 200);
    }

    /**
     * @param int $saleId
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("/{saleId}", name="campaign_sale_delete")
     * @Method("DELETE")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     *
     * @ApiDoc(
     *     section="Campaign Sale",
     *     description="Delete an Sale",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     statusCodes={
     *         204="Returned when Sale is deleted",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function deleteAction(int $saleId): View
    {
        /* @var Sale $sale */
        $sale = $this->getEntityFinder()->find(['id' => (int) $saleId]);

        $this->checkAccount($sale->getCampaign()->getAccount()->getId());

        $this->delete($sale);

        return $this->view(null, 204);
    }

    /**
     * @param AbstractEntity $entity
     *
     * @throws ApiException
     */
    public function validate(AbstractEntity &$entity)
    {
        parent::validate($entity);

        /* @var Sale $entity */
        if ($entity->getFrom() > $entity->getTo()) {
            throw new ApiException(ApiError::INVALID_REQUEST, ['From date must be before To date']);
        }
    }
}

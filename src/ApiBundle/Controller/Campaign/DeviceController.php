<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Controller\Campaign;

use Doctrine\Common\Collections\ArrayCollection;
use FOS\RestBundle\View\View;
use Itsup\Bundle\ApiBundle\Controller\AbstractEntityController;
use Itsup\Bundle\ApiBundle\Entity\Campaign;
use Itsup\Bundle\ApiBundle\Exception\ApiException;
use Itsup\Bundle\ApiBundle\Form\Type\Campaign\DeviceCollectionType;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author Martin Karadzinov <martin.karadzinov@devsy.com>
 * @Route("/campaign/{campaignId}/device")
 */
class DeviceController extends AbstractEntityController
{
    protected $type = 'campaign';

    /**
     * @param Request $request
     * @param int     $campaignId
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("", name="campaign_device_create_bulk")
     * @Method("POST")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     *
     * @ApiDoc(
     *     section="Campaign Device",
     *     description="Bulk replace all Devices in a Campaign",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     input="Itsup\Bundle\ApiBundle\Form\Type\Campaign\DeviceCollectionType",
     *     output="Itsup\Bundle\ApiBundle\Entity\Campaign",
     *     statusCodes={
     *         200="Returned when ISPs are replaced in the Campaign",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function setAction(Request $request, int $campaignId)
    {
        /* @var Campaign $campaign */
        $campaign = $this->getEntityFinder()->find(['id' => (int) $campaignId]);

        $form = $this->createForm(DeviceCollectionType::class);
        $form->submit($request->request->all(), false);
        $data    = $form->getData();
        $devices = isset($data['collection']) ? $data['collection'] : [];

        $tmp = new ArrayCollection();
        foreach ($devices as $device) {
            $tmp->add($device);
        }
        $campaign->setDevices($tmp);
        $this->update($campaign);

        return $this->view($campaign, 200);
    }
}

<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Controller\Campaign;

use FOS\RestBundle\View\View;
use Itsup\Bundle\ApiBundle\ApiError;
use Itsup\Bundle\ApiBundle\Controller\AbstractEntityController;
use Itsup\Bundle\ApiBundle\Entity\AbstractEntity;
use Itsup\Bundle\ApiBundle\Entity\Campaign\Limit;
use Itsup\Bundle\ApiBundle\Exception\ApiException;
use Itsup\Bundle\ApiBundle\Form\Type\Campaign\LimitType;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author Martin Karadzinov <martin@myhost.mk>
 * @Route("/campaign/limit")
 */
class LimitController extends AbstractEntityController
{
    protected $type = 'campaign.limit';

    protected $accountSearch = false;

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     * @Route("", name="campaign_limit_find")
     * @Method("GET")
     * @Security("has_role('ROLE_INTERNAL')")
     *
     * @ApiDoc(
     *     section="Campaign Limit",
     *     description="Fetch Limit information, at least one of the parameters must be passed",
     *     tags={"stable"},
     *     views={"internal"},
     *     parameters={
     *         {"name"="id", "dataType"="string", "required"=false, "description"="id"},
     *         {"name"="campaign", "dataType"="string", "required"=false, "description"="campaign"},
     *     }
     * )
     */
    public function findAction(Request $request): View
    {
        $context = $this->getContext($request, 'details');

        return $this->setContext($context, $this->view($this->find($request)));
    }

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     * @Route("/all", name="campaign_limit_find_all")
     * @Method("GET")
     * @Security("has_role('ROLE_INTERNAL')")
     *
     * @ApiDoc(
     *     section="Campaign Limit",
     *     description="Fetch many Limit information",
     *     tags={"stable"},
     *     views={"internal"},
     *     parameters={
     *         {"name"="id", "dataType"="string", "required"=false, "description"="id"},
     *         {"name"="campaign", "dataType"="string", "required"=false, "description"="campaign"},
     *         {"name"="from", "dataType"="string", "required"=false, "description"="from"},
     *         {"name"="to", "dataType"="string", "required"=false, "description"="to"},
     *         {"name"="dateReached", "dataType"="string", "required"=false, "description"="dateReached"},
     *         {"name"="typeReached", "dataType"="string", "required"=false, "description"="typeReached"},
     *     }
     * )
     */
    public function findAllAction(Request $request): View
    {
        $context = $this->getContext($request, 'list');

        return $this->setContext($context, $this->view($this->findAll($request)));
    }

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("", name="campaign_limit_create")
     * @Method("POST")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     *
     * @ApiDoc(
     *     section="Campaign Limit",
     *     description="Create a new Limit",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     input="Itsup\Bundle\ApiBundle\Form\Type\Campaign\LimitType",
     *     output="Itsup\Bundle\ApiBundle\Entity\Campaign\Limit",
     *     statusCodes={
     *         201="Returned when Limit is created",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function createAction(Request $request): View
    {
        $limit = new Limit();
        $this->processForm($request, $limit, LimitType::class);

        $this->checkAccount($limit->getCampaign()->getAccount()->getId());

        $this->save($limit);

        return $this->view($limit, 201);
    }

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("/update", name="campaign_limit_update")
     * @Method("POST")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     *
     * @ApiDoc(
     *     section="Campaign Limit",
     *     description="Update an existing Limit",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     input="Itsup\Bundle\ApiBundle\Form\Type\Campaign\LimitType",
     *     output="Itsup\Bundle\ApiBundle\Entity\Campaign\Limit",
     *     statusCodes={
     *         200="Returned when Limit is updated",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function updateAction(Request $request): View
    {
        $limitId = $this->getRequestProperty($request, 'id');
        if (empty($limitId)) {
            throw new ApiException(ApiError::INVALID_REQUEST, ['Missing LimitId']);
        }

        /* @var Limit $limit */
        $limit      = $this->getEntityFinder()->find(['id' => (int) $limitId]);
        $this->processForm($request, $limit, LimitType::class);

        $this->checkAccount($limit->getCampaign()->getAccount()->getId());

        $this->update($limit);

        return $this->view($limit, 200);
    }

    /**
     * @param int $limitId
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("/{limitId}", name="campaign_limit_delete")
     * @Method("DELETE")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     *
     * @ApiDoc(
     *     section="Campaign Limit",
     *     description="Delete an Limit",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     statusCodes={
     *         204="Returned when Limit is deleted",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function deleteAction(int $limitId): View
    {
        /* @var Limit $limit */
        $limit = $this->getEntityFinder()->find(['id' => (int) $limitId]);

        $this->checkAccount($limit->getCampaign()->getAccount()->getId());

        $this->delete($limit);

        return $this->view(null, 204);
    }

    /**
     * @param AbstractEntity $entity
     *
     * @throws ApiException
     */
    public function validate(AbstractEntity &$entity)
    {
        parent::validate($entity);

        /* @var Limit $entity */
        if ($entity->getFrom() > $entity->getTo()) {
            throw new ApiException(ApiError::INVALID_REQUEST, ['From date must be before To date']);
        }
    }
}

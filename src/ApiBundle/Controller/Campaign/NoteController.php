<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Controller\Campaign;

use FOS\RestBundle\View\View;
use Itsup\Bundle\ApiBundle\ApiError;
use Itsup\Bundle\ApiBundle\Controller\AbstractEntityController;
use Itsup\Bundle\ApiBundle\Entity\Campaign;
use Itsup\Bundle\ApiBundle\Entity\Note;
use Itsup\Bundle\ApiBundle\Exception\ApiException;
use Itsup\Bundle\ApiBundle\Finder\NoteFinder;
use Itsup\Bundle\ApiBundle\Form\Type\NoteType;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author Martin Karadzinov <martin@myhost.mk>
 * @Route("/campaign/{campaignId}/note")
 */
class NoteController extends AbstractEntityController
{
    protected $type = 'campaign';

    /**
     * @param Request $request
     * @param int     $campaignId
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("", name="campaign_note_add")
     * @Method("POST")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     * @ApiDoc(
     *     section="Campaign Note",
     *     description="Add a Note to an Campaign",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     input="Itsup\Bundle\ApiBundle\Form\Type\NoteType",
     *     output="Itsup\Bundle\ApiBundle\Entity\Campaign",
     *     statusCodes={
     *         200="Returned when Note is added",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function addAction(Request $request, int $campaignId): View
    {
        if (empty($campaignId)) {
            throw new ApiException(ApiError::INVALID_REQUEST, ['Missing CampaignId']);
        }
        $note = new Note();
        $this->processForm($request, $note, NoteType::class);

        /* @var Campaign $campaign */
        $campaign  = $this->getEntityFinder()->find(['id' => (int) $campaignId]);
        $account   = $campaign->getAccount();
        $this->checkAccount($account->getId());
        $note->setDate(new \DateTime());
        $note->setUser($this->getRequestedUser($request));
        $campaign->addNote($note);
        $this->update($campaign);

        return $this->view($campaign, 200);
    }

    /**
     * @param int $campaignId
     * @param int $noteId
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("/{noteId}", name="campaign_note_remove")
     * @Method("DELETE")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     * @ApiDoc(
     *     section="Campaign Note",
     *     description="Remove a Note from an Campaign",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     output="Itsup\Bundle\ApiBundle\Entity\Campaign",
     *     statusCodes={
     *         204="Returned when the Note is removed",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function removeAction(int $campaignId, int $noteId): View
    {
        if (empty($campaignId)) {
            throw new ApiException(ApiError::INVALID_REQUEST, ['Missing CampaignId']);
        }
        if (empty($noteId)) {
            throw new ApiException(ApiError::INVALID_REQUEST, ['Missing noteId']);
        }

        /* @var Campaign $campaign */
        $campaign  = $this->getEntityFinder()->find(['id' => (int) $campaignId]);
        $account   = $campaign->getAccount();
        $this->checkAccount($account->getId());

        /* @var NoteFinder $finder */
        $finder = $this->get('finder.note');

        /* @var Note $note */
        $note   = $finder->find(['id' => (int) $noteId]);
        $campaign->removeNote($note);
        $this->update($campaign);

        return $this->view(null, 204);
    }
}

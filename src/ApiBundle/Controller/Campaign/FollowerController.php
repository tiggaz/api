<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Controller\Campaign;

use FOS\RestBundle\View\View;
use Itsup\Bundle\ApiBundle\ApiError;
use Itsup\Bundle\ApiBundle\Controller\AbstractEntityController;
use Itsup\Bundle\ApiBundle\Entity\Campaign;
use Itsup\Bundle\ApiBundle\Entity\User;
use Itsup\Bundle\ApiBundle\Exception\ApiException;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @author Martin Karadzinov <martin@myhost.mk>
 * @Route("/campaign/{campaignId}/follower")
 */
class FollowerController extends AbstractEntityController
{
    protected $type = 'campaign';

    /**
     * @param int $campaignId
     * @param int $userId
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("/{userId}", name="campaign_follower_add")
     * @Method("POST")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     * @ApiDoc(
     *     section="Campaign Follower",
     *     description="Add a Follower to an Campaign",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     output="Itsup\Bundle\ApiBundle\Entity\Campaign",
     *     statusCodes={
     *         200="Returned when Follower is added",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function addAction(int $campaignId, int $userId): View
    {
        if (empty($campaignId)) {
            throw new ApiException(ApiError::INVALID_REQUEST, ['Missing campaignId']);
        }

        if (empty($userId)) {
            throw new ApiException(ApiError::INVALID_REQUEST, ['Missing userId']);
        }

        /* @var Campaign $campaign */
        $campaign  = $this->getEntityFinder()->find(['id' => (int) $campaignId]);
        /* @var User $user */
        $user    = $this->get('finder.user')->find(['id' => (int) $userId]);
        $account = $campaign->getAccount();
        $this->checkAccount($account->getId());
        $campaign->addFollower($user);
        $this->update($campaign);

        return $this->view($campaign, 200);
    }

    /**
     * @param int $campaignId
     * @param int $userId
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("/{userId}", name="Campaign_follower_remove")
     * @Method("DELETE")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     * @ApiDoc(
     *     section="Campaign Follower",
     *     description="Remove a Follower from an Campaign",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     statusCodes={
     *         204="Returned when the Follower is removed",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function removeAction(int $campaignId, int $userId): View
    {
        if (empty($campaignId)) {
            throw new ApiException(ApiError::INVALID_REQUEST, ['Missing campaignId']);
        }

        if (empty($userId)) {
            throw new ApiException(ApiError::INVALID_REQUEST, ['Missing userId']);
        }

        /* @var Campaign $campaign */
        $campaign  = $this->getEntityFinder()->find(['id' => (int) $campaignId]);
        /* @var User $user */
        $user    = $this->get('finder.user')->find(['id' => (int) $userId]);
        $account = $campaign->getAccount();
        $this->checkAccount($account->getId());
        $campaign->removeFollower($user);
        $this->update($campaign);

        return $this->view(null, 204);
    }
}

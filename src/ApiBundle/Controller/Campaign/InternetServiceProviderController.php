<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Controller\Campaign;

use Doctrine\Common\Collections\ArrayCollection;
use FOS\RestBundle\View\View;
use Itsup\Bundle\ApiBundle\Controller\AbstractEntityController;
use Itsup\Bundle\ApiBundle\Entity\Campaign;
use Itsup\Bundle\ApiBundle\Exception\ApiException;
use Itsup\Bundle\ApiBundle\Form\Type\Campaign\InternetServiceProviderCollectionType;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author Martin Karadzinov <martin@myhost.mk>
 * @Route("/campaign/{campaignId}/isp")
 */
class InternetServiceProviderController extends AbstractEntityController
{
    protected $type = 'campaign';

    /**
     * @param Request $request
     * @param int     $campaignId
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("", name="campaign_isp_create_bulk")
     * @Method("POST")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     *
     * @ApiDoc(
     *     section="Campaign ISP",
     *     description="Bulk replace all ISPs in a Campaign",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     input="Itsup\Bundle\ApiBundle\Form\Type\Campaign\InternetServiceProviderCollectionType",
     *     output="Itsup\Bundle\ApiBundle\Entity\Campaign",
     *     statusCodes={
     *         200="Returned when ISPs are replaced in the Campaign",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function setAction(Request $request, int $campaignId)
    {
        /* @var Campaign $campaign */
        $campaign = $this->getEntityFinder()->find(['id' => (int) $campaignId]);
        $this->checkAccount($campaign->getAccount()->getId());

        $form = $this->createForm(InternetServiceProviderCollectionType::class);
        $form->submit($request->request->all(), false);
        $data    = $form->getData();
        $isps    = isset($data['collection']) ? $data['collection'] : [];

        $tmp = new ArrayCollection();
        foreach ($isps as $isp) {
            $tmp->add($isp);
        }
        $campaign->setInternetServiceProviders($tmp);
        $this->update($campaign);

        return $this->view($campaign, 200);
    }
}

<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Controller;

use FOS\RestBundle\View\View;
use Itsup\Bundle\ApiBundle\Entity\Account;
use Itsup\Bundle\ApiBundle\Exception\ApiException;
use Itsup\Bundle\ApiBundle\Form\Type\AccountType;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author Martin Karadzinov <martin@myhost.mk>
 * @Route("/account")
 */
class AccountController extends AbstractEntityController
{
    protected $type = 'account';

    protected $accountSearch = false;

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     * @Route("", name="account_find")
     * @Method("GET")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     *
     * @ApiDoc(
     *     section="Account",
     *     description="Fetch Account information, at least one of the parameters must be passed",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     parameters={
     *         {"name"="id", "dataType"="string", "required"=false, "description"="id"},
     *         {"name"="name", "dataType"="string", "required"=false, "description"="name"},
     *         {"name"="taxId", "dataType"="string", "required"=false, "description"="taxId"}
     *     }
     * )
     */
    public function findAction(Request $request): View
    {
        $context = $this->getContext($request, 'details');
        /* @var Account $account */
        $account = $this->find($request);
        $this->checkAccount($account->getId());

        return $this->setContext($context, $this->view($account));
    }

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     * @Route("/all", name="account_find_all")
     * @Method("GET")
     * @Security("has_role('ROLE_INTERNAL')")
     *
     * @ApiDoc(
     *     section="Account",
     *     description="Fetch many Accounts information",
     *     tags={"stable"},
     *     views={"internal"},
     *     parameters={
     *         {"name"="id", "dataType"="string", "required"=false, "description"="id"},
     *         {"name"="name", "dataType"="string", "required"=false, "description"="name"},
     *         {"name"="country", "dataType"="string", "required"=false, "description"="country"},
     *         {"name"="state", "dataType"="string", "required"=false, "description"="state"},
     *         {"name"="city", "dataType"="string", "required"=false, "description"="city"},
     *         {"name"="enabled", "dataType"="boolean", "required"=false, "description"="enabled"},
     *         {"name"="taxId", "dataType"="string", "required"=false, "description"="taxId"}
     *     }
     * )
     */
    public function findAllAction(Request $request): View
    {
        $context = $this->getContext($request, 'list');

        return $this->setContext($context, $this->view($this->findAll($request)));
    }

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("", name="account_create")
     * @Method("POST")
     * @Security("has_role('ROLE_INTERNAL')")
     *
     * @ApiDoc(
     *     section="Account",
     *     description="Create a new Account",
     *     tags={"stable"},
     *     views={"internal"},
     *     input="Itsup\Bundle\ApiBundle\Form\Type\AccountType",
     *     output="Itsup\Bundle\ApiBundle\Entity\Account",
     *     statusCodes={
     *         201="Returned when account is created",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function createAction(Request $request): View
    {
        $account = new Account();
        $this->processForm($request, $account, AccountType::class);
        $this->save($account);

        return $this->view($account, 201);
    }

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("/update", name="account_update")
     * @Method("POST")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN')")
     *
     * @ApiDoc(
     *     section="Account",
     *     description="Update an existing Account",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     input="Itsup\Bundle\ApiBundle\Form\Type\AccountType",
     *     output="Itsup\Bundle\ApiBundle\Entity\Account",
     *     statusCodes={
     *         200="Returned when account is updated",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function updateAction(Request $request): View
    {
        $account = $this->getAccount($request);
        $this->processForm($request, $account, AccountType::class);
        $this->update($account);

        return $this->view($account, 200);
    }

    /**
     * @param int $accountId
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("/{accountId}", name="account_delete")
     * @Method("DELETE")
     * @Security("has_role('ROLE_INTERNAL')")
     *
     * @ApiDoc(
     *     section="Account",
     *     description="Delete an Account",
     *     tags={"stable"},
     *     views={"internal"},
     *     statusCodes={
     *         204="Returned when account is deleted",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function deleteAction(int $accountId): View
    {
        /* @var Account $account */
        $account = $this->getEntityFinder()->find(['id' => (int) $accountId]);
        $this->checkAccount($accountId);
        $this->delete($account);

        return $this->view(null, 204);
    }
}

<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Controller\AdZone;

use FOS\RestBundle\View\View;
use Itsup\Bundle\ApiBundle\ApiError;
use Itsup\Bundle\ApiBundle\Controller\AbstractEntityController;
use Itsup\Bundle\ApiBundle\Entity\AbstractEntity;
use Itsup\Bundle\ApiBundle\Entity\AdZone\Accounting;
use Itsup\Bundle\ApiBundle\Exception\ApiException;
use Itsup\Bundle\ApiBundle\Form\Type\AdZone\AccountingType;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author Martin Karadzinov <martin@myhost.mk>
 * @Route("/adzone/accounting")
 */
class AccountingController extends AbstractEntityController
{
    protected $type = 'ad_zone.accounting';

    protected $accountSearch = false;

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     * @Route("", name="ad_zone_accounting_find")
     * @Method("GET")
     * @Security("has_role('ROLE_INTERNAL')")
     *
     * @ApiDoc(
     *     section="AdZone Accounting",
     *     description="Fetch Accounting information, at least one of the parameters must be passed",
     *     tags={"stable"},
     *     views={"internal"},
     *     parameters={
     *         {"name"="id", "dataType"="string", "required"=false, "description"="id"},
     *     }
     * )
     */
    public function findAction(Request $request): View
    {
        $context = $this->getContext($request, 'details');

        return $this->setContext($context, $this->view($this->find($request)));
    }

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     * @Route("/all", name="ad_zone_accounting_find_all")
     * @Method("GET")
     * @Security("has_role('ROLE_INTERNAL')")
     *
     * @ApiDoc(
     *     section="AdZone Accounting",
     *     description="Fetch many Accounting information",
     *     tags={"stable"},
     *     views={"internal"},
     *     parameters={
     *         {"name"="id", "dataType"="string", "required"=false, "description"="id"},
     *         {"name"="from", "dataType"="string", "required"=false, "description"="from"},
     *         {"name"="to", "dataType"="string", "required"=false, "description"="to"},
     *         {"name"="type", "dataType"="string", "required"=false, "description"="type"},
     *         {"name"="cost", "dataType"="float", "required"=false, "description"="cost"},
     *     }
     * )
     */
    public function findAllAction(Request $request): View
    {
        $context = $this->getContext($request, 'list');

        return $this->setContext($context, $this->view($this->findAll($request)));
    }

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("", name="ad_zone_accounting_create")
     * @Method("POST")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     *
     * @ApiDoc(
     *     section="AdZone Accounting",
     *     description="Create a new Accounting",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     input="Itsup\Bundle\ApiBundle\Form\Type\AdZone\AccountingType",
     *     output="Itsup\Bundle\ApiBundle\Entity\AdZone\Accounting",
     *     statusCodes={
     *         201="Returned when Accounting is created",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function createAction(Request $request): View
    {
        $accounting = new Accounting();
        $this->processForm($request, $accounting, AccountingType::class);

        $this->checkAccount($accounting->getAdZone()->getAccount()->getId());

        $this->save($accounting);

        return $this->view($accounting, 201);
    }

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("/update", name="ad_zone_accounting_update")
     * @Method("POST")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     *
     * @ApiDoc(
     *     section="AdZone Accounting",
     *     description="Update an existing Accounting",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     input="Itsup\Bundle\ApiBundle\Form\Type\AdZone\AccountingType",
     *     output="Itsup\Bundle\ApiBundle\Entity\AdZone\Accounting",
     *     statusCodes={
     *         200="Returned when Accounting is updated",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function updateAction(Request $request): View
    {
        $accountingId = $this->getRequestProperty($request, 'id');
        if (empty($accountingId)) {
            throw new ApiException(ApiError::INVALID_REQUEST, ['Missing AccountingId']);
        }

        /* @var Accounting $accounting */
        $accounting = $this->getEntityFinder()->find(['id' => (int) $accountingId]);
        $this->processForm($request, $accounting, AccountingType::class);

        $this->checkAccount($accounting->getAdZone()->getAccount()->getId());

        $this->update($accounting);

        return $this->view($accounting, 200);
    }

    /**
     * @param int $accountingId
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("/{accountingId}", name="ad_zone_accounting_delete")
     * @Method("DELETE")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     *
     * @ApiDoc(
     *     section="AdZone Accounting",
     *     description="Delete an Accounting",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     statusCodes={
     *         204="Returned when Accounting is deleted",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function deleteAction(int $accountingId): View
    {
        /* @var Accounting $accounting */
        $accounting = $this->getEntityFinder()->find(['id' => (int) $accountingId]);

        $this->checkAccount($accounting->getAdZone()->getAccount()->getId());

        $this->delete($accounting);

        return $this->view(null, 204);
    }

    /**
     * @param AbstractEntity $entity
     *
     * @throws ApiException
     */
    public function validate(AbstractEntity &$entity)
    {
        parent::validate($entity);

        /* @var Accounting $entity */
        if ($entity->getFrom() > $entity->getTo()) {
            throw new ApiException(ApiError::INVALID_REQUEST, ['From date must be before To date']);
        }
    }
}

<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Controller\AdZone;

use Doctrine\Common\Collections\ArrayCollection;
use FOS\RestBundle\View\View;
use Itsup\Bundle\ApiBundle\ApiError;
use Itsup\Bundle\ApiBundle\Controller\AbstractEntityController;
use Itsup\Bundle\ApiBundle\Entity\AbstractEntity;
use Itsup\Bundle\ApiBundle\Entity\AdZone;
use Itsup\Bundle\ApiBundle\Entity\AdZone\AdZoneCampaign;
use Itsup\Bundle\ApiBundle\Entity\Campaign;
use Itsup\Bundle\ApiBundle\Exception\ApiException;
use Itsup\Bundle\ApiBundle\Form\Type\AdZone\AdZoneCampaignCollectionType;
use Itsup\Bundle\ApiBundle\Form\Type\AdZone\AdZoneCampaignType;
use Itsup\Bundle\ApiBundle\Model\AdZone\AdZoneCampaignCollection;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author Martin Karadzinov <martin@myhost.mk>
 * @Route("/adzone/campaign")
 */
class CampaignController extends AbstractEntityController
{
    protected $type = 'ad_zone.campaign';

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("", name="ad_zone_campaign_create")
     * @Method("POST")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     *
     * @ApiDoc(
     *     section="AdZone Campaign",
     *     description="Add a Campaign to an AdZone",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     input="Itsup\Bundle\ApiBundle\Form\Type\AdZone\AdZoneCampaignType",
     *     output="Itsup\Bundle\ApiBundle\Entity\AdZone",
     *     statusCodes={
     *         200="Returned when Campaign is added to the AdZone",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function createAction(Request $request): View
    {
        $campaign       = $this->getCampaign($request);
        $adZone         = $this->getAdZone($request);
        $adZoneCampaign = new AdZoneCampaign();
        try {
            /* @var AdZoneCampaign $adZoneCampaign */
            $adZoneCampaign = $this->getEntityFinder()->find(['campaign' => $campaign, 'adZone' => $adZone]);
        } catch (ApiException $e) {
            if ($e->getCode() !== 404) {
                throw $e;
            }
        }
        $this->processForm($request, $adZoneCampaign, AdZoneCampaignType::class);
        $this->save($adZoneCampaign);

        return $this->view($adZone, 201);
    }

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("/update", name="ad_zone_campaign_update")
     * @Method("POST")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     *
     * @ApiDoc(
     *     section="AdZone Campaign",
     *     description="Update a Campaign weight and status for an AdZone",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     input="Itsup\Bundle\ApiBundle\Form\Type\AdZone\AdZoneCampaignType",
     *     output="Itsup\Bundle\ApiBundle\Entity\AdZone",
     *     statusCodes={
     *         200="Returned when Campaign info is updated",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function updateAction(Request $request): View
    {
        $campaign       = $this->getCampaign($request);
        $adZone         = $this->getAdZone($request);
        $adZoneCampaign = new AdZoneCampaign();
        try {
            /* @var AdZoneCampaign $adZoneCampaign */
            $adZoneCampaign = $this->getEntityFinder()->find(['campaign' => $campaign, 'adZone' => $adZone]);
        } catch (ApiException $e) {
            if ($e->getCode() !== 404) {
                throw $e;
            }
        }
        $this->processForm($request, $adZoneCampaign, AdZoneCampaignType::class);
        $this->update($campaignWeight);

        return $this->view($adZone, 200);
    }

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("/bulk", name="ad_zone_campaign_create_bulk")
     * @Method("POST")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     *
     * @ApiDoc(
     *     section="AdZone Campaign",
     *     description="Bulk add Campaigns to an AdZone",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     input="Itsup\Bundle\ApiBundle\Form\Type\AdZone\AdZoneCampaignCollectionType",
     *     output="Itsup\Bundle\ApiBundle\Entity\AdZone",
     *     statusCodes={
     *         201="Returned when Campaigns are added to AdZone",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function setAction(Request $request)
    {
        $adZone                   = $this->getAdZone($request);
        $adZoneCampaignCollection = new AdZoneCampaignCollection();
        $this->processForm($request, $adZoneCampaignCollection, AdZoneCampaignCollectionType::class);

        $campaigns = $adZone->getCampaigns();
        foreach ($campaigns as $campaignWeight) {
            $this->delete($campaignWeight);
        }
        $adZone->setCampaigns(new ArrayCollection());
        foreach ($adZoneCampaignCollection->getCampaigns() as $adZoneCampaign) {
            try {
                $this->checkAccount($adZoneCampaign->getCampaign()->getAccount()->getId());
            } catch (ApiException $e) {
                continue;
            }
            $adZoneCampaign->setAdZone($adZone);
            $this->save($adZoneCampaign);
        }

        return $this->view($adZone, 200);
    }

    /**
     * @param int $adZoneId
     * @param int $campaignId
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("/{adZoneId}/{campaignId}", name="ad_zone_campaign_delete")
     * @Method("DELETE")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     *
     * @ApiDoc(
     *     section="AdZone Campaign",
     *     description="Remove a Campaign from an AdZone",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     statusCodes={
     *         204="Returned when Campaign is removed",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function removeAction(int $adZoneId, int $campaignId): View
    {
        $adZone   = $this->getAdZoneById($adZoneId);
        $campaign = $this->getCampaignById($campaignId);

        /* @var AdZoneCampaign $adZoneCampaign */
        $adZoneCampaign = $this->getEntityFinder()->find(['campaign' => $campaign, 'adZone' => $adZone]);
        $this->delete($adZoneCampaign);

        return $this->view(null, 204);
    }

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return AdZone
     */
    private function getAdZone(Request $request): AdZone
    {
        $adZoneId = $this->getRequestProperty($request, 'adZone');

        return $this->getAdZoneById((int) $adZoneId);
    }

    /**
     * @param int $adZoneId
     *
     * @return AdZone
     */
    private function getAdZoneById(int $adZoneId): AdZone
    {
        /* @var AdZone $adZone */
        $adZone   = $this->get('finder.ad_zone')->find(['id' => (int) $adZoneId]);
        $this->checkAccount($adZone->getAccount()->getId());

        return $adZone;
    }

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return Campaign
     */
    private function getCampaign(Request $request): Campaign
    {
        $campaignId = $this->getRequestProperty($request, 'campaign');

        return $this->getCampaignById((int) $campaignId);
    }

    /**
     * @param int $campaignId
     *
     * @return Campaign
     */
    private function getCampaignById(int $campaignId): Campaign
    {
        /* @var Campaign $campaign */
        $campaign   = $this->get('finder.campaign')->find(['id' => (int) $campaignId]);
        $this->checkAccount($campaign->getAccount()->getId());

        return $campaign;
    }

    /**
     * @param AbstractEntity $entity
     *
     * @throws ApiException
     */
    public function validate(AbstractEntity &$entity)
    {
        parent::validate($entity);

        if ($entity->getAdZone()->getType() === 'iframe') {
            if (!in_array($entity->getCampaign()->getType(), ['iframe', 'image'])) {
                throw new ApiException(ApiError::INVALID_REQUEST, ['Campaign must be iframe or image']);
            } else {
                if ($entity->getCampaign()->getType() === 'image') {
                    if ($entity->getAdZone()->getWidth() !== $entity->getCampaign()->getWidth() ||
                        $entity->getAdZone()->getHeight() !== $entity->getCampaign()->getHeight()
                    ) {
                        $message = 'Campaign width and height must match AdZone';
                        throw new ApiException(ApiError::INVALID_REQUEST, [$message]);
                    }
                }
            }
        }
    }
}

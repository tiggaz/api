<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Controller\AdZone;

use FOS\RestBundle\View\View;
use Itsup\Bundle\ApiBundle\ApiError;
use Itsup\Bundle\ApiBundle\Controller\AbstractEntityController;
use Itsup\Bundle\ApiBundle\Entity\AdZone;
use Itsup\Bundle\ApiBundle\Entity\Note;
use Itsup\Bundle\ApiBundle\Exception\ApiException;
use Itsup\Bundle\ApiBundle\Finder\NoteFinder;
use Itsup\Bundle\ApiBundle\Form\Type\NoteType;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author Martin Karadzinov <martin@myhost.mk>
 * @Route("/adzone/{adZoneId}/note")
 */
class NoteController extends AbstractEntityController
{
    protected $type = 'ad_zone';

    /**
     * @param Request $request
     * @param int     $adZoneId
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("", name="adzone_note_add")
     * @Method("POST")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     * @ApiDoc(
     *     section="AdZone Note",
     *     description="Add a Note to an AdZone",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     input="Itsup\Bundle\ApiBundle\Form\Type\NoteType",
     *     output="Itsup\Bundle\ApiBundle\Entity\AdZone",
     *     statusCodes={
     *         200="Returned when Note is added",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function addAction(Request $request, int $adZoneId): View
    {
        if (empty($adZoneId)) {
            throw new ApiException(ApiError::INVALID_REQUEST, ['Missing AdZoneId']);
        }
        $note = new Note();
        $this->processForm($request, $note, NoteType::class);

        /* @var AdZone $adZone */
        $adZone  = $this->getEntityFinder()->find(['id' => (int) $adZoneId]);
        $account = $adZone->getAccount();
        $this->checkAccount($account->getId());
        $note->setDate(new \DateTime());
        $note->setUser($this->getRequestedUser($request));
        $adZone->addNote($note);
        $this->update($adZone);

        return $this->view($adZone, 200);
    }

    /**
     * @param int $adZoneId
     * @param int $noteId
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("/{noteId}", name="adzone_note_remove")
     * @Method("DELETE")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     * @ApiDoc(
     *     section="AdZone Note",
     *     description="Remove a Note from an AdZone",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     statusCodes={
     *         204="Returned when the Note is removed",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function removeAction(int $adZoneId, int $noteId): View
    {
        if (empty($adZoneId)) {
            throw new ApiException(ApiError::INVALID_REQUEST, ['Missing AdZoneId']);
        }
        if (empty($noteId)) {
            throw new ApiException(ApiError::INVALID_REQUEST, ['Missing noteId']);
        }

        /* @var AdZone $adZone */
        $adZone  = $this->getEntityFinder()->find(['id' => (int) $adZoneId]);
        $account = $adZone->getAccount();
        $this->checkAccount($account->getId());

        /* @var NoteFinder $finder */
        $finder = $this->get('finder.note');

        /* @var Note $note */
        $note   = $finder->find(['id' => (int) $noteId]);
        $adZone->removeNote($note);
        $this->update($adZone);

        return $this->view(null, 204);
    }
}

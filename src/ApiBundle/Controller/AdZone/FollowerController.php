<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Controller\AdZone;

use FOS\RestBundle\View\View;
use Itsup\Bundle\ApiBundle\ApiError;
use Itsup\Bundle\ApiBundle\Controller\AbstractEntityController;
use Itsup\Bundle\ApiBundle\Entity\AdZone;
use Itsup\Bundle\ApiBundle\Entity\User;
use Itsup\Bundle\ApiBundle\Exception\ApiException;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @author Martin Karadzinov <martin@myhost.mk>
 * @Route("/adzone/{adZoneId}/follower")
 */
class FollowerController extends AbstractEntityController
{
    protected $type = 'ad_zone';

    /**
     * @param int $adZoneId
     * @param int $userId
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("/{userId}", name="adzone_follower_add")
     * @Method("POST")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     * @ApiDoc(
     *     section="AdZone Follower",
     *     description="Add a Follower to an AdZone",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     output="Itsup\Bundle\ApiBundle\Entity\AdZone",
     *     statusCodes={
     *         200="Returned when Follower is added",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function addAction(int $adZoneId, int $userId): View
    {
        if (empty($adZoneId)) {
            throw new ApiException(ApiError::INVALID_REQUEST, ['Missing AdZoneId']);
        }

        if (empty($userId)) {
            throw new ApiException(ApiError::INVALID_REQUEST, ['Missing userId']);
        }

        /* @var AdZone $adZone */
        $adZone  = $this->getEntityFinder()->find(['id' => (int) $adZoneId]);
        /* @var User $user */
        $user    = $this->get('finder.user')->find(['id' => (int) $userId]);
        $account = $adZone->getAccount();
        $this->checkAccount($account->getId());
        $adZone->addFollower($user);
        $this->update($adZone);

        return $this->view($adZone, 200);
    }

    /**
     * @param int $adZoneId
     * @param int $userId
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("/{userId}", name="adzone_follower_remove")
     * @Method("DELETE")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     * @ApiDoc(
     *     section="AdZone Follower",
     *     description="Remove a Follower from an AdZone",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     statusCodes={
     *         204="Returned when the Follower is removed",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function removeAction(int $adZoneId, int $userId): View
    {
        if (empty($adZoneId)) {
            throw new ApiException(ApiError::INVALID_REQUEST, ['Missing AdZoneId']);
        }

        if (empty($userId)) {
            throw new ApiException(ApiError::INVALID_REQUEST, ['Missing userId']);
        }

        /* @var AdZone $adZone */
        $adZone  = $this->getEntityFinder()->find(['id' => (int) $adZoneId]);
        /* @var User $user */
        $user    = $this->get('finder.user')->find(['id' => (int) $userId]);
        $account = $adZone->getAccount();
        $this->checkAccount($account->getId());
        $adZone->removeFollower($user);
        $this->update($adZone);

        return $this->view(null, 204);
    }
}

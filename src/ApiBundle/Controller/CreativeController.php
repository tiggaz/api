<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Controller;

use FOS\RestBundle\View\View;
use Itsup\Bundle\ApiBundle\ApiError;
use Itsup\Bundle\ApiBundle\Entity\AbstractEntity;
use Itsup\Bundle\ApiBundle\Entity\Creative;
use Itsup\Bundle\ApiBundle\Exception\ApiException;
use Itsup\Bundle\ApiBundle\Form\Type\CreativeType;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author Martin Karadzinov <martin@myhost.mk>
 * @Route("/creative")
 */
class CreativeController extends AbstractTagController
{
    protected $type = 'creative';

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     * @Route("", name="creative_find")
     * @Method("GET")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     *
     * @ApiDoc(
     *     section="Creative",
     *     description="Fetch Creative information, at least one of the parameters must be passed",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     parameters={
     *         {"name"="id", "dataType"="string", "required"=false, "description"="id"},
     *         {"name"="name", "dataType"="string", "required"=false, "description"="name"},
     *     }
     * )
     */
    public function findAction(Request $request): View
    {
        $context = $this->getContext($request, 'details');

        return $this->setContext($context, $this->view($this->find($request)));
    }

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     * @Route("/all", name="creative_find_all")
     * @Method("GET")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     * @ApiDoc(
     *     section="Creative",
     *     description="Fetch many Creative information",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     parameters={
     *         {"name"="id", "dataType"="string", "required"=false, "description"="id"},
     *         {"name"="name", "dataType"="string", "required"=false, "description"="name"},
     *         {"name"="type", "dataType"="string", "required"=false, "description"="type"},
     *         {"name"="width", "dataType"="integer", "required"=false, "description"="width"},
     *         {"name"="height", "dataType"="integer", "required"=false, "description"="height"},
     *         {"name"="code", "dataType"="string", "required"=false, "description"="code"},
     *         {"name"="status", "dataType"="string", "required"=false, "description"="status"},
     *         {"name"="size", "dataType"="integer", "required"=false, "description"="size"},
     *         {"name"="dateCreated", "dataType"="string", "required"=false, "description"="date"},
     *     }
     * )
     */
    public function findAllAction(Request $request): View
    {
        $context = $this->getContext($request, 'list');

        return $this->setContext($context, $this->view($this->findAll($request)));
    }

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("", name="creative_create")
     * @Method("POST")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     * @ApiDoc(
     *     section="Creative",
     *     description="Create a new Creative",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     input="Itsup\Bundle\ApiBundle\Form\Type\CreativeType",
     *     output="Itsup\Bundle\ApiBundle\Entity\Creative",
     *     statusCodes={
     *         201="Returned when contact is created",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function createAction(Request $request): View
    {
        $account  = $this->getAccount($request);
        $creative = new Creative();

        $this->processForm($request, $creative, CreativeType::class);
        $creativeTags = $this->processTags($creative->getTags(), $account);
        $creative->setTags($creativeTags);

        $creative->setAccount($account);
        $this->save($creative);

        return $this->view($creative, 201);
    }

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("/update", name="creative_update")
     * @Method("POST")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     *
     * @ApiDoc(
     *     section="Creative",
     *     description="Update an existing Creative",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     input="Itsup\Bundle\ApiBundle\Form\Type\CreativeType",
     *     output="Itsup\Bundle\ApiBundle\Entity\Creative",
     *     statusCodes={
     *         200="Returned when Creative is updated",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function updateAction(Request $request): View
    {
        $account    = $this->getAccount($request);
        $creativeId = $this->getRequestProperty($request, 'id');

        if (empty($creativeId)) {
            throw new ApiException(ApiError::INVALID_REQUEST, ['Missing CreativeId']);
        }

        /* @var Creative $creative */
        $creative = $this->getEntityFinder()->find(['id' => (int) $creativeId, 'account' => $account]);
        $creative->resetTags();
        $this->processForm($request, $creative, CreativeType::class);
        $creativeTags = $this->processTags($creative->getTags(), $account);
        $creative->setTags($creativeTags);

        $this->update($creative);

        return $this->view($creative, 200);
    }

    /**
     * @param int $creativeId
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("/{creativeId}", name="creative_delete")
     * @Method("DELETE")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     *
     * @ApiDoc(
     *     section="Creative",
     *     description="Delete an Creative",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     statusCodes={
     *         204="Returned when contact is deleted",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function deleteAction(int $creativeId): View
    {
        /* @var Creative $creative */
        $creative = $this->getEntityFinder()->find(['id' => (int) $creativeId]);
        $this->checkAccount($creative->getAccount()->getId());
        $this->delete($creative);

        return $this->view(null, 204);
    }

    /**
     * @param AbstractEntity $entity
     *
     * @throws ApiException
     */
    public function validate(AbstractEntity &$entity)
    {
        parent::validate($entity);
        if ($entity->getType() == 'iframe') {
            if ($entity->getWidth() === 0 || $entity->getHeight() === 0) {
                throw new ApiException(ApiError::INVALID_REQUEST, ['Width and Height must be set']);
            }
        }
    }
}

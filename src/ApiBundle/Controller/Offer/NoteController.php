<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Controller\Offer;

use FOS\RestBundle\View\View;
use Itsup\Bundle\ApiBundle\ApiError;
use Itsup\Bundle\ApiBundle\Controller\AbstractEntityController;
use Itsup\Bundle\ApiBundle\Entity\Note;
use Itsup\Bundle\ApiBundle\Entity\Offer;
use Itsup\Bundle\ApiBundle\Exception\ApiException;
use Itsup\Bundle\ApiBundle\Finder\NoteFinder;
use Itsup\Bundle\ApiBundle\Form\Type\NoteType;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author Martin Karadzinov <martin@myhost.mk>
 * @Route("/offer/{offerId}/note")
 */
class NoteController extends AbstractEntityController
{
    protected $type = 'offer';

    /**
     * @param Request $request
     * @param int     $offerId
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("", name="offer_note_add")
     * @Method("POST")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     * @ApiDoc(
     *     section="Offer Note",
     *     description="Add a Note to an Offer",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     input="Itsup\Bundle\ApiBundle\Form\Type\NoteType",
     *     output="Itsup\Bundle\ApiBundle\Entity\Offer",
     *     statusCodes={
     *         200="Returned when Note is added",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function addAction(Request $request, int $offerId): View
    {
        if (empty($offerId)) {
            throw new ApiException(ApiError::INVALID_REQUEST, ['Missing OfferId']);
        }
        $note = new Note();
        $this->processForm($request, $note, NoteType::class);

        /* @var Offer $offer */
        $offer   = $this->getEntityFinder()->find(['id' => (int) $offerId]);
        $account = $offer->getAccount();
        $this->checkAccount($account->getId());
        $note->setDate(new \DateTime());
        $note->setUser($this->getRequestedUser($request));
        $offer->addNote($note);
        $this->save($offer);

        return $this->view($offer, 200);
    }

    /**
     * @param int $offerId
     * @param int $noteId
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("/{noteId}", name="offer_note_remove")
     * @Method("DELETE")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     * @ApiDoc(
     *     section="Offer Note",
     *     description="Remove a Note from an Offer",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     statusCodes={
     *         204="Returned when the Note is removed",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function removeAction(int $offerId, int $noteId): View
    {
        if (empty($offerId)) {
            throw new ApiException(ApiError::INVALID_REQUEST, ['Missing OfferId']);
        }
        if (empty($noteId)) {
            throw new ApiException(ApiError::INVALID_REQUEST, ['Missing noteId']);
        }

        /* @var Offer $offer */
        $offer   = $this->getEntityFinder()->find(['id' => (int) $offerId]);
        $account = $offer->getAccount();
        $this->checkAccount($account->getId());

        /* @var NoteFinder $finder */
        $finder  = $this->get('finder.note');

        /* @var Note $note */
        $note    = $finder->find(['id' => (int) $noteId]);
        $offer->removeNote($note);
        $this->save($offer);

        return $this->view(null, 204);
    }
}

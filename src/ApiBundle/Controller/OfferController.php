<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Controller;

use FOS\RestBundle\View\View;
use Itsup\Bundle\ApiBundle\ApiError;
use Itsup\Bundle\ApiBundle\Entity\Offer;
use Itsup\Bundle\ApiBundle\Exception\ApiException;
use Itsup\Bundle\ApiBundle\Form\Type\OfferType;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author Martin Karadzinov <martin@myhost.mk>
 * @Route("/offer")
 */
class OfferController extends AbstractTagController
{
    protected $type = 'offer';

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     * @Route("", name="offer_find")
     * @Method("GET")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     *
     * @ApiDoc(
     *     section="Offer",
     *     description="Fetch Offer information, at least one of the parameters must be passed",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     parameters={
     *         {"name"="id", "dataType"="string", "required"=false, "description"="id"},
     *         {"name"="name", "dataType"="string", "required"=false, "description"="name"},
     *     }
     * )
     */
    public function findAction(Request $request): View
    {
        $context = $this->getContext($request, 'details');

        return $this->setContext($context, $this->view($this->find($request)));
    }

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     * @Route("/all", name="offer_find_all")
     * @Method("GET")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     * @ApiDoc(
     *     section="Offer",
     *     description="Fetch many Offer information",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     parameters={
     *         {"name"="id", "dataType"="string", "required"=false, "description"="id"},
     *         {"name"="name", "dataType"="string", "required"=false, "description"="name"},
     *         {"name"="url", "dataType"="string", "required"=false, "description"="url"},
     *         {"name"="payout_type", "dataType"="string", "required"=false, "description"="payout_type"},
     *         {"name"="default_value", "dataType"="string", "required"=false, "description"="default_value"},
     *         {"name"="statistics_url", "dataType"="string", "required"=false, "description"="statistics_url"},
     *         {"name"="status", "dataType"="string", "required"=false, "description"="status"},
     *     }
     * )
     */
    public function findAllAction(Request $request): View
    {
        $context = $this->getContext($request, 'list');

        return $this->setContext($context, $this->view($this->findAll($request)));
    }

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("", name="offer_create")
     * @Method("POST")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     * @ApiDoc(
     *     section="Offer",
     *     description="Create a new Offer",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     input="Itsup\Bundle\ApiBundle\Form\Type\OfferType",
     *     output="Itsup\Bundle\ApiBundle\Entity\Offer",
     *     statusCodes={
     *         201="Returned when contact is created",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function createAction(Request $request): View
    {
        $account = $this->getAccount($request);
        $offer   = new Offer();
        $this->processForm($request, $offer, OfferType::class);

        $offerTags = $this->processTags($offer->getTags(), $account);
        $offer->setTags($offerTags);

        $offer->setAccount($account);
        $this->save($offer);

        return $this->view($offer, 201);
    }

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("/update", name="offer_update")
     * @Method("POST")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     *
     * @ApiDoc(
     *     section="Offer",
     *     description="Update an existing Offer",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     input="Itsup\Bundle\ApiBundle\Form\Type\OfferType",
     *     output="Itsup\Bundle\ApiBundle\Entity\Offer",
     *     statusCodes={
     *         200="Returned when offer is updated",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function updateAction(Request $request): View
    {
        $account   = $this->getAccount($request);
        $offerId   = $this->getRequestProperty($request, 'id');

        if (empty($offerId)) {
            throw new ApiException(ApiError::INVALID_REQUEST, ['Missing OfferId']);
        }

        /* @var Offer $offer */
        $offer = $this->getEntityFinder()->find(['id' => (int) $offerId, 'account' => $account]);
        $offer->resetTags();
        $this->processForm($request, $offer, OfferType::class);
        $offerTags = $this->processTags($offer->getTags(), $account);
        $offer->setTags($offerTags);

        $this->update($offer);

        return $this->view($offer, 200);
    }

    /**
     * @param int $offerId
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("/{offerId}", name="offer_delete")
     * @Method("DELETE")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     *
     * @ApiDoc(
     *     section="Offer",
     *     description="Delete an Offer",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     statusCodes={
     *         204="Returned when contact is deleted",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function deleteAction(int $offerId): View
    {
        /* @var Offer $offer */
        $offer = $this->getEntityFinder()->find(['id' => (int) $offerId]);
        $this->checkAccount($offer->getAccount()->getId());
        $this->delete($offer);

        return $this->view(null, 204);
    }
}

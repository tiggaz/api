<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Controller;

use FOS\RestBundle\View\View;
use Itsup\Bundle\ApiBundle\ApiError;
use Itsup\Bundle\ApiBundle\Entity\AbstractEntity;
use Itsup\Bundle\ApiBundle\Entity\Campaign;
use Itsup\Bundle\ApiBundle\Exception\ApiException;
use Itsup\Bundle\ApiBundle\Form\Type\CampaignType;
use Itsup\Bundle\ApiBundle\Manager\Campaign\CampaignCreativeManager;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author Martin Karadzinov <martin@myhost.mk>
 * @Route("/campaign")
 */
class CampaignController extends AbstractTagController
{
    protected $type = 'campaign';

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     * @Route("", name="campaign_find")
     * @Method("GET")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     *
     * @ApiDoc(
     *     section="Campaign",
     *     description="Fetch Campaign information, at least one of the parameters must be passed",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     parameters={
     *         {"name"="id", "dataType"="string", "required"=false, "description"="id"},
     *         {"name"="name", "dataType"="string", "required"=false, "description"="name"},
     *     }
     * )
     */
    public function findAction(Request $request): View
    {
        $context = $this->getContext($request, 'details');

        return $this->setContext($context, $this->view($this->find($request)));
    }

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     * @Route("/all", name="campaign_find_all")
     * @Method("GET")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     *
     * @ApiDoc(
     *     section="Campaign",
     *     description="Fetch many Campaign information",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     parameters={
     *         {"name"="id", "dataType"="string", "required"=false, "description"="id"},
     *         {"name"="name", "dataType"="string", "required"=false, "description"="name"},
     *     }
     * )
     */
    public function findAllAction(Request $request): View
    {
        $context = $this->getContext($request, 'list');

        return $this->setContext($context, $this->view($this->findAll($request)));
    }

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("", name="campaign_create")
     * @Method("POST")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     *
     * @ApiDoc(
     *     section="Campaign",
     *     description="Create a new Campaign",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     input="Itsup\Bundle\ApiBundle\Form\Type\CampaignType",
     *     output="Itsup\Bundle\ApiBundle\Entity\Campaign",
     *     statusCodes={
     *         201="Returned when Campaign is created",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function createAction(Request $request): View
    {
        $account  = $this->getAccount($request);
        $user     = $this->getRequestedUser($request, 'createdBy');
        $campaign = new Campaign();
        $this->processForm($request, $campaign, CampaignType::class);
        $campaignTags = $this->processTags($campaign->getTags(), $account);
        $campaign->setTags($campaignTags);
        $campaign->setCreatedBy($user);
        $campaign->setDateCreated(new \DateTime());
        $campaign->setUpdatedBy($user);
        $campaign->setDateUpdated(new \DateTime());
        $campaign->setAccount($account);
        $this->save($campaign);

        return $this->view($campaign, 201);
    }

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("/update", name="campaign_update")
     * @Method("POST")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     *
     * @ApiDoc(
     *     section="Campaign",
     *     description="Update an existing Campaign",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     input="Itsup\Bundle\ApiBundle\Form\Type\CampaignType",
     *     output="Itsup\Bundle\ApiBundle\Entity\Campaign",
     *     statusCodes={
     *         200="Returned when Campaign is updated",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function updateAction(Request $request): View
    {
        $account    = $this->getAccount($request);
        $user       = $this->getRequestedUser($request, 'updatedBy');
        $campaignId = $this->getRequestProperty($request, 'id');
        if (empty($campaignId)) {
            throw new ApiException(ApiError::INVALID_REQUEST, ['Missing campaignId']);
        }
        /* @var Campaign $campaign */
        $campaign = $this->getEntityFinder()->find(['id' => (int) $campaignId, 'account' => $account]);
        $campaign->resetTags();
        $this->processForm($request, $campaign, CampaignType::class);
        $this->checkAccount($campaign->getAccount()->getId());
        $this->processLimits($campaign);

        $campaignTags = $this->processTags($campaign->getTags(), $campaign->getAccount());
        $campaign->setTags($campaignTags);
        $campaign->setUpdatedBy($user);
        $campaign->setDateUpdated(new \DateTime());
        $this->update($campaign);

        return $this->view($campaign, 200);
    }

    /**
     * @param int    $campaignId
     * @param string $type
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("/{campaignId}/duplicate/{type}", name="campaign_duplicate")
     * @Method("POST")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     *
     * @ApiDoc(
     *     section="Campaign",
     *     description="Duplicate an existing Campaign",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     output="Itsup\Bundle\ApiBundle\Entity\Campaign",
     *     statusCodes={
     *         201="Returned when Campaign is duplicated",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function duplicateAction(int $campaignId, string $type): View
    {
        if (empty($campaignId)) {
            throw new ApiException(ApiError::INVALID_REQUEST, ['Missing CampaignId']);
        }

        /* @var Campaign $campaign */
        $campaign  = $this->getEntityFinder()->find(['id' => (int) $campaignId]);
        $this->checkAccount($campaign->getAccount()->getId());
        $user = $this->isGranted('ROLE_INTERNAL') ? $campaign->getCreatedBy() : $this->getUser();

        /* @var Campaign $newCampaign */
        $newCampaign = $this->getEntityManager()->clone($campaign);
        $newCampaign->setCreatedBy($user);
        $newCampaign->setUpdatedBy($user);

        if ($type === 'creatives') {
            /* @var CampaignCreativeManager $manager */
            $manager = $this->get('manager.campaign.creative');
            foreach ($campaign->getCreatives() as $creative) {
                /* @var Campaign\CampaignCreative $newCreative */
                $newCreative = $manager->clone($creative);
                $newCampaign->addCreative($newCreative);
            }
        }
        $this->save($newCampaign);

        return $this->view($newCampaign, 201);
    }

    /**
     * @param int $campaignId
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("/{campaignId}", name="campaign_delete")
     * @Method("DELETE")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     *
     * @ApiDoc(
     *     section="Campaign",
     *     description="Delete an Campaign",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     statusCodes={
     *         204="Returned when Campaign is deleted",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function deleteAction(int $campaignId): View
    {
        /* @var Campaign $campaign */
        $campaign = $this->getEntityFinder()->find(['id' => (int) $campaignId]);
        $this->checkAccount($campaign->getAccount()->getId());
        $this->delete($campaign);

        return $this->view(null, 204);
    }

    /**
     * @param AbstractEntity $entity
     *
     * @throws ApiException
     */
    public function validate(AbstractEntity &$entity)
    {
        parent::validate($entity);

        /* @var Campaign $entity */
        if ($entity->getType() == 'iframe') {
            if ($entity->getWidth() === 0 || $entity->getHeight() === 0) {
                throw new ApiException(ApiError::INVALID_REQUEST, ['Width and Height must be set']);
            }

            foreach ($entity->getCreatives() as $creative) {
                if (!in_array($creative->getCreative()->getType(), ['iframe', 'image'])) {
                    throw new ApiException(ApiError::INVALID_REQUEST, ['Creative must be iframe or image']);
                } else {
                    if ($creative->getCreative()->getType() === 'image') {
                        if ($entity->getWidth() !== $creative->getCreative()->getWidth() ||
                            $entity->getHeight() !== $creative->getCreative()->getHeight()
                        ) {
                            $message = 'Creative width and height must match Campaign';
                            throw new ApiException(ApiError::INVALID_REQUEST, [$message]);
                        }
                    }
                }
            }
        }

        if ($entity->getRevenue() === 'conversion' && empty($entity->getOffer())) {
            throw new ApiException(ApiError::INVALID_REQUEST, ['Offer must be set when revenue is set to conversion']);
        }
    }

    /**
     * @param Campaign $campaign
     */
    private function processLimits(Campaign &$campaign)
    {
        $limits = $campaign->getLimits();

        foreach ($limits as $limit) {
            if (is_null($limit->getDateReached()) && $limit->getTo() > date('Y-m-d')) {
                $limit->setDateReached(date('Y-m-d'));
            }
        }
    }
}

<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\PersistentCollection;
use Itsup\Bundle\ApiBundle\Entity\Account;
use Itsup\Bundle\ApiBundle\Entity\Tag;

/**
 * @author Martin Karadzinov <martin@myhost.mk>
 */
class AbstractTagController extends AbstractEntityController
{
    /**
     * @param ArrayCollection|PersistentCollection $tags
     * @param Account                              $account
     *
     * @return ArrayCollection
     */
    public function processTags($tags, Account $account): ArrayCollection
    {
        $accountTags = $this->get('finder.tag')->findAll(['account' => $account]);
        $tmpName     = [];
        $tmpId       = [];
        /* @var Tag $tag */
        foreach ($accountTags as $tag) {
            $tmpName[$tag->getName()] = $tag;
            $tmpId[$tag->getId()]     = $tag;
        }

        $return = new ArrayCollection();

        foreach ($tags as $one) {
            if (isset($tmpId[$one->getId()])) {
                $newTag = $tmpId[$one->getId()];
            } elseif (isset($tmpName[$one->getName()])) {
                $newTag = $tmpName[$one->getName()];
            } else {
                $newTag = new Tag();
                $newTag->setName($one->getName());
                $newTag->setAccount($account);
            }

            $return->add($newTag);
        }

        return $return;
    }
}

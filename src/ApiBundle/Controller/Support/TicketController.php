<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Controller\Support;

use FOS\RestBundle\View\View;
use Itsup\Bundle\ApiBundle\Controller\AbstractController;
use Itsup\Bundle\ApiBundle\Exception\ApiException;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author Martin Karadzinov <martin@myhost.mk>
 * @Route("/support/ticket")
 */
class TicketController extends AbstractController
{
    protected $type = 'ticket';

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     * @Route("", name="ticket_find")
     * @Method("GET")
     * @Security("has_role('ROLE_INTERNAL')")
     *
     * @ApiDoc(
     *     section="Support Ticket",
     *     description="Fetch Ticket information, at least one of the parameters must be passed",
     *     tags={"stable"},
     *     views={"internal"},
     *     parameters={
     *         {"name"="id", "dataType"="string", "required"=true, "description"="id"},
     *     }
     * )
     */
    public function findAction(Request $request): View
    {
        $ticket = $this->get('zendesk')->getInstance()->find(['id' => $request->query->get('id')]);

        $this->checkAccount($ticket->ticket->custom_fields[0]->value);

        $ticketLogs = $this->get('zendesk')->tickets()->audits()->findAll(['ticket_id' => $request->query->get('id')]);
        $messages   = [];

        foreach ($ticketLogs->audits as $logEntry) {
            foreach ($logEntry->events as $event) {
                if ($event->type === 'Comment') {
                    $messages[] = [
                        'body' => $event->body,
                        'ts'   => $logEntry->created_at,
                    ];
                }
            }
        }

        $ticketStatus = (string) array_search($ticket->ticket->status, $this->getParameter('zendesk')['statuses']);

        $context = $this->getContext($request, 'details');

        return $this->setContext(
            $context,
            $this->view([
                'ticketId' => $request->query->get('id'),
                'status'   => $ticketStatus,
                'ticket'   => $ticket,
                'messages' => $messages,
            ])
        );
    }

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     * @Route("/all", name="ticket_find_all")
     * @Method("POST")
     * @Security("has_role('ROLE_INTERNAL')")
     *
     * @ApiDoc(
     *     section="Support Ticket",
     *     description="Fetch many Ticket information",
     *     tags={"stable"},
     *     views={"internal"},
     *     parameters={
     *         {"name"="account", "dataType"="string", "required"=false, "description"="account"},
     *         {"name"="status", "dataType"="string", "required"=false, "description"="status"},
     *     }
     * )
     */
    public function findAllAction(Request $request): View
    {
        $data = $request->request->get('Ticket');

        $accountId = $this->isGranted('ROLE_INTERNAL') ?
            $request->request->get('account') : $this->getUser()->getAccount()->getId();

        $queryString  = 'type:ticket';
        $queryString .= ',fieldValue:{'.$accountId.'}';
        if (in_array($data['status'], $this->getParameter('zendesk')['statuses'])) {
            $queryString .= ',status:'.$data['status'];
        }

        $tickets = $this->get('zendesk')->getInstance()->search(['query' => $queryString]);

        $context = $this->getContext($request, 'details');

        return $this->setContext($context, $this->view($tickets));
    }

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("", name="ticket_create")
     * @Method("POST")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     *
     * @ApiDoc(
     *     section="Support Ticket",
     *     description="Create a new Ticket",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     statusCodes={
     *         201="Returned when Ticket is created",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function createAction(Request $request): View
    {
        $data = $request->request->get('Ticket');

        $comment = $data['comment'];

        if (!empty($data['campaigns'])) {
            $campaignNames = [];

            $finder = $this->get('finder.campaign');

            foreach ($data['campaigns'] as $value) {
                $campaign = $finder->find(['id' => (int) $value]);

                if ($campaign) {
                    $campaignNames[] = $campaign->getName();
                }
            }

            $comment .= '\n\nCampaigns: '.implode(',', $campaignNames);
        }

        if (!empty($data['adzones'])) {
            $adZoneNames = [];

            $finder = $this->get('finder.ad_zone');

            foreach ($data['adzones'] as $value) {
                $adZone = $finder->find(['id' => (int) $value]);

                if ($adZone) {
                    $adZoneNames[] = $adZone->getName();
                }
            }

            $comment .= '\n\nAdzones: '.implode(',', $adZoneNames);
        }

        $this->checkAccount($this->getUser()->getAccount()->getId());

        $ticketData = [
            'subject'       => $data['subject'],
            'requester'     => [
                'name'  => $this->getUser()->getName(),
                'email' => $this->getUser()->getEmail(),
            ],
            'comment'       => [
                'body' => $comment,
            ],
            'priority'      => 'normal',
        ];

        $custom_fields = [];

        foreach ($this->getParameter('zendesk')['ids'] as $zenDeskId) {
            $custom_fields[] = ['id' => $zenDeskId, 'value' => $this->getUser()->getAccount()->getId()];
        }

        $ticketData['custom_fields'] = $custom_fields;

        $ticket = $this->get('zendesk')->getInstance()->tickets()->create($ticketData);

        return $this->view($ticket, 201);
    }

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("/update", name="ticket_update")
     * @Method("POST")
     * @Security("has_role('ROLE_INTERNAL')")
     *
     * @ApiDoc(
     *     section="Support Ticket",
     *     description="Update an existing Ticket",
     *     tags={"stable"},
     *     views={"internal"},
     *     statusCodes={
     *         200="Returned when Ticket is updated",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function updateAction(Request $request): View
    {
        $user        = $this->getUser();
        $zenDeskUser = $this->get('zendesk')->getInstance()->users()->search([
            'query' => 'email:{'.$user->getEmail().'}',
        ]);

        if (empty($zenDeskUser->users)) {
            $zenDeskUser   = $this->get('zendesk')->getInstance()->users()->create(
                [
                    'name'     => $user->getName().'test',
                    'email'    => $user->getEmail().'test',
                    'verified' => true,
                ]
            );
            $zenDeskUserId = $zenDeskUser->user->id;
        } else {
            $zenDeskUserId = $zenDeskUser->users[0]->id;
        }

        $ticketStatus = 'open';

        if ($request->request->get('status') === 'closed') {
            // not possible to change status for a closed ticket
            $ticketStatus = 'closed';
        }

        $ticketData = [
            'id'      => $request->request->get('ticketId'),
            'comment' => [
                'body'      => $request->request->get('comment'),
                'author_id' => $zenDeskUserId,
            ],
            'status'  => $ticketStatus,
        ];

        $this->get('zendesk')->getInstance()->update($ticketData);

        return $this->view(['success' => 'true'], 201);
    }
}

<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Controller\Metrics;

use FOS\RestBundle\View\View;
use Itsup\Bundle\ApiBundle\ApiError;
use Itsup\Bundle\ApiBundle\Controller\AbstractEntityController;
use Itsup\Bundle\ApiBundle\Entity\Metrics\OperatingSystem;
use Itsup\Bundle\ApiBundle\Exception\ApiException;
use Itsup\Bundle\ApiBundle\Form\Type\Metrics\OperatingSystemType;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author Martin Karadzinov <martin@myhost.mk>
 * @Route("/metrics/os")
 */
class OperatingSystemController extends AbstractEntityController
{
    protected $type = 'metrics.os';

    protected $accountSearch = false;

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     * @Route("", name="metrics_os_find")
     * @Method("GET")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     *
     * @ApiDoc(
     *     section="Metrics OS",
     *     description="Fetch OperatingSystem Metric information, at least one of the parameters must be passed",
     *     tags={"stable"},
     *     views={"internal"},
     *     parameters={
     *         {"name"="id", "dataType"="string", "required"=false, "description"="id"},
     *         {"name"="name", "dataType"="string", "required"=false, "description"="name"},
     *     }
     * )
     */
    public function findAction(Request $request): View
    {
        $context = $this->getContext($request, 'details');

        return $this->setContext($context, $this->view($this->find($request)));
    }

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     * @Route("/all", name="metrics_os_find_all")
     * @Method("GET")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     *
     * @ApiDoc(
     *     section="Metrics OS",
     *     description="Fetch many Countries information",
     *     tags={"stable"},
     *     views={"internal"},
     *     parameters={
     *         {"name"="id", "dataType"="string", "required"=false, "description"="id"},
     *         {"name"="name", "dataType"="string", "required"=false, "description"="name"},
     *     }
     * )
     */
    public function findAllAction(Request $request): View
    {
        $context = $this->getContext($request, 'list');

        return $this->setContext($context, $this->view($this->findAll($request)));
    }

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("", name="metrics_os_create")
     * @Method("POST")
     * @Security("has_role('ROLE_INTERNAL')")
     *
     * @ApiDoc(
     *     section="Metrics OS",
     *     description="Create a new OperatingSystem",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     input="Itsup\Bundle\ApiBundle\Form\Type\Metrics\OperatingSystemType",
     *     output="Itsup\Bundle\ApiBundle\Entity\Metrics\OperatingSystem",
     *     statusCodes={
     *         201="Returned when OperatingSystem is created",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function createAction(Request $request): View
    {
        $os = new OperatingSystem();
        $this->processForm($request, $os, OperatingSystemType::class);
        $this->save($os);

        return $this->view($os, 201);
    }

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("/update", name="metrics_os_update")
     * @Method("POST")
     * @Security("has_role('ROLE_INTERNAL')")
     *
     * @ApiDoc(
     *     section="Metrics OS",
     *     description="Update an existing OperatingSystem",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     input="Itsup\Bundle\ApiBundle\Form\Type\Metrics\OperatingSystemType",
     *     output="Itsup\Bundle\ApiBundle\Entity\Metrics\OperatingSystem",
     *     statusCodes={
     *         200="Returned when OperatingSystem is updated",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function updateAction(Request $request): View
    {
        $osId = $this->getRequestProperty($request, 'id');
        if (empty($osId)) {
            throw new ApiException(ApiError::INVALID_REQUEST, ['Missing OperatingSystemId']);
        }

        /* @var OperatingSystem $os */
        $os = $this->getEntityFinder()->find(['id' => (int) $osId]);
        $this->processForm($request, $os, OperatingSystemType::class);

        $this->update($os);

        return $this->view($os, 200);
    }

    /**
     * @param int $osId
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("/{osId}", name="metrics_os_delete")
     * @Method("DELETE")
     * @Security("has_role('ROLE_INTERNAL')")
     *
     * @ApiDoc(
     *     section="Metrics OS",
     *     description="Delete an OperatingSystem",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     statusCodes={
     *         204="Returned when OperatingSystem is deleted",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function deleteAction(int $osId): View
    {
        /* @var OperatingSystem $os */
        $os = $this->getEntityFinder()->find(['id' => (int) $osId]);

        $this->delete($os);

        return $this->view(null, 204);
    }
}

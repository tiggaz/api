<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Controller\Metrics;

use FOS\RestBundle\View\View;
use Itsup\Bundle\ApiBundle\ApiError;
use Itsup\Bundle\ApiBundle\Controller\AbstractEntityController;
use Itsup\Bundle\ApiBundle\Entity\Metrics\InternetServiceProvider;
use Itsup\Bundle\ApiBundle\Exception\ApiException;
use Itsup\Bundle\ApiBundle\Form\Type\Metrics\InternetServiceProviderType;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author Martin Karadzinov <martin@myhost.mk>
 * @Route("/metrics/isp")
 */
class InternetServiceProviderController extends AbstractEntityController
{
    protected $type = 'metrics.isp';

    protected $accountSearch = false;

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     * @Route("", name="metrics_isp_find")
     * @Method("GET")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     *
     * @ApiDoc(
     *     section="Metrics ISP",
     *     description="Fetch InternetServiceProvider Metric information, one of the parameters must be passed",
     *     tags={"stable"},
     *     views={"internal"},
     *     parameters={
     *         {"name"="id", "dataType"="string", "required"=false, "description"="id"},
     *         {"name"="name", "dataType"="string", "required"=false, "description"="name"},
     *     }
     * )
     */
    public function findAction(Request $request): View
    {
        $context = $this->getContext($request, 'details');

        return $this->setContext($context, $this->view($this->find($request)));
    }

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     * @Route("/all", name="metrics_isp_find_all")
     * @Method("GET")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     *
     * @ApiDoc(
     *     section="Metrics ISP",
     *     description="Fetch many Countries information",
     *     tags={"stable"},
     *     views={"internal"},
     *     parameters={
     *         {"name"="id", "dataType"="string", "required"=false, "description"="id"},
     *         {"name"="name", "dataType"="string", "required"=false, "description"="name"},
     *     }
     * )
     */
    public function findAllAction(Request $request): View
    {
        $context = $this->getContext($request, 'list');

        return $this->setContext($context, $this->view($this->findAll($request)));
    }

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("", name="metrics_isp_create")
     * @Method("POST")
     * @Security("has_role('ROLE_INTERNAL')")
     *
     * @ApiDoc(
     *     section="Metrics ISP",
     *     description="Create a new InternetServiceProvider",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     input="Itsup\Bundle\ApiBundle\Form\Type\Metrics\InternetServiceProviderType",
     *     output="Itsup\Bundle\ApiBundle\Entity\Metrics\InternetServiceProvider",
     *     statusCodes={
     *         201="Returned when InternetServiceProvider is created",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function createAction(Request $request): View
    {
        $isp = new InternetServiceProvider();
        $this->processForm($request, $isp, InternetServiceProviderType::class);
        $this->save($isp);

        return $this->view($isp, 201);
    }

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("/update", name="metrics_isp_update")
     * @Method("POST")
     * @Security("has_role('ROLE_INTERNAL')")
     *
     * @ApiDoc(
     *     section="Metrics ISP",
     *     description="Update an existing InternetServiceProvider",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     input="Itsup\Bundle\ApiBundle\Form\Type\Metrics\InternetServiceProviderType",
     *     output="Itsup\Bundle\ApiBundle\Entity\Metrics\InternetServiceProvider",
     *     statusCodes={
     *         200="Returned when InternetServiceProvider is updated",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function updateAction(Request $request): View
    {
        $ispId = $this->getRequestProperty($request, 'id');
        if (empty($ispId)) {
            throw new ApiException(ApiError::INVALID_REQUEST, ['Missing InternetServiceProviderId']);
        }

        /* @var InternetServiceProvider $isp */
        $isp = $this->getEntityFinder()->find(['id' => (int) $ispId]);
        $this->processForm($request, $isp, InternetServiceProviderType::class);

        $this->update($isp);

        return $this->view($isp, 200);
    }

    /**
     * @param int $ispId
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("/{ispId}", name="metrics_isp_delete")
     * @Method("DELETE")
     * @Security("has_role('ROLE_INTERNAL')")
     *
     * @ApiDoc(
     *     section="Metrics ISP",
     *     description="Delete an InternetServiceProvider",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     statusCodes={
     *         204="Returned when InternetServiceProvider is deleted",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function deleteAction(int $ispId): View
    {
        /* @var InternetServiceProvider $isp */
        $isp = $this->getEntityFinder()->find(['id' => (int) $ispId]);

        $this->delete($isp);

        return $this->view(null, 204);
    }
}

<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Controller\Metrics;

use FOS\RestBundle\View\View;
use Itsup\Bundle\ApiBundle\ApiError;
use Itsup\Bundle\ApiBundle\Controller\AbstractEntityController;
use Itsup\Bundle\ApiBundle\Entity\Metrics\Language;
use Itsup\Bundle\ApiBundle\Exception\ApiException;
use Itsup\Bundle\ApiBundle\Form\Type\Metrics\LanguageType;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author Martin Karadzinov <martin.karadzinov@devsy.com>
 * @Route("/metrics/language")
 */
class LanguageController extends AbstractEntityController
{
    protected $type = 'metrics.language';

    protected $accountSearch = false;

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     * @Route("", name="metrics_language_find")
     * @Method("GET")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     *
     * @ApiDoc(
     *     section="Metrics Language",
     *     description="Fetch Language information.",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     parameters={
     *         {"name"="id", "dataType"="string", "required"=false, "description"="id"},
     *     }
     * )
     */
    public function findAction(Request $request): View
    {
        $context = $this->getContext($request, 'details');

        return $this->setContext($context, $this->view($this->find($request)));
    }

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     * @Route("/all", name="metrics_language_find_all")
     * @Method("GET")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     *
     * @ApiDoc(
     *     section="Metrics Language",
     *     description="Fetch many Language information",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     parameters={
     *         {"name"="id", "dataType"="string", "required"=false, "description"="id"},
     *     }
     * )
     */
    public function findAllAction(Request $request): View
    {
        $context = $this->getContext($request, 'list');

        return $this->setContext($context, $this->view($this->findAll($request)));
    }

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("", name="metrics_language_create")
     * @Method("POST")
     * @Security("has_role('ROLE_INTERNAL')")
     *
     * @ApiDoc(
     *     section="Metrics Language",
     *     description="Add a Language",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     input="Itsup\Bundle\ApiBundle\Form\Type\Metrics\Language",
     *     output="Itsup\Bundle\ApiBundle\Form\Type\Metrics\Language",
     *     statusCodes={
     *         201="Returned when Language is added",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function createAction(Request $request): View
    {
        $language = new Language();
        $this->processForm($request, $language, LanguageType::class);
        $this->save($language);

        return $this->view($language, 201);
    }

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("/update", name="metrics_language_update")
     * @Method("POST")
     * @Security("has_role('ROLE_INTERNAL')")
     *
     * @ApiDoc(
     *     section="Metrics Language",
     *     description="Update an existing Language",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     input="Itsup\Bundle\ApiBundle\Form\Type\Metrics\Language",
     *     output="Itsup\Bundle\ApiBundle\Entity\Metrics\Language",
     *     statusCodes={
     *         200="Returned when language is updated",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function updateAction(Request $request): View
    {
        $languageId = $this->getRequestProperty($request, 'id');
        if (empty($languageId)) {
            throw new ApiException(ApiError::INVALID_REQUEST, ['Missing languageId']);
        }

        /* @var Language $language */
        $language = $this->getEntityFinder()->find(['id' => $languageId]);
        $this->processForm($request, $language, LanguageType::class);
        $this->update($language);

        return $this->view($language, 200);
    }

    /**
     * @param string $languageId
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("/{languageId}", name="metrics_language_delete")
     * @Method("DELETE")
     * @Security("has_role('ROLE_INTERNAL')")
     *
     * @ApiDoc(
     *     section="Metrics Language",
     *     description="Delete an Language",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     statusCodes={
     *         204="Returned when Language is deleted",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function deleteAction(string $languageId): View
    {
        /* @var Language $language */
        $language = $this->getEntityFinder()->find(['id' => $languageId]);

        $this->delete($language);

        return $this->view(null, 204);
    }
}

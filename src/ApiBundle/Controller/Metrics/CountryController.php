<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Controller\Metrics;

use FOS\RestBundle\View\View;
use Itsup\Bundle\ApiBundle\ApiError;
use Itsup\Bundle\ApiBundle\Controller\AbstractEntityController;
use Itsup\Bundle\ApiBundle\Entity\Metrics\Country;
use Itsup\Bundle\ApiBundle\Exception\ApiException;
use Itsup\Bundle\ApiBundle\Form\Type\Metrics\CountryType;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author Martin Karadzinov <martin@myhost.mk>
 * @Route("/metrics/country")
 */
class CountryController extends AbstractEntityController
{
    protected $type = 'metrics.country';

    protected $accountSearch = false;

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     * @Route("", name="metrics_country_find")
     * @Method("GET")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     *
     * @ApiDoc(
     *     section="Metrics Country",
     *     description="Fetch Country Metric information, at least one of the parameters must be passed",
     *     tags={"stable"},
     *     views={"internal"},
     *     parameters={
     *         {"name"="id", "dataType"="string", "required"=false, "description"="id"},
     *         {"name"="name", "dataType"="string", "required"=false, "description"="name"},
     *     }
     * )
     */
    public function findAction(Request $request): View
    {
        $context = $this->getContext($request, 'details');

        return $this->setContext($context, $this->view($this->find($request)));
    }

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     * @Route("/all", name="metrics_country_find_all")
     * @Method("GET")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     *
     * @ApiDoc(
     *     section="Metrics",
     *     description="Fetch many Countries information",
     *     tags={"stable"},
     *     views={"internal"},
     *     parameters={
     *         {"name"="id", "dataType"="string", "required"=false, "description"="id"},
     *         {"name"="name", "dataType"="string", "required"=false, "description"="name"},
     *     }
     * )
     */
    public function findAllAction(Request $request): View
    {
        $context = $this->getContext($request, 'list');

        return $this->setContext($context, $this->view($this->findAll($request)));
    }

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("", name="metrics_country_create")
     * @Method("POST")
     * @Security("has_role('ROLE_INTERNAL')")
     *
     * @ApiDoc(
     *     section="Metrics Country",
     *     description="Create a new Country",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     input="Itsup\Bundle\ApiBundle\Form\Type\Metrics\CountryType",
     *     output="Itsup\Bundle\ApiBundle\Entity\Metrics\Country",
     *     statusCodes={
     *         201="Returned when Country is created",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function createAction(Request $request): View
    {
        $country = new Country();
        $this->processForm($request, $country, CountryType::class);
        $this->save($country);

        return $this->view($country, 201);
    }

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("/update", name="metrics_country_update")
     * @Method("POST")
     * @Security("has_role('ROLE_INTERNAL')")
     *
     * @ApiDoc(
     *     section="Metrics Country",
     *     description="Update an existing Country",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     input="Itsup\Bundle\ApiBundle\Form\Type\Metrics\CountryType",
     *     output="Itsup\Bundle\ApiBundle\Entity\Metrics\Country",
     *     statusCodes={
     *         200="Returned when Country is updated",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function updateAction(Request $request): View
    {
        $countryId = $this->getRequestProperty($request, 'id');
        if (empty($countryId)) {
            throw new ApiException(ApiError::INVALID_REQUEST, ['Missing CountryId']);
        }

        /* @var Country $country */
        $country = $this->getEntityFinder()->find(['id' => $countryId]);
        $this->processForm($request, $country, CountryType::class);

        $this->update($country);

        return $this->view($country, 200);
    }

    /**
     * @param string $countryId
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("/{countryId}", name="metrics_country_delete")
     * @Method("DELETE")
     * @Security("has_role('ROLE_INTERNAL')")
     *
     * @ApiDoc(
     *     section="Metrics Country",
     *     description="Delete an Country",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     statusCodes={
     *         204="Returned when Country is deleted",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function deleteAction(string $countryId): View
    {
        /* @var Country $country */
        $country = $this->getEntityFinder()->find(['id' => $countryId]);

        $this->delete($country);

        return $this->view(null, 204);
    }
}

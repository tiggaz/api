<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Controller\Metrics;

use FOS\RestBundle\View\View;
use Itsup\Bundle\ApiBundle\ApiError;
use Itsup\Bundle\ApiBundle\Controller\AbstractEntityController;
use Itsup\Bundle\ApiBundle\Entity\Metrics\Device;
use Itsup\Bundle\ApiBundle\Exception\ApiException;
use Itsup\Bundle\ApiBundle\Form\Type\Metrics\DeviceType;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author Martin Karadzinov <martin.karadzinov@devsy.com>
 * @Route("/metrics/device")
 */
class DeviceController extends AbstractEntityController
{
    protected $type = 'metrics.device';

    protected $accountSearch = false;

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     * @Route("", name="metrics_device_find")
     * @Method("GET")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     *
     * @ApiDoc(
     *     section="Metrics Device",
     *     description="Fetch Device information.",
     *     tags={"stable"},
     *     views={"internal"},
     *     parameters={
     *         {"name"="id", "dataType"="string", "required"=false, "description"="id"},
     *     }
     * )
     */
    public function findAction(Request $request): View
    {
        $context = $this->getContext($request, 'details');

        return $this->setContext($context, $this->view($this->find($request)));
    }

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     * @Route("/all", name="metrics_device_find_all")
     * @Method("GET")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     *
     * @ApiDoc(
     *     section="Metrics Device",
     *     description="Fetch many Device information",
     *     tags={"stable"},
     *     views={"internal"},
     *     parameters={
     *         {"name"="id", "dataType"="string", "required"=false, "description"="id"},
     *     }
     * )
     */
    public function findAllAction(Request $request): View
    {
        $context = $this->getContext($request, 'list');

        return $this->setContext($context, $this->view($this->findAll($request)));
    }

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("", name="metrics_device_create")
     * @Method("POST")
     * @Security("has_role('ROLE_INTERNAL')")
     *
     * @ApiDoc(
     *     section="Metrics Device",
     *     description="Add a Device",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     input="Itsup\Bundle\ApiBundle\Form\Type\Metrics\Device",
     *     output="Itsup\Bundle\ApiBundle\Form\Type\Metrics\Device",
     *     statusCodes={
     *         201="Returned when Device is added",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function createAction(Request $request): View
    {
        $device = new Device();
        $this->processForm($request, $device, DeviceType::class);
        $this->save($device);

        return $this->view($device, 201);
    }

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("/update", name="metrics_device_update")
     * @Method("POST")
     * @Security("has_role('ROLE_INTERNAL')")
     *
     * @ApiDoc(
     *     section="Metrics Device",
     *     description="Update an existing Device",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     input="Itsup\Bundle\ApiBundle\Form\Type\Metrics\Device",
     *     output="Itsup\Bundle\ApiBundle\Entity\Metrics\Device",
     *     statusCodes={
     *         200="Returned when device is updated",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function updateAction(Request $request): View
    {
        $deviceId = $this->getRequestProperty($request, 'id');
        if (empty($deviceId)) {
            throw new ApiException(ApiError::INVALID_REQUEST, ['Missing deviceId']);
        }
        /* @var Device $device */
        $device = $this->getEntityFinder()->find(['id' => (int) $deviceId]);
        $this->processForm($request, $device, DeviceType::class);
        $this->update($device);

        return $this->view($device, 200);
    }

    /**
     * @param int $deviceId
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("/{deviceId}", name="metrics_device_delete")
     * @Method("DELETE")
     * @Security("has_role('ROLE_INTERNAL')")
     *
     * @ApiDoc(
     *     section="Metrics Device",
     *     description="Delete an Device",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     statusCodes={
     *         204="Returned when Device is deleted",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function deleteAction(int $deviceId): View
    {
        /* @var Device $device */
        $device = $this->getEntityFinder()->find(['id' => (int) $deviceId]);

        $this->delete($device);

        return $this->view(null, 204);
    }
}

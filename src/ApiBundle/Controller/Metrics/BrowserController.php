<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Controller\Metrics;

use FOS\RestBundle\View\View;
use Itsup\Bundle\ApiBundle\ApiError;
use Itsup\Bundle\ApiBundle\Controller\AbstractEntityController;
use Itsup\Bundle\ApiBundle\Entity\Metrics\Browser;
use Itsup\Bundle\ApiBundle\Exception\ApiException;
use Itsup\Bundle\ApiBundle\Form\Type\Metrics\BrowserType;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author Martin Karadzinov <martin.karadzinov@devsy.com>
 * @Route("/metrics/browser")
 */
class BrowserController extends AbstractEntityController
{
    protected $type = 'metrics.browser';

    protected $accountSearch = false;

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     * @Route("", name="metrics_browser_find")
     * @Method("GET")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     *
     * @ApiDoc(
     *     section="Metrics Browser",
     *     description="Fetch Browser information.",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     parameters={
     *         {"name"="id", "dataType"="string", "required"=false, "description"="id"},
     *     }
     * )
     */
    public function findAction(Request $request): View
    {
        $context = $this->getContext($request, 'details');

        return $this->setContext($context, $this->view($this->find($request)));
    }

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     * @Route("/all", name="metrics_browser_find_all")
     * @Method("GET")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     *
     * @ApiDoc(
     *     section="Metrics Browser",
     *     description="Fetch many Browser information",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     parameters={
     *         {"name"="id", "dataType"="string", "required"=false, "description"="id"},
     *     }
     * )
     */
    public function findAllAction(Request $request): View
    {
        $context = $this->getContext($request, 'list');

        return $this->setContext($context, $this->view($this->findAll($request)));
    }

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("", name="metrics_browser_create")
     * @Method("POST")
     * @Security("has_role('ROLE_INTERNAL')")
     *
     * @ApiDoc(
     *     section="Metrics Browser",
     *     description="Add a Browser",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     input="Itsup\Bundle\ApiBundle\Form\Type\Metrics\Browser",
     *     output="Itsup\Bundle\ApiBundle\Form\Type\Metrics\Browser",
     *     statusCodes={
     *         201="Returned when Browser is added",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function createAction(Request $request): View
    {
        $browser = new Browser();
        $this->processForm($request, $browser, BrowserType::class);
        $this->save($browser);

        return $this->view($browser, 201);
    }

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("/update", name="metrics_browser_update")
     * @Method("POST")
     * @Security("has_role('ROLE_INTERNAL')")
     *
     * @ApiDoc(
     *     section="Metrics Browser",
     *     description="Update an existing Browser",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     input="Itsup\Bundle\ApiBundle\Form\Type\Metrics\Browser",
     *     output="Itsup\Bundle\ApiBundle\Entity\Metrics\Browser",
     *     statusCodes={
     *         200="Returned when browser is updated",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function updateAction(Request $request): View
    {
        $browserId = $this->getRequestProperty($request, 'id');
        if (empty($browserId)) {
            throw new ApiException(ApiError::INVALID_REQUEST, ['Missing browserId']);
        }

        /* @var Browser $browser */
        $browser = $this->getEntityFinder()->find(['id' => (int) $browserId]);
        $this->processForm($request, $browser, BrowserType::class);
        $this->update($browser);

        return $this->view($browser, 200);
    }

    /**
     * @param int $browserId
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("/{browserId}", name="metrics_browser_delete")
     * @Method("DELETE")
     * @Security("has_role('ROLE_INTERNAL')")
     *
     * @ApiDoc(
     *     section="Metrics Browser",
     *     description="Delete an Browser",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     statusCodes={
     *         204="Returned when Browser is deleted",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function deleteAction(int $browserId): View
    {
        /* @var Browser $browser */
        $browser = $this->getEntityFinder()->find(['id' => (int) $browserId]);

        $this->delete($browser);

        return $this->view(null, 204);
    }
}

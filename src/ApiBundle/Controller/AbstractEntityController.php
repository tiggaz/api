<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Controller;

use Itsup\Bundle\ApiBundle\ApiError;
use Itsup\Bundle\ApiBundle\Entity\AbstractEntity;
use Itsup\Bundle\ApiBundle\Entity\User;
use Itsup\Bundle\ApiBundle\Exception\ApiException;
use Itsup\Bundle\ApiBundle\Finder\AbstractFinder;
use Itsup\Bundle\ApiBundle\Manager\AbstractManager;
use Symfony\Component\HttpFoundation\Request;

abstract class AbstractEntityController extends AbstractController
{
    /**
     * @var string
     */
    protected $type;

    /**
     * @var bool
     */
    protected $accountSearch = true;

    /**
     * Retrieve an object using the corresponding finder.
     *
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return AbstractEntity
     */
    protected function find(Request $request): AbstractEntity
    {
        $this->addUserAccountToSearchParameters($request);

        return $this->getEntityFinder()->find($request->query->all());
    }

    /**
     * Retrieve an object using the corresponding finder.
     *
     * @param Request $request
     * @param bool    $count   If true, must returns a count
     *
     * @throws ApiException
     *
     * @return array
     */
    protected function findAll(Request $request, bool $count = false): array
    {
        $this->addUserAccountToSearchParameters($request);
        $array = $this->getEntityFinder()->findAll($request->query->all(), $count);

        if (is_null($array) || (is_array($array) && count($array) === 0)) {
            throw new ApiException(ApiError::NOT_FOUND);
        }

        return $array;
    }

    /**
     * Save an object using the corresponding manager.
     *
     * @param AbstractEntity $entity
     *
     * @throws ApiException
     */
    protected function save(AbstractEntity &$entity)
    {
        $this->validate($entity);
        $this->getEntityManager()->save($entity);
    }

    /**
     * Update an object using the corresponding manager.
     *
     * @param AbstractEntity $entity
     *
     * @throws ApiException
     */
    protected function update(AbstractEntity &$entity)
    {
        $this->validate($entity);
        $this->getEntityManager()->update($entity);
    }

    /**
     * Delete an object using the corresponding manager.
     *
     * @param AbstractEntity $entity
     *
     * @throws ApiException
     */
    protected function delete(AbstractEntity &$entity)
    {
        $this->getEntityManager()->delete($entity);
    }

    /**
     * Return the proper object finder.
     *
     * @return AbstractFinder
     */
    public function getEntityFinder()
    {
        /* @var AbstractFinder $finder */
        $finder = $this->get('finder.'.$this->type);

        return $finder;
    }

    /**
     * Return the proper object manager.
     *
     * @return AbstractManager
     */
    public function getEntityManager()
    {
        /* @var AbstractManager $manager */
        $manager = $this->get('manager.'.$this->type);

        return $manager;
    }

    /**
     * Return the Account the request is attached to.
     *
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return \Itsup\Bundle\ApiBundle\Entity\Account
     */
    public function getAccount(Request $request)
    {
        $accountId = $this->getRequestProperty($request, 'account');
        $user      = $this->getUser();
        if (!$this->isGranted('ROLE_INTERNAL') || ($user instanceof User && empty($accountId))) {
            return $this->getUser()->getAccount();
        }
        $account = $this->get('finder.account')->find(['id' => (int) $accountId]);
        if (empty($account)) {
            throw new ApiException(ApiError::INVALID_REQUEST, ['Missing account']);
        }

        return $account;
    }

    /**
     * Return the User the request is attached to.
     *
     * @param Request $request
     * @param string  $userObjectName
     *
     * @throws ApiException
     *
     * @return \Itsup\Bundle\ApiBundle\Entity\User
     */
    public function getRequestedUser(Request $request, string $userObjectName = 'user')
    {
        $userId = $this->getRequestProperty($request, $userObjectName);
        $user   = $this->getUser();
        if (!$this->isGranted('ROLE_INTERNAL') || ($user instanceof User && empty($userId))) {
            return $user;
        }
        $user = $this->get('finder.user')->find(['id' => (int) $userId]);
        if (empty($user)) {
            throw new ApiException(ApiError::INVALID_REQUEST, ['Missing user']);
        }

        return $user;
    }

    /**
     * Add the account parameter to search criteria.
     *
     * @param Request $request
     */
    public function addUserAccountToSearchParameters(Request &$request)
    {
        if (!$this->isGranted('ROLE_INTERNAL') && $this->accountSearch) {
            /* @var User $user */
            $user = $this->getUser();
            $request->query->set('account', $user->getAccount());
        }
    }

    /**
     * Validate the object form.
     *
     * @param Request        $request
     * @param AbstractEntity $entity
     * @param string         $class
     * @param array          $options
     *
     * @throws ApiException
     */
    public function processForm(
        Request $request,
        AbstractEntity &$entity,
        string $class,
        array $options = []
    ) {
        $form = $this->createForm($class, $entity, $options);
        $form->submit($request->request->get($form->getName()), false);
    }

    /**
     * Validate the object.
     *
     * @param AbstractEntity $entity
     *
     * @throws ApiException
     */
    public function validate(AbstractEntity &$entity)
    {
        $errors     = [];
        $violations = $this->get('validator')->validate($entity);

        foreach ($violations as $violation) {
            $errors[] = $violation->getMessage();
        }

        if (!empty($errors)) {
            throw new ApiException(ApiError::TYPE_VALIDATION, $errors);
        }
    }
}

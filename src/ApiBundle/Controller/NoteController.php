<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Controller;

use FOS\RestBundle\View\View;
use Itsup\Bundle\ApiBundle\ApiError;
use Itsup\Bundle\ApiBundle\Entity\Note;
use Itsup\Bundle\ApiBundle\Exception\ApiException;
use Itsup\Bundle\ApiBundle\Form\Type\NoteType;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author Martin Karadzinov <martin@myhost.mk>
 * @Route("/note")
 */
class NoteController extends AbstractEntityController
{
    /**
     * @var string
     */
    protected $type = 'note';

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     * @Route("", name="note_find")
     * @Method("GET")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     *
     * @ApiDoc(
     *     section="Note",
     *     description="Fetch Note information, at least one of the parameters must be passed",
     *     tags={"stable"},
     *     views={"internal"},
     *     parameters={
     *         {"name"="id", "dataType"="string", "required"=false, "description"="id"},
     *         {"name"="text", "dataType"="string", "required"=false, "description"="text"}
     *     }
     * )
     */
    public function findAction(Request $request): View
    {
        $context = $this->getContext($request, 'details');

        return $this->setContext($context, $this->view($this->find($request)));
    }

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     * @Route("/all", name="note_find_all")
     * @Method("GET")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     *
     * @ApiDoc(
     *     section="Note",
     *     description="Fetch many Notes information.",
     *     tags={"stable"},
     *     views={"internal"},
     *     parameters={
     *         {"name"="id", "dataType"="string", "required"=false, "description"="id"},
     *         {"name"="text", "dataType"="string", "required"=false, "description"="text"},
     *         {"name"="user", "dataType"="string", "required"=false, "description"="user"}
     *     }
     * )
     */
    public function findAllAction(Request $request): View
    {
        $context = $this->getContext($request, 'list');

        return $this->setContext($context, $this->view($this->findAll($request)));
    }

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("", name="note_create")
     * @Method("POST")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     *
     * @ApiDoc(
     *     section="Note",
     *     description="Create a new Note",
     *     tags={"stable"},
     *     views={"internal"},
     *     input="Itsup\Bundle\ApiBundle\Form\Type\NoteType",
     *     output="Itsup\Bundle\ApiBundle\Entity\Note",
     *     statusCodes={
     *         201="Returned when note is created",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function createAction(Request $request): View
    {
        $user    = $this->getRequestedUser($request);
        $note    = new Note();
        $options = [
            'required' => [
                'text',
            ],
        ];
        try {
            $this->validateForm($request, $note, NoteType::class, $options);
        } catch (\Exception $e) {
            self::throwException($e);
        }
        $note->setUser($user);
        $this->save($note);

        return $this->view($note, 201);
    }

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("/update", name="note_update")
     * @Method("POST")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     *
     * @ApiDoc(
     *     section="Note",
     *     description="Update an existing Note",
     *     tags={"stable"},
     *     views={"internal"},
     *     input="Itsup\Bundle\ApiBundle\Form\Type\NoteType",
     *     output="Itsup\Bundle\ApiBundle\Entity\Note",
     *     statusCodes={
     *         200="Returned when note is updated",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function updateAction(Request $request): View
    {
        $noteId  = $this->getRequestProperty($request, 'id');
        if (empty($noteId)) {
            throw new ApiException(ApiError::INVALID_REQUEST, ['Missing NoteId']);
        }
        /* @var Note $note */
        $note    = $this->getEntityFinder()->find(['id' => (int) $noteId]);
        $options = [
            'required' => [
                'text',
            ],
        ];
        try {
            $this->validateForm($request, $note, NoteType::class, $options);
        } catch (\Exception $e) {
            self::throwException($e);
        }
        $this->update($note);

        return $this->view($note, 200);
    }

    /**
     * @param int $noteId
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("/{noteId}", name="note_delete")
     * @Method("DELETE")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     *
     *
     * @ApiDoc(
     *     section="Note",
     *     description="Delete a Note",
     *     tags={"stable"},
     *     views={"internal"},
     *     statusCodes={
     *         204="Returned when note is deleted",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function deleteAction(int $noteId): View
    {
        /* @var Note $note */
        $note = $this->getEntityFinder()->find(['id' => (int) $noteId]);
        $this->checkAccount($note->getUser()->getId());

        $this->delete($note);

        return $this->view(null, 204);
    }
}

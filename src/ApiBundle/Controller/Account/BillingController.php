<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Controller\Account;

use FOS\RestBundle\View\View;
use Itsup\Bundle\ApiBundle\ApiError;
use Itsup\Bundle\ApiBundle\Controller\AbstractEntityController;
use Itsup\Bundle\ApiBundle\Entity\Account\Billing;
use Itsup\Bundle\ApiBundle\Exception\ApiException;
use Itsup\Bundle\ApiBundle\Form\Type\Account\BillingType;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author Martin Karadzinov <martin@myhost.mk>
 * @Route("/account/billing")
 */
class BillingController extends AbstractEntityController
{
    protected $type = 'account.billing';

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     * @Route("", name="account_billing_find")
     * @Method("GET")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN')")
     *
     * @ApiDoc(
     *     section="Account Billing",
     *     description="Fetch Account Billing information for a given date.",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     parameters={
     *         {"name"="date", "dataType"="datetime", "required"=false, "description"="datetime of billing"},
     *     }
     * )
     */
    public function findAction(Request $request): View
    {
        $context = $this->getContext($request, 'details');

        return $this->setContext($context, $this->view($this->find($request)));
    }

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     * @Route("/all", name="account_billing_find_all")
     * @Method("GET")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN')")
     *
     * @ApiDoc(
     *     section="Account Billing",
     *     description="Fetch many Billing information",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     parameters={
     *         {"name"="date", "dataType"="datetime", "required"=false, "description"="datetime of billing"},
     *     }
     * )
     */
    public function findAllAction(Request $request): View
    {
        $context = $this->getContext($request, 'list');

        return $this->setContext($context, $this->view($this->findAll($request)));
    }

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("", name="account_billing_create")
     * @Method("POST")
     * @Security("has_role('ROLE_INTERNAL')")
     *
     * @ApiDoc(
     *     section="Account Billing",
     *     description="Add a Billing to an Account",
     *     tags={"stable"},
     *     views={"internal"},
     *     input="Itsup\Bundle\ApiBundle\Form\Type\Account\BillingType",
     *     output="Itsup\Bundle\ApiBundle\Entity\Account\Billing",
     *     statusCodes={
     *         201="Returned when Billing is added to the Account",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function createAction(Request $request): View
    {
        $billing = new Billing();
        $this->processForm($request, $billing, BillingType::class);
        $this->save($billing);

        return $this->view($billing, 201);
    }

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("/update", name="account_billing_update")
     * @Method("POST")
     * @Security("has_role('ROLE_INTERNAL')")
     *
     * @ApiDoc(
     *     section="Account Billing",
     *     description="Update an existing Account Billing",
     *     tags={"stable"},
     *     views={"internal"},
     *     input="Itsup\Bundle\ApiBundle\Form\Type\Account\BillingType",
     *     output="Itsup\Bundle\ApiBundle\Entity\Account\Billing",
     *     statusCodes={
     *         200="Returned when billing is updated",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function updateAction(Request $request): View
    {
        $account     = $this->getAccount($request);
        $billingDate = $this->getRequestProperty($request, 'date');

        if (empty($billingDate)) {
            throw new ApiException(ApiError::INVALID_REQUEST, ['Missing billingDate']);
        }

        if (empty($account)) {
            throw new ApiException(ApiError::INVALID_REQUEST, ['Missing account']);
        }

        /* @var Billing $billingDate */
        $billing = $this->getEntityFinder()->find(['date' => $billingDate, 'account' => $account]);
        $this->processForm($request, $billing, BillingType::class);
        $this->update($billing);

        return $this->view($billing, 200);
    }

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("", name="account_billing_delete")
     * @Method("DELETE")
     * @Security("has_role('ROLE_INTERNAL')")
     *
     * @ApiDoc(
     *     section="Account Billing",
     *     description="Remove a Billing from an Account",
     *     tags={"stable"},
     *     views={"internal"},
     *     statusCodes={
     *         204="Returned when Billing is removed",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function deleteAction(Request $request): View
    {
        $account     = $this->getAccount($request);
        $billingDate = $this->getRequestProperty($request, 'date');

        if (empty($billingDate)) {
            throw new ApiException(ApiError::INVALID_REQUEST, ['Missing billingDate']);
        }

        if (empty($account)) {
            throw new ApiException(ApiError::INVALID_REQUEST, ['Missing account']);
        }

        /* @var Billing $billing */
        $billing = $this->getEntityFinder()->find(['date' => $billingDate, 'account' => $account]);
        $this->delete($billing);

        return $this->view(null, 204);
    }
}

<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Controller\Account;

use FOS\RestBundle\View\View;
use Itsup\Bundle\ApiBundle\ApiError;
use Itsup\Bundle\ApiBundle\Controller\AbstractEntityController;
use Itsup\Bundle\ApiBundle\Entity\AbstractEntity;
use Itsup\Bundle\ApiBundle\Entity\Account\Pricing;
use Itsup\Bundle\ApiBundle\Exception\ApiException;
use Itsup\Bundle\ApiBundle\Form\Type\Account\PricingType;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author Martin Karadzinov <martin@myhost.mk>
 * @Route("/account/pricing")
 */
class PricingController extends AbstractEntityController
{
    protected $type = 'account.pricing';

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     * @Route("", name="account_pricing_find")
     * @Method("GET")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN')")
     *
     * @ApiDoc(
     *     section="Account Pricing",
     *     description="Fetch Account Pricing information, at least one of the parameters must be passed",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     parameters={
     *         {"name"="id", "dataType"="string", "required"=false, "description"="id"},
     *     }
     * )
     */
    public function findAction(Request $request): View
    {
        $context = $this->getContext($request, 'details');

        return $this->setContext($context, $this->view($this->find($request)));
    }

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     * @Route("/all", name="account_pricing_find_all")
     * @Method("GET")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN')")
     *
     * @ApiDoc(
     *     section="Account Pricing",
     *     description="Fetch many Pricing information.",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     parameters={
     *         {"name"="id", "dataType"="string", "required"=false, "description"="id"},
     *         {"name"="from", "dataType"="string", "required"=false, "description"="from"},
     *         {"name"="to", "dataType"="string", "required"=false, "description"="to"},
     *         {"name"="cpm", "dataType"="boolean", "required"=false, "description"="is cpm"},
     *     }
     * )
     */
    public function findAllAction(Request $request): View
    {
        $context = $this->getContext($request, 'list');

        return $this->setContext($context, $this->view($this->findAll($request)));
    }

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("", name="account_pricing_create")
     * @Method("POST")
     * @Security("has_role('ROLE_INTERNAL')")
     *
     * @ApiDoc(
     *     section="Account Pricing",
     *     description="Add a new Pricing to an Account",
     *     tags={"stable"},
     *     views={"internal"},
     *     input="Itsup\Bundle\ApiBundle\Form\Type\Account\PricingType",
     *     output="Itsup\Bundle\ApiBundle\Entity\Account\Pricing",
     *     statusCodes={
     *         201="Returned when Pricing is added",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function createAction(Request $request): View
    {
        $account = $this->getAccount($request);
        $pricing = new Pricing();
        $this->processForm($request, $pricing, PricingType::class);
        $pricing->setAccount($account);
        $this->save($pricing);

        return $this->view($pricing, 201);
    }

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("/update", name="account_pricing_update")
     * @Method("POST")
     * @Security("has_role('ROLE_INTERNAL')")
     *
     * @ApiDoc(
     *     section="Account Pricing",
     *     description="Update an existing Pricing",
     *     tags={"stable"},
     *     views={"internal"},
     *     input="Itsup\Bundle\ApiBundle\Form\Type\Account\PricingType",
     *     output="Itsup\Bundle\ApiBundle\Entity\Account\Pricing",
     *     statusCodes={
     *         200="Returned when Pricing is updated",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function updateAction(Request $request): View
    {
        $pricingId = $this->getRequestProperty($request, 'id');
        if (empty($pricingId)) {
            throw new ApiException(ApiError::INVALID_REQUEST, ['Missing PricingId']);
        }

        /* @var Pricing $pricing */
        $pricing = $this->getEntityFinder()->find(['id' => (int) $pricingId]);
        $this->checkAccount($pricing->getAccount()->getId());
        $this->processForm($request, $pricing, PricingType::class);
        $this->update($pricing);

        return $this->view($pricing, 200);
    }

    /**
     * @param int $pricingId
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("/{pricingId}", name="account_pricing_delete")
     * @Method("DELETE")
     * @Security("has_role('ROLE_INTERNAL')")
     *
     * @ApiDoc(
     *     section="Account Pricing",
     *     description="Remove a Pricing from an Account",
     *     tags={"stable"},
     *     views={"internal"},
     *     statusCodes={
     *         204="Returned when Pricing is deleted",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function deleteAction(int $pricingId): View
    {
        /* @var Pricing $pricing */
        $pricing = $this->getEntityFinder()->find(['id' => (int) $pricingId]);
        $this->checkAccount($pricing->getAccount()->getId());
        $this->delete($pricing);

        return $this->view(null, 204);
    }

    /**
     * @param AbstractEntity $entity
     *
     * @throws ApiException
     */
    public function validate(AbstractEntity &$entity)
    {
        parent::validate($entity);

        /* @var Pricing $entity */
        if ($entity->getFrom() > $entity->getTo()) {
            throw new ApiException(ApiError::INVALID_REQUEST, ['From date must be before To date']);
        }
    }
}

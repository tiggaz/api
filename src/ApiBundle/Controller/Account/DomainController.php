<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Controller\Account;

use FOS\RestBundle\View\View;
use Itsup\Bundle\ApiBundle\ApiError;
use Itsup\Bundle\ApiBundle\Controller\AbstractEntityController;
use Itsup\Bundle\ApiBundle\Entity\Account\Domain;
use Itsup\Bundle\ApiBundle\Exception\ApiException;
use Itsup\Bundle\ApiBundle\Form\Type\Account\DomainType;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author Martin Karadzinov <martin@myhost.mk>
 * @Route("/account/domain")
 */
class DomainController extends AbstractEntityController
{
    protected $type = 'account.domain';

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     * @Route("", name="account_domain_find")
     * @Method("GET")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     *
     * @ApiDoc(
     *     section="Account Domain",
     *     description="Fetch Domain information, at least one of the parameters must be passed",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     parameters={
     *         {"name"="id", "dataType"="string", "required"=false, "description"="id"},
     *         {"name"="name", "dataType"="string", "required"=false, "description"="name"},
     *     }
     * )
     */
    public function findAction(Request $request): View
    {
        $context = $this->getContext($request, 'details');

        return $this->setContext($context, $this->view($this->find($request)));
    }

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     * @Route("/all", name="account_domain_find_all")
     * @Method("GET")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     *
     * @ApiDoc(
     *     section="Account Domain",
     *     description="Fetch many Domains information",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     parameters={
     *         {"name"="id", "dataType"="string", "required"=false, "description"="id"},
     *         {"name"="account", "dataType"="string", "required"=false, "description"="account"},
     *         {"name"="type", "dataType"="string", "required"=false, "description"="type"},
     *         {"name"="name", "dataType"="string", "required"=false, "description"="name"},
     *         {"name"="ssl", "dataType"="boolean", "required"=false, "description"="is ssl"},
     *         {"name"="defaultDomain", "dataType"="boolean", "required"=false, "description"="is default domain"},
     *         {"name"="installed", "dataType"="boolean", "required"=false, "description"="is installed"}
     *     }
     * )
     */
    public function findAllAction(Request $request): View
    {
        $context = $this->getContext($request, 'list');

        return $this->setContext($context, $this->view($this->findAll($request)));
    }

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("", name="account_domain_create")
     * @Method("POST")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN')")
     *
     * @ApiDoc(
     *     section="Account Domain",
     *     description="Add a new Domain to an Account",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     input="Itsup\Bundle\ApiBundle\Form\Type\Account\DomainType",
     *     output="Itsup\Bundle\ApiBundle\Entity\Account\Domain",
     *     statusCodes={
     *         201="Returned when Domain is added",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function createAction(Request $request): View
    {
        $account = $this->getAccount($request);
        $domain  = new Domain();
        $this->processForm($request, $domain, DomainType::class);
        $domain->setAccount($account);
        $this->save($domain);

        return $this->view($domain, 201);
    }

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("/update", name="account_domain_update")
     * @Method("POST")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN')")
     *
     * @ApiDoc(
     *     section="Account Domain",
     *     description="Update an existing Domain",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     input="Itsup\Bundle\ApiBundle\Form\Type\Account\DomainType",
     *     output="Itsup\Bundle\ApiBundle\Entity\Account\Domain",
     *     statusCodes={
     *         200="Returned when Domain is updated",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function updateAction(Request $request): View
    {
        $domainId = $this->getRequestProperty($request, 'id');
        if (empty($domainId)) {
            throw new ApiException(ApiError::INVALID_REQUEST, ['Missing DomainId']);
        }
        /* @var Domain $domain */
        $domain  = $this->getEntityFinder()->find(['id' => (int) $domainId]);
        $this->checkAccount($domain->getAccount()->getId());
        $this->processForm($request, $domain, DomainType::class);
        $this->update($domain);

        return $this->view($domain, 200);
    }

    /**
     * @param int $domainId
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("/{domainId}", name="account_domain_delete")
     * @Method("DELETE")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN')")
     *
     * @ApiDoc(
     *     section="Account Domain",
     *     description="Remove a Domain from an Account",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     statusCodes={
     *         204="Returned when Domain is removed",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function deleteAction(int $domainId): View
    {
        /* @var Domain $domain */
        $domain = $this->getEntityFinder()->find(['id' => (int) $domainId]);
        $this->checkAccount($domain->getAccount()->getId());
        $this->delete($domain);

        return $this->view(null, 204);
    }
}

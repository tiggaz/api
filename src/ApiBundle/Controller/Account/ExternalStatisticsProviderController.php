<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Controller\Account;

use FOS\RestBundle\View\View;
use Itsup\Bundle\ApiBundle\ApiError;
use Itsup\Bundle\ApiBundle\Controller\AbstractEntityController;
use Itsup\Bundle\ApiBundle\Entity\AbstractEntity;
use Itsup\Bundle\ApiBundle\Entity\Account\ExternalStatisticsProvider;
use Itsup\Bundle\ApiBundle\Exception\ApiException;
use Itsup\Bundle\ApiBundle\Form\Type\Account\ExternalStatisticsProviderType;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author Martin Karadzinov <martin@myhost.mk>
 * @Route("/account/esp")
 */
class ExternalStatisticsProviderController extends AbstractEntityController
{
    protected $type = 'account.esp';

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     * @Route("", name="account_esp_find")
     * @Method("GET")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     *
     * @ApiDoc(
     *     section="Account ExternalStatisticsProvider",
     *     description="Fetch ExternalStatisticsProvider information, at least one of the parameters must be passed",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     parameters={
     *         {"name"="id", "dataType"="string", "required"=false, "description"="id"},
     *         {"name"="username", "dataType"="string", "required"=false, "description"="username"},
     *     }
     * )
     */
    public function findAction(Request $request): View
    {
        $context = $this->getContext($request, 'details');

        return $this->setContext($context, $this->view($this->find($request)));
    }

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     * @Route("/all", name="account_esp_find_all")
     * @Method("GET")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     *
     * @ApiDoc(
     *     section="Account ExternalStatisticsProvider",
     *     description="Fetch many ExternalStatisticsProvider information information",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     parameters={
     *         {"name"="id", "dataType"="string", "required"=false, "description"="id"},
     *         {"name"="name", "dataType"="string", "required"=false, "description"="name"},
     *         {"name"="username", "dataType"="string", "required"=false, "description"="username"},
     *     }
     * )
     */
    public function findAllAction(Request $request): View
    {
        $context = $this->getContext($request, 'list');

        return $this->setContext($context, $this->view($this->findAll($request)));
    }

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("", name="account_esp_create")
     * @Method("POST")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN')")
     *
     * @ApiDoc(
     *     section="Account ExternalStatisticsProvider",
     *     description="Add a new ExternalStatisticsProvider to an Account",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     input="Itsup\Bundle\ApiBundle\Form\Type\Account\ExternalStatisticsProviderType",
     *     output="Itsup\Bundle\ApiBundle\Entity\Account\ExternalStatisticsProvider",
     *     statusCodes={
     *         201="Returned when ExternalStatisticsProvider is added",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function createAction(Request $request): View
    {
        $account = $this->getAccount($request);
        $esp     = new ExternalStatisticsProvider();
        $this->processForm($request, $esp, ExternalStatisticsProviderType::class);
        $esp->setAccount($account);
        $this->save($esp);

        return $this->view($esp, 201);
    }

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("/update", name="account_esp_update")
     * @Method("POST")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN')")
     *
     * @ApiDoc(
     *     section="Account ExternalStatisticsProvider",
     *     description="Update an existing ExternalStatisticsProvider",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     input="Itsup\Bundle\ApiBundle\Form\Type\Account\ExternalStatisticsProviderType",
     *     output="Itsup\Bundle\ApiBundle\Entity\Account\ExternalStatisticsProvider",
     *     statusCodes={
     *         200="Returned when ExternalStatisticsProvider is updated",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function updateAction(Request $request): View
    {
        $espId = $this->getRequestProperty($request, 'id');
        if (empty($espId)) {
            throw new ApiException(ApiError::INVALID_REQUEST, ['Missing espId']);
        }

        /* @var ExternalStatisticsProvider $esp */
        $esp = $this->getEntityFinder()->find(['id' => (int) $espId]);
        $this->checkAccount($esp->getAccount()->getId());
        $this->processForm($request, $esp, ExternalStatisticsProviderType::class);
        $this->update($esp);

        return $this->view($esp, 200);
    }

    /**
     * @param int $espId
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("/{espId}", name="account_esp_delete")
     * @Method("DELETE")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN')")
     *
     * @ApiDoc(
     *     section="Account ExternalStatisticsProvider",
     *     description="Remove an ExternalStatisticsProvider from an Account",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     statusCodes={
     *         204="Returned when ExternalStatisticsProvider is removed",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function deleteAction(int $espId): View
    {
        /* @var ExternalStatisticsProvider $esp */
        $esp = $this->getEntityFinder()->find(['id' => (int) $espId]);
        $this->checkAccount($esp->getAccount()->getId());
        $this->delete($esp);

        return $this->view(null, 204);
    }

    /**
     * @param AbstractEntity $entity
     *
     * @throws ApiException
     */
    public function validate(AbstractEntity &$entity)
    {
        parent::validate($entity);

        /* @var ExternalStatisticsProvider $entity */
        if ($entity->getName() === 'reporo' && empty($entity->getPassword())) {
            throw new ApiException(ApiError::INVALID_REQUEST, ['Missing API secret/password']);
        }
    }
}

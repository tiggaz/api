<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Controller;

use FOS\RestBundle\View\View;
use Itsup\Bundle\ApiBundle\ApiError;
use Itsup\Bundle\ApiBundle\Entity\AbstractEntity;
use Itsup\Bundle\ApiBundle\Entity\AdZone;
use Itsup\Bundle\ApiBundle\Entity\Campaign;
use Itsup\Bundle\ApiBundle\Exception\ApiException;
use Itsup\Bundle\ApiBundle\Form\Type\AdZoneType;
use Itsup\Bundle\ApiBundle\Manager\AdZone\AdZoneCampaignManager;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author Martin Karadzinov <martin@myhost.mk>
 * @Route("/adzone")
 */
class AdZoneController extends AbstractTagController
{
    protected $type = 'ad_zone';

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     * @Route("", name="ad_zone_find")
     * @Method("GET")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     *
     * @ApiDoc(
     *     section="AdZone",
     *     description="Fetch AdZone information, at least one of the parameters must be passed",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     parameters={
     *         {"name"="id", "dataType"="string", "required"=false, "description"="id"},
     *         {"name"="name", "dataType"="string", "required"=false, "description"="name"},
     *     }
     * )
     */
    public function findAction(Request $request): View
    {
        $context = $this->getContext($request, 'details');

        return $this->setContext($context, $this->view($this->find($request)));
    }

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     * @Route("/all", name="ad_zone_find_all")
     * @Method("GET")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     *
     * @ApiDoc(
     *     section="AdZone",
     *     description="Fetch many AdZone information",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     parameters={
     *         {"name"="id", "dataType"="string", "required"=false, "description"="id"},
     *         {"name"="name", "dataType"="string", "required"=false, "description"="name"},
     *         {"name"="hosting", "dataType"="string", "required"=false, "description"="hosting"},
     *         {"name"="type", "dataType"="string", "required"=false, "description"="type"},
     *         {"name"="status", "dataType"="string", "required"=false, "description"="status"},
     *         {"name"="display", "dataType"="string", "required"=false, "description"="display"},
     *         {"name"="dateCreated", "dataType"="string", "required"=false, "description"="dateCreated"},
     *         {"name"="dateUpdated", "dataType"="string", "required"=false, "description"="dateUpdated"},
     *         {"name"="createdBy", "dataType"="integer", "required"=false, "description"="createdBy"},
     *         {"name"="updatedBy", "dataType"="integer", "required"=false, "description"="updatedBy"},
     *     }
     * )
     */
    public function findAllAction(Request $request): View
    {
        $context = $this->getContext($request, 'list');

        return $this->setContext($context, $this->view($this->findAll($request)));
    }

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("", name="ad_zone_create")
     * @Method("POST")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     *
     * @ApiDoc(
     *     section="AdZone",
     *     description="Create a new AdZone",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     input="Itsup\Bundle\ApiBundle\Form\Type\AdZoneType",
     *     output="Itsup\Bundle\ApiBundle\Entity\AdZone",
     *     statusCodes={
     *         201="Returned when AdZone is created",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function createAction(Request $request): View
    {
        $account = $this->getAccount($request);
        $user    = $this->getRequestedUser($request, 'createdBy');
        $adZone  = new AdZone();
        $this->processForm($request, $adZone, AdZoneType::class);

        $adZoneTags = $this->processTags($adZone->getTags(), $account);
        $adZone->setTags($adZoneTags);

        $adZone->setCreatedBy($user);
        $adZone->setDateCreated(new \DateTime());
        $adZone->setUpdatedBy($user);
        $adZone->setDateUpdated(new \DateTime());
        $adZone->setAccount($account);
        $this->save($adZone);

        return $this->view($adZone, 201);
    }

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("/update", name="ad_zone_update")
     * @Method("POST")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     *
     * @ApiDoc(
     *     section="AdZone",
     *     description="Update an existing AdZone",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     input="Itsup\Bundle\ApiBundle\Form\Type\AdZoneType",
     *     output="Itsup\Bundle\ApiBundle\Entity\AdZone",
     *     statusCodes={
     *         200="Returned when AdZone is updated",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function updateAction(Request $request): View
    {
        $account  = $this->getAccount($request);
        $user     = $this->getRequestedUser($request, 'updatedBy');
        $adZoneId = $this->getRequestProperty($request, 'id');
        if (empty($adZoneId)) {
            throw new ApiException(ApiError::INVALID_REQUEST, ['Missing AdZoneId']);
        }
        /* @var AdZone $adZone */
        $adZone = $this->getEntityFinder()->find(['id' => (int) $adZoneId, 'account' => $account]);
        $adZone->resetTags();
        $this->processForm($request, $adZone, AdZoneType::class);

        $adZoneTags = $this->processTags($adZone->getTags(), $account);
        $adZone->setTags($adZoneTags);

        $adZone->setUpdatedBy($user);
        $adZone->setDateUpdated(new \DateTime());
        $this->update($adZone);

        return $this->view($adZone, 200);
    }

    /**
     * @param int    $adZoneId
     * @param string $type
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("/{adZoneId}/duplicate/{type}", name="ad_zone_duplicate")
     * @Method("POST")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     *
     * @ApiDoc(
     *     section="AdZone",
     *     description="Duplicate an existing AdZone",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     output="Itsup\Bundle\ApiBundle\Entity\AdZone",
     *     statusCodes={
     *         201="Returned when AdZone is duplicated",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function duplicateAction(int $adZoneId, string $type): View
    {
        if (empty($adZoneId)) {
            throw new ApiException(ApiError::INVALID_REQUEST, ['Missing AdZoneId']);
        }
        /* @var AdZone $adZone */
        $adZone = $this->getEntityFinder()->find(['id' => (int) $adZoneId]);
        $this->checkAccount($adZone->getAccount()->getId());
        $user = $this->isGranted('ROLE_INTERNAL') ? $adZone->getCreatedBy() : $this->getUser();

        /* @var AdZone $newAdZone */
        $newAdZone = $this->getEntityManager()->clone($adZone);
        $newAdZone->setCreatedBy($user);
        $newAdZone->setUpdatedBy($user);

        if ($type === 'campaigns') {
            /* @var AdZoneCampaignManager $manager */
            $manager = $this->get('manager.ad_zone.campaign');
            foreach ($adZone->getCampaigns() as $campaign) {
                /* @var AdZone\AdZoneCampaign $newCampaign */
                $newCampaign = $manager->clone($campaign);
                $newAdZone->addCampaign($newCampaign);
            }
        }
        $this->save($newAdZone);

        return $this->view($newAdZone, 201);
    }

    /**
     * @param int $adZoneId
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("/{adZoneId}", name="ad_zone_delete")
     * @Method("DELETE")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     *
     * @ApiDoc(
     *     section="AdZone",
     *     description="Delete an AdZone",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     statusCodes={
     *         204="Returned when AdZone is deleted",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function deleteAction(int $adZoneId): View
    {
        /* @var AdZone $adZone */
        $adZone = $this->getEntityFinder()->find(['id' => (int) $adZoneId]);
        $this->checkAccount($adZone->getAccount()->getId());
        $this->delete($adZone);

        return $this->view(null, 204);
    }

    /**
     * @param AbstractEntity $entity
     *
     * @throws ApiException
     */
    public function validate(AbstractEntity &$entity)
    {
        parent::validate($entity);

        /* @var AdZone $entity */
        if ($entity->getHosting() === 'remote' && $entity->getType() !== 'direct link') {
            throw new ApiException(ApiError::INVALID_REQUEST, ['If hosting is remote, type must be direct link']);
        }

        if ($entity->getHosting() !== 'remote' && $entity->getType() === 'direct link') {
            throw new ApiException(ApiError::INVALID_REQUEST, ['If type is direct link, hosting must be remote']);
        }

        if ($entity->getType() === 'iframe') {
            if ($entity->getWidth() === 0 || $entity->getHeight() === 0) {
                throw new ApiException(ApiError::INVALID_REQUEST, ['Width and Height must be set']);
            }

            if (!empty($entity->getDefaultCampaign())) {
                if (!in_array($entity->getDefaultCampaign()->getType(), ['iframe', 'image'])) {
                    throw new ApiException(ApiError::INVALID_REQUEST, ['Campaign must be iframe or image']);
                } else {
                    if ($entity->getDefaultCampaign()->getType() === 'image') {
                        if ($entity->getWidth() !== $entity->getDefaultCampaign()->getWidth() ||
                            $entity->getHeight() !== $entity->getDefaultCampaign()->getHeight()
                        ) {
                            $message = 'Campaign width and height must match AdZone';
                            throw new ApiException(ApiError::INVALID_REQUEST, [$message]);
                        }
                    }
                }
            }
        }
    }
}

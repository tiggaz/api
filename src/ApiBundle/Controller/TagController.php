<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Controller;

use FOS\RestBundle\View\View;
use Itsup\Bundle\ApiBundle\ApiError;
use Itsup\Bundle\ApiBundle\Entity\Tag;
use Itsup\Bundle\ApiBundle\Exception\ApiException;
use Itsup\Bundle\ApiBundle\Form\Type\TagType;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author Martin Karadzinov <martin@myhost.mk>
 * @Route("/tag")
 */
class TagController extends AbstractEntityController
{
    protected $type = 'tag';

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     * @Route("", name="tag_find")
     * @Method("GET")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     *
     * @ApiDoc(
     *     section="Tag",
     *     description="Fetch Tag information, at least one of the parameters must be passed",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     parameters={
     *         {"name"="id", "dataType"="string", "required"=false, "description"="id"},
     *         {"name"="name", "dataType"="string", "required"=false, "description"="name"},
     *     }
     * )
     */
    public function findAction(Request $request): View
    {
        $context = $this->getContext($request, 'details');

        return $this->setContext($context, $this->view($this->find($request)));
    }

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     * @Route("/all", name="tag_find_all")
     * @Method("GET")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     * @ApiDoc(
     *     section="Tag",
     *     description="Fetch many Tag information",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     parameters={
     *         {"name"="id", "dataType"="string", "required"=false, "description"="id"},
     *         {"name"="name", "dataType"="string", "required"=false, "description"="name"},
     *     }
     * )
     */
    public function findAllAction(Request $request): View
    {
        $context = $this->getContext($request, 'list');

        return $this->setContext($context, $this->view($this->findAll($request)));
    }

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("", name="tag_create")
     * @Method("POST")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     * @ApiDoc(
     *     section="Tag",
     *     description="Create a new Tag",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     input="Itsup\Bundle\ApiBundle\Form\Type\TagType",
     *     output="Itsup\Bundle\ApiBundle\Entity\Tag",
     *     statusCodes={
     *         201="Returned when contact is created",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function createAction(Request $request): View
    {
        $account  = $this->getAccount($request);
        $tag      = new Tag();
        $this->processForm($request, $tag, TagType::class);
        $tag->setAccount($account);
        $this->save($tag);

        return $this->view($tag, 201);
    }

    /**
     * @param Request $request
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("/update", name="tag_update")
     * @Method("POST")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     *
     * @ApiDoc(
     *     section="Tag",
     *     description="Update an existing Tag",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     input="Itsup\Bundle\ApiBundle\Form\Type\TagType",
     *     output="Itsup\Bundle\ApiBundle\Entity\Tag",
     *     statusCodes={
     *         200="Returned when Tag is updated",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function updateAction(Request $request): View
    {
        $account    = $this->getAccount($request);
        $tagId      = $this->getRequestProperty($request, 'id');

        if (empty($tagId)) {
            throw new ApiException(ApiError::INVALID_REQUEST, ['Missing TagId']);
        }

        /* @var Tag $tag */
        $tag = $this->getEntityFinder()->find(['id' => (int) $tagId, 'account' => $account]);

        $this->processForm($request, $tag, TagType::class);
        $this->update($tag);

        return $this->view($tag, 200);
    }

    /**
     * @param int $tagId
     *
     * @throws ApiException
     *
     * @return View
     *
     * @Route("/{tagId}", name="tag_delete")
     * @Method("DELETE")
     * @Security("has_role('ROLE_INTERNAL') or has_role('ROLE_ADMIN') or has_role('ROLE_USER')")
     *
     * @ApiDoc(
     *     section="Tag",
     *     description="Delete an Tag",
     *     tags={"stable"},
     *     views={"default", "internal"},
     *     statusCodes={
     *         204="Returned when contact is deleted",
     *         500="Returned on errors"
     *     }
     * )
     */
    public function deleteAction(int $tagId): View
    {
        /* @var Tag $tag */
        $tag = $this->getEntityFinder()->find(['id' => (int) $tagId]);
        $this->checkAccount($tag->getAccount()->getId());
        $this->delete($tag);

        return $this->view(null, 204);
    }
}

<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Service;

use Zendesk\API\Client as ZendeskAPI;

class ZenDesk
{
    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $key;

    /**
     * @var string
     */
    private $subdomain;

    /**
     * @var string
     */
    private $apiurl;

    public function __construct(array $config)
    {
        $this->email     = $config['email'];
        $this->key       = $config['key'];
        $this->subdomain = $config['subdomain'];
        $this->apiurl    = $config['apiurl'];
    }

    public function getInstance() : ZendeskAPI
    {
        $zenDesk = new ZendeskAPI($this->subdomain, $this->email);
        $zenDesk->setAuth('token', $this->key);

        return $zenDesk;
    }
}

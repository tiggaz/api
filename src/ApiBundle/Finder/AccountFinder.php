<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Finder;

class AccountFinder extends AbstractFinder
{
    /**
     * @var string
     */
    protected $entity = 'Itsup\Bundle\ApiBundle\Entity\Account';

    /**
     * @var array
     */
    protected $defined = [
        'id',
        'type',
        'name',
        'state',
        'country',
        'taxId',
        'enabled',
    ];

    /**
     * @var array
     */
    protected $types = [
        'id'      => 'int',
        'enabled' => 'bool',
    ];
}

<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Finder;

class OfferFinder extends AbstractFinder
{
    /**
     * @var string
     */
    protected $entity = 'Itsup\Bundle\ApiBundle\Entity\Offer';

    /**
     * @var array
     */
    protected $defined = [
        'id',
        'name',
        'url',
        'payout_type',
        'default_value',
        'statistics_url',
        'status',
    ];

    /**
     * @var array
     */
    protected $types = [
        'id' => 'int',
    ];
}

<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Finder\Account;

use Itsup\Bundle\ApiBundle\Finder\AbstractFinder;

class PricingFinder extends AbstractFinder
{
    /**
     * @var string
     */
    protected $entity = 'Itsup\Bundle\ApiBundle\Entity\Account\Pricing';

    /**
     * @var array
     */
    protected $defined = [
        'id',
        'account',
        'from',
        'to',
        'cpm',
    ];

    /**
     * @var array
     */
    protected $types = [
        'id'      => 'int',
        'from'    => 'date',
        'to'      => 'date',
        'cpm'     => 'bool',
    ];
}

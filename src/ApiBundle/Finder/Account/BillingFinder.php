<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Finder\Account;

use Itsup\Bundle\ApiBundle\Finder\AbstractFinder;

class BillingFinder extends AbstractFinder
{
    /**
     * @var string
     */
    protected $entity = 'Itsup\Bundle\ApiBundle\Entity\Account\Billing';

    /**
     * @var array
     */
    protected $defined = [
        'account',
        'date',
        'impressions',
        'clicks',
        'events',
        'price',
    ];

    /**
     * @var array
     */
    protected $types = [
        'date'        => 'date',
        'impressions' => 'int',
        'clicks'      => 'int',
        'events'      => 'int',
        'price'       => 'float',
    ];

    /**
     * @var string
     */
    protected $defaultOrder = 'date';
}

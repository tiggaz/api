<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Finder\Account;

use Itsup\Bundle\ApiBundle\Finder\AbstractFinder;

class DomainFinder extends AbstractFinder
{
    /**
     * @var string
     */
    protected $entity = 'Itsup\Bundle\ApiBundle\Entity\Account\Domain';

    /**
     * @var array
     */
    protected $defined = ['id', 'type', 'name', 'ssl', 'defaultDomain', 'installed'];

    /**
     * @var array
     */
    protected $types = ['id' => 'int', 'ssl' => 'bool', 'defaultDomain' => 'bool', 'installed' => 'bool'];
}

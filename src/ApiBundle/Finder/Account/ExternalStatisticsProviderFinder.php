<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Finder\Account;

use Itsup\Bundle\ApiBundle\Finder\AbstractFinder;

class ExternalStatisticsProviderFinder extends AbstractFinder
{
    /**
     * @var string
     */
    protected $entity = 'Itsup\Bundle\ApiBundle\Entity\Account\ExternalStatisticsProvider';

    /**
     * @var array
     */
    protected $defined = ['id', 'account', 'name', 'token', 'username', 'password', 'valid'];

    /**
     * @var array
     */
    protected $types = ['id' => 'int', 'valid' => 'bool'];
}

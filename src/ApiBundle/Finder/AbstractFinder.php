<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Finder;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Itsup\Bundle\ApiBundle\ApiError;
use Itsup\Bundle\ApiBundle\Entity\AbstractEntity;
use Itsup\Bundle\ApiBundle\Exception\ApiException;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;

abstract class AbstractFinder
{
    /**
     * @var Registry
     */
    private $doctrine;

    /**
     * @var string
     */
    protected $entity;

    /**
     * @var array
     */
    protected $defined = [];

    /**
     * @var array
     */
    protected $commonParameters = ['account', 'orderBy'];

    /**
     * @var array
     */
    protected $types = [];

    /**
     * @var string
     */
    protected $defaultOrder = 'id';

    /**
     * @var string
     */
    protected $defaultDirection = 'asc';

    /**
     * AbstractFinder constructor.
     *
     * @param Registry $doctrine
     */
    public function __construct(Registry $doctrine)
    {
        $this->doctrine = $doctrine;
    }

    /**
     * @return Registry
     */
    public function getDoctrine(): Registry
    {
        return $this->doctrine;
    }

    /**
     * @return string
     */
    public function getEntity(): string
    {
        return $this->entity;
    }

    /**
     * @return array
     */
    public function getDefined(): array
    {
        return array_merge($this->defined, $this->commonParameters);
    }

    /**
     * @return array
     */
    public function getTypes(): array
    {
        return $this->types;
    }

    /**
     * @return string
     */
    public function getDefaultOrder(): string
    {
        return $this->defaultOrder;
    }

    /**
     * @return string
     */
    public function getDefaultDirection(): string
    {
        return $this->defaultDirection;
    }

    /**
     * @return EntityManager
     */
    public function getManager(): EntityManager
    {
        return $this->getDoctrine()->getManager();
    }

    /**
     * @param string $name
     *
     * @return EntityRepository
     */
    public function getRepository(string $name): EntityRepository
    {
        return $this->getManager()->getRepository($name);
    }

    /**
     * @param array $parameters
     *
     * @throws ApiException
     *
     * @return ParameterBag
     */
    private function resolveParameters(array $parameters): ParameterBag
    {
        $resolver = new OptionsResolver();
        $this->configureOptions($resolver);

        // Hacky, but we need to allow the api key, if its in the params, and _format
        unset(
            $parameters['i-api-key'],
            $parameters['u-api-key'],
            $parameters['_format']
        );

        try {
            return new ParameterBag($resolver->resolve($parameters));
        } catch (\Exception $e) {
            self::throwException($e, ApiError::INVALID_PARAMETER);
        }
    }

    /**
     * Finds one object with the given parameters.
     *
     * @param array $parameters
     *
     * @throws ApiException
     *
     * @return AbstractEntity
     */
    final public function find(array $parameters): AbstractEntity
    {
        $parameters = $this->resolveParameters($parameters);
        if ($parameters->count() === 0) {
            throw new ApiException(ApiError::INVALID_PARAMETER, 'Must pass a valid parameter to search on.');
        }

        try {
            $query  = $this->getQuery($parameters, true);

            return $query->getSingleResult();
        } catch (\Exception $e) {
            self::throwException($e);
        }
    }

    /**
     * Finds objects with the given parameters.
     *
     * @param array $parameters
     * @param bool  $count
     *
     * @throws ApiException
     *
     * @return array
     */
    final public function findAll(array $parameters, bool $count = false): array
    {
        try {
            $query = $this->getQuery($this->resolveParameters($parameters));

            $res = $query->execute();
            if ($count) {
                return count($res);
            }

            return $res;
        } catch (\Exception $e) {
            self::throwException($e);
        }
    }

    /**
     * Configures the options for the finder.
     *
     * @param OptionsResolver $resolver
     *
     * @return void
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefined($this->getDefined());

        foreach ($this->getTypes() as $key => $type) {
            switch ($type) {
                case 'int':
                    $resolver->setNormalizer(
                        $key,
                        function (Options $options, $value) {
                            return (int) $value;
                        }
                    );
                    break;
                case 'bool':
                    $resolver->setNormalizer(
                        $key,
                        function (Options $options, $value) {
                            return (bool) $value;
                        }
                    );
                    break;
            }
        }
    }

    /**
     * Generate the query with the given parameters.
     *
     * @param ParameterBag $parameters
     * @param bool         $single
     *
     * @return Query
     */
    public function getQuery(ParameterBag $parameters, bool $single = false): Query
    {
        $qb = $this->getManager()->createQueryBuilder();
        $qb->select('t')
            ->from($this->getEntity(), 't');
        foreach ($parameters->all() as $key => $val) {
            if (!empty($val)) {
                $qb->andWhere('t.'.$key.' = :'.$key)
                    ->setParameter($key, $val);
            }
        }
        $qb = $this->addOrderBy($qb, $parameters);
        if ($single) {
            $qb->setMaxResults(1);
        }

        return $qb->getQuery();
    }

    /**
     * Apply order condition to the query.
     *
     * @param QueryBuilder $qb
     * @param ParameterBag $parameters
     * @param string       $tableName
     *
     * @return QueryBuilder
     */
    public function addOrderBy(
        QueryBuilder $qb,
        ParameterBag $parameters,
        string $tableName = 't'
    ): QueryBuilder {
        $order     = $this->getDefaultOrder();
        $direction = $this->getDefaultDirection();
        if ($parameters->has('orderBy')) {
            $tmp       = explode(',', $parameters->get('orderBy'));
            $order     = $tmp[0];
            $direction =
                (isset($tmp[1]) && in_array(strtolower($tmp[1]), ['asc', 'desc'])) ? strtolower($tmp[1]) : 'asc';
        }
        $qb->orderBy($tableName.'.'.$order, $direction);

        return $qb;
    }

    /**
     * Throw an ApiException.
     *
     * @param \Exception $e
     * @param string     $errorType
     *
     * @throws ApiException
     */
    public static function throwException(\Exception $e, string $errorType = ApiError::FINDER)
    {
        if ($e instanceof ApiException) {
            throw $e;
        }

        if (strpos($e->getMessage(), 'No result was found') !== false) {
            $errorType = ApiError::NOT_FOUND;
        }

        throw new ApiException(
            $errorType,
            [
                'message' => $e->getMessage(),
                'file'    => $e->getFile(),
                'line'    => $e->getLine(),
                'stack'   => $e->getTrace(),
            ]
        );
    }
}

<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Finder\AdZone;

use Itsup\Bundle\ApiBundle\Finder\AbstractFinder;

class AccountingFinder extends AbstractFinder
{
    /**
     * @var string
     */
    protected $entity = 'Itsup\Bundle\ApiBundle\Entity\AdZone\Accounting';

    /**
     * @var array
     */
    protected $defined = [
        'id',
        'from',
        'to',
        'type',
        'cost',
    ];

    /**
     * @var array
     */
    protected $types = [
        'id'   => 'int',
        'from' => 'date',
        'to'   => 'date',
        'cost' => 'float',
    ];

    /**
     * @var string
     */
    protected $defaultOrder = 'id';
}

<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Finder\AdZone;

use Itsup\Bundle\ApiBundle\Finder\AbstractFinder;

class AdZoneCampaignFinder extends AbstractFinder
{
    /**
     * @var string
     */
    protected $entity = 'Itsup\Bundle\ApiBundle\Entity\AdZone\AdZoneCampaign';

    /**
     * @var array
     */
    protected $defined = [
        'adZone',
        'campaign',
        'weight',
        'status',
        'impressionsLimit',
    ];

    /**
     * @var array
     */
    protected $types = [
        'weight'           => 'int',
        'impressionsLimit' => 'int',
    ];

    /**
     * @var string
     */
    protected $defaultOrder = 'campaign';
}

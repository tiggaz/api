<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Finder;

class AdZoneFinder extends AbstractFinder
{
    /**
     * @var string
     */
    protected $entity = 'Itsup\Bundle\ApiBundle\Entity\AdZone';

    /**
     * @var array
     */
    protected $defined = [
        'id',
        'name',
    ];

    /**
     * @var array
     */
    protected $types = [
        'id' => 'int',
    ];
}

<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Finder;

class ContactFinder extends AbstractFinder
{
    /**
     * @var string
     */
    protected $entity = 'Itsup\Bundle\ApiBundle\Entity\Contact';

    /**
     * @var array
     */
    protected $defined = [
        'id',
        'name',
        'email',
        'skype',
        'phone',
        'company',
        'country',
        'website_url',
        'publisher',
        'broker',
        'advertiser',
    ];

    /**
     * @var array
     */
    protected $types = [
        'id' => 'int',
    ];
}

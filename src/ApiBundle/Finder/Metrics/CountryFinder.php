<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Finder\Metrics;

use Itsup\Bundle\ApiBundle\Finder\AbstractFinder;

class CountryFinder extends AbstractFinder
{
    /**
     * @var string
     */
    protected $entity = 'Itsup\Bundle\ApiBundle\Entity\Metrics\Country';

    /**
     * @var array
     */
    protected $defined = [
        'id',
        'name',
        'officialName',
        'countryCode3',
        'region',
        'subRegion',
        'position',
    ];

    /**
     * @var array
     */
    protected $types = ['position' => 'int'];

    /**
     * @var string
     */
    protected $defaultOrder = 'id';
}

<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Finder\Metrics;

use Itsup\Bundle\ApiBundle\Finder\AbstractFinder;

class InternetServiceProviderFinder extends AbstractFinder
{
    /**
     * @var string
     */
    protected $entity = 'Itsup\Bundle\ApiBundle\Entity\Metrics\InternetServiceProvider';

    /**
     * @var array
     */
    protected $defined = [
        'id',
        'name',
    ];

    /**
     * @var array
     */
    protected $types = ['id' => 'int'];

    /**
     * @var string
     */
    protected $defaultOrder = 'id';
}

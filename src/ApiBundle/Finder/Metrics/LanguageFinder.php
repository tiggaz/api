<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Finder\Metrics;

use Itsup\Bundle\ApiBundle\Finder\AbstractFinder;

class LanguageFinder extends AbstractFinder
{
    /**
     * @var string
     */
    protected $entity = 'Itsup\Bundle\ApiBundle\Entity\Metrics\Language';

    /**
     * @var array
     */
    protected $defined = [
        'id',
        'name',
        'position',
    ];

    /**
     * @var string
     */
    protected $defaultOrder = 'id';
}

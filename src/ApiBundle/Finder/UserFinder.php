<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Finder;

class UserFinder extends AbstractFinder
{
    /**
     * @var string
     */
    protected $entity = 'Itsup\Bundle\ApiBundle\Entity\User';

    /**
     * @var array
     */
    protected $defined = [
        'id',
        'name',
        'username',
        'email',
        'roles',
        'apiKey',
        'enabled',
    ];

    /**
     * @var array
     */
    protected $types = [
        'id'      => 'int',
        'enabled' => 'bool',
    ];
}

<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Finder\Campaign;

use Itsup\Bundle\ApiBundle\Finder\AbstractFinder;

class SaleFinder extends AbstractFinder
{
    /**
     * @var string
     */
    protected $entity = 'Itsup\Bundle\ApiBundle\Entity\Campaign\Sale';

    /**
     * @var array
     */
    protected $defined = [
        'id',
        'campaign',
        'limit',
        'type',
        'from',
        'to',
        'expire',
    ];

    /**
     * @var array
     */
    protected $types = [];

    /**
     * @var string
     */
    protected $defaultOrder = 'id';
}

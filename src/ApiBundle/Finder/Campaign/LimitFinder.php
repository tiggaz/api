<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Finder\Campaign;

use Itsup\Bundle\ApiBundle\Finder\AbstractFinder;

class LimitFinder extends AbstractFinder
{
    /**
     * @var string
     */
    protected $entity = 'Itsup\Bundle\ApiBundle\Entity\Campaign\Limit';

    /**
     * @var array
     */
    protected $defined = [
        'id',
        'campaign',
        'from',
        'to',
        'dateReached',
        'typeReached',
    ];

    /**
     * @var array
     */
    protected $types = [];

    /**
     * @var string
     */
    protected $defaultOrder = 'id';
}

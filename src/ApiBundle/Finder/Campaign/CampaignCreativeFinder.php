<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Finder\Campaign;

use Itsup\Bundle\ApiBundle\Finder\AbstractFinder;

class CampaignCreativeFinder extends AbstractFinder
{
    /**
     * @var string
     */
    protected $entity = 'Itsup\Bundle\ApiBundle\Entity\Campaign\CampaignCreative';

    /**
     * @var array
     */
    protected $defined = [
        'campaign',
        'creative',
        'weight',
        'status',
        'url',
    ];

    /**
     * @var array
     */
    protected $types = [
        'weight'           => 'int',
    ];

    /**
     * @var string
     */
    protected $defaultOrder = 'campaign';
}

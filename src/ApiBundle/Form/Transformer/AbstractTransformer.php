<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Form\Transformer;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Itsup\Bundle\ApiBundle\ApiError;
use Itsup\Bundle\ApiBundle\Entity\AbstractEntity;
use Itsup\Bundle\ApiBundle\Exception\ApiException;
use Symfony\Component\Form\DataTransformerInterface;

abstract class AbstractTransformer implements DataTransformerInterface
{
    /**
     * @var Registry
     */
    private $doctrine;

    /**
     * @var string
     */
    protected $entity;

    /**
     * AbstractTransformer constructor.
     *
     * @param Registry $doctrine
     */
    public function __construct(Registry $doctrine)
    {
        $this->doctrine = $doctrine;
    }

    /**
     * @return string
     */
    public function getEntity(): string
    {
        return $this->entity;
    }

    /**
     * @return Registry
     */
    public function getDoctrine(): Registry
    {
        return $this->doctrine;
    }

    /**
     * @return EntityManager
     */
    public function getManager(): EntityManager
    {
        return $this->getDoctrine()->getManager();
    }

    /**
     * @param string $name
     *
     * @return EntityRepository
     */
    public function getRepository(string $name): EntityRepository
    {
        return $this->getManager()->getRepository($name);
    }

    /**
     * Return the object Id.
     *
     * @param AbstractEntity $object
     *
     * @return string
     */
    public function transform($object): string
    {
        if (is_null($object)) {
            return '';
        }

        return $object->getId();
    }

    /**
     * Return an object for the given Id.
     *
     * @param string $id
     *
     * @throws ApiException
     *
     * @return AbstractEntity
     */
    public function reverseTransform($id): AbstractEntity
    {
        if (empty($id)) {
            throw new ApiException(ApiError::INVALID_PARAMETER, [sprintf('Missing %s Id', $this->getEntity())]);
        }

        $object = $this->getRepository($this->getEntity())->find(['id' => $id]);

        if (empty($object)) {
            throw new ApiException(
                ApiError::NOT_FOUND,
                [sprintf('%s with Id "%s" does not exist!', [$this->getEntity(), $id])]
            );
        }

        return $object;
    }
}

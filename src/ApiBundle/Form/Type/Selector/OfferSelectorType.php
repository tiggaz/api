<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Form\Type\Selector;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Itsup\Bundle\ApiBundle\Form\Transformer\OfferTransformer;

class OfferSelectorType extends AbstractSelectorType
{
    /**
     * @var string
     */
    protected $type = 'offer';

    public function __construct(Registry $doctrine)
    {
        parent::__construct($doctrine);
        $this->transformer = new OfferTransformer($this->doctrine);
    }
}

<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Form\Type\Selector;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Itsup\Bundle\ApiBundle\Form\Transformer\ContactTransformer;

class ContactSelectorType extends AbstractSelectorType
{
    /**
     * @var string
     */
    protected $type = 'contact';

    public function __construct(Registry $doctrine)
    {
        parent::__construct($doctrine);
        $this->transformer = new ContactTransformer($this->doctrine);
    }
}

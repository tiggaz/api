<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Form\Type\Selector\Campaign;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Itsup\Bundle\ApiBundle\Form\Transformer\Campaign\LimitTransformer;
use Itsup\Bundle\ApiBundle\Form\Type\Selector\AbstractSelectorType;

class LimitSelectorType extends AbstractSelectorType
{
    /**
     * @var string
     */
    protected $type = 'limit';

    public function __construct(Registry $doctrine)
    {
        parent::__construct($doctrine);
        $this->transformer = new LimitTransformer($this->doctrine);
    }
}

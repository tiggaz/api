<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Form\Type\Selector;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Itsup\Bundle\ApiBundle\Form\Transformer\AbstractTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

abstract class AbstractSelectorType extends AbstractType
{
    /**
     * @var string
     */
    protected $type;

    /**
     * @var Registry
     */
    protected $doctrine;

    /**
     * @var AbstractTransformer
     */
    protected $transformer;

    public function __construct(Registry $doctrine)
    {
        $this->doctrine = $doctrine;
    }

    /**
     * Convert object id to object.
     * Allow to use object id instead of whole object in Forms.
     *
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addModelTransformer($this->transformer);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'invalid_message' => 'The selected '.$this->type.' does not exist',
        ]);
    }

    public function getParent()
    {
        return TextType::class;
    }
}

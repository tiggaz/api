<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Form\Type\Selector\Account;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Itsup\Bundle\ApiBundle\Form\Transformer\Account\ExternalStatisticsProviderTransformer;
use Itsup\Bundle\ApiBundle\Form\Type\Selector\AbstractSelectorType;

class ExternalStatisticsProviderSelectorType extends AbstractSelectorType
{
    /**
     * @var string
     */
    protected $type = 'esp';

    public function __construct(Registry $doctrine)
    {
        parent::__construct($doctrine);
        $this->transformer = new ExternalStatisticsProviderTransformer($this->doctrine);
    }
}

<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Form\Type;

use Itsup\Bundle\ApiBundle\Form\Type\Selector\AccountSelectorType;
use Itsup\Bundle\ApiBundle\Form\Type\Selector\CampaignSelectorType;
use Itsup\Bundle\ApiBundle\Form\Type\Selector\EventSelectorType;
use Itsup\Bundle\ApiBundle\Form\Type\Selector\OfferSelectorType;
use Itsup\Bundle\ApiBundle\Form\Type\Selector\UserSelectorType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class CampaignType extends AbstractEntityType
{
    /**
     * @var string
     */
    protected $dataClass = 'Itsup\Bundle\ApiBundle\Entity\Campaign';

    /**
     * @var string
     */
    protected $prefix = 'Campaign';

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('id', IntegerType::class)
            ->add('account', AccountSelectorType::class)
            ->add('offer', OfferSelectorType::class)
            ->add('name', TextType::class)
            ->add(
                'revenue',
                ChoiceType::class,
                [
                    'choices'  => [
                        'conversion' => 'conversion',
                        'sale'       => 'sale',
                    ],
                ]
            )
            ->add(
                'keepAlive',
                ChoiceType::class,
                [
                    'choices'  => [
                        true  => true,
                        false => false,
                    ],
                ]
            )
            ->add('fallbackCampaign', CampaignSelectorType::class)
            ->add('event', EventSelectorType::class)
            ->add(
                'type',
                ChoiceType::class,
                [
                    'choices'  => [
                        'iframe'      => 'iframe',
                        'direct link' => 'direct link',
                        'text'        => 'text',
                        'json'        => 'json',
                        'popunder'    => 'popunder',
                        'image'       => 'image',
                    ],
                ]
            )
            ->add('width', IntegerType::class)
            ->add('height', IntegerType::class)
            ->add('url', TextType::class)
            ->add(
                'status',
                ChoiceType::class,
                [
                    'choices'  => [
                        'on'  => 'on',
                        'off' => 'off',
                    ],
                ]
            )
            ->add('dateCreated', DateTimeType::class)
            ->add('dateUpdated', DateTimeType::class)
            ->add('createdBy', UserSelectorType::class)
            ->add('updatedBy', UserSelectorType::class)
            ->add(
                'tags',
                CollectionType::class,
                [
                    'entry_type' => TagType::class,
                    'allow_add'  => true,
                ]
            );
    }
}

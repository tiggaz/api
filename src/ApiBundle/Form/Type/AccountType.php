<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Form\Type;

use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class AccountType extends AbstractEntityType
{
    /**
     * @var string
     */
    protected $dataClass = 'Itsup\Bundle\ApiBundle\Entity\Account';

    /**
     * @var string
     */
    protected $prefix = 'Account';

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('id', IntegerType::class)
            ->add('type', ChoiceType::class, [
                'required' => false,
                'multiple' => false,
                'choices'  => [
                    'individual' => 'individual',
                    'business'   => 'business',
                ],
            ])
            ->add('name', TextType::class)
            ->add('address', TextType::class)
            ->add('addressComplement', TextType::class)
            ->add('city', TextType::class)
            ->add('zipCode', TextType::class)
            ->add('state', TextType::class)
            ->add('country', TextType::class)
            ->add('taxId', TextType::class)
            ->add('websiteUrl', TextType::class)
            ->add('invoiceFrom', ChoiceType::class, [
                'required' => false,
                'data'     => 'sctr',
                'multiple' => false,
                'choices'  => [
                    'sctr' => 'sctr',
                ],
            ])
            ->add('enabled', ChoiceType::class, [
                'required' => false,
                'data'     => true,
                'choices'  => [
                    true  => true,
                    false => false,
                ],
            ]);
    }
}

<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Form\Type;

use Itsup\Bundle\ApiBundle\Form\Type\Selector\AccountSelectorType;
use Itsup\Bundle\ApiBundle\Form\Type\Selector\ContactSelectorType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;

class OfferType extends AbstractEntityType
{
    /**
     * @var string
     */
    protected $dataClass = 'Itsup\Bundle\ApiBundle\Entity\Offer';

    /**
     * @var string
     */
    protected $prefix = 'Offer';

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('id', IntegerType::class)
                ->add('account', AccountSelectorType::class)
                ->add('contact', ContactSelectorType::class)
                ->add('name', TextType::class)
                ->add('url', UrlType::class)
                ->add(
                    'payoutType',
                    ChoiceType::class,
                    [
                        'choices'  => [
                            'pps'        => 'pps',
                            'revshare'   => 'revshare',
                            'ppl'        => 'ppl',
                            'cpc'        => 'cpc',
                            'cpm imps'   => 'cpm imps',
                            'cpm clicks' => 'cpm clicks',
                        ],
                    ]
                )
                ->add('defaultValue', IntegerType::class)
                ->add('statisticsUrl', TextareaType::class)
                ->add(
                    'status',
                    ChoiceType::class,
                    [
                        'choices'  => [
                            'active'   => 'active',
                            'paused'   => 'paused',
                            'inactive' => 'inactive',
                        ],
                    ]
                )
                ->add(
                    'tags',
                    CollectionType::class,
                    [
                        'entry_type' => TagType::class,
                        'allow_add'  => true,
                    ]
                );
    }
}

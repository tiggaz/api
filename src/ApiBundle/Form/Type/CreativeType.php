<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Form\Type;

use Itsup\Bundle\ApiBundle\Form\Type\Selector\AccountSelectorType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class CreativeType extends AbstractEntityType
{
    /**
     * @var string
     */
    protected $dataClass = 'Itsup\Bundle\ApiBundle\Entity\Creative';

    /**
     * @var string
     */
    protected $prefix = 'Creative';

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('id', IntegerType::class)
            ->add('account', AccountSelectorType::class)
            ->add('name', TextType::class)
            ->add(
                'type',
                ChoiceType::class,
                [
                    'choices'  => [
                        'iframe'             => 'iframe',
                        'image'              => 'image',
                        'text'               => 'text',
                        'popunder'           => 'popunder',
                    ],
                ]
            )
            ->add('width', IntegerType::class)
            ->add('height', IntegerType::class)
            ->add('code', TextareaType::class)
            ->add(
                'status',
                ChoiceType::class,
                [
                    'choices'  => [
                        'on'    => 'on',
                        'off'   => 'off',
                    ],
                ]
            )
            ->add('size', IntegerType::class)
            ->add(
                'tags',
                CollectionType::class,
                [
                    'entry_type' => TagType::class,
                    'allow_add'  => true,
                ]
            );
    }
}

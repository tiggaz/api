<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Form\Type;

use Itsup\Bundle\ApiBundle\Form\Type\Selector\AccountSelectorType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class EventType extends AbstractEntityType
{
    /**
     * @var string
     */
    protected $dataClass = 'Itsup\Bundle\ApiBundle\Entity\Event';

    /**
     * @var string
     */
    protected $prefix = 'Event';

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('id', IntegerType::class)
            ->add('account', AccountSelectorType::class)
            ->add('name', TextType::class)
            ->add('type', ChoiceType::class, [
                'required' => false,
                'multiple' => false,
                'choices'  => [
                    'count'       => 'count',
                    'conversion'  => 'conversion',
                    'transaction' => 'transaction',
                ],
            ])
            ->add('value', IntegerType::class);
    }
}

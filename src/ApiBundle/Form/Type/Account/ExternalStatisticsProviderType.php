<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Form\Type\Account;

use Itsup\Bundle\ApiBundle\Form\Type\AbstractEntityType;
use Itsup\Bundle\ApiBundle\Form\Type\Selector\AccountSelectorType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class ExternalStatisticsProviderType extends AbstractEntityType
{
    /**
     * @var string
     */
    protected $dataClass = 'Itsup\Bundle\ApiBundle\Entity\Account\ExternalStatisticsProvider';

    /**
     * @var string
     */
    protected $prefix = 'ExternalStatisticsProvider';

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('id', IntegerType::class)
            ->add('account', AccountSelectorType::class)
            ->add('name', ChoiceType::class, [
                'required' => false,
                'multiple' => false,
                'choices'  => [
                    'trafficjunky' => 'trafficjunky',
                    'exoclick'     => 'exoclick',
                    'trafficforce' => 'trafficforce',
                    'reporo'       => 'reporo',
                    'adnium'       => 'adnium',
                ],
            ])
            ->add('token', TextType::class)
            ->add('username', TextType::class)
            ->add('password', TextType::class)
            ->add('valid', ChoiceType::class, [
                'required' => false,
                'data'     => true,
                'choices'  => [
                    true  => true,
                    false => false,
                ],
            ]);
    }
}

<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Form\Type\Account;

use Itsup\Bundle\ApiBundle\Form\Type\AbstractEntityType;
use Itsup\Bundle\ApiBundle\Form\Type\Selector\AccountSelectorType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class PricingType extends AbstractEntityType
{
    /**
     * @var string
     */
    protected $dataClass = 'Itsup\Bundle\ApiBundle\Entity\Account\Pricing';

    /**
     * @var string
     */
    protected $prefix = 'Pricing';

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('id', IntegerType::class)
            ->add('account', AccountSelectorType::class)
            ->add('from', TextType::class)
            ->add('to', TextType::class)
            ->add('cpm', NumberType::class);
    }
}

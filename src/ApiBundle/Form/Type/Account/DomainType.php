<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Form\Type\Account;

use Itsup\Bundle\ApiBundle\Form\Type\AbstractEntityType;
use Itsup\Bundle\ApiBundle\Form\Type\Selector\AccountSelectorType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class DomainType extends AbstractEntityType
{
    /**
     * @var string
     */
    protected $dataClass = 'Itsup\Bundle\ApiBundle\Entity\Account\Domain';

    /**
     * @var string
     */
    protected $prefix = 'Domain';

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('id', IntegerType::class)
            ->add('account', AccountSelectorType::class)
            ->add('type', ChoiceType::class, [
                'multiple' => false,
                'choices'  => [
                    'tracking' => 'tracking',
                    'cdn'      => 'cdn',
                ],
            ])
            ->add('name', TextType::class)
            ->add('ssl', ChoiceType::class, [
                'data'     => true,
                'choices'  => [
                    true  => true,
                    false => false,
                ],
            ])
            ->add('defaultDomain', ChoiceType::class, [
                'data'     => true,
                'choices'  => [
                    true  => true,
                    false => false,
                ],
            ])
            ->add('installed', ChoiceType::class, [
                'data'     => true,
                'choices'  => [
                    true  => true,
                    false => false,
                ],
            ]);
    }
}

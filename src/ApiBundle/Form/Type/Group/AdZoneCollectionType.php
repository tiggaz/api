<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Form\Type\Group;

use Itsup\Bundle\ApiBundle\Form\Type\Selector\AdZoneSelectorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;

class AdZoneCollectionType extends AbstractType
{
    /**
     * @var string
     */
    protected $dataClass = '';

    /**
     * @var string
     */
    protected $prefix = '';

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'collection',
            CollectionType::class,
            [
                'entry_type' => AdZoneSelectorType::class,
                'allow_add'  => true,
            ]
        );
    }
}

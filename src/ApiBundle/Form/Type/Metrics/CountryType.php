<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Form\Type\Metrics;

use Itsup\Bundle\ApiBundle\Form\Type\AbstractEntityType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class CountryType extends AbstractEntityType
{
    /**
     * @var string
     */
    protected $dataClass = 'Itsup\Bundle\ApiBundle\Entity\Metrics\Country';

    /**
     * @var string
     */
    protected $prefix = 'Country';

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('id', TextType::class)
            ->add('name', TextType::class)
            ->add('officialName', TextType::class)
            ->add('countryCode3', TextType::class)
            ->add('region', TextType::class)
            ->add('subRegion', TextType::class)
            ->add('position', IntegerType::class);
    }
}

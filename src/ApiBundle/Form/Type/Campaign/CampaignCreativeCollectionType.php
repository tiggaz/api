<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Form\Type\Campaign;

use Itsup\Bundle\ApiBundle\Form\Type\AbstractEntityType;
use Itsup\Bundle\ApiBundle\Form\Type\Selector\CampaignSelectorType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;

class CampaignCreativeCollectionType extends AbstractEntityType
{
    /**
     * @var string
     */
    protected $dataClass = 'Itsup\Bundle\ApiBundle\Model\Campaign\CampaignCreativeCollection';

    /**
     * @var string
     */
    protected $prefix = 'CampaignCreativeCollection';

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('campaign', CampaignSelectorType::class)
            ->add(
                'creatives',
                CollectionType::class,
                [
                    'entry_type' => CampaignCreativeType::class,
                    'allow_add'  => true,
                ]
            );
    }
}

<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Form\Type\Campaign;

use Itsup\Bundle\ApiBundle\Form\Type\Selector\Metrics\OperatingSystemSelectorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;

class OperatingSystemCollectionType extends AbstractType
{
    /**
     * @var string
     */
    protected $dataClass = '';

    /**
     * @var string
     */
    protected $prefix = '';

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'collection',
            CollectionType::class,
            [
                'entry_type' => OperatingSystemSelectorType::class,
                'allow_add'  => true,
            ]
        );
    }
}

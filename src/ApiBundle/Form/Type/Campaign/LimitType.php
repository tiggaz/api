<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Form\Type\Campaign;

use Itsup\Bundle\ApiBundle\Form\Type\AbstractEntityType;
use Itsup\Bundle\ApiBundle\Form\Type\Selector\CampaignSelectorType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class LimitType extends AbstractEntityType
{
    /**
     * @var string
     */
    protected $dataClass = 'Itsup\Bundle\ApiBundle\Entity\Campaign\Limit';

    /**
     * @var string
     */
    protected $prefix = 'Limit';

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('id', IntegerType::class)
            ->add('campaign', CampaignSelectorType::class)
            ->add('from', TextType::class)
            ->add('to', TextType::class)
            ->add('impressions', IntegerType::class)
            ->add('clicks', IntegerType::class)
            ->add('dateReached', TextType::class)
            ->add('typeReached', ChoiceType::class, [
                'data'    => true,
                'choices' => [
                    'impressions' => 'impressions',
                    'clicks'      => 'clicks',
                ],
            ]);
    }
}

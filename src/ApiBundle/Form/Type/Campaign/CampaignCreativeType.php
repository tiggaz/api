<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Form\Type\Campaign;

use Itsup\Bundle\ApiBundle\Form\Type\AbstractEntityType;
use Itsup\Bundle\ApiBundle\Form\Type\Selector\CampaignSelectorType;
use Itsup\Bundle\ApiBundle\Form\Type\Selector\CreativeSelectorType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class CampaignCreativeType extends AbstractEntityType
{
    /**
     * @var string
     */
    protected $dataClass = 'Itsup\Bundle\ApiBundle\Entity\Campaign\CampaignCreative';

    /**
     * @var string
     */
    protected $prefix = 'CampaignCreative';

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('campaign', CampaignSelectorType::class)
            ->add('creative', CreativeSelectorType::class)
            ->add('weight', IntegerType::class)
            ->add('status', ChoiceType::class, [
                'data'    => true,
                'choices' => [
                    'on'  => 'on',
                    'off' => 'off',
                ],
            ])
            ->add('url', TextType::class);
    }
}

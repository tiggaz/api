<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Form\Type\Campaign;

use Itsup\Bundle\ApiBundle\Form\Type\AbstractEntityType;
use Itsup\Bundle\ApiBundle\Form\Type\Selector\Campaign\LimitSelectorType;
use Itsup\Bundle\ApiBundle\Form\Type\Selector\CampaignSelectorType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class SaleType extends AbstractEntityType
{
    /**
     * @var string
     */
    protected $dataClass = 'Itsup\Bundle\ApiBundle\Entity\Campaign\Sale';

    /**
     * @var string
     */
    protected $prefix = 'Sale';

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('id', IntegerType::class)
            ->add('campaign', CampaignSelectorType::class)
            ->add('limit', LimitSelectorType::class)
            ->add('type', ChoiceType::class, [
                'data'    => true,
                'choices' => [
                    'flat'       => 'flat',
                    'cpm imps'   => 'cpm imps',
                    'cpc'        => 'cpc',
                    'cpm clicks' => 'cpm clicks',
                ],
            ])
            ->add('from', TextType::class)
            ->add('to', TextType::class)
            ->add('expire', ChoiceType::class, [
                'data'    => true,
                'choices' => [
                    true  => true,
                    false => false,
                ],
            ])
            ->add('price', NumberType::class);
    }
}

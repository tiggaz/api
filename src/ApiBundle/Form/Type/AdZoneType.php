<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Form\Type;

use Itsup\Bundle\ApiBundle\Form\Type\Selector\Account\ExternalStatisticsProviderSelectorType;
use Itsup\Bundle\ApiBundle\Form\Type\Selector\AccountSelectorType;
use Itsup\Bundle\ApiBundle\Form\Type\Selector\CampaignSelectorType;
use Itsup\Bundle\ApiBundle\Form\Type\Selector\ContactSelectorType;
use Itsup\Bundle\ApiBundle\Form\Type\Selector\UserSelectorType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class AdZoneType extends AbstractEntityType
{
    /**
     * @var string
     */
    protected $dataClass = 'Itsup\Bundle\ApiBundle\Entity\AdZone';

    /**
     * @var string
     */
    protected $prefix = 'AdZone';

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('id', IntegerType::class)
            ->add('account', AccountSelectorType::class)
            ->add('contact', ContactSelectorType::class)
            ->add('name', TextType::class)
            ->add(
                'hosting',
                ChoiceType::class,
                [
                    'choices'  => [
                        'local'  => 'local',
                        'remote' => 'remote',
                    ],
                ]
            )
            ->add(
                'type',
                ChoiceType::class,
                [
                    'choices'  => [
                        'iframe'      => 'iframe',
                        'direct link' => 'direct link',
                        'text'        => 'text',
                        'json'        => 'json',
                        'popunder'    => 'popunder',
                    ],
                ]
            )
            ->add('width', IntegerType::class)
            ->add('height', IntegerType::class)
            ->add(
                'status',
                ChoiceType::class,
                [
                    'choices'  => [
                        'on'     => 'on',
                        'off'    => 'off',
                        'paused' => 'paused',
                    ],
                ]
            )
            ->add(
                'display',
                ChoiceType::class,
                [
                    'choices'  => [
                        'order'  => 'order',
                        'weight' => 'weight',
                    ],
                ]
            )
            ->add('defaultCampaign', CampaignSelectorType::class)
            ->add('externalStatisticsProvider', ExternalStatisticsProviderSelectorType::class)
            ->add('externalStatisticsId', TextType::class)
            ->add('externalStatisticsIdComplement', TextType::class)
            ->add(
                'externalStatisticsOptions',
                ChoiceType::class,
                [
                    'multiple' => true,
                    'choices'  => [
                        'imps' => 'imps',
                        'clks' => 'clks',
                        'cost' => 'cost',
                    ],
                ]
            )
            ->add('createdBy', UserSelectorType::class)
            ->add('updatedBy', UserSelectorType::class)
            ->add(
                'tags',
                CollectionType::class,
                [
                    'entry_type'   => TagType::class,
                    'allow_add'    => true,
                    'allow_delete' => true,
                    'delete_empty' => true,
                ]
            );
    }
}

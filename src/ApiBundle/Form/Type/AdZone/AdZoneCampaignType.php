<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Form\Type\AdZone;

use Itsup\Bundle\ApiBundle\Form\Type\AbstractEntityType;
use Itsup\Bundle\ApiBundle\Form\Type\Selector\AdZoneSelectorType;
use Itsup\Bundle\ApiBundle\Form\Type\Selector\CampaignSelectorType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;

class AdZoneCampaignType extends AbstractEntityType
{
    /**
     * @var string
     */
    protected $dataClass = 'Itsup\Bundle\ApiBundle\Entity\AdZone\AdZoneCampaign';

    /**
     * @var string
     */
    protected $prefix = 'AdZoneCampaign';

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('adZone', AdZoneSelectorType::class)
            ->add('campaign', CampaignSelectorType::class)
            ->add('weight', IntegerType::class)
            ->add('status', ChoiceType::class, [
                'required' => false,
                'data'     => true,
                'choices'  => [
                    'active'   => 'active',
                    'paused'   => 'paused',
                    'archived' => 'archived',
                ],
            ])
            ->add('impressionsLimit', IntegerType::class);
    }
}

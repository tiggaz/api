<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Form\Type\AdZone;

use Itsup\Bundle\ApiBundle\Form\Type\AbstractEntityType;
use Itsup\Bundle\ApiBundle\Form\Type\Selector\AdZoneSelectorType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;

class AdZoneCampaignCollectionType extends AbstractEntityType
{
    /**
     * @var string
     */
    protected $dataClass = 'Itsup\Bundle\ApiBundle\Model\AdZone\AdZoneCampaignCollection';

    /**
     * @var string
     */
    protected $prefix = 'AdZoneCampaignCollection';

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('adZone', AdZoneSelectorType::class)
            ->add(
                'campaigns',
                CollectionType::class,
                [
                    'entry_type' => AdZoneCampaignType::class,
                    'allow_add'  => true,
                ]
            );
    }
}

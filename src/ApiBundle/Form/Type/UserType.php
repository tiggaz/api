<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Form\Type;

use Itsup\Bundle\ApiBundle\Form\Type\Selector\AccountSelectorType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class UserType extends AbstractEntityType
{
    /**
     * @var string
     */
    protected $dataClass = 'Itsup\Bundle\ApiBundle\Entity\User';

    /**
     * @var string
     */
    protected $prefix = 'User';

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('id', IntegerType::class)
            ->add('account', AccountSelectorType::class)
            ->add('email', EmailType::class)
            ->add('name', TextType::class)
            ->add('username', TextType::class)
            ->add('password', TextType::class)
            ->add('salt', TextType::class)
            ->add(
                'roles',
                ChoiceType::class,
                [
                    'multiple' => true,
                    'data'     => ['ROLE_USER'],
                    'choices'  => [
                        'ROLE_USER'        => 'ROLE_USER',
                        'ROLE_ADMIN'       => 'ROLE_ADMIN',
                        'ROLE_SUPER_ADMIN' => 'ROLE_SUPER_ADMIN',
                    ],
                ]
            )
            ->add('phone', TextType::class)
            ->add('skype', TextType::class)
            ->add('timezone', TextType::class)
            ->add(
                'enabled',
                ChoiceType::class,
                [
                    'data'     => true,
                    'choices'  => [
                        true  => true,
                        false => false,
                    ],
                ]
            );
    }
}

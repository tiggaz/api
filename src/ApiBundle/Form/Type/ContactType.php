<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Form\Type;

use Itsup\Bundle\ApiBundle\Form\Type\Selector\AccountSelectorType;
use Itsup\Bundle\ApiBundle\Form\Type\Selector\UserSelectorType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;

class ContactType extends AbstractEntityType
{
    /**
     * @var string
     */
    protected $dataClass = 'Itsup\Bundle\ApiBundle\Entity\Contact';

    /**
     * @var string
     */
    protected $prefix = 'Contact';

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('id', IntegerType::class)
            ->add('account', AccountSelectorType::class)
            ->add('user', UserSelectorType::class)
            ->add('name', TextType::class)
            ->add('email', EmailType::class)
            ->add('skype', TextType::class)
            ->add('phone', TextType::class)
            ->add('company', TextType::class)
            ->add('country', TextType::class)
            ->add('websiteUrl', UrlType::class)
            ->add(
                'publisher',
                ChoiceType::class,
                [
                    'choices'  => [
                        true  => true,
                        false => false,
                    ],
                ]
            )
            ->add(
                'broker',
                ChoiceType::class,
                [
                    'choices'  => [
                        true  => true,
                        false => false,
                    ],
                ]
            )
            ->add(
                'advertiser',
                ChoiceType::class,
                [
                    'choices'  => [
                        true  => true,
                        false => false,
                    ],
                ]
            );
    }
}

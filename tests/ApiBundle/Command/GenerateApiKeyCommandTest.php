<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Tests\Command;

/**
 * @author Martin Karadzinov <martin@myhost.mk>
 */
class GenerateApiKeyCommandTest extends AbstractCommandTestCase
{
    public function setUp()
    {
        parent::setUp();
    }

    public function testExecuteInternal()
    {
        $output = $this->runCommand($this->client, 'itsup:api:key:generate --type=internal');

        $this->assertRegExp('/Internal already has an API key/', $output);
    }
}

<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Tests;

use Cache\CacheBundle\Cache\Recording\TaggablePool;
use Doctrine\ORM\EntityManager;
use Itsup\Bundle\ApiBundle\Entity\Account;
use Itsup\Bundle\ApiBundle\Entity\User;
use Itsup\Bundle\ApiBundle\Generator\ApiKeyGenerator;
use Itsup\Bundle\ApiBundle\Model\InternalUser;
use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

/**
 * @author Martin Karadzinov <martin@myhost.mk>
 */
abstract class AbstractTestCase extends WebTestCase
{
    const ITEM_CACHE_TTL = 1800;

    protected static $class = 'AppKernel';

    /**
     * @var Client
     */
    protected $client = null;

    /**
     * @var TaggablePool
     */
    protected $cache;

    /**
     * @var EntityManager
     */
    protected $orm;

    /**
     * @var Account
     */
    protected $account;

    /**
     * @var User
     */
    protected $user;

    /**
     * @var User
     */
    protected $admin;

    /**
     * @var InternalUser
     */
    protected $internal;

    /**
     * @var array
     */
    protected $results = [];

    public function setUp()
    {
        parent::setUp();

        $this->client = static::createClient();
        $this->orm    = $this->client->getContainer()->get('doctrine')->getManager();
        $this->cache  = $this->client->getContainer()->get('cache');
        $this->initializeCache();

        // Hack so the Account and users are not re-created after the end of the tests
        if ($this->checkIfTestLeft()) {
            $this->createUsers();
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * Create the account and users needed to run the tests.
     *
     * @throws \Exception
     */
    private function createUsers()
    {
        $finder = $this->client->getContainer()->get('finder.account');
        try {
            $this->account = $finder->find(['name' => 'api_test_account']);
        } catch (\Exception $e) {
            if ($e->getCode() !== 404) {
                throw $e;
            }
            $this->account = new Account();
            $this->account->setName('api_test_account');
            $this->account->setType('business');
            $this->account->setAddress('address');
            $this->account->setZipCode('09678');
            $this->account->setCity('city');
            $this->account->setCountry('US');
            $this->account->setWebsiteUrl('http://www.itsup.com');
            $this->orm->persist($this->account);
        }

        $finder = $this->client->getContainer()->get('finder.user');
        try {
            $this->admin = $finder->find(['email' => 'adminApiTest@itsup.com']);
        } catch (\Exception $e) {
            if ($e->getCode() !== 404) {
                throw $e;
            }
            $this->admin = new User();
            $this->admin->setAccount($this->account);
            $this->admin->setEmail('adminApiTest@itsup.com');
            $this->admin->setName('api Admin');
            $this->admin->setUsername('apiAdmin');
            $this->admin->setPassword('somePassword');
            $this->admin->setRoles(['ROLE_USER', 'ROLE_ADMIN']);
            $this->admin->setApiKey(ApiKeyGenerator::generate());
            $this->orm->persist($this->admin);
        }
        try {
            $this->user = $finder->find(['email' => 'userApiTest@itsup.com']);
        } catch (\Exception $e) {
            if ($e->getCode() !== 404) {
                throw $e;
            }
            $this->user = new User();
            $this->user->setAccount($this->account);
            $this->user->setEmail('userApiTest@itsup.com');
            $this->user->setName('api User');
            $this->user->setUsername('apiUser');
            $this->user->setPassword('somePassword');
            $this->user->setRoles(['ROLE_USER']);
            $this->user->setApiKey(ApiKeyGenerator::generate());
            $this->orm->persist($this->user);
        }

        $this->orm->flush();

        $k              = $this->client->getContainer()->getParameter('api_key');
        $this->internal = new InternalUser($k);
    }

    /**
     * Assert if the status code is equivalent to the one in the test and if the return response is in json format.
     *
     * @param Response $response
     * @param int      $statusCode
     */
    protected function assertJsonResponse($response, $statusCode)
    {
        $this->assertEquals(
            (int) $statusCode,
            (int) $response->getStatusCode(),
            $response->getContent()
        );
        if ($response->getStatusCode() !== 204) {
            $this->assertTrue(
                $response->headers->contains('Content-Type', 'application/json'),
                $response->headers
            );
        }
    }

    /**
     * Remove test Account and users.
     *
     * @throws \Exception
     */
    public static function removeUsers()
    {
        $client  = static::createClient();
        $finder  = $client->getContainer()->get('finder.user');
        $manager = $client->getContainer()->get('manager.user');
        try {
            $user = $finder->find(['email' => 'userApiTest@itsup.com']);
        } catch (\Exception $e) {
            if ($e->getCode() !== 404) {
                throw $e;
            }
        }
        if (!empty($user)) {
            try {
                $manager->delete($user);
            } catch (\Exception $e) {
            }
        }
        try {
            $user = $finder->find(['email' => 'adminApiTest@itsup.com']);
        } catch (\Exception $e) {
            if ($e->getCode() !== 404) {
                throw $e;
            }
        }
        if (!empty($user)) {
            try {
                $manager->delete($user);
            } catch (\Exception $e) {
            }
        }

        $finder  = $client->getContainer()->get('finder.account');
        $manager = $client->getContainer()->get('manager.account');
        try {
            $account = $finder->find(['name' => 'api_test_account']);
        } catch (\Exception $e) {
            if ($e->getCode() !== 404) {
                throw $e;
            }
        }
        if (!empty($account)) {
            try {
                $manager->delete($account);
            } catch (\Exception $e) {
            }
        }

        $cache = $client->getContainer()->get('cache');
        $cache->deleteItem('api_test_nb_test_left');
    }

    protected function initializeCache()
    {
    }

    protected function checkIfTestLeft()
    {
    }
}

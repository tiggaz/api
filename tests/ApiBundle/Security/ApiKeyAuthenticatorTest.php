<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Tests\Security;

use Itsup\Bundle\ApiBundle\Entity\User;
use Itsup\Bundle\ApiBundle\Exception\ApiException;
use Itsup\Bundle\ApiBundle\Generator\ApiKeyGenerator;
use Itsup\Bundle\ApiBundle\Security\ApiKeyAuthenticator;
use Itsup\Bundle\ApiBundle\Security\ApiKeyUserProvider;
use Mockery as m;
use Mockery\MockInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\AnonymousToken;
use Symfony\Component\Security\Core\Authentication\Token\PreAuthenticatedToken;

/**
 * @author Martin Karadzinov <martin@myhost.mk>
 */
//class ApiKeyAuthenticatorTest extends \PHPUnit_Framework_TestCase
class ApiKeyAuthenticatorTest
{
    /**
     * @var ApiKeyAuthenticator
     */
    private $authenticator;

    /**
     * @var MockInterface|ApiKeyUserProvider
     */
    private $userProvider;

    public function setUp()
    {
        $this->userProvider = m::mock(ApiKeyUserProvider::class);

        $this->authenticator = new ApiKeyAuthenticator($this->userProvider);
    }

    public function testConstructor()
    {
        $this->assertInstanceOf(ApiKeyAuthenticator::class, $this->authenticator);
    }

    public function testCreateToken()
    {
        $request = new Request();
        $request->headers->set('Authorization', 'Bearer '.ApiKeyGenerator::generate());

        $this->assertInstanceOf(PreAuthenticatedToken::class, $this->authenticator->createToken($request, 'test'));
    }

    public function testAuthenticateTokenAnonymous()
    {
        $token = $this->authenticator->authenticateToken(
            $this->authenticator->createToken(new Request(), 'test'),
            $this->userProvider,
            'test'
        );

        $this->assertInstanceOf(AnonymousToken::class, $token);
    }

    public function testAuthenticateTokenNoUser()
    {
        $this->setExpectedExceptionRegExp(\Exception::class, '/^(.+) is not a valid api key$/');

        $apiKey = ApiKeyGenerator::generate();

        $this->userProvider->shouldReceive('loadUserByUsername')->withArgs([$apiKey])->andReturnNull();

        $request = new Request();
        $request->headers->set('Authorization', 'Bearer '.$apiKey);

        try {
            $this->authenticator->authenticateToken(
                $this->authenticator->createToken($request, 'test'),
                $this->userProvider,
                'test'
            );
        } catch (ApiException $e) {
            throw new \Exception($e->getMessages()[0]);
        }
    }

    public function testAuthenticateTokenDisabledUser()
    {
        $this->setExpectedExceptionRegExp(\Exception::class, '/^(.+) is not a enabled$/');

        $apiKey = ApiKeyGenerator::generate();

        $user = new User();
        $user->setApiKey($apiKey);
        $user->setEnabled(false);

        $this->userProvider->shouldReceive('loadUserByUsername')->withArgs([$apiKey])->andReturn($user);

        $request = new Request();
        $request->headers->set('Authorization', 'Bearer '.$apiKey);

        try {
            $this->authenticator->authenticateToken(
                $this->authenticator->createToken($request, 'test'),
                $this->userProvider,
                'test'
            );
        } catch (ApiException $e) {
            throw new \Exception($e->getMessages()[0]);
        }
    }
}

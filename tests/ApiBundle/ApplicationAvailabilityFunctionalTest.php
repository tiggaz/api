<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

namespace Itsup\Bundle\ApiBundle\Tests;

/**
 * @author Martin Karadzinov <martin@myhost.mk>
 */
class ApplicationAvailabilityFunctionalTest extends AbstractTestCase
{
    private static $index;

    /**
     * {@inheritdoc}
     */
    public static function setUpBeforeClass()
    {
        parent::setUpBeforeClass();

        static::removeUsers();

        self::$index = 1;
    }

    /**
     * {@inheritdoc}
     */
    public static function tearDownAfterClass()
    {
        parent::tearDownAfterClass();

        static::removeUsers();
    }

    /**
     * {@inheritdoc}
     */
    public function setUp()
    {
        parent::setUp();
    }

    /**
     * @dataProvider urlProvider
     *
     * @param string   $method
     * @param string   $url
     * @param array    $parameters
     * @param string   $user
     * @param int|null $status
     * @param string   $resultId
     */
    public function testPageIsSuccessful(
        string $method,
        string $url,
        array $parameters = [],
        string $user = '',
        int $status = 200,
        string $resultId = ''
    ) {
        if (!empty($user) && in_array($user, ['internal', 'user', 'admin'])) {
            $queryChar = (strpos($url, '?') === false ? '?' : '&');
            $apiKey    = $this->$user->getApiKey();
            $prefix    = 'u';
            if ($user === 'internal') {
                $prefix = 'i';
            }
            $url .= $queryChar.$prefix.'-api-key='.$apiKey;
        }

        $this->convertUrl($url);
        if (!empty($parameters)) {
            $this->convertParameters($parameters);
            $this->client->request($method, $url, $parameters);
        } else {
            $this->client->request($method, $url);
        }

        $responseContent = json_decode($this->client->getResponse()->getContent(), true);
        $dir             = __DIR__.'/../../../build/responses/';
        @mkdir($dir, 0777, true);
        file_put_contents(
            $dir.(self::$index++).'.json',
            json_encode(
                [
                    'method'          => $method,
                    'url'             => $url,
                    'parameters'      => $parameters,
                    'user'            => $user,
                    'expectedStatus'  => $status,
                    'responseStatus'  => $this->client->getResponse()->getStatusCode(),
                    'responseContent' => $responseContent,
                ],
                JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES
            )
        );

        $this->assertJsonResponse($this->client->getResponse(), $status);

        if (!empty($resultId)) {
            $item = $this->cache->getItem('api_test_'.$resultId);
            $item->set(self::getResultId($responseContent['content']));
            $item->expiresAfter(self::ITEM_CACHE_TTL);
            $this->cache->save($item);
        }

        if (empty($status)) {
            $this->assertNotEquals($this->client->getResponse()->getStatusCode(), 404);
            $this->assertTrue($this->client->getResponse()->isSuccessful());
        } else {
            $this->assertEquals($this->client->getResponse()->getStatusCode(), $status);
        }
        $this->decrementTestLeft();
    }

    /**
     * Find the field 'id' in the returned response and return his value.
     *
     * @param array $data
     *
     * @return int
     */
    public static function getResultId(array $data)
    {
        return $data['id'] ?? 0;
    }

    /**
     * Replace the {var} and %var% by their ID in the URL to call.
     *
     * @param string $url
     */
    public function convertUrl(string &$url)
    {
        preg_match_all('/\{([^\}]+)\}/', $url, $keys);
        foreach ($keys[1] as $key) {
            $item = $this->cache->getItem('api_test_'.$key);
            if ($item->isHit()) {
                $url = str_replace('{'.$key.'}', $item->get(), $url);
            }
        }
        $url = str_replace('%account%', $this->account->getId(), $url);
        $url = str_replace('%admin%', $this->admin->getId(), $url);
        $url = str_replace('%user%', $this->user->getId(), $url);
    }

    /**
     * Replace the {var} by their ID in the parameters array.
     *
     * @param array $parameters
     */
    public function convertParameters(array &$parameters)
    {
        array_walk_recursive($parameters, [$this, 'convertUrl']);
    }

    protected function initializeCache()
    {
        parent::initializeCache();
        $item = $this->cache->getItem('api_test_nb_test_left');
        if (!$item->isHit()) {
            $nbTest = count($this->urlProvider());
            $item->set($nbTest);
            $item->expiresAfter(self::ITEM_CACHE_TTL);
            $this->cache->save($item);
        }
    }

    protected function decrementTestLeft()
    {
        $item = $this->cache->getItem('api_test_nb_test_left');
        if ($item->isHit()) {
            $nbTest = (int) $item->get() - 1;
            $item->set($nbTest);
            $item->expiresAfter(self::ITEM_CACHE_TTL);
            $this->cache->save($item);
        }
    }

    protected function checkIfTestLeft()
    {
        $item = $this->cache->getItem('api_test_nb_test_left');
        if ($item->isHit()) {
            return (int) $item->get() > 0 ? true : false;
        }

        return false;
    }

    /**
     * URLs and options to call for the tests ([RequestMethod, URL, Parameters, UserType, ExpectedStatusCode, CacheId]).
     *
     * @return array
     */
    public function urlProvider(): array
    {
        return [
            /* Account */
            ['GET', '/account/all', [], 'internal'],
            ['GET', '/account/all', [], 'admin', 403],
            ['GET', '/account', ['name' => 'api_test_account'], 'user'],
            [
                'POST',
                '/account',
                [
                    'Account' => [
                        'name'       => 'api_test_account_1',
                        'type'       => 'individual',
                        'address'    => 'address account 1',
                        'city'       => 'city account 1',
                        'zipCode'    => '09000',
                        'country'    => 'US',
                        'websiteUrl' => 'http://www.acount1.com',
                    ],
                ],
                'internal',
                201,
                'account_1',
            ],
            ['DELETE', '/account/{account_1}', [], 'user', 403],
            /* Account Billing */
            [
                'POST',
                '/account/billing',
                [
                    'Billing' => [
                        'account'     => '{account_1}',
                        'date'        => '2016-12-01',
                        'impressions' => 100000,
                        'clicks'      => 50000,
                        'events'      => 5000,
                        'price'       => 1529.56,
                    ],
                ],
                'internal',
                201,
            ],
            [
                'POST',
                '/account/billing',
                [
                    'Billing' => [
                        'account'     => '{account_1}',
                        'date'        => '2016-12-15',
                        'impressions' => 120000,
                        'clicks'      => 78000,
                        'events'      => 3400,
                        'price'       => 567.99,
                    ],
                ],
                'internal',
                201,
            ],
            [
                'POST',
                '/account/billing/update',
                [
                    'Billing' => [
                        'account' => '{account_1}',
                        'date'    => '2016-12-01',
                        'price'   => 1629.56,
                    ],
                ],
                'internal',
            ],
            ['GET', '/account/billing/all', ['account' => '{account_1}'], 'internal'],
            ['GET', '/account/billing/all', ['account' => '{account_1}'], 'admin', 404],
            ['GET', '/account/billing', ['account' => '{account_1}', 'date' => '2016-12-01'], 'internal'],
            ['DELETE', '/account/billing', ['account' => '{account_1}', 'date' => '2016-12-01'], 'internal', 204],
            /* Account Pricing */
            [
                'POST',
                '/account/pricing',
                [
                    'Pricing' => [
                        'account' => '{account_1}',
                        'from'    => '2016-12-01',
                        'to'      => '2016-12-15',
                        'cpm'     => 0.0012,
                    ],
                ],
                'internal',
                201,
                'pricing_1',
            ],
            [
                'POST',
                '/account/pricing/update',
                [
                    'Pricing' => [
                        'id'  => '{pricing_1}',
                        'cpm' => 0.0015,
                    ],
                ],
                'internal',
            ],
            ['GET', '/account/pricing/all', ['account' => '{account_1}'], 'internal'],
            ['GET', '/account/pricing/all', ['account' => '{account_1}'], 'admin', 404],
            ['GET', '/account/pricing', ['id' => '{pricing_1}'], 'internal'],
            ['DELETE', '/account/pricing/{pricing_1}', [], 'internal', 204],
            /* User */
            [
                'POST',
                '/user',
                [
                    'User' => [
                        'account'    => '{account_1}',
                        'name'       => 'api_test_user_1',
                        'username'   => 'user_1',
                        'password'   => 'password_1',
                        'email'      => 'user_1@itsup.com',
                    ],
                ],
                'internal',
                201,
                'user_1',
            ],
            [
                'POST',
                '/user',
                [
                    'User' => [
                        'name'       => 'api_test_user',
                        'username'   => 'user_2',
                        'password'   => 'password_2',
                        'email'      => 'user_2@itsup.com',
                    ],
                ],
                'admin',
                201,
                'user_2',
            ],
            [
                'POST',
                '/user/update',
                [
                    'User' => [
                        'id'         => '{user_2}',
                        'name'       => 'api_test_user_2',
                    ],
                ],
                'admin',
                200,
            ],
            ['GET', '/user/all', [], 'internal'],
            ['GET', '/user/all', [], 'admin'],
            ['GET', '/user', ['id' => '%user%'], 'user'],
            ['DELETE', '/user/{user_1}', [], 'internal', 204],
            ['DELETE', '/user/{user_2}', [], 'internal', 204],
            ['DELETE', '/account/{account_1}', [], 'internal', 204],
            /* Account Domain */
            [
                'POST',
                '/account/domain',
                [
                    'Domain' => [
                        'type'          => 'cdn',
                        'name'          => 'CDN Domain',
                        'ssl'           => true,
                        'defaultDomain' => true,
                    ],
                ],
                'admin',
                201,
                'domain_1',
            ],
            [
                'POST',
                '/account/domain',
                [
                    'Domain' => [
                        'type'          => 'tracking',
                        'name'          => 'Tracking Domain',
                        'defaultDomain' => true,
                    ],
                ],
                'admin',
                201,
                'domain_2',
            ],
            [
                'POST',
                '/account/domain/update',
                [
                    'Domain' => [
                        'id'            => '{domain_1}',
                        'defaultDomain' => false,
                    ],
                ],
                'user',
                403,
            ],
            ['GET', '/account/domain/all', ['account' => '%account%'], 'internal'],
            ['GET', '/account/domain/all', [], 'user'],
            ['GET', '/account/domain', ['id' => '{domain_1}'], 'admin'],
            ['DELETE', '/account/domain/{domain_1}', [], 'user', 403],
            /* Account ESP */
            [
                'POST',
                '/account/esp',
                [
                    'ExternalStatisticsProvider' => [
                        'name'     => 'reporo',
                        'token'    => 'ydsgfjysdagfjdsuhesguheriutruwgtiuerwhtieuwrhg',
                        'password' => 'password',
                    ],
                ],
                'admin',
                201,
                'esp_1',
            ],
            [
                'POST',
                '/account/esp',
                [
                    'ExternalStatisticsProvider' => [
                        'name'  => 'trafficjunky',
                        'token' => 'ydhhjtyejusgfjysdagfjdsuhesguheriutruwgttieuwrhg',
                    ],
                ],
                'admin',
                201,
                'esp_2',
            ],
            [
                'POST',
                '/account/esp/update',
                [
                    'ExternalStatisticsProvider' => [
                        'id'       => '{esp_1}',
                        'username' => 'username',
                    ],
                ],
                'user',
                403,
            ],
            ['GET', '/account/esp/all', ['account' => '%account%'], 'internal'],
            ['GET', '/account/esp/all', [], 'user'],
            ['GET', '/account/esp', ['id' => '{esp_1}'], 'admin'],
            ['DELETE', '/account/esp/{esp_1}', [], 'user', 403],
            /* Contact */
            [
                'POST',
                '/contact',
                [
                    'Contact' => [
                        'name'       => 'Contact 1',
                        'email'      => 'contact_1@itsup.com',
                        'skype'      => 'contact1',
                        'phone'      => '1-805-890-567',
                        'company'    => 'SCTR',
                        'country'    => 'US',
                        'websiteUrl' => 'http://www.sctr.net',
                        'broker'     => true,
                        'advertiser' => true,
                    ],
                ],
                'admin',
                201,
                'contact_1',
            ],
            [
                'POST',
                '/contact',
                [
                    'Contact' => [
                        'name'       => 'Contact 2',
                        'skype'      => 'contact2',
                        'phone'      => '1-809-890-567',
                        'company'    => 'SCTR',
                        'websiteUrl' => 'http://www.sctr.net',
                        'broker'     => true,
                    ],
                ],
                'user',
                400,
            ],
            [
                'POST',
                '/contact',
                [
                    'Contact' => [
                        'name'       => 'Contact 2',
                        'email'      => 'contact_2@itsup.com',
                        'company'    => 'SCTR',
                        'websiteUrl' => 'http://www.sctr.net',
                        'broker'     => true,
                    ],
                ],
                'user',
                201,
                'contact_2',
            ],
            [
                'POST',
                '/contact/update',
                [
                    'Contact' => [
                        'id'         => '{contact_2}',
                        'name'       => 'Contact New 2',
                        'advertiser' => true,
                        'broker'     => '0',
                    ],
                ],
                'user',
            ],
            ['GET', '/contact/all', ['account' => '%account%'], 'internal'],
            ['GET', '/contact/all', [], 'admin'],
            ['GET', '/contact', ['id' => '{contact_1}'], 'user'],
            /* Event */
            [
                'POST',
                '/event',
                [
                    'Event' => [
                        'name' => 'Event 1',
                        'type' => 'count',
                    ],
                ],
                'admin',
                201,
                'event_1',
            ],
            [
                'POST',
                '/event',
                [
                    'Event' => [
                        'name'  => 'Event 2',
                        'type'  => 'conversion',
                        'value' => 29.95,
                    ],
                ],
                'user',
                201,
                'event_2',
            ],
            [
                'POST',
                '/event/update',
                [
                    'Event' => [
                        'id'    => '{event_2}',
                        'value' => 39.95,
                    ],
                ],
                'user',
            ],
            ['GET', '/event/all', ['account' => '%account%'], 'internal'],
            ['GET', '/event/all', [], 'admin'],
            ['GET', '/event', ['id' => '{event_1}'], 'user'],
            /* Tag */
            [
                'POST',
                '/tag',
                [
                    'Tag' => [
                        'name' => 'Tag 1',
                    ],
                ],
                'admin',
                201,
                'tag_1',
            ],
            [
                'POST',
                '/tag',
                [
                    'Tag' => [
                        'name' => 'Tag 2',
                    ],
                ],
                'user',
                201,
                'tag_2',
            ],
            ['GET', '/tag/all', ['account' => '%account%'], 'internal'],
            ['GET', '/tag/all', [], 'admin'],
            ['GET', '/tag', ['id' => '{tag_1}'], 'user'],
            /* Offer */
            [
                'POST',
                '/offer',
                [
                    'Offer' => [
                        'name'          => 'Offer 1',
                        'contact'       => '{contact_1}',
                        'url'           => 'http://www.itsup.com/offer_1.php',
                        'payoutType'    => 'pps',
                        'defaultValue'  => 15.99,
                        'statisticsUrl' => 'http://statistics.itsup.com/offer_1.php',
                        'status'        => 'active',
                    ],
                ],
                'admin',
                201,
                'offer_1',
            ],
            [
                'POST',
                '/offer',
                [
                    'Offer' => [
                        'name'          => 'Offer 2',
                        'url'           => 'http://www.itsup.com/offer_2.php',
                        'payoutType'    => 'cpm imps',
                        'statisticsUrl' => 'http://statistics.itsup.com/offer_1.php',
                        'status'        => 'active',
                        'tags'          => [
                            ['id' => '{tag_1}'],
                        ],
                    ],
                ],
                'user',
                201,
                'offer_2',
            ],
            [
                'POST',
                '/offer/update',
                [
                    'Offer' => [
                        'id'            => '{offer_2}',
                        'statisticsUrl' => 'http://statistics.itsup.com/offer_2.php',
                        'tags'          => [
                            ['name' => 'Tag 2'],
                        ],
                    ],
                ],
                'user',
            ],
            [
                'POST',
                '/offer/{offer_1}/note',
                [
                    'Note' => [
                        'text' => 'Note Offer 1',
                    ],
                ],
                'admin',
            ],
            ['GET', '/offer/all', ['account' => '%account%'], 'internal'],
            ['GET', '/offer/all', [], 'admin'],
            ['GET', '/offer', ['id' => '{offer_1}'], 'user'],
            /* Creative */
            [
                'POST',
                '/creative',
                [
                    'Creative' => [
                        'name'   => 'Creative 1',
                        'type'   => 'iframe',
                        'width'  => 250,
                        'height' => 250,
                        'code'   => 'http://feeds.itsup.com/iframe_1.php',
                        'status' => 'on',
                    ],
                ],
                'admin',
                201,
                'creative_1',
            ],
            [
                'POST',
                '/creative/update',
                [
                    'Creative' => [
                        'code'   => 'http://feeds.itsup.com/creative/iframe_1.php',
                        'status' => 'off',
                    ],
                ],
                'admin',
                400,
            ],
            [
                'POST',
                '/creative',
                [
                    'Creative' => [
                        'name'   => 'Creative 2',
                        'type'   => 'iframe',
                        'width'  => 250,
                        'height' => 250,
                        'code'   => 'http://feeds.itsup.com/iframe_2.php',
                        'status' => 'on',
                        'tags'   => [
                            ['id' => '{tag_2}'],
                        ],
                    ],
                ],
                'user',
                201,
                'creative_2',
            ],
            [
                'POST',
                '/creative',
                [
                    'Creative' => [
                        'name'   => 'Creative 3',
                        'type'   => 'iframe',
                        'width'  => 250,
                        'height' => 250,
                        'code'   => 'http://feeds.itsup.com/iframe_3.php',
                        'status' => 'on',
                    ],
                ],
                'user',
                201,
                'creative_3',
            ],
            [
                'POST',
                '/creative',
                [
                    'Creative' => [
                        'name'   => 'Creative 4',
                        'type'   => 'text',
                        'code'   => 'creative text',
                        'status' => 'on',
                    ],
                ],
                'admin',
                201,
                'creative_4',
            ],
            [
                'POST',
                '/creative',
                [
                    'Creative' => [
                        'name'   => 'Creative 5',
                        'type'   => 'image',
                        'width'  => 350,
                        'height' => 350,
                        'code'   => 'http://api.itsup.com/someimage.jpg',
                        'status' => 'on',
                    ],
                ],
                'user',
                201,
                'creative_5',
            ],
            [
                'POST',
                '/creative/update',
                [
                    'Creative' => [
                        'id'     => '{creative_2}',
                        'code'   => 'http://feeds.itsup.com/creative/iframe_2.php',
                        'status' => 'off',
                        'tags'   => [
                            ['name' => 'Tag 2'],
                        ],
                    ],
                ],
                'user',
            ],
            ['GET', '/creative/all', ['account' => '%account%'], 'internal'],
            ['GET', '/creative/all', [], 'admin'],
            ['GET', '/creative', ['id' => '{creative_1}'], 'user'],
            /* Campaign */
            [
                'POST',
                '/campaign',
                [
                    'Campaign' => [
                        'name'      => 'Campaign 1',
                        'offer'     => '{offer_1}',
                        'revenue'   => 'conversion',
                        'type'      => 'iframe',
                        'width'     => 250,
                        'height'    => 250,
                        'event'     => '{event_1}',
                        'url'       => 'http://www.itsup.com/campaign_1.php',
                        'status'    => 'on',
                        'keepAlive' => true,
                    ],
                ],
                'admin',
                201,
                'campaign_1',
            ],
            [
                'POST',
                '/campaign',
                [
                    'Campaign' => [
                        'name'      => 'Campaign 2',
                        'revenue'   => 'sale',
                        'type'      => 'iframe',
                        'width'     => 250,
                        'height'    => 250,
                        'url'       => 'http://www.itsup.com/campaign_2.php',
                        'status'    => 'on',
                        'keepAlive' => true,
                        'tags'      => [
                            ['id' => '{tag_1}'],
                        ],
                    ],
                ],
                'user',
                201,
                'campaign_2',
            ],
            [
                'POST',
                '/campaign',
                [
                    'Campaign' => [
                        'name'      => 'Campaign 3',
                        'revenue'   => 'sale',
                        'type'      => 'image',
                        'width'     => 350,
                        'height'    => 350,
                        'url'       => 'http://www.itsup.com/campaign_3.php',
                        'status'    => 'on',
                        'keepAlive' => true,
                        'tags'      => [
                            ['id' => '{tag_1}'],
                        ],
                    ],
                ],
                'admin',
                201,
                'campaign_3',
            ],
            [
                'POST',
                '/campaign',
                [
                    'Campaign' => [
                        'name'      => 'Campaign 4',
                        'revenue'   => 'sale',
                        'type'      => 'text',
                        'url'       => 'http://www.itsup.com/campaign_4.php',
                        'status'    => 'on',
                        'keepAlive' => true,
                        'tags'      => [
                            ['id' => '{tag_1}'],
                        ],
                    ],
                ],
                'admin',
                201,
                'campaign_4',
            ],
            [
                'POST',
                '/campaign/update',
                [
                    'Campaign' => [
                        'id'     => '{campaign_2}',
                        'tags'   => [
                            ['id' => '{tag_2}'],
                        ],
                    ],
                ],
                'user',
            ],
            ['POST', '/campaign/{campaign_1}/follower/%user%', [], 'user'],
            [
                'POST',
                '/campaign/{campaign_1}/note',
                [
                    'Note' => [
                        'text' => 'Note Campaign 1',
                    ],
                ],
                'user',
            ],
            [
                'POST',
                '/campaign/limit',
                [
                    'Limit' => [
                        'campaign'    => '{campaign_1}',
                        'from'        => '2016-12-01',
                        'to'          => '2016-12-31',
                        'impressions' => 100000,
                    ],
                ],
                'user',
                201,
                'limit_1',
            ],
            [
                'POST',
                '/campaign/sale',
                [
                    'Sale' => [
                        'campaign' => '{campaign_1}',
                        'type'     => 'flat',
                        'from'     => '2016-12-01',
                        'to'       => '2016-12-31',
                        'expire'   => true,
                        'price'    => 1500.89,
                    ],
                ],
                'user',
                201,
                'sale_1',
            ],
            [
                'POST',
                '/campaign/sale',
                [
                    'Sale' => [
                        'campaign' => '{campaign_2}',
                        'limit'    => '{limit_1}',
                        'type'     => 'cpc',
                        'from'     => '2016-12-01',
                        'to'       => '2016-12-31',
                        'expire'   => true,
                        'price'    => 0.012,
                    ],
                ],
                'user',
                201,
                'sale_2',
            ],
            ['POST', '/campaign/{campaign_1}/duplicate/creatives', [], 'user', 201, 'campaign_1_duplicate'],
            ['POST', '/campaign/{campaign_2}/duplicate/settings', [], 'admin', 201, 'campaign_2_duplicate'],
            ['GET', '/campaign/all', ['account' => '%account%'], 'internal'],
            ['GET', '/campaign/all', [], 'admin'],
            ['GET', '/campaign', ['id' => '{campaign_1}'], 'user'],
            /* CampaignCreative */
            [
                'POST',
                '/campaign/creative',
                [
                    'CampaignCreative' => [
                        'campaign' => '{campaign_1}',
                        'creative' => '{creative_1}',
                        'weight'   => 10,
                        'status'   => 'on',
                    ],
                ],
                'admin',
                201,
            ],
            [
                'POST',
                '/campaign/creative',
                [
                    'CampaignCreative' => [
                        'campaign' => '{campaign_2}',
                        'creative' => '{creative_4}',
                        'weight'   => 10,
                        'status'   => 'on',
                    ],
                ],
                'admin',
                400,
            ],
            [
                'POST',
                '/campaign/creative',
                [
                    'CampaignCreative' => [
                        'campaign' => '{campaign_2}',
                        'creative' => '{creative_5}',
                        'weight'   => 10,
                        'status'   => 'on',
                    ],
                ],
                'admin',
                400,
            ],
            [
                'POST',
                '/campaign/creative/bulk',
                [
                    'CampaignCreativeCollection' => [
                        'campaign'  => '{campaign_2}',
                        'creatives' => [
                            [
                                'creative' => '{creative_1}',
                                'weight'   => 10,
                                'status'   => 'on',
                            ],
                            [
                                'creative' => '{creative_2}',
                                'weight'   => 50,
                                'status'   => 'on',
                            ],
                        ],
                    ],
                ],
                'user',
            ],
            /* AdZone */
            [
                'POST',
                '/adzone',
                [
                    'AdZone' => [
                        'name'    => 'AdZone 1',
                        'contact' => '{contact_1}',
                        'hosting' => 'local',
                        'type'    => 'iframe',
                        'width'   => 250,
                        'height'  => 250,
                        'status'  => 'on',
                        'display' => 'order',
                        'tags'    => [
                            ['name' => 'Tag 1'],
                            ['name' => 'Tag 2'],
                        ],
                    ],
                ],
                'admin',
                201,
                'adzone_1',
            ],
            [
                'POST',
                '/adzone',
                [
                    'AdZone' => [
                        'name'                           => 'AdZone 2',
                        'hosting'                        => 'remote',
                        'type'                           => 'direct link',
                        'status'                         => 'on',
                        'display'                        => 'weight',
                        'externalStatisticsProvider'     => '{esp_1}',
                        'externalStatisticsId'           => '1234',
                        'externalStatisticsIdComplement' => '567',
                        'externalStatisticsOptions'      => ['imps', 'cost'],
                    ],
                ],
                'user',
                201,
                'adzone_2',
            ],
            [
                'POST',
                '/adzone/update',
                [
                    'AdZone' => [
                        'id'     => '{adzone_2}',
                        'status' => 'paused',
                    ],
                ],
                'user',
            ],
            [
                'POST',
                '/adzone/update',
                [
                    'AdZone' => [
                        'id'      => '{adzone_1}',
                        'status'  => 'paused',
                        'tags'    => [
                            ['id' => '{tag_2}'],
                        ],
                    ],
                ],
                'user',
            ],
            ['POST', '/adzone/{adzone_1}/follower/%user%', [], 'user'],
            [
                'POST',
                '/adzone/{adzone_2}/note',
                [
                    'Note' => [
                        'text' => 'Note AdZone 1',
                    ],
                ],
                'user',
            ],
            [
                'POST',
                '/adzone/accounting',
                [
                    'Accounting' => [
                        'adZone' => '{adzone_1}',
                        'from'   => '2016-12-01',
                        'to'     => '2016-12-31',
                        'type'   => 'flat',
                        'cost'   => 10000,
                    ],
                ],
                'user',
                201,
                'accounting_1',
            ],
            ['POST', '/adzone/{adzone_1}/duplicate/campaigns', [], 'user', 201, 'adzone_1_duplicate'],
            ['POST', '/adzone/{adzone_2}/duplicate/settings', [], 'admin', 201, 'adzone_2_duplicate'],
            ['GET', '/adzone/all', ['account' => '%account%'], 'internal'],
            ['GET', '/adzone/all', [], 'admin'],
            ['GET', '/adzone', ['id' => '{adzone_1}'], 'user'],
            /* AdZoneCampaign */
            [
                'POST',
                '/adzone/campaign',
                [
                    'AdZoneCampaign' => [
                        'adZone'   => '{adzone_1}',
                        'campaign' => '{campaign_1}',
                        'weight'   => 10,
                        'status'   => 'active',
                    ],
                ],
                'user',
                201,
            ],
            [
                'POST',
                '/adzone/campaign',
                [
                    'AdZoneCampaign' => [
                        'adZone'   => '{adzone_1}',
                        'campaign' => '{campaign_3}',
                        'weight'   => 10,
                        'status'   => 'active',
                    ],
                ],
                'user',
                400,
            ],
            [
                'POST',
                '/adzone/campaign',
                [
                    'AdZoneCampaign' => [
                        'adZone'   => '{adzone_1}',
                        'campaign' => '{campaign_4}',
                        'weight'   => 10,
                        'status'   => 'active',
                    ],
                ],
                'user',
                400,
            ],
            [
                'POST',
                '/adzone/campaign/bulk',
                [
                    'AdZoneCampaignCollection' => [
                        'adZone'    => '{adzone_2}',
                        'campaigns' => [
                            [
                                'campaign' => '{campaign_1}',
                                'weight'   => 10,
                                'status'   => 'active',
                            ],
                            [
                                'campaign' => '{campaign_2}',
                                'weight'   => 50,
                                'status'   => 'active',
                            ],
                        ],
                    ],
                ],
                'admin',
            ],
            /* GROUP */
            [
                'POST',
                '/group',
                [
                    'Group' => [
                        'name' => 'Group 1',
                    ],
                ],
                'admin',
                201,
                'group_1',
            ],
            [
                'POST',
                '/group',
                [
                    'Group' => [
                        'name' => 'Group 2',
                    ],
                ],
                'user',
                201,
                'group_2',
            ],
            ['POST', '/group/{group_1}/adzone/{adzone_1}', [], 'user'],
            [
                'POST',
                '/group/{group_2}/adzone',
                [
                    'collection' => [
                        '{adzone_1}',
                        '{adzone_2}',
                    ],
                ],
                'admin',
            ],
            ['GET', '/group/all', ['account' => '%account%'], 'internal'],
            ['GET', '/group/all', [], 'admin'],
            ['GET', '/group', ['id' => '{group_1}'], 'user'],
            /* METRICS */
            /* COUNTRY */
            [
                'POST',
                '/metrics/country',
                [
                    'Country' => [
                        'id'            => 'C1',
                        'name'          => 'Country 1',
                        'official_name' => 'Full Country 1',
                        'region'        => 'Region',
                        'subRegion'     => 'Sub Region',
                        'position'      => 1,
                    ],
                ],
                'internal',
                201,
                'country_1',
            ],
            [
                'POST',
                '/metrics/country',
                [
                    'Country' => [
                        'id'            => 'C2',
                        'name'          => 'Country 2',
                        'official_name' => 'Full Country 2',
                        'region'        => 'Region',
                        'subRegion'     => 'Sub Region',
                        'position'      => 1,
                    ],
                ],
                'admin',
                403,
            ],
            [
                'POST',
                '/metrics/country',
                [
                    'Country' => [
                        'id'            => 'C3',
                        'name'          => 'Country 3',
                        'official_name' => 'Full Country 3',
                        'region'        => 'Region',
                        'subRegion'     => 'Sub Region',
                        'position'      => 1,
                    ],
                ],
                'user',
                403,
            ],
            [
                'POST',
                '/metrics/country',
                [
                    'Country' => [
                        'id'            => 'C4',
                        'name'          => 'Country 4',
                        'official_name' => 'Full Country 4',
                        'region'        => 'Region',
                        'subRegion'     => 'Sub Region',
                        'position'      => 1,
                    ],
                ],
                'internal',
                201,
                'country_4',
            ],
            [
                'POST',
                '/metrics/country/update',
                [
                    'Country' => [
                        'id'   => 'C1',
                        'name' => 'Country 1 Update',
                    ],
                ],
                'internal',
                200,
            ],
            [
                'POST',
                '/metrics/country/update',
                [
                    'Country' => [
                        'id'   => 'C1',
                        'name' => 'Country 1 Update',
                    ],
                ],
                'admin',
                403,
            ],
            [
                'POST',
                '/metrics/country/update',
                [
                    'Country' => [
                        'id'   => 'C1',
                        'name' => 'Country 1 Update',
                    ],
                ],
                'user',
                403,
            ],
            ['GET', '/metrics/country/all', [], 'internal'],
            ['GET', '/metrics/country/all', [], 'admin'],
            ['GET', '/metrics/country/all', [], 'user'],
            ['GET', '/metrics/country', ['id' => '{country_1}'], 'internal'],
            ['GET', '/metrics/country', ['id' => '{country_1}'], 'admin'],
            ['GET', '/metrics/country', ['id' => '{country_1}'], 'user'],
            [
                'POST',
                '/campaign/{campaign_1}/country',
                [
                    'collection' => ['{country_1}', '{country_4}'],
                ],
                'internal',
                200,
            ],
            ['DELETE', '/metrics/country/{country_1}', [], 'admin', 403],
            ['DELETE', '/metrics/country/{country_1}', [], 'user', 403],
            /* INTERNET SERVICE PROVIDER */
            [
                'POST',
                '/metrics/isp',
                [
                    'InternetServiceProvider' => [
                        'name' => 'ISP 1',
                    ],
                ],
                'internal',
                201,
                'isp_1',
            ],
            [
                'POST',
                '/metrics/isp',
                [
                    'InternetServiceProvider' => [
                        'name' => 'ISP 2',
                    ],
                ],
                'admin',
                403,
            ],
            [
                'POST',
                '/metrics/isp',
                [
                    'InternetServiceProvider' => [
                        'name' => 'ISP 3',
                    ],
                ],
                'user',
                403,
            ],
            [
                'POST',
                '/metrics/isp',
                [
                    'InternetServiceProvider' => [
                        'name' => 'ISP 4',
                    ],
                ],
                'internal',
                201,
                'isp_4',
            ],
            [
                'POST',
                '/metrics/isp/update',
                [
                    'InternetServiceProvider' => [
                        'id'   => '{isp_1}',
                        'name' => 'ISP 1 Update',
                    ],
                ],
                'internal',
                200,
            ],
            [
                'POST',
                '/metrics/isp/update',
                [
                    'InternetServiceProvider' => [
                        'id'   => '{isp_1}',
                        'name' => 'ISP 1 Update',
                    ],
                ],
                'admin',
                403,
            ],
            [
                'POST',
                '/metrics/isp/update',
                [
                    'InternetServiceProvider' => [
                        'id'   => '{isp_1}',
                        'name' => 'ISP 1 Update',
                    ],
                ],
                'user',
                403,
            ],
            ['GET', '/metrics/isp/all', [], 'internal'],
            ['GET', '/metrics/isp/all', [], 'admin'],
            ['GET', '/metrics/isp/all', [], 'user'],
            ['GET', '/metrics/isp', ['id' => '{isp_1}'], 'internal'],
            ['GET', '/metrics/isp', ['id' => '{isp_1}'], 'admin'],
            ['GET', '/metrics/isp', ['id' => '{isp_1}'], 'user'],
            [
                'POST',
                '/campaign/{campaign_1}/isp',
                [
                    'collection' => ['{isp_1}', '{isp_4}'],
                ],
                'internal',
                200,
            ],
            ['DELETE', '/metrics/isp/{isp_1}', [], 'admin', 403],
            ['DELETE', '/metrics/isp/{isp_1}', [], 'user', 403],
            /* BROWSER */
            [
                'POST',
                '/metrics/browser',
                [
                    'Browser' => [
                        'name' => 'Safari',
                    ],
                ],
                'internal',
                201,
                'browser_1',
            ],
            [
                'POST',
                '/metrics/browser',
                [
                    'Browser' => [
                        'name' => 'Opera',
                    ],
                ],
                'admin',
                403,
            ],
            [
                'POST',
                '/metrics/browser',
                [
                    'Browser' => [
                        'name' => 'Firefox',
                    ],
                ],
                'user',
                403,
            ],
            [
                'POST',
                '/metrics/browser',
                [
                    'Browser' => [
                        'name' => 'Lynx',
                    ],
                ],
                'internal',
                201,
                'browser_4',
            ],
            [
                'POST',
                '/metrics/browser/update',
                [
                    'Browser' => [
                        'id'   => '{browser_1}',
                        'name' => 'IExplore',
                    ],
                ],
                'internal',
                200,
            ],
            ['GET', '/metrics/browser/all', [], 'internal'],
            ['GET', '/metrics/browser/all', [], 'admin'],
            ['GET', '/metrics/browser/all', [], 'user'],
            ['GET', '/metrics/browser', ['id' => '{browser_1}'], 'internal'],
            ['GET', '/metrics/browser', ['id' => '{browser_1}'], 'admin'],
            ['GET', '/metrics/browser', ['id' => '{browser_1}'], 'user'],
            [
                'POST',
                '/campaign/{campaign_1}/browser',
                [
                    'collection' => ['{browser_1}', '{browser_4}'],
                ],
                'internal',
                200,
            ],
            ['DELETE', '/metrics/browser/{browser_1}', [], 'admin', 403],
            ['DELETE', '/metrics/browser/{browser_1}', [], 'user', 403],
            /* DEVICE */
            [
                'POST',
                '/metrics/device',
                [
                    'Device' => [
                        'name' => 'iPhone',
                    ],
                ],
                'internal',
                201,
                'device_1',
            ],
            [
                'POST',
                '/metrics/device',
                [
                    'Device' => [
                        'name' => 'Samsung',
                    ],
                ],
                'admin',
                403,
            ],
            [
                'POST',
                '/metrics/device',
                [
                    'Device' => [
                        'name' => 'Nokia',
                    ],
                ],
                'user',
                403,
            ],
            [
                'POST',
                '/metrics/device',
                [
                    'Device' => [
                        'name' => 'Motorola',
                    ],
                ],
                'internal',
                201,
                'device_4',
            ],
            [
                'POST',
                '/metrics/device/update',
                [
                    'Device' => [
                        'id'   => '{device_1}',
                        'name' => 'iPad',
                    ],
                ],
                'internal',
                200,
            ],
            ['GET', '/metrics/device/all', [], 'internal'],
            ['GET', '/metrics/device/all', [], 'admin'],
            ['GET', '/metrics/device/all', [], 'user'],
            ['GET', '/metrics/device', ['id' => '{device_1}'], 'internal'],
            ['GET', '/metrics/device', ['id' => '{device_1}'], 'admin'],
            ['GET', '/metrics/device', ['id' => '{device_1}'], 'user'],
            [
                'POST',
                '/campaign/{campaign_1}/device',
                [
                    'collection' => ['{device_1}', '{device_4}'],
                ],
                'internal',
                200,
            ],
            ['DELETE', '/metrics/device/{device_1}', [], 'admin', 403],
            ['DELETE', '/metrics/device/{device_4}', [], 'user', 403],
            /* LANGUAGE */
            [
                'POST',
                '/metrics/language',
                [
                    'Language' => [
                        'id'       => 'MK',
                        'name'     => 'Macedonia',
                        'position' => 1,
                    ],
                ],
                'internal',
                201,
                'language_1',
            ],
            [
                'POST',
                '/metrics/language',
                [
                    'Language' => [
                        'id'       => 'AL',
                        'name'     => 'Albania',
                        'position' => 2,
                    ],
                ],
                'admin',
                403,
            ],
            [
                'POST',
                '/metrics/language',
                [
                    'Language' => [
                        'id'       => 'GR',
                        'name'     => 'Greece',
                        'position' => 3,
                    ],
                ],
                'user',
                403,
            ],
            [
                'POST',
                '/metrics/language',
                [
                    'Language' => [
                        'id'       => 'RU',
                        'name'     => 'Russia',
                        'position' => 4,
                    ],
                ],
                'internal',
                201,
                'language_4',
            ],
            [
                'POST',
                '/metrics/language/update',
                [
                    'Language' => [
                        'id'   => 'MK',
                        'name' => 'R Macedonia',
                    ],
                ],
                'internal',
                200,
            ],
            ['GET', '/metrics/language/all', [], 'internal'],
            ['GET', '/metrics/language/all', [], 'admin'],
            ['GET', '/metrics/language/all', [], 'user'],
            ['GET', '/metrics/language', ['id' => '{language_1}'], 'internal'],
            ['GET', '/metrics/language', ['id' => '{language_1}'], 'admin'],
            ['GET', '/metrics/language', ['id' => '{language_1}'], 'user'],
            [
                'POST',
                '/campaign/{campaign_1}/language',
                [
                    'collection' => ['{language_1}', '{language_4}'],
                ],
                'internal',
                200,
            ],
            ['DELETE', '/metrics/language/{language_1}', [], 'admin', 403],
            ['DELETE', '/metrics/language/{language_4}', [], 'user', 403],
            /* OPERATING SYSTEM */
            [
                'POST',
                '/metrics/os',
                [
                    'OperatingSystem' => [
                        'name' => 'OS 1',
                    ],
                ],
                'internal',
                201,
                'os_1',
            ],
            [
                'POST',
                '/metrics/os',
                [
                    'OperatingSystem' => [
                        'name' => 'OS 2',
                    ],
                ],
                'admin',
                403,
            ],
            [
                'POST',
                '/metrics/os',
                [
                    'OperatingSystem' => [
                        'name' => 'OS 3',
                    ],
                ],
                'user',
                403,
            ],
            [
                'POST',
                '/metrics/os/update',
                [
                    'OperatingSystem' => [
                        'id'   => '{os_1}',
                        'name' => 'OS 1 Update',
                    ],
                ],
                'internal',
                200,
            ],
            [
                'POST',
                '/metrics/os/update',
                [
                    'OperatingSystem' => [
                        'id'   => '{os_1}',
                        'name' => 'OS 1 Update',
                    ],
                ],
                'admin',
                403,
            ],
            [
                'POST',
                '/metrics/os/update',
                [
                    'OperatingSystem' => [
                        'id'   => '{os_1}',
                        'name' => 'OS 1 Update',
                    ],
                ],
                'user',
                403,
            ],
            [
                'POST',
                '/metrics/os',
                [
                    'OperatingSystem' => [
                        'name'          => 'OS 4',
                    ],
                ],
                'internal',
                201,
                'os_4',
            ],
            ['GET', '/metrics/os/all', [], 'internal'],
            ['GET', '/metrics/os/all', [], 'admin'],
            ['GET', '/metrics/os/all', [], 'user'],
            ['GET', '/metrics/os', ['id' => '{os_1}'], 'internal'],
            ['GET', '/metrics/os', ['id' => '{os_1}'], 'admin'],
            ['GET', '/metrics/os', ['id' => '{os_1}'], 'user'],
            [
                'POST',
                '/campaign/{campaign_1}/os',
                [
                    'collection' => ['{os_1}', '{os_4}'],
                ],
                'internal',
                200,
            ],
            ['DELETE', '/metrics/os/{os_1}', [], 'admin', 403],
            ['DELETE', '/metrics/os/{os_1}', [], 'user', 403],
            /* DELETING ALL TESTS */
            ['DELETE', '/group/{group_1}', [], 'admin', 204],
            ['DELETE', '/group/{group_2}', [], 'user', 204],
            ['DELETE', '/adzone/{adzone_1}', [], 'admin', 204],
            ['DELETE', '/adzone/{adzone_2}', [], 'user', 204],
            ['DELETE', '/adzone/{adzone_1_duplicate}', [], 'user', 204],
            ['DELETE', '/adzone/{adzone_2_duplicate}', [], 'user', 204],
            ['DELETE', '/campaign/sale/{sale_1}', [], 'admin', 204],
            ['DELETE', '/campaign/sale/{sale_2}', [], 'admin', 204],
            ['DELETE', '/campaign/{campaign_1}', [], 'admin', 204],
            ['DELETE', '/campaign/{campaign_2}', [], 'user', 204],
            ['DELETE', '/campaign/{campaign_3}', [], 'user', 204],
            ['DELETE', '/campaign/{campaign_4}', [], 'user', 204],
            ['DELETE', '/campaign/{campaign_1_duplicate}', [], 'user', 204],
            ['DELETE', '/campaign/{campaign_2_duplicate}', [], 'user', 204],
            ['DELETE', '/metrics/country/{country_1}', [], 'internal', 204],
            ['DELETE', '/metrics/country/{country_4}', [], 'internal', 204],
            ['DELETE', '/metrics/isp/{isp_1}', [], 'internal', 204],
            ['DELETE', '/metrics/isp/{isp_4}', [], 'internal', 204],
            ['DELETE', '/metrics/browser/{browser_1}', [], 'internal', 204],
            ['DELETE', '/metrics/browser/{browser_4}', [], 'internal', 204],
            ['DELETE', '/metrics/device/{device_1}', [], 'internal', 204],
            ['DELETE', '/metrics/device/{device_4}', [], 'internal', 204],
            ['DELETE', '/metrics/language/{language_1}', [], 'internal', 204],
            ['DELETE', '/metrics/language/{language_4}', [], 'internal', 204],
            ['DELETE', '/metrics/os/{os_1}', [], 'internal', 204],
            ['DELETE', '/metrics/os/{os_4}', [], 'internal', 204],
            ['DELETE', '/creative/{creative_1}', [], 'admin', 204],
            ['DELETE', '/creative/{creative_2}', [], 'user', 204],
            ['DELETE', '/creative/{creative_3}', [], 'user', 204],
            ['DELETE', '/creative/{creative_4}', [], 'admin', 204],
            ['DELETE', '/creative/{creative_5}', [], 'admin', 204],
            ['DELETE', '/offer/{offer_1}', [], 'admin', 204],
            ['DELETE', '/offer/{offer_2}', [], 'user', 204],
            ['DELETE', '/event/{event_1}', [], 'admin', 204],
            ['DELETE', '/event/{event_2}', [], 'user', 204],
            ['DELETE', '/contact/{contact_1}', [], 'admin', 204],
            ['DELETE', '/contact/{contact_2}', [], 'user', 204],
            ['DELETE', '/tag/{tag_1}', [], 'admin', 204],
            ['DELETE', '/tag/{tag_2}', [], 'user', 204],
            ['DELETE', '/account/domain/{domain_1}', [], 'admin', 204],
            ['DELETE', '/account/domain/{domain_2}', [], 'admin', 204],
            ['DELETE', '/account/esp/{esp_1}', [], 'admin', 204],
            ['DELETE', '/account/esp/{esp_2}', [], 'admin', 204],
        ];
    }
}

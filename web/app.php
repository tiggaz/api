<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

use Aequasi\Environment\Environment;
use Symfony\Component\HttpFoundation\Request;

umask(0000);

$loader = require_once __DIR__.'/../app/autoload.php';
require_once __DIR__.'/../var/bootstrap.php.cache';

$filename = __DIR__.preg_replace('#(\?.*)$#', '', $_SERVER['REQUEST_URI']);
if (php_sapi_name() === 'cli-server' && is_file($filename)) {
    return false;
}

$env = new Environment();
Request::enableHttpMethodParameterOverride();
$request = Request::createFromGlobals();

$kernel = new AppKernel($env->getType(), $env->isDebug());
$kernel->loadClassCache();
if (false && !$env->isDebug()) {
    $kernel = new AppCache($kernel);
}

$ips = [
    $request->server->get('REMOTE_ADDR'),
    '10.10.0.0/24',
    '10.10.200.0/24',
    '10.10.200.10/24',
    '10.10.250.0/24',
    '10.10.250.250/24',
    '172.30.0.0/16', // AWS ELBs
    '103.21.244.0/22', // cloudflare IPv4
    '103.22.200.0/22',
    '103.31.4.0/22',
    '104.16.0.0/12',
    '108.162.192.0/18',
    '131.0.72.0/22',
    '141.101.64.0/18',
    '162.158.0.0/15',
    '172.64.0.0/13',
    '173.245.48.0/20',
    '188.114.96.0/20',
    '190.93.240.0/20',
    '197.234.240.0/22',
    '198.41.128.0/17',
    '199.27.128.0/21',
    '2400:cb00::/32', // cloudflare IPv6
    '2405:8100::/32',
    '2405:b500::/32',
    '2606:4700::/32',
    '2803:f800::/32',
];

Request::setTrustedProxies($ips);
if ($request->headers->get('Cf-Connecting-Ip')) {
    Request::setTrustedHeaderName(Request::HEADER_FORWARDED, null);
}

$response = $kernel->handle($request);
$response->send();
$kernel->terminate($request, $response);

<?php

/*
 * Copyright 2016 SCTR Services
 *
 * Distribution and reproduction are prohibited.
 *
 * @package     api.itsup.com
 * @copyright   SCTR Services LLC 2016
 * @license     No License (Proprietary)
 */

use Aequasi\Environment\SymfonyEnvironment;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\ArgvInput;
use Symfony\Component\Debug\Debug;

class Console
{
    /**
     * @return Console
     */
    public static function create()
    {
        return new static();
    }

    public function __construct()
    {
        set_time_limit(0);
        $input       = new ArgvInput();
        $environment = new SymfonyEnvironment($input);

        if ($environment->isDebug()) {
            Debug::enable();
        }

        $application = new Application(new AppKernel($environment->getType(), $environment->isDebug()));
        $application->run($input);
    }
}
